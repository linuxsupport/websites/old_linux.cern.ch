<!--#include virtual="/linux/layout/header7.shtml" -->
<script>setTitle('CC7 - Documentation');</script>
<h1>CERN CentOS 7 Documentation</h1>

<ul>
    <li><a href="install.shtml">Installation instructions</a>
        <ul>
            <li><a href="bootmedia.shtml">Boot Media preparation</a>
            <li><a href="kickstart.shtml">Simple Kickstart file example</a>
        </ul>
    <li><a href="../hardware/">Supported hardware</a>
    <li><a href="http://wiki.centos.org/">CentOS Wiki</a>
    <li><a href="docker.shtml">Using Docker on CentOS 7</a>
    <li><a href="softwarecollections.shtml">Software Collections - addtional software for CentOS 7</a>
<li>Interoperability with CERN Microsoft Windows environment (NICE)
 <ul>
  <li><a href="mountdfs.shtml">Mounting DFS file system</a>
  <li><a href="sambasrv.shtml">Setting up Kerberized Samba Server</a>
  <li><a href="nfssrv.shtml">Setting up Kerberized NFS Server</a> <em>New!</em>
  <li><a href="../../docs/msexchange.shtml">Thunderbird integration with MS Exchange 2007/2010 Calendaring and Tasks</a>
  <li><a href="../../docs/ssokrb5.shtml">Using Kerberos (passwordless) authentication for CERN Single Sign On (SSO)</a> 
	<ul>
	<li><a href="../../docs/cernssocookie.shtml">CERN Single Sign On authentication using scripts/programs</a>
	</ul>
  <li><a href="../../docs/mailkrb5.shtml">Using Kerberos (passwordless) authentication for CERN e-mail services</a>
  <li><a href="../../docs/msexchange.shtml">Thunderbird integration with MS Exchange 2007/2010 Calendaring and Tasks</a>
  <li><a href="shibboleth.shtml">Apache integration with CERN Single Sign On (SSO) using shibboleth</a>
  <li><a href="mod_auth_mellon.shtml">Apache integration with CERN Single Sign On (SSO) using mod_auth_mellon</a> 
  <li><a href="../../docs/certificate-autoenroll.shtml">CERN Host Certificate AutoEnrollement and AutoRenewal</a> 
  <li><a href="../../docs/lyncmsg.shtml">Pidgin Instant Messaging integration with MS Lync / OCS messaging service</a>
  <li><a href="../../docs/lyncav.shtml">Pidgin Instant Messaging with Voice/Video integration with MS Lync/Skype for Business (preview)</a>  <em>New!</em>
  <li><a href="../../docs/mattermost.shtml">Pidgin Instant Messaging integration with Mattermost</a> <em>New!</em>
  
 </ul> 
</ul>

<h1>Red Hat Enterprise Linux 7 Documentation (local copy)</h1>
<p>
Most of <a href="https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/">RHEL 7 documentation</a> applies to CC7 systems notable exception being: 
installation instructions, Red Hat Subscription Manager and update setup instructions. 
For the former please refer to CC7 specific documentation hereabove.
</p>


<ul>
 <li><b>Getting started</b>
 <ul>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-7.0_Release_Notes-en-US.pdf">7.0 Release Notes</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Installation_Guide-en-US.pdf">Installation Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Migration_Planning_Guide-en-US.pdf">Migration Planning Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Desktop_Migration_and_Administration_Guide-en-US.pdf">Desktop Migration and Administration Guide</a> 
 </ul>
 <li><b>System administration</b>
 <ul>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-System_Administrators_Guide-en-US.pdf">System Administrators Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Networking_Guide-en-US.pdf">Networking Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Resource_Management_and_Linux_Containers_Guide-en-US.pdf">Resource Management and Linux Containers Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Performance_Tuning_Guide-en-US.pdf">Performance Tuning Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Linux_Domain_Identity_Authentication_and_Policy_Guide-en-US.pdf">Linux Domain Identity Authentication and Policy Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Package_Manifest-en-US.pdf">Package Manifest</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Power_Management_Guide-en-US.pdf">Power Management Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Windows_Integration_Guide-en-US.pdf">Windows Integration Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Kernel_Crash_Dump_Guide-en-US.pdf">Kernel Crash Dump Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-SystemTap_Beginners_Guide-en-US.pdf">SystemTap Beginners Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-SystemTap_Tapset_Reference-en-US.pdf">SystemTap Tapset Reference</a>
 </ul>
 <li><b>Storage</b>
 <ul>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Storage_Administration_Guide-en-US.pdf">Storage Administration Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Logical_Volume_Manager_Administration-en-US.pdf">Logical Volume Manager Administration</a>
   <li><a href="rhel/DM_Multipath-en-US.pdf">DM Multipath</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Global_File_System_2-en-US.pdf">Global File System 2</a>
 </ul>
 <li><b>Development</b>
 <ul>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Developer_Guide-en-US.pdf">Developer Guide</a>
 </ul>
 <li><b>Security</b>
 <ul>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Security_Guide-en-US.pdf">Security Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-SELinux_Users_and_Administrators_Guide-en-US.pdf">SELinux Users and Administrators Guide</a>
 </ul>
 <li><b>Virtualization</b>
 <ul>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Virtualization_Getting_Started_Guide-en-US.pdf">Virtualization Getting Started Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Virtualization_Deployment_and_Administration_Guide-en-US.pdf">Virtualization Deployment and Administration Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Virtualization_Security_Guide-en-US.pdf">Virtualization Security Guide</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Virtualization_Tuning_and_Optimization_Guide-en-US.pdf">Virtualization Tuning and Optimization Guide</a>
 </ul>
 <li><b>Clustering</b>
 <ul>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-High_Availability_Add-On_Overview-en-US.pdf">High Availability Add-On Overview</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-High_Availability_Add-On_Administration-en-US.pdf">High Availability Add-On Administration</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-Load_Balancer_Administration-en-US.pdf">Load Balancer Administration</a>
   <li><a href="rhel/Red_Hat_Enterprise_Linux-7-High_Availability_Add-On_Reference-en-US.pdf">High Availability Add-On Reference</a>
 </ul>
</ul>



 
 
































</ul>


<!--#include virtual="/linux/layout/footer.shtml" -->
