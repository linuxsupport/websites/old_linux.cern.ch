<!--#include virtual="/linux/layout/header7.shtml" -->
<script>setTitle('CC7 - Installation instructions');</script>
<table>
  <tbody>
    <tr>
      <td colspan="3" bgcolor="#cccccc">
      
      <h2>Before you start</h2>
      
      </td>
    </tr>
    <tr>
      <td colspan="3">
      <ul>
        <li>Check that your <a target="other"
 href="http://network.cern.ch/">machine is properly registered</a> (in case it is on
the CERN network)</li>
        <br>
        <li>Check that CERN Domain Name Service is updated for your
machine (in case it is in the CERN network):<br>
&nbsp;&nbsp;<i>host yourmachine</i> command should return an answer.</li>
        <br>
<!--        <li>If the system you are installing on is one of CERN standard types:
        Please check for <a href="../hardware/">hardware-specific instructions</a>.</li>
        <br> 
-->
        <li>Check that your machine meets minimum system requirements<br>
          <ul>
            <li>Memory: Minimum <b>2 GB</b> (system will run with 1 GB but performance will be affected)</li>
            <li>Disk space: <b>10 GB</b> (including <b>1 GB</b> user data) for default setup ( <b>1 GB</b> for minimal setup)</li>
          </ul>
        </li>
        <br>
        <li>Please see Network/PXE installation procedure at: <a href="/linux/install/">Linux Installation</a>, if you use it you may skip following points up to <a href="#installhere">system installation</a>.
        <br>
        <br>
        <li>Prepare boot media (you will need a single recordable CD or USB memory key).<br>( Check the <a href="bootmedia.shtml">boot media
            preparation</a> page for instructions how to prepare (and check) your boot media.)</li>
         <ul>
            <li>Available boot media - <b>boot CD/USB image</b>:<br>
            <a href="http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/images/boot.iso">http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/images/boot.iso</a></li>
            <li>Installation method - <b>http</b>:<br>
use: <a href="http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/">http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/</a> as installation path.</li>
         </ul>
         <br>
          <li>Note: use CD/USB image installation method <b>ONLY</b> if Network/PXE installation is not possible.</li>
         

    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>

    <tr>
      <td colspan="3" bgcolor="#cccccc">
      
      <h2>System installation</h2><a name="installhere"></a>
      
      </td>
    </tr>

    <tr>
      <td> <a target="snapwindow" href="installshots/cc7-1.png"><img src="installshots/cc7-1.png" height=140></a> </td>
      <td>Installation language and keyboard selection<br> 
      </td>
    </tr>        
   
    <tr>
      <td> <a target="snapwindow" href="installshots/cc7-4.png"><img src="installshots/cc7-4.png" height=140></a> </td>
      <td>Select 'Installation Destination', 'Date & Time', 'Installation Source' and 'Software Selection' are already preselected.      
      </td>
    </tr>

    <tr>
      <td> <a target="snapwindow" href="installshots/cc7-5.png"><img src="installshots/cc7-5.png" height=140></a> </td>
      <td>Select the device to be used for the installation.</td>
    </tr>
    <tr>
      <td> <a target="snapwindow" href="installshots/cc7-6.png"><img src="installshots/cc7-6.png" height=140></a> </td>
  
      <td>If selected device has been already used for previous version of operating system use 'Reclaim space'
    </tr>
    <tr>
      <td> <a target="snapwindow" href="installshots/cc7-8.png"><img src="installshots/cc7-8.png" height=140></a> </td>
      <td>Select 'Begin installation'</td>
    </tr>
    <tr>
      <td> <a target="snapwindow" href="installshots/cc7-9.png"><img src="installshots/cc7-9.png" height=140></a> </td>
      <td>Set 'root' (administrative account) password.<br> <em>Note:</em> 'User creation' option creates only local user accounts, NOT CERN accounts.     </td>
    </tr>
    <tr>
      <td> <a target="snapwindow" href="installshots/cc7-11.png"><img src="installshots/cc7-11.png" height=140></a> </td>
      <td></td>
    </tr>
    <tr>
      <td> <a target="snapwindow" href="installshots/cc7-12.png"><img src="installshots/cc7-12.png" height=140></a> </td>
      <td>Installation complete.</td>
    </tr>

     <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#cccccc">
      
      <h2>Firstboot configuration adjustment</h2>
      
      </td>
    </tr>
    <tr>
        <td colspan="3"><em>Warning</em> On certain hardware, you may see the following license screen for a short while and then it will disapear. You can press <b>CTRL+ALT+F1</b> to continue the installation. This bug is being investigated by Linux support team.<p />
            On first system boot, following configuration screens, allowing 
  customization your system for use in CERN computing environment, will be shown.</td>
      <br>
      <br>
    </tr>
    <tr>
      <td> <a target="snapwindow" href="installshots/cc7-13.png"><img src="installshots/cc7-13.png" height=140></a> </td>
      <td> License information.
      </td>
    </tr>
    <tr>
      <td> <a target="snapwindow" href="installshots/cc7-17.png"><img src="installshots/cc7-17.png" height=140></a> </td>
      <td> 
       CERN customization screen will allow setup of system updates mode and if AFS / CVMFS / EOS client should start on system boot.
       Site configuration defaults will be applied for Kerberos 5, sendmail etc... <br>
	   <br><em>Note</em> As of CERN CentOS 7.3 EOS and CVMFS clients are provided as a <b>TEST</b> to encourage users migration from AFS.<br>
	   <br><em>Note</em> We recommend to accept default settings which should be correct for most of CERN users.


      </td>
      <td> <br>
      </td>
    </tr>


   <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#cccccc"><a name="loginsess"></a>
      
      <h2>Logging in
</h2>
      
      </td>
    </tr>

   <tr>
      <td> <a target="snapwindow" href="installshots/cc7-18.png"><img src="installshots/cc7-18.png" height=140></a> </td>
      <td> Login screen (note: no user list is shown since no local user accounts have been created)</td>
    </tr>



    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#cccccc"><a name="manualpostinst"></a>
      
      <h2>Manual post-install configuration adjustment</h2>
      
      </td>
    </tr>
    <tr>
      <td colspan="3"> If you have selected not to run X graphical
environment on your machine, or you have installed system using <a
 href="kickstart.shtml">kickstart</a> and you want to apply site
configuration settings, here is a short recipe:
      <ul>
        <li><b>AFS client</b>
          <ul>
            <li>Run <i>/usr/bin/locmap --configure afs</i> 
            <li> Run <i>/usr/bin/locmap --configure ntp </i>to
preconfigure AFS client for CERN site (and AFS logins)</li>
          </ul>
        </li>
        <li><b>Automatic update system</b> (can also be set for
machines outside CERN network)<br>
&nbsp;&nbsp;Edit <i>/etc/sysconfig/yum-autoupdate</i> and set:
          <ul>
            <li><i>YUMUPDATE=0</i> to be informed about available
updates (by e-mail to <i>root</i>)</li>
            <li><i>YUMUPDATE=1</i> for the automatic updates to be
applied</li>
          </ul>
&nbsp;&nbsp;Next configure automatic update system:
          <ul>
            <li><i>/usr/bin/systemctl enable yum-autoupdate</i></li>
<!--            <li><i>/usr/bin/systemctl start yum-autoupdate</i></li> -->
          </ul>
        </li>
        <li><b>Apply CERN site configuration defaults</b></li>
        <ul>
          <li>run <i>/usr/bin/locmap --configure all</i> 
          to configure all enabled modules in your system, defaults are sudo, sendmail, afs, ntp, kerberos, nscd, ssh and lpadmin </li>
&nbsp;&nbsp;&nbsp;You can use <i>/usr/bin/locmap --list</i> to check all the available puppet modules
                  with their current state.<br>
          <b> Configure CVMFS filesystem</b>
          <li> run <i> /usr/bin/locmap --enable cvmfs</i> to enable the cvmfs puppet module</li>
          <li> and then run  <i> /usr/bin/locmap --configure cvmfs</i> to setup CVMFS filesystem</li>
        <b> Configure EOS Fuse client </b>
        <li> run <i> /usr/bin/locmap --enable eosclient</i> to enable the eosclient puppet module</li>
        <li> and then run  <i> /usr/bin/locmap --configure eosclient</i> to configure EOSclient</li>
        </ul>
        <li><b>Check currently running/enabled services</b>
        via <tt>/usr/bin/systemctl list-units</tt>
        <ul>
           <li>turn off those services you don't need: <tt>/usr/bin/systemctl stop <b>servicename</b>; /usr/bin/systemctl disable <b>servicename</b></tt></li>
           <li>turn on other services (once you configured them) via <tt>/usr/bin/systemctl enable <b>servicename</b></tt>,<br>
               To immediately start the service, use <tt>/usr/bin/systemctl start <b>servicename</b></tt></li>
        </ul>

        </li>
      </ul>
      </td>
    </tr>
    <tr>
      <td colspan="3"> &nbsp; </td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#cccccc"><a name="addusercern"></a>

      <h2>Locmap default puppet modules will configure user accounts, root access, printers</h2>

      </td>
    </tr>
    <tr>
      <td colspan="3"> After the successfull installation and initial configuration of the system, if the computer is 
      on the CERN network and you chose the default options locmap script should have already create user accounts, 
      provide root access, add printers, ...  based on the Xldap information of the device. Otherwise as root, run:<br> 

      <pre>
	# /usr/bin/locmap  --configure all
      </pre>

<!--
      <br>
      <em>Note:</em><strike>As of July 2014 cern-config-users is not available for CC7 yet.</strike><br>
          <strike>As of August 2014 EPEL repository (on which cern-config-users depends) is still in BETA,
          therefore</strike> to install it one needs to execute:
      <pre> # /usr/bin/yum -x-enablerepo=epel-beta install cern-config-users</pre>
      <pre> # /usr/bin/yum install cern-config-users</pre>
      <br>
      <br>
-->
      This will:
      <ul>
        <li> Forward root e-mails to the LANdb responsible by updating /root/.forward
        <li> Configure /root/.k5login to allow Kerberized root access for the LANdb responsible
        <li> Configure /ets/sudoers to allow sudo root access for the LANdb responsible
        <li> Add AFS accounts for the LANdb responsible and main users.
        <li> Add printers in the building(s) where the machine resides and where the LANdb responsible and main users have their offices.
      </ul>

      Note that this tool expands E-groups.

      <p>

      Alternatively, you can (as root):

      <ul>
        <li> run <i>/usr/sbin/addusercern <b>my_login_id</b></i> to add AFS user accounts
        <li> edit <tt>/root/.forward</tt> to forward e-mails sent to the root account, and make sure the SElinux context is correct. Example:
<pre>
# cat /root/.forward
User.Name@cern.ch
# restorecon /root/.forward
# ls -Z /root/.forward
-rw-r--r--. root root system_u:object_r:mail_home_t:s0 /root/.forward </pre>
        <li> edit <tt>/root/.k5login</tt> to allow kerberized root logins, and make sure the SElinux context is correct. Example:
<pre>
# cat /root/.k5login
<b>my_login_id</b>@CERN.CH
# restorecon /root/.k5login
# ls -Z /root/.k5login
-rw-r--r--. root root system_u:object_r:krb5_home_t:s0 /root/.k5login </pre>
       <li> add centrally managed printers with <tt>/usr/sbin/lpadmincern <b>printername</b> --add</tt><br>
            A list of all printers available at CERN in given building can be obtained using: 
<pre>
# /usr/sbin/lpadmincern --building <b>XXXX</b> --list </pre>
     </ul>
     
      </td>
    </tr>



    <tr>
      <td colspan="3" bgcolor="#cccccc">

      <h2>Applying software updates</h2>
      
      </td>
    </tr>
    <tr>
      <td colspan="3"> You should update your system immediately after its installation: Eventual security errata and bug fixes will be
applied this way before you start using it.<br>
      <br>
As root run:
<pre>
 # /usr/bin/yum -y update 
</pre>
to apply all available updates.
<br>
<br>
<!--
For more information about system updates please check <a
 href="softwaremgmt.shtml">Software Management</a> page. <br>
For more information about system configuration please check <a
 href="configmgmt.shtml">Configuration Management</a> page. </td>
-->
    </tr>
    <tr>
      <td colspan="3" bgcolor="#cccccc">
      <div align="right"><a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a>
      </div>
      </td>
    </tr>
  </tbody>
</table>
<!--#include virtual="/linux/layout/footer.shtml" -->
