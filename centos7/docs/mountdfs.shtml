<!--#include virtual="/linux/layout/header7.shtml" -->
<script>setTitle('Mounting  CERN DFS file system on linux');</script>
<h1>Mounting CERN DFS file system on linux</h1>
CERN uses <a href="https://dfsweb.web.cern.ch/dfsweb/">Microsoft DFS</a> file system for storing users and application data for Windows systems.
<p>
This documentation outlines the setup process allowing Linux clients to mount and access CERN DFS file system.
<p>


<h2>Software installation</h2>

As root on your CC7 system run:
<pre>
# yum  install cifs-utils 
</pre>
<h2>Configuration</h2>
As root on your system:
<p>
Please verify that your host keytab is valid: 
<pre>
# klist -k
</pre>
it should show output similar to:
<pre>
Keytab name: WRFILE:/etc/krb5.keytab
KVNO Principal
---- --------------------------------------------------------------------------
  10 host/<tt>yourhost</tt>.cern.ch@CERN.CH
   9 host/<tt>yourhost</tt>.cern.ch@CERN.CH
   1 host/<tt>yourhost</tt>.cern.ch@CERN.CH
</pre>
(actual output may vary depending on when and how keytab was set)
<p>
Create mountpoint:
<pre>
# mkdir /dfs
</pre>
Create <b>/etc/cron.d/host-kinit</b> file with following content:
<pre>
#This cron job will reacquire host credentials every 12 hours
01 */12 * * * root /usr/bin/kinit -k
</pre>

<hr>
<em>Note:</em> DFS (cifs) mounting with protocol version 2.0/2.1/3.0 is not functional as of now on CC7, following DFS referrals
(links to remote servers) does not work as expected:
<pre>
# ls /dfs/Departments/IT/Groups/IS/
ls: cannot access /dfs/Departments/IT/Groups/IS/: Function not implemented
</pre>
we are investigating the problem, please do not use <i>vers=X</i> parameter for now.
<strike>

<hr>

<h2>Filesystem mount</h2>
Please choose one of the following two methods on your system.

<h4>Mounting with other filesystems</h4>
Edit <b>/etc/rc.local</b> and insert there these lines:
<pre>
# Mount DFS
/usr/bin/kinit -k
/bin/mount /dfs -o vers=2.1
</pre>
Edit <b>/etc/fstab</b> and add at the end this line:
<pre>
//cerndfs.cern.ch/dfs   /dfs            cifs    noauto,nocase,sec=krb5,multiuser,uid=0,gid=0,vers=2.1    0 0
</pre>
Next, execute:
<pre>
# /etc/rc.local
</pre>
On subsequent system reboots DFS will be mounted automatically.
<p>
<b>Note:</b> <i>vers=2.1</i> parameter is needed in order to use SMB version 2 protocol.

<h4>Mounting with automounter</h4>

Edit <b>/etc/auto.master</b> and add following line:
<pre>
/dfs/ /etc/auto.dfs
</pre>
Create <b>/etc/auto.dfs</b> with following content:
<pre>
#!/bin/sh
[ !`/usr/bin/kinit -k 2>&1 >> /dev/null` ] && echo " -fstype=cifs,sec=krb5,multiuser,user=0,uid=0,gid=0,vers=2.1 ://cerndfs.cern.ch/dfs/&"
</pre>
Execute:
<pre>
# chmod 755 /etc/auto.dfs
</pre>
<p>
To finish the configuration please enable and restart the automounter:
<pre>
# /sbin/chkconfig --levels 345 autofs on
# /sbin/service autofs restart
</pre>
<em>Note:</em> The DFS filesystem is automounted: therefore until user accesses it nothing is 
visible under <b>/dfs/</b>: try <b>ls /dfs/Users</b> or <b>ls /dfs/Applications</b> to see the content.
<p>
<b>Note:</b> <i>vers=2.1</i> parameter is needed in order to use SMB version 2 protocol.

<h2>Usage notes</h2>
<ul>
<li>This method of accessing DFS requires a valid Kerberos host key - which can be allocated ONLY to systems on CERN network.
<li>User access to files requires a valid Kerberos ticket from CERN KDC, please check yours using: <b>klist</b>. 
<li>Case sensitivity: DFS mount on Linux emulates Windows behaviour Files/Folders are case-sensitive upon creation, but case-insensitive for later access.
<li>User ownership and permissions on files/directories are shown as full root user permissions and root ownership:
<pre>
ls -l /dfs/
total 140
drwxr-xr-x 1 root root   32768 Feb  9 14:41 Applications
[...]
</pre>
but actual access permisions are mapped correctly, if you create files these will be created with default Windows permissions in given folder.
<!-- <li>Ownership/access mode changing using chmod/chown will not work on DFS files. -->
<!-- <li>Getting/setting ACLs for DFS is not supported with current kernels (do not try <it>cifsacl</it> mount option it <b>is</b> buggy ...) -->
<li>...
</ul>

<p>
<em>Note:</em> SMB2 protocol is not compatible with SLC6 and SLC5.

<!--#include virtual="/linux/layout/footer.shtml" -->
