<!--#include virtual="/linux/layout/header7.shtml" -->
<script>setTitle('CC7: Standard hardware');</script>
<h2>Installation and Configuration for CERN standard hardware PC models and peripherials.</h2>

This page describes CC7 installation and configuration procedures 
for standard CERN PC hardware models sold by CERN stores.
While many more types of PC hardware are known to operate correctly 
while running CentOS 7, only models listed below have been tested
by the Linux Support team.<br>

<br>
Please note: Lists below describe only PC hardware available for
purchase from CERN stores starting 2015.
Previous models are also supported unless stated
otherwise on this pages. 

<ul>
<li>Desktops
	<ul>
	<li><a href="#hp800g2">HP 800G2</a>
	<li><a href="#dell9020">Dell Optiplex 9020</a>
	<li><a href="#dell7060">Dell Optiplex 7060</a>
	<li><a href="#TTLTeknoPro">TTL TeknoPro</a>
	</ul>
<li>Laptops (<em>Please note: since 2011 we DO NOT TEST laptops anymore</em>)
        <ul><a href="#thinkpadX1carbon">Thinkpad X1 Carbon</a>
        </ul>
<li>Other
	<ul>
	<li><a href="#nvidiaquadro2000D">Nvidia Quadro 2000 graphics card</a>
        <li><a href="#hpnvidiagt730">HP Nvidia GT 730 graphics card</a>
	<li><a href="#logitechp710e">Logitech P710e speakerphone</a>
	<li><a href="#logitechh570e">Logitech H570e headset</a>
	</ul>
</ul>

<hr>
<h2><a name="hp800g2"></a>HP 800G2</h2>
<img src="images/small_hp800g2.jpg">

<ul>
<li><em>Note:</em> Built-in graphics card support is limited to 1024x768 resolution in unaccelerated mode with single display only.
<br />
Since April 2016 as a workaround, we recommend for better performance, stability and multi-screen support the purchase of an additional Nvidia card. 
<br />
Please read carefully the known limitations and specifications of the proposed card <a href="#hpnvidiagt730">HP Nvidia GT 730</a>.
<li><em>Note:</em> Requires (CERN) CentOS <b>7.2</b> or newer.
<li>Please follow the <a href="/linux/centos7/docs/install.shtml">standard installation procedure</a>.
<li> If you use the addtional Nvidia card you must disable Integrated Video in HP BIOS: Enter Bios, go to the advanced tab and unclick "Integrated Video".

<br  />
<img src="images/bioshp800g2.png" alt="Bios Screenshot" \>
</ul>


<hr>
<h2><a name="dell9020"></a>Dell Optiplex 9020</h2>
<img src="images/small_dell9020.jpg">

<ul>
<li>Please follow the <a href="/linux/centos7/docs/install.shtml">standard installation procedure</a>.
</ul>

<hr>
<h2><a name="dell7060"></a>Dell Optiplex 7060 (Micro)</h2>
<img src="images/small_dell7060.jpg">
<ul>
<li><em>Note:</em> The Dell BIOS from the factory enables by default 'Active State Power Management' (ASPM). With ASPM enabled, the network interface card is incorrectly instructed to go to sleep - resulting in a severely slow or failed installation. This setting should be DISABLED in the BIOS (to enter the BIOS, press F2 on system boot) as per the below image in order to successfully install CC7. 
<br />
<img src="images/biosdell7060.png" alt="Bios Screenshot" \>

<li><em>Note:</em> It is also necessary to disable 'Legacy Boot' and use UEFI natively. This setting should be DISABLED in the BIOS (to enter the BIOS, press F2 on system boot) as per the below image in order to successfully install CC7.
<br />
<img src="images/bios2dell7060.png" alt="Bios Screenshot" \>

<li>Please follow the <a href="/linux/centos7/docs/install.shtml">standard installation procedure</a>.

</ul>

<hr>
<h2><a name="TTLTeknoPro"></a>TTL TeknoPro</h2>
<img src="images/small_ttl.png">

<ul>
<li>Please follow the <a href="/linux/centos7/docs/install.shtml">standard installation procedure</a>.
</ul>


<hr>
<h1>Laptops</h1>
<em>Please note:</em> We do not provide support for Linux on Laptops since 2011...
Descriptions below are provided 'as-is' without any warranty.
<hr>
<a name="thinkpadX1carbon"></a>
<h2>Lenovo ThinkPad X1 Carbon</h2>
<img src="images/thinkpadX1carbon.jpg">
<ul>
<li>In the laptop BIOS make sure 'Secure Boot' is disabled, and optionally enable 'UEFI boot'.
<li>Install CERN CentOS 7.2 using PXE, and included ethernet adapter.
<li>On first system boot, uncheck 'start AFS on system boot'.
<li>Update the system: <pre># yum update</pre> (but <b>DO NOT</b> reboot yet).
<li>Install upstream kernel (4.7 as of Sep 2016): <pre># yum --enablerepo=elrepo-kernel install kernel-ml</pre>
<li>Set system to boot with above kernel: <pre># grep -P "submenu|^menuentry" /etc/grub2{-efi}.cfg | cut -d "'" -f2
# grub2-set-default "&lt;submenu title&gt;&lt;menu entry title&gt;"</pre>
<li>Install updated firmware for WiFi card from: <a href="https://wireless.wiki.kernel.org/en/users/drivers/iwlwifi">https://wireless.wiki.kernel.org/en/users/drivers/iwlwifi</a>
 <ul>
 <li>Download version iwlwifi-8000-ucode-16.242414.0.tgz (or newer).
 <li>unzip the archive, copy iwlwifi-8000C-16.ucode to <pre>/lib/firmware</pre>
 <li>set selinux context: <pre># chcon system_u:object_r:lib_t:s0 /lib/firmware/iwlwifi-8000C-16.ucode</pre>
 </ul>
<li>Reboot the system.
</ul>

<em>Please note:</em> remember to periodically update the <b>kernel-ml</b> manually by running: <pre># yum --enablerepo=elrepo-kernel install kernel-ml</pre> (automatic system update procedure will not perform this update).
<br>
All features of ThinkPad X1 Carbon system installed as above do function correctly except:
<ul>
 <li> Camera 'kill switch' (F7 key) - which has no effect.
 <li> Fingerprint reader - (may work with updated fprintd ?)
 <li> (micro)SD card reader - (driver update needed ?)
</ul>


  
</ul>

<hr>

<h1>Other</h1>
<a name="#other"></a>
<hr>
<a name="nvidiaquadro2000D"></a>
<h2>Nvidia Quadro 2000D</h2>
<img src="images/nvidia-quadro-2000D.png">
<ul>
<li>Install nvidia drivers from ElRepo repository:
<pre>
# yum --enablerepo=elrepo install nvidia-x11-drv (nvidia-x11-drv-32bit)
# reboot
</pre>
<li>While monitors setup can be autoconfigured, you may want to put in place
your own <pre>/etc/X11/xorg.conf</pre> X11 configuration file.
(Example <a href="nvidia-quadro2000-xorg.conf">configuration file</a> for 3 monitor setup)
</ul>

<hr>
<a name="hpnvidiagt730"></a>
<h2>HP NVIDIA GeForce GT 730 2GB PCIe </h2>
<img src="images/nvidia-gt730.png">
<ul>
    <li> Note: Test have been done with 2 monitors ; 1 connected through DVI and the other one through Display port.
    <li> <em color="red">Note</em>: The highest supported resolution over DVI is 1920x1080 (As of April 2016, higher resolution showed graphic artifacts and unreadable fonts). 
    <br />
    <li> <em color>Note</em>: Disable integrated graphic in your BIOS before starting the installation.
    <li> Default Open source driver (nouveau) works, however if you want better OpenGL performances you should activate the proprietary nvidia driver.
    <li> Install nvidia drivers from ElRepo repository:
    <pre>
    # yum --enablerepo=elrepo install nvidia-x11-drv
    # reboot 
    </pre>
</ul>

<hr>
<a name="logitechp710e"></a>
<h2>Logitech P710e Speakerphone</h2>
<img src="images/logitechp710e.jpg">
Without a device specific configuration some mouse/keyboard lockups may happen while pressing buttons on the device.
<ul>
 <li>Please install:
     <pre>
     # yum install logitech-p710e-config
     </pre>
 <li>Then reboot your system (or restart XWindows)
 <li>Configure the device as <b>Duplex analog audio</b> in your audio mixer.

</ul>
this shall fix problems.  Please read <i>/usr/share/doc/logitech-p710e-config*/README</i> for configuration information.

<hr>
<a name="logitechh570e"></a>
<h2>Logitech H570e Headset</h2>
<img src="images/logitechh570e.jpg">
Without a device specific configuration some mouse/keyboard lockups may happen while pressing buttons on the device.
<ul>
 <li>Please install:
     <pre>
     # yum install logitech-h570e-config
     </pre>
 <li>Then reboot your system (or restart XWindows)
 <li>Configure the device as <b>Duplex analog audio</b> in your audio mixer.
</ul>
this shall fix problems. Please read <i>/usr/share/doc/logitech-h570e-config*/README</i> for configuration information.


<!--#include virtual="/linux/layout/footer.shtml" -->
