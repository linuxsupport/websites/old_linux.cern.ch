<!--#include virtual="/linux/layout/header.shtml" -->
<script>setTitle('Access using Kerberos');</script>
<h1>Accessing CERN Linux machines via Kerberos</h1>

<!-- why -->

<p>Kerberos is shared-secret networked authentication system. Its use at
  CERN serves a dual purpose:
  <ul>
<li>user convenience: once signed in, authentication is handled
      "automagically" on the users' behalf.</li>
<li>security: by reducing the need to enter passwords into potentially
      compromised machines, the impact of such a compromise can be
      greatly reduced.
</li>
</ul>

While the CERN environment does not offer a true "Single-Sign-On"
(SSO) mechanism yet, the use of Kerberos means that UNIX-to-UNIX
remote interaction can usually be done without typing passwords. </p>

This document does not aim to explain Kerberos. For an overview and
introduction into the concepts, please see <a
href="http://man.linuxquestions.org/?query=kerberos&type=2&section=1"><tt>man
kerberos</tt></a>, <a
href="http://web.mit.edu/Kerberos/#what_is">MIT's site</a> or the <a
href="http://www.faqs.org/faqs/kerberos-faq/general/">Kerberos
FAQ</a>. Several books exist on the subject as well.</p>

<hr>
<h2>Client-side configuration</h2>

<p>In order for Kerberos to work, the user needs to be registered in a
central database (KDC). In CERN parlance, this means an <i>AFS
account</i> is required for the user. However, it is not required that
the user actually has or uses an AFS home directory on a target
machine.</p>

<p>Kerberos client binaries are part of the default install of many
operating systems (such as Mac OS-X, BSD, Linux, Solaris, ..), or are
at least available as add-ons. This document does not cover how to
install these binaries on a particular operating system.</p>

<a name="try"><h3>Trying it out</h3></a>

<p>To test whether a client is configured correctly, please use
    <tt>kinit</tt> and <tt>klist</tt>. These commands should live in
    your default PATH, otherwise look into <tt>/usr/kerberos/bin</tt>
    or <tt>/usr/heimdal/bin</tt> (please avoid using <tt>kinit</tt>
    from a Java JRE, these often have problems such as echoing the
    password).

<pre>$ kinit afsusername@CERN.CH
afsusername@CERN.CH's Password: &lt;enter your password&gt;
$</pre>
<ul>
<li>no message means no error</li>
<li>your password should <b>not</b> get displayed on the terminal</li>
<li>On a correctly configured machine inside the CERN domain, <tt>kinit afsusername</tt> would be
    sufficient.</li>
</ul>
In order to verify that everything went fine, use <tt>klist</tt>:
<pre>$ klist
Credentials cache: FILE:/tmp/krb5cc_<i>USERID</i>_<i>somenumber</i>
        Principal: <i>afsusername</i>@CERN.CH

  Issued           Expires          Principal
Sep 28 15:20:24  Sep 29 16:20:23  krbtgt/CERN.CH@CERN.CH
Sep 28 15:20:24  Sep 29 16:20:23  afs@CERN.CH

   V4-ticket file: /tmp/tkt<i>USERID</i>_<i>someothernumber</i>
        Principal: <i>afsusername</i>@CERN.CH

  Issued           Expires          Principal
Sep 28 15:20:24  Sep 29 16:46:45  krbtgt.CERN.CH@CERN.CH</pre>
In this example, both Kerberos 5 and Kerberos 4 credentials for the
    CERN realm were
    obtained, and the expiry time is in the future (at the time of
      this writing). Looks good.</p>

<p>In order to get access to <b>AFS</b> after a successful authentication,
    you will need to get a "forwardable" TGT, and instruct your ssh
    client to actually transfer it over to the server (who will then
    use it to obtain an AFS token for your session). Use <tt>klist
      -f</tt> to check whether your TGT is "forwardable" - usually it
    then has a flag "F":
<pre>  Issued           Expires        Flags    Principal 
Sep 29 13:24:41  Sep 30 13:24:03  <b>F</b>PR   krbtgt/CERN.CH@CERN.CH</pre>
</p>

<p>To continue testing, use <tt>ssh -v <i>afsusername</i>@hostname.cern.ch
      klist -f\; tokens\; touch .sshtest</tt> (this should log you in,
    show your credentials - both Kerberos and AFS, and try to actually
    write something into your AFS home directory) - if all succeeds,
    you are set to use passwordless Kerberos. If you get error
    messages from these commands or are being asked for your passwords, the "verbose"
    output should allow you to pinpoint what went wrong. You may need
    to explicitly set some of the <a href="#sshopts">ssh client options</a>.</p>




<h3>Client configuration - Kerberos</h3>
<p>In most cases, no client side configuration is required - one of
    the CERN KDCs is reachable via <tt>kerberos.cern.ch</tt>, which most clients
    should use if no specific configuration is present. If this is not
    the case, the name of the CERN KDC(s) will need to be specified
    explicitly. The syntax accepted by both the MIT and Heimdal implementations
     would be
<pre>[realms]
<i>...</i>
 CERN.CH = {
  default_domain = cern.ch
  kpasswd_server = afskrb5m.cern.ch
  admin_server = afskrb5m.cern.ch
  kdc = afsdb1.cern.ch
  kdc = afsdb2.cern.ch
  kdc = afsdb3.cern.ch
 }
<i>...</i>
[domain_realm]
<i>...</i>
 .cern.ch = CERN.CH
<i>...</i></pre>
A working config file (with CERN as default realm) can be copied from any of
    the LXPLUS machines. Default location for this configuration file
    is <tt>/etc/krb5.conf</tt> on most Linuxes and Mac OS X (here
<i>Library/Preferences/edu.mit.Kerberos</i> would work as well), on some systems (Solaris) it is in
    <tt>/etc/krb5/</tt>. The above information can typically also be
entered via any Kerberos-configuring user interface your vendor may
provide. 
If you do not have "root" privileges on the client machine, you can
instruct MIT Kerberos to
    look at a different file by setting the <tt>KRB5_CONFIG</tt>
    environment variable.

<p>You might want to always get "forwardable" tickets by default
	(since they are needed to get AFS credentials later when
	logging into some other machine via SSH). The
	option for this in the <tt>/etc/krb5.conf</tt> file is 
<pre>[libdefaults]
<i>...</i>
 forwardable = true
</pre>

<p>Once you can get TGTs from the CERN realm <a href="#try">as described above</a>, the
	client-side Kerberos configuration is assumed to work.

<a name="sshopts"><h3>Client configuration - SSH</h3></a>
<p>Two flavours of the SSH protocol exist, which support Kerberos
	    differently:
<ul><li>SSH1: ancient and theoretically less secure, it had
		nevertheless direct support for Kerberos4, Kerberos5
		and AFS. Typically, SSH programs nowadays needs to be
		specifically patched/recompiled
		to understand Kerberos4 and AFS.</li>
<li>SSH2: the modern successor that does not "speak" Kerberos
		  directly, but uses an intermediate mechanism called
		  GSSAPI ("Generic Security Services API", which itself uses Kerberos5). It is
		  blissfully unaware of AFS, but widely available.</li>
		</ul>
<p>A common implementation of these protocols is found in <a
		href="http://openssh.com">OpenSSH</a>, widely used in
	      the Linux world, and also part of CERN Linux
	      distributions.

<p>ssh clients may need some help to try Kerberos for
authentication. The following options should be enabled (via the
command line or in ~/.ssh/config, see <tt>man ssh_config</tt>):

<ul><li><b>SSH1</b> - since separate patches are involved, not all
		    clients understand all of these options,
		    consult the man page of the version you are using.

 <ul>
 <li>KerberosAuthentication: as the name implies, tells the ssh
     client to try Kerberos (both Kerberos4 and/or Kerberos5)</li>
 <li>KerberosTgtPassing: tell the client to forward the Kerberos
			TGT to the remote side (kerberos5 TGTs need
			to be marked as "forwardable" for this to
			work).</li>
<li>AFSTokenPassing: tell the client to forward the AFS
			credentials.</li>
</ul>
The command-line switch "-k" can be
			used to <u>not</u> forward AFS/Kerberos
		    credentials for a particular session, if the
		    default configuration would say otherwise.
</li>
<li><b>SSH2</b>:
 <ul>
 <li>GSSAPIAuthentication: try Kerberos5 authentication</li>
<li>GSSAPIDelegateCredentials: tell the client to forward the Kerberos5
			credentials to the remote side
</ul>
</ul>

The command-line switch "-v" (verbose) usually is very helpful - you
	      should see messages mentioning Kerberos/AFS/GSSAPI (the
	      following examples are from different sessions):
<pre>debug1: Trying Kerberos v5 authentication.
debug1: Kerberos v5 authentication accepted.
debug1: Kerberos v4 authentication accepted.
debug1: Kerberos v4 challenge successful.
debug1: Kerberos v4 TGT forwarded (afsaccount@CERN.CH).
debug1: AFS token for cell cern.ch forwarded.
debug1: Next authentication method: gssapi-with-mic
debug1: Delegating credentials
debug1: Authentication succeeded (gssapi-with-mic).
</pre>

<h4>Some common SSH errors:</h4>
<ul>
<li> <b>no credentials on the client:</b>
<pre>debug1: Kerberos v5: krb5_mk_req failed: No credentials cache found
debug1: Kerberos v4 krb_mk_req failed: Couldn't access ticket file (tf_util)
debug1: Miscellaneous failure
 No credentials cache found
</pre>
(this should get fixed by re-running <tt>kinit</tt>)
</li>
<p>
<li> <b>credentials not delegated:</b> (this appears to effect Mac OS Tiger (and newer?)).
<p>Symptons: You can login without having to specify a password, but you get 
<pre>
fs: You don't have the required access rights on '/afs/cern.ch/user/j/jouser'
hepix: E: /usr/bin/fs returned error, no tokens?
</pre>
Make sure you use the following options either in your local <tt>~/.ssh/config</tt>, or
in your local systemwide <tt>/etc/ssh_config</tt> (could also be at <tt>/etc/ssh/ssh_config</tt>):
<pre>
HOST lxplus*
    GSSAPITrustDns yes   # not needed (and not even compatible) with SLC5
    GSSAPIAuthentication yes
    GSSAPIDelegateCredentials yes
</pre>
Please read the ssh_config man pages before modifying one of the config files.
<p>
While you are at it you might want to add the <pre>ForwardX11 yes</pre> for
convenience in the HOST lxplus* section, but in that case do not forget to put
the <pre>ForwardX11Trusted no</pre> there as well, for not making your setup 
too insecure. It is not recommended to put the 'GSSAPIDelegateCredentials yes'
or the 'ForwardX11 yes' in the global section of the systemwide
<tt>ssh_config</tt> file.
<p>
</li>
<p>
<li> <a name="bug"> <b>Buggy GSSAPI call inside openssh</b></a> (this
e.g. affects MacOS ssh
and most "vendor" ssh'es when connecting to one of the DNS-round-robin
clusters such as LXPLUS, it will fall through to password
authentication in such cases):

<pre>debug1: Next authentication method: gssapi-with-mic
debug2: we sent a gssapi-with-mic packet, wait for reply
debug1: Miscellaneous failure
Generic error (see e-text)</pre>

or 
<pre>debug1: Delegating credentials
debug1: An invalid name was supplied
No error</pre>

A symptom is that the credentials cache ("<tt>klist</tt>")
contains a service ticket (<tt>host/lxplus123.cern.ch@CERN.CH</tt>)
that does not match the machine ssh actually connected to:
<pre>
debug1: Connecting to lxplus [137.138.4.22] port 22.
</pre> (in this case <tt>lxplus</tt> == <tt>137.138.4.22</tt> ==  <tt>lxplus207</tt> !=
<tt>lxplus123</tt>).<br>

This bug has been patched in the CERN SLC ssh client and in some other
Linux distributions, it is being tracked upstream at the <a
href="http://bugzilla.mindrot.org/show_bug.cgi?id=1008">OpenSSH
bugzilla</a>. If you are lucky, your <tt> man ssh_config</tt> knows
about a <tt>GSSAPITrustDNS</tt> parameter - in this case please try
<tt>ssh -oGSSAPITrustDNS=yes ....</tt>.

<p>Other workarounds include using the SSH-1 protocol (<tt>ssh -1
...</tt> - if your version still supports this, and supports Kerberos
over SSH-1), 
or connecting to an individual node out of the cluster, instead of to
the DNS alias (i.e. use <tt>ssh lxplus202.cern.ch</tt> instead of
<tt>ssh lxplus.cern.ch</tt>). Obviously you will miss the benefits of
the DNS alias - occasionally your target may be down, overloaded or
simply no longer there.</p>
</li>


</ul>


<h3>Client configuration - other applications</h3>
<p>It is possible to use Kerberos for local authentication (this is how
		CERN Linux machines are generally set up). However, such a
		setup only makes sense on machines that are permanently
		connected to the network, and have permanent
		connectivity to the CERN KDCs. In such
		cases, TGTs are typically obtained directly on login
		and get renewed via screensavers.  For CERN Linux machines,
		a variant of Red Hat's pam_krb5 is being used to achieve this.

<p>Besides SSH, other network service can also be made
Kerberos-aware. Examples including web services (via the <a
href="http://en.wikipedia.org/wiki/SPNEGO">SPNEGO/GSSAPI</a> authentication
mechanism), ORACLE or CERN tools such as the <a
href="http://cern.ch/quattor/">Quattor CDB client</a>.

<hr>
<h2>Server-side configuration and troubleshooting</h2>

<p>Kerberos is a shared-secret authentication system. This implies
that both the users and the machines need to be registered at a
central place, the KDC. These shared secrets are versioned, and both
the machine and the KDC need to be using the same version of the
secret.  <b>Most CERN Linux machines should get registered at install
time</b>, but in case of authentication problems the following may help to
troubleshoot.</p>

<ul><li>Check that <tt>/etc/krb5.keytab</tt> is present, non-empty,
owned by "root" and readable <b>only</b> for "root". If you need to support
legacy Kerberos4 clients, you will also need <tt>/etc/srvtab</tt> with
the same properties:
<pre>$ ls -l /etc/krb5.keytab /etc/srvtab
-rw-------    1 root     root          197 Jan 14  2005 /etc/krb5.keytab
-rw-------    1 root     root           32 Jan 17  2005 /etc/srvtab</pre>


If one or both files are missing, run <tt>cern-config-srvtab</tt>
as "root" to (re-)create them. This program comes with the
<tt>arc</tt> RPM, which itself is available form the <a
href="http://linuxsoft.cern.ch">CERN Linux software repository</a>. It
will only work on the CERN network.<br>This program (re-)creates the
	  machine's secret on the KDC and stores a matching version
	  inside these files. By running it, you invalidate existing
	  service tickets for the machine in question (users will need
      to re-run <tt>kinit</tt> before they can connect again).<br></li>

<li>If these files exist, verify that the "key version" number (KVNO) is correct: On the server, run
<pre>#klist -k -t -e
Keytab name: FILE:/etc/krb5.keytab
KVNO Timestamp         Principal
---- ----------------- --------------------------------------------------------
   <b>0</b> 09/24/04 15:57:21 host/hostname.cern.ch@CERN.CH (DES cbc mode with RSA-MD5)
   0 09/24/04 15:57:21 host/hostname.cern.ch@CERN.CH (DES cbc mode with RSA-MD4)
   0 09/24/04 15:57:21 host/hostname.cern.ch@CERN.CH (DES cbc mode with CRC-32)
</pre>
On a client, use <pre>$ kvno host/hostname.cern.ch@CERN.CH
host/hostname.cern.ch@CERN.CH: kvno = <b>2</b>
</pre> to get the KDC's idea of the current KVNO. If they are different (as in the example),
		      remove <tt>/etc/krb5.keytab /etc/srvtab</tt> and recreate them as per above.</li>

<li>Check <tt>/var/log/messages</tt> for hints why a given service cannot use Kerberos, for example if it has trouble accessing the
		      keytab files. For <tt>sshd</tt>, you might want to run in
		      debug mode (<tt>sshd -d -d -d -p <i>someport</i></tt>) for further hints.
</ul>

<!--#include virtual="/linux/layout/footer.shtml" -->
