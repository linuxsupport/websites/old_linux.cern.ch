<!--#include virtual="/linux/layout/header.shtml" -->
<script>setTitle('Reconfiguring Nautilus .. to make it a more useable file manager');</script>
<h1>Nautilus file manager reconfiguration.</h1>
The default configuration of the Nautilus File Manager makes browsing multiple folders with deep file system hierarchies a little bit impractical,
and sometimes slow - especially for files stored on AFS. Please follow the configuration steps described below to optimize its setup.
<table>
  <tbody>
   <tr>
    <td> <a target="snapwindow" href="nautilus/Screenshot.png"><img src="nautilus/Screenshot.png" width="300"></a> </td>
    <td>Open <b>Nautilus</b> window by clicking <b>Places -> Home Folder</b>, the default view of your Home Folder will show.<p>
        Select <b>Edit -> Preferences</b>.</td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="nautilus/Screenshot-2.png"><img src="nautilus/Screenshot-2.png" width="300"></a> </td>
    <td>In <b>Views</b> pane:<br>
        Select <b>View new folders</b> to be using <b>List View</b>,<br>
        check <b>Use compact layout</b>,<br>
        check <b>Text beside icons</b> and<br>
        select <b>Default zoom level</b> to be <b>50%</b>.</td>
   </tr> 
   <tr>
    <td> <a target="snapwindow" href="nautilus/Screenshot-3.png"><img src="nautilus/Screenshot-3.png" width="300"></a> </td>
    <td>In <b>Behavior</b> pane:<br>
        Check <b>Always open in browser window</b>.</td>
   </tr> 
   <tr>
    <td> <a target="snapwindow" href="nautilus/Screenshot-4.png"><img src="nautilus/Screenshot-4.png" width="300"></a> </td>
    <td>In <b>Preview</b> pane:<br>
        Select <b>Never</b> for all file previews.
        <p>
        <em>Note:</em> Nautilus considers files on <b>AFS</b> as <b>Local files</b>, so if previews are enabled browsing your home folder on AFS will be extremely slow.</td>
   </tr> 
   <tr>
    <td>  </td>
    <td>Close <b>ALL</b> nautilus windows for the reconfiguration to take effect.</td>
   </tr> 
  <tr>
    <td> <a target="snapwindow" href="nautilus/Screenshot-5.png"><img src="nautilus/Screenshot-5.png" width="300"></a> </td>
    <td>Once reconfigured a much more functional <b>Nautilus</b> layout will be presented.</td>
   </tr> 
 </tbody>
</table>


<!--#include virtual="/linux/layout/footer.shtml" -->
