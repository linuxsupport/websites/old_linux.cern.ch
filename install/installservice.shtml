<!--#include virtual="/linux/layout/header.shtml" -->
<script>setTitle('Linux installation service');</script>
<h1>CERN Linux installation service</h1>

<p>
CERN Linux installation server - <a href="http://linuxsoft.cern.ch/">linuxsoft.cern.ch</a> provides installation of linux distributions over <b>http</b>.

<!--
methods:
<ul>
<li>
<strike> <b>NFS</b> <em><b>*</b></em> </strike> NFS method has been removed at the beginning of 2013.
</li>
<li>
<strike> <b>ftp</b> <em><b>*</b></em> </strike> ftp method has been removed at the beginning of 2015.
</li>
<li>
 <b>http</b>
</li>
</ul>

(<em><b>*</b></em> - available <b>only</b> from internal CERN networks).
</p>
-->

<h2>Available Linux distributions</h2>
<p>
Following Linux distributions are available:
</p>

<table>
<tbody><tr bgcolor="#cccccc">
<td>Distribution</td><td>Installation path</td><td>Remarks</td>
</tr>
<tr bgcolor="#cccccc">
<td colspan="3" align="center"><h3>CERN recommended and supported distributions</h3></td>
</tr>
<tr>
<td><a href="/linux/centos7/">CERN CentOS 7</a>
</td><td>
<a href="http://linuxsoft.cern.ch/cern/centos/7/os/x86_64">/cern/centos/7/os/x86_64</a><br>
</td>
<td>for 64bit systems<br>
See: <a href="/linux/centos7/docs/install.shtml">CC7 installation instructions.</a><br>
</td>
</tr>
<tr>
<td><a href="/linux/scientific6/">Scientific Linux CERN 6</a>
</td><td>
<a href="http://linuxsoft.cern.ch/cern/slc6X/i386">/cern/slc6X/i386</a><br>
<a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64">/cern/slc6X/x86_64</a><br>
</td>
<td>for i686 / athlon,<strike>Itanium 2 (ia64)</strike> and Opteron/Athlon 64/Intel with EM64T (AMD64/x86_64)<br>
See: <a href="/linux/scientific6/docs/install.shtml">SLC6 installation instructions.</a><br>
</td>
</tr>
<tr>
<td><a href="/linux/scientific5/">Scientific Linux CERN 5</a>
</td><td>
<a href="http://linuxsoft.cern.ch/cern/slc5X/i386">/cern/slc5X/i386</a><br>
<!-- <strike><a href="http://linuxsoft.cern.ch/cern/slc5X/ia64">/cern/slc5X/ia64</a><br></strike> -->
<a href="http://linuxsoft.cern.ch/cern/slc5X/x86_64">/cern/slc5X/x86_64</a><br>
</td>
<td>for i686 / athlon,<strike>Itanium 2 (ia64)</strike> and Opteron/Athlon 64/Intel with EM64T (AMD64/x86_64)<br>
See: <a href="/linux/scientific5/docs/install.shtml">SLC5 installation instructions.</a><br>
</td>
</tr>
<!--
<tr>
<td><a href="/linux/scientific4/">Scientific Linux CERN 4.8</a>
</td><td>
<a href="http://linuxsoft.cern.ch:/cern/slc4X/i386">/cern/slc4X/i386</a><br>
<strike><a href="http://linuxsoft.cern.ch:/cern/slc4X/ia64">/cern/slc4X/ia64</a><br></strike>
<a href="http://linuxsoft.cern.ch:/cern/slc4X/x86_64">/cern/slc4X/x86_64</a><br>
</a>
</td>
<td>for i686 / athlon,<strike>Itanium 2 (ia64)</strike> and Opteron/Athlon 64/Intel with EM64T (AMD64/x86_64)<br>
See: <a href="/linux/scientific4/docs/install.shtml">SLC4 installation instructions.</a><br>
      <small><a href="http://linuxsoft.cern.ch/cern/slc47/iso/">ISO images</a> are available, but this installation method is <span style="font-weight: bold;">not</span> recommended for CERN machines.
CDs can be purchased at the <a href="http://cern.ch/it-div/bookshop/">IT Bookshop</a>.</small>
</td>
</tr>
-->
<!--
<tr>
<td><a href="scientific3/">Scientific Linux CERN 3.0.8</a>
</td><td>
<a href="http://linuxsoft.cern.ch:/cern/slc308/i386">/cern/slc308/i386</a><br>
<a href="http://linuxsoft.cern.ch:/cern/slc308/ia64">/cern/slc308/ia64</a><br>
<a href="http://linuxsoft.cern.ch:/cern/slc308/x86_64">/cern/slc308/x86_64</a><br>
</a>
</td>
<td>for i686 / athlon,Itanium 2 (ia64) and Opteron (AMD64/x86_64)<br>
See: <a href="scientific3/docs/install.shtml">SLC3 installation instructions.</a><br>
      <small><a href="http://linuxsoft.cern.ch/cern/slc308/iso/">ISO images</a> are available, but this installation method is <span style="font-weight: bold;">not</span> recommended for CERN machines.
CDs can be purchased at the <a href="http://cern.ch/it-div/bookshop/">IT Bookshop</a>.</small>
</td>
-->
</tr>
<tr bgcolor="#cccccc">
<td colspan="3" align="center"><h3>CERN test / under certification / distributions</h3></td>
</tr>
<!--
<tr>
<td><a href="/linux/centos7/">CERN CentOS 7 TEST</a></td><td><a href="http://linuxsoft.cern.ch//cern/centos/7/os/x86_64/">/cern/centos/7/os/x86_64/</a></td><td>for x86_64 architecture<br>See: <a href="/linux/centos7/docs/install.shtml">CC7 installation instructions</a>.</td>
</tr>
-->
<!--
<tr>
<td><a href="/linux/scientific6/">Scientific Linux CERN 6.1 BETA</a>
</td><td>
<a href="http://linuxsoft.cern.ch/cern/slc6X/i386">/cern/slc6X/i386</a><br>
<a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64">/cern/slc6X/x86_64</a><br>
</td>
<td>for i686 / athlon and Opteron/Athlon 64/Intel with EM64T (AMD64/x86_64)<br>
</td>
</tr>
-->
<!--
<tr>
<td><em>NEW!</em><a href="/linux/scientific5/">Scientific Linux CERN 5.9 TEST</a>
</td><td>
<a href="http://linuxsoft.cern.ch/cern/slc59/i386">/cern/slc59/i386</a><br>
<a href="http://linuxsoft.cern.ch/cern/slc59/x86_64">/cern/slc59/x86_64</a><br>
</td>
<td>for i686 / athlon and Opteron/Athlon 64/Intel with EM64T (AMD64/x86_64)<br>
See: <a href="/linux/scientific5/docs/install.shtml">SLC5 installation instructions.</a><br>
</td>
</tr>
-->
<tr bgcolor="#cccccc">
<td colspan="3" align="center"><h3><a href="http://redhat.com"><b>Red Hat Enterprise Linux</b></a> licensed distributions</h3></td>
</tr>
<tr>
<!--<td>Red Hat Enterprise Linux 3 ES (Update 9)</td><td>/enterprise/3ES/en/os/i386/</td><td><font color="red"><b>**</b></font></td>-->
</tr><tr>
<td>Red&nbsp;Hat&nbsp;Enterprise&nbsp;Linux&nbsp;5&nbsp;Server&nbsp;/&nbsp;x86&nbsp;(Update&nbsp;11)</td><td>/enterprise/5Server/en/os/i386/</td><td><font color="red"><b>**</b></font></td>
</tr><tr>
<td>Red&nbsp;Hat&nbsp;Enterprise&nbsp;Linux&nbsp;5&nbsp;Server&nbsp;/&nbsp;x86_64&nbsp;(Update&nbsp;11)</td><td>/enterprise/5Server/en/os/x86_64/</td><td><font color="red"><b>**</b></font></td>
</tr><tr>
<td>Red&nbsp;Hat&nbsp;Enterprise&nbsp;Linux&nbsp;6&nbsp;Server&nbsp;/&nbsp;x86&nbsp;(Update&nbsp;6)</td><td>/enterprise/6Server/en/os/i386/</td><td><font color="red"><b>**</b></font></td>
</tr><tr>
<td>Red&nbsp;Hat&nbsp;Enterprise&nbsp;Linux&nbsp;6&nbsp;Server&nbsp;/&nbsp;x86_64&nbsp;(Update&nbsp;6)</td><td>/enterprise/6Server/en/os/x86_64/</td><td><font color="red"><b>**</b></font></td>
</tr><tr>
<td>Red&nbsp;Hat&nbsp;Enterprise&nbsp;Linux&nbsp;7&nbsp;Server&nbsp;/&nbsp;x86_64&nbsp;(Update&nbsp;1)</td><td>/enterprise/7Server/en/os/x86_64/</td><td><font color="red"><b>**</b></font></td>
</tr><tr bgcolor="#cccccc">
<td colspan="3" align="center"><h3><a href="http://fedoraproject.org"><b>Fedora Linux</b></a> distributions provided AS IS</h3></td>
</tr>

<tr>
<td>Fedora 21</td><td>
<a href="http://linuxsoft.cern.ch/fedora/linux/releases/21/">/fedora/linux/releases/21/</a><br>
</td>
</tr>

<tr>
<td>Fedora 20</td><td>
<a href="http://linuxsoft.cern.ch/fedora/linux/releases/20/Fedora/i386/os/">/fedora/linux/releases/20/Fedora/i386/os/</a><br>
<a href="http://linuxsoft.cern.ch/fedora/linux/releases/20/Fedora/x86_64/os/">/fedora/linux/releases/20/Fedora/x86_64/os/</a><br>
</td>
</tr>

<tr>
<td>Fedora 19</td><td>
<a href="http://linuxsoft.cern.ch/fedora/linux/releases/19/Fedora/i386/os/">/fedora/linux/releases/19/Fedora/i386/os/</a><br>
<a href="http://linuxsoft.cern.ch/fedora/linux/releases/19/Fedora/x86_64/os/">/fedora/linux/releases/19/Fedora/x86_64/os/</a><br>
</td>
</tr>
<tr>
<td>Fedora 18</td><td>
<a href="http://linuxsoft.cern.ch/fedora/linux/releases/18/Fedora/i386/os/">/fedora/linux/releases/18/Fedora/i386/os/</a><br>
<a href="http://linuxsoft.cern.ch/fedora/linux/releases/18/Fedora/x86_64/os/">/fedora/linux/releases/18/Fedora/x86_64/os/</a><br>
</td>
</tr>
<tr bgcolor="#cccccc">
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td colspan="3">
<em>*</em> - accessible only from CERN internal networks (use nearest <a href="http://www.redhat.com/download/mirror.html">Red Hat Mirror</a> if outside CERN)
<br>
<em>**</em> - Access is restricted to valid license owners only, only from CERN internal networks and only over <strike>NFS and</strike> http, please contact <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a> for more information.
</td>
</tr>
</tbody></table>

<h2>Automatic Installation Management System (AIMS)</h2>
<p>
To ease the installation of a large number of machines we propose to
use the kickstart based Automatic Installation Mnanagement System, <a
href="https://twiki.cern.ch/twiki/bin/view/LinuxSupport/Aims2">AIMS</a>. 
</p>
<hr>
<p>
<em>Note:</em> Access to all unsupported and obsolete Linux versions (CERN and Red Hat) has been disabled
(this includes following versions):
<ul>
<li>CERN: 4.1 5.1 6.1 6.1.1 7.2.1 7.3.1 7.3.2 7.3.3 CEL3 slc302 slc303 slc304 slc305 slc306 slc307 slc308</li>
<li>Red Hat Linux: 6.1 6.2 7.2 7.3 8 9</li>
<li>Red Hat Enterprise Linux: 2.1 3</li>
<li>Fedora Linux: 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18</li>
<li>All ia64 (Itanium II) SLC versions</li>
</ul>
</p>
<p>
The CERN Linux <b>AFS</b> repository is located at <a href="file:///afs/cern.ch/project/linux/">/afs/cern.ch/project/linux/</a>

</p>
<p>
It's directory layout matches the one on the installation server. (Except for Fedora Linux distributions
which are accessible from linuxsoft.cern.ch only) 
</p>
<p>
For Scientific Linux CERN 6 you'll find files in 
</p>
<p>
<ul>

<li>the AFS directory <a href="file:///afs/cern.ch/project/linux/cern/slc6X/">/afs/cern.ch/project/linux/cern/slc6X/</a>
</li>
<li>over HTTP on <a href="http://linuxsoft.cern.ch/cern/slc6X/">http://linuxsoft.cern.ch/cern/slc6X/</a>
</li>
<li>i<strike>over FTP on <a href="ftp://linuxsoft.cern.ch/cern/slc6X/">ftp://linuxsoft.cern.ch/cern/slc6X/</a></strike> FTP installation method has been removed in 2015.
</li>
<li><strike>over NFS on nfs://linuxsoft.cern.ch/cern/slc5X/</strike> NFS installation method has been removed in 2013.
</li>

</ul>
</p>


<!--#include virtual="/linux/layout/footer.shtml" -->
