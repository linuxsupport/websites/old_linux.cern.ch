<!--#include virtual="/linux/layout/header4.shtml" -->
<script>setTitle('SLC4 documentation');</script>
<h1>Scientific Linux CERN 4 Documentation</h1>

<ul>
 <li>
  <a href="install.shtml">Installation instructions</a>
  <ul>
   <li><a href="bootmedia.shtml">Boot Media preparation</a></li>
   <li><a href="kickstart.shtml">Simple Kickstart file example</a></li>
   <li><a href="../hardware/">Hardware-specific instructions</a> 
   <li><a href="wireless.shtml">Wireless Network (Wi-Fi) cards</a></li>
  </li>
  </ul>
 </li>
 <li><a href="quickupdate.shtml">Quick Update instructions</a>
 </li>
 <li><a href="softwaremgmt.shtml">Software Management documentation</a>
   <ul>
    <li><a href="yumex.shtml">Yumex Software Management GUI</a></li>
   </ul>
 <li><a href="configmgmt.shtml">Configuration Management documentation</a>
 </li>
 <li><a href="../../docs/printing.shtml">Printing at CERN</a></li>
 <li><a href="https://twiki.cern.ch/twiki/bin/view/LinuxSupport/WebHome">Linux Support TWiki</a> and <a href="https://twiki.cern.ch/twiki/bin/view/LinuxSupport/LinuxSupportSLCFAQ">F.A.Q</a> </li>

 <li>Interoperability with CERN Microsoft Windows environment (NICE)</li>
 <ul>
  <li><a href="webdav.shtml">Accessing Microsoft DFS at CERN (<i>\\dfs\\cern.ch\</i>)</a></li>
<!--
  <li><a href="evolution.shtml">Setting up Evolution as Microsoft Exchange Mail/Calendar/Public Folders client</a></li>
-->
  <li><a href="wts.shtml">Connecting to Microsoft Windows Terminal Services at CERN</a></li>
  <li><a href="shibboleth.shtml">Apache integration with Windows Single Sign On (shibboleth)</a></li> 
 </ul>
</ul>

<h1>Red Hat Enterprise Linux 4 Documentation</h1>
<p>
Most of RHEL 4 documentation applies to SLC4 systems notable exception being: installation
instructions, Red Hat Network and up2date agent setup instructions. For the former
please refer to SLC4 specific documentation hereabove.
</p>
<ul>
 <li><a href="rhel-ig-x8664-multi-en-4/">Installation Guide for the x86, Itanium, and AMD64 Architectures</a>
 <li><a href="rhel-sag-en-4/">System Administration Guide</a>
 <li><a href="rhel-sg-en-4/">Security Guide</a>
 <li><a href="rhel-selg-en-4/">SELinux Guide</a>
 <li><a href="rhel-isa-en-4/">Introduction to System Administration</a>
 <li><a href="rhel-rg-en-4/">Reference Guide</a>
 <li><a href="rhel-sbs-en-4/">Step By Step Guide</a>
 <li><a href="rhel-devtools-en-4/">Developer Tools Guide</a>
 <li><a href="rhel-gcc-en-4/">Using the GNU Compiler Collection (GCC)</a>
 <li><a href="rhel-binutils-en-4/">Using binutils, the GNU Binary Utilities</a>
 <li><a href="rhel-cpp-en-4/">Using cpp, the C Preprocessor</a>
 <li><a href="rhel-ld-en-4/">Using ld, the GNU Linker</a>
 <li><a href="rhel-as-en-4/">Using as, the GNU Assembler</a>
 <li><a href="rhel-gdb-en-4/">Debugging with gdb</a>
 <li><a href="http://www.redhat.com/docs/errata/RHEL-4-Manual/" target="errata">Errata to above guides (on Red Hat site)</a>
</ul>
<h1>Unsupported setup instructions</h1>
<ul>
 <li>3D accelerated proprietary OpenGL drivers:</li> 
 <ul>
 <li><a href="nvidia.shtml">NVIDIA (NVIDIA GeForce cards)</a>
 <li><a href="ati-fglrx.shtml">ATI fglrx (ATI Radeon / ATI FireGL cards)</a>
 </ul>
</ul>
<!--#include virtual="/linux/layout/footer.shtml" -->
