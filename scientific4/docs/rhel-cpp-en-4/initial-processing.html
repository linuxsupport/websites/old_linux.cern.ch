<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Initial processing</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="The C Preprocessor"
HREF="index.html"><LINK
REL="UP"
TITLE="Overview"
HREF="overview.html"><LINK
REL="PREVIOUS"
TITLE="Overview"
HREF="overview.html"><LINK
REL="NEXT"
TITLE="Tokenization"
HREF="tokenization.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>The C Preprocessor: Using cpp, the C Preprocessor</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="overview.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 1. Overview</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="tokenization.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="INITIAL-PROCESSING"
></A
>1.2. Initial processing</H1
><P
>The preprocessor performs a series of textual transformations on its
input.  These happen before all other processing.  Conceptually, they
happen in a rigid order, and the entire file is run through each
transformation before the next one begins.  CPP actually does them
all at once, for performance reasons.  These transformations correspond
roughly to the first three "phases of translation" described in the C
standard.
   </P
><P
></P
><OL
TYPE="1"
><LI
><P
>The input file is read into memory and broken into lines.
     </P
><P
>Different systems use different conventions to indicate the end of a
line.  GCC accepts the ASCII control sequences <TT
CLASS="USERINPUT"
><B
>LF</B
></TT
>, <TT
CLASS="USERINPUT"
><B
>CR
LF</B
></TT
> and <TT
CLASS="USERINPUT"
><B
>CR</B
></TT
> as end-of-line markers.  These are the canonical
sequences used by Unix, DOS and VMS, and the classic Mac OS (before
OSX) respectively.  You may therefore safely copy source code written
on any of those systems to a different one and use it without
conversion.  (GCC may lose track of the current line number if a file
doesn't consistently use one convention, as sometimes happens when it
is edited on computers with different conventions that share a network
file system.)
     </P
><P
>If the last line of any input file lacks an end-of-line marker, the end
of the file is considered to implicitly supply one.  The C standard says
that this condition provokes undefined behavior, so GCC will emit a
warning message.
     </P
></LI
><LI
><P
>If trigraphs are enabled, they are replaced by their
corresponding single characters.  By default GCC ignores trigraphs,
but if you request a strictly conforming mode with the <TT
CLASS="COMMAND"
>-std</TT
>
option, or you specify the <TT
CLASS="COMMAND"
>-trigraphs</TT
> option, then it
converts them.
     </P
><P
>These are nine three-character sequences, all starting with <TT
CLASS="COMMAND"
>??</TT
>,
that are defined by ISO C to stand for single characters.  They permit
obsolete systems that lack some of C's punctuation to use C.  For
example, <TT
CLASS="COMMAND"
>??/</TT
> stands for <TT
CLASS="COMMAND"
>\</TT
>, so <I
CLASS="WORDASWORD"
>'??/n'</I
> is a character
constant for a newline.
     </P
><P
>Trigraphs are not popular and many compilers implement them
incorrectly.  Portable code should not rely on trigraphs being either
converted or ignored.  With <TT
CLASS="COMMAND"
>-Wtrigraphs</TT
> GCC will warn you
when a trigraph may change the meaning of your program if it were
converted.
     </P
><P
>In a string constant, you can prevent a sequence of question marks
from being confused with a trigraph by inserting a backslash between
the question marks, or by separating the string literal at the
trigraph and making use of string literal concatenation.  <I
CLASS="WORDASWORD"
>"(??\?)"</I
>
is the string <TT
CLASS="COMMAND"
>(???)</TT
>, not <TT
CLASS="COMMAND"
>(?]</TT
>.  Traditional C compilers
do not recognize these idioms.
     </P
><P
>The nine trigraphs and their replacements are
     </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="90%"
><TR
><TD
><PRE
CLASS="SCREEN"
>Trigraph:       ??(  ??)  ??&#60;  ??&#62;  ??=  ??/  ??'  ??!  ??-
Replacement:      [    ]    {    }    #    \    ^    |    ~</PRE
></TD
></TR
></TABLE
>          </P
></LI
><LI
><P
>Continued lines are merged into one long line.
     </P
><P
>A continued line is a line which ends with a backslash, <TT
CLASS="COMMAND"
>\</TT
>.  The
backslash is removed and the following line is joined with the current
one.  No space is inserted, so you may split a line anywhere, even in
the middle of a word.  (It is generally more readable to split lines
only at white space.)
     </P
><P
>The trailing backslash on a continued line is commonly referred to as a
<I
CLASS="FIRSTTERM"
>backslash-newline</I
>.
     </P
><P
>If there is white space between a backslash and the end of a line, that
is still a continued line.  However, as this is usually the result of an
editing mistake, and many compilers will not accept it as a continued
line, GCC will warn you about it.
     </P
></LI
><LI
><P
>All comments are replaced with single spaces.
     </P
><P
>There are two kinds of comments.  <I
CLASS="FIRSTTERM"
>Block comments</I
> begin with
<TT
CLASS="COMMAND"
>/*</TT
> and continue until the next <TT
CLASS="COMMAND"
>*/</TT
>.  Block comments do not
nest:
     </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="90%"
><TR
><TD
><PRE
CLASS="SCREEN"
>/* this is /* one comment */ text outside comment</PRE
></TD
></TR
></TABLE
>          </P
><P
><I
CLASS="FIRSTTERM"
>Line comments</I
> begin with <TT
CLASS="COMMAND"
>//</TT
> and continue to the end of the
current line.  Line comments do not nest either, but it does not matter,
because they would end in the same place anyway.
     </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="90%"
><TR
><TD
><PRE
CLASS="SCREEN"
>// this is // one comment
text outside comment</PRE
></TD
></TR
></TABLE
>          </P
></LI
></OL
><P
>It is safe to put line comments inside block comments, or vice versa.
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>/* block comment
   // contains line comment
   yet more comment
 */ outside comment

// line comment /* contains block comment */
     </PRE
></TD
></TR
></TABLE
>   </P
><P
>But beware of commenting out one end of a block comment with a line
comment.
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
> // l.c.  /* block comment begins
    oops! this isn't a comment anymore */
     </PRE
></TD
></TR
></TABLE
>   </P
><P
>Comments are not recognized within string literals.
<I
CLASS="WORDASWORD"
>"/* blah */"</I
> is the string constant <TT
CLASS="COMMAND"
>/* blah */</TT
>, not
an empty string.
   </P
><P
>Line comments are not in the 1989 edition of the C standard, but they
are recognized by GCC as an extension.  In C++ and in the 1999 edition
of the C standard, they are an official part of the language.
   </P
><P
>Since these transformations happen before all other processing, you can
split a line mechanically with backslash-newline anywhere.  You can
comment out the end of a line.  You can continue a line comment onto the
next line with backslash-newline.  You can even split <TT
CLASS="COMMAND"
>/*</TT
>,
<TT
CLASS="COMMAND"
>*/</TT
>, and <TT
CLASS="COMMAND"
>//</TT
> onto multiple lines with backslash-newline.
For example:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>/\
*
*/ # /*
*/ defi\
ne FO\
O 10\
20
     </PRE
></TD
></TR
></TABLE
>   </P
><P
>is equivalent to <TT
CLASS="COMMAND"
>#define FOO 1020</TT
>.  All these tricks are
extremely confusing and should not be used in code intended to be
readable.
   </P
><P
>There is no way to prevent a backslash at the end of a line from being
interpreted as a backslash-newline.  This cannot affect any correct
program, however.
   </P
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="overview.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="tokenization.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Overview</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="overview.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Tokenization</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>