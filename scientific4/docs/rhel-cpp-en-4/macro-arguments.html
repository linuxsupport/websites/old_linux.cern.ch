<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Macro Arguments</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="The C Preprocessor"
HREF="index.html"><LINK
REL="UP"
TITLE="Macros"
HREF="macros.html"><LINK
REL="PREVIOUS"
TITLE="Function-like Macros"
HREF="function-like-macros.html"><LINK
REL="NEXT"
TITLE="Stringification"
HREF="stringification.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>The C Preprocessor: Using cpp, the C Preprocessor</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="function-like-macros.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 3. Macros</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="stringification.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="MACRO-ARGUMENTS"
></A
>3.3. Macro Arguments</H1
><P
>&#13;Function-like macros can take <I
CLASS="FIRSTTERM"
>arguments</I
>, just like true functions.
To define a macro that uses arguments, you insert <I
CLASS="FIRSTTERM"
>parameters</I
>
between the pair of parentheses in the macro definition that make the
macro function-like.  The parameters must be valid C identifiers,
separated by commas and optionally whitespace.
   </P
><P
>To invoke a macro that takes arguments, you write the name of the macro
followed by a list of <I
CLASS="FIRSTTERM"
>actual arguments</I
> in parentheses, separated
by commas.  The invocation of the macro need not be restricted to a
single logical line--it can cross as many lines in the source file as
you wish.  The number of arguments you give must match the number of
parameters in the macro definition.  When the macro is expanded, each
use of a parameter in its body is replaced by the tokens of the
corresponding argument.  (You need not use all of the parameters in the
macro body.)
   </P
><P
>As an example, here is a macro that computes the minimum of two numeric
values, as it is defined in many C programs, and some uses.
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define min(X, Y)  ((X) &#60; (Y) ? (X) : (Y))
  x = min(a, b);          ==&#62;  x = ((a) &#60; (b) ? (a) : (b));
  y = min(1, 2);          ==&#62;  y = ((1) &#60; (2) ? (1) : (2));
  z = min(a + 28, *p);    ==&#62;  z = ((a + 28) &#60; (*p) ? (a + 28) : (*p));</PRE
></TD
></TR
></TABLE
>      </P
><P
>(In this small example you can already see several of the dangers of
macro arguments.  <A
HREF="macro-pitfalls.html"
>Section 3.10 <I
>Macro Pitfalls</I
></A
>, for detailed explanations.)
   </P
><P
>Leading and trailing whitespace in each argument is dropped, and all
whitespace between the tokens of an argument is reduced to a single
space.  Parentheses within each argument must balance; a comma within
such parentheses does not end the argument.  However, there is no
requirement for square brackets or braces to balance, and they do not
prevent a comma from separating arguments.  Thus,
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>macro (array[x = y, x + 1])</PRE
></TD
></TR
></TABLE
>   </P
><P
>passes two arguments to <TT
CLASS="COMMAND"
>macro</TT
>: <TT
CLASS="COMMAND"
>array[x = y</TT
> and <TT
CLASS="COMMAND"
>x +
1]</TT
>.  If you want to supply <TT
CLASS="COMMAND"
>array[x = y, x + 1]</TT
> as an argument,
you can write it as <TT
CLASS="COMMAND"
>array[(x = y, x + 1)]</TT
>, which is equivalent C
code.
   </P
><P
>All arguments to a macro are completely macro-expanded before they are
substituted into the macro body.  After substitution, the complete text
is scanned again for macros to expand, including the arguments.  This rule
may seem strange, but it is carefully designed so you need not worry
about whether any function call is actually a macro invocation.  You can
run into trouble if you try to be too clever, though.  <A
HREF="macro-pitfalls.html#ARGUMENT-PRESCAN"
>Section 3.10.6 <I
>Argument Prescan</I
></A
>, for detailed discussion.
   </P
><P
>For example, <TT
CLASS="COMMAND"
>min (min (a, b), c)</TT
> is first expanded to
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>  min (((a) &#60; (b) ? (a) : (b)), (c))</PRE
></TD
></TR
></TABLE
>   </P
><P
>and then to
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>((((a) &#60; (b) ? (a) : (b))) &#60; (c)
 ? (((a) &#60; (b) ? (a) : (b)))
 : (c))
     </PRE
></TD
></TR
></TABLE
>   </P
><P
>(Line breaks shown here for clarity would not actually be generated.)
   </P
><P
>You can leave macro arguments empty; this is not an error to the
preprocessor (but many macros will then expand to invalid code).
You cannot leave out arguments entirely; if a macro takes two arguments,
there must be exactly one comma at the top level of its argument list.
Here are some silly examples using <TT
CLASS="COMMAND"
>min</TT
>:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>min(, b)        ==&#62; ((   ) &#60; (b) ? (   ) : (b))
min(a, )        ==&#62; ((a  ) &#60; ( ) ? (a  ) : ( ))
min(,)          ==&#62; ((   ) &#60; ( ) ? (   ) : ( ))
min((,),)       ==&#62; (((,)) &#60; ( ) ? ((,)) : ( ))

min()      error--&#62; macro "min" requires 2 arguments, but only 1 given
min(,,)    error--&#62; macro "min" passed 3 arguments, but takes just 2</PRE
></TD
></TR
></TABLE
>   </P
><P
>Whitespace is not a preprocessing token, so if a macro <TT
CLASS="COMMAND"
>foo</TT
> takes
one argument, <TT
CLASS="COMMAND"
>foo ()</TT
> and <TT
CLASS="COMMAND"
>foo ( )</TT
> both supply it an
empty argument.  Previous GNU preprocessor implementations and
documentation were incorrect on this point, insisting that a
function-like macro that takes a single argument be passed a space if an
empty argument was required.
   </P
><P
>Macro parameters appearing inside string literals are not replaced by
their corresponding actual arguments.
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define foo(x) x, "x"
foo(bar)        ==&#62; bar, "x"</PRE
></TD
></TR
></TABLE
>      </P
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="function-like-macros.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="stringification.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Function-like Macros</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="macros.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Stringification</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>