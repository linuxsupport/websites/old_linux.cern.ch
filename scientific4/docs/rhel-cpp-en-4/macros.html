<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Macros</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="The C Preprocessor"
HREF="index.html"><LINK
REL="PREVIOUS"
TITLE="System Headers"
HREF="system-headers.html"><LINK
REL="NEXT"
TITLE="Function-like Macros"
HREF="function-like-macros.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="CHAPTER"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>The C Preprocessor: Using cpp, the C Preprocessor</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="system-headers.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="function-like-macros.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="CHAPTER"
><H1
><A
NAME="MACROS"
></A
>Chapter 3. Macros</H1
><P
>A <I
CLASS="FIRSTTERM"
>macro</I
> is a fragment of code which has been given a name.
Whenever the name is used, it is replaced by the contents of the macro.
There are two kinds of macros.  They differ mostly in what they look
like when they are used.  <I
CLASS="FIRSTTERM"
>Object-like</I
> macros resemble data objects
when used, <I
CLASS="FIRSTTERM"
>function-like</I
> macros resemble function calls.
  </P
><P
>You may define any valid identifier as a macro, even if it is a C
keyword.  The preprocessor does not know anything about keywords.  This
can be useful if you wish to hide a keyword such as <TT
CLASS="COMMAND"
>const</TT
> from an
older compiler that does not understand it.  However, the preprocessor
operator <TT
CLASS="COMMAND"
>defined</TT
> (<A
HREF="conditional-syntax.html#DEFINED"
>Section 4.2.3 <I
>Defined</I
></A
>) can never be defined as a
macro, and C++'s named operators (<A
HREF="predefined-macros.html#C---NAMED-OPERATORS"
>Section 3.7.4 <I
>C++ Named Operators</I
></A
>) cannot be
macros when you are compiling C++.
  </P
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="OBJECT-LIKE-MACROS"
></A
>3.1. Object-like Macros</H1
><P
>&#13;An <I
CLASS="FIRSTTERM"
>object-like macro</I
> is a simple identifier which will be replaced
by a code fragment.  It is called object-like because it looks like a
data object in code that uses it.  They are most commonly used to give
symbolic names to numeric constants.
   </P
><P
>You create macros with the <TT
CLASS="COMMAND"
>#define</TT
> directive.  <TT
CLASS="COMMAND"
>#define</TT
> is
followed by the name of the macro and then the token sequence it should
be an abbreviation for, which is variously referred to as the macro's
<I
CLASS="FIRSTTERM"
>body</I
>, <I
CLASS="FIRSTTERM"
>expansion</I
> or <I
CLASS="FIRSTTERM"
>replacement list</I
>.  For example,
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define BUFFER_SIZE 1024</PRE
></TD
></TR
></TABLE
>      </P
><P
>defines a macro named <TT
CLASS="COMMAND"
>BUFFER_SIZE</TT
> as an abbreviation for the
token <TT
CLASS="COMMAND"
>1024</TT
>.  If somewhere after this <TT
CLASS="COMMAND"
>#define</TT
> directive
there comes a C statement of the form
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>foo = (char *) malloc (BUFFER_SIZE);</PRE
></TD
></TR
></TABLE
>   </P
><P
>then the C preprocessor will recognize and <I
CLASS="FIRSTTERM"
>expand</I
> the macro
<TT
CLASS="COMMAND"
>BUFFER_SIZE</TT
>.  The C compiler will see the same tokens as it would
if you had written
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>foo = (char *) malloc (1024);</PRE
></TD
></TR
></TABLE
>   </P
><P
>By convention, macro names are written in uppercase.  Programs are
easier to read when it is possible to tell at a glance which names are
macros.
   </P
><P
>The macro's body ends at the end of the <TT
CLASS="COMMAND"
>#define</TT
> line.  You may
continue the definition onto multiple lines, if necessary, using
backslash-newline.  When the macro is expanded, however, it will all
come out on one line.  For example,
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define NUMBERS 1, \
                2, \
                3
int x[] = { NUMBERS };
     ==&#62; int x[] = { 1, 2, 3 };</PRE
></TD
></TR
></TABLE
>      </P
><P
>The most common visible consequence of this is surprising line numbers
in error messages.
   </P
><P
>There is no restriction on what can go in a macro body provided it
decomposes into valid preprocessing tokens.  Parentheses need not
balance, and the body need not resemble valid C code.  (If it does not,
you may get error messages from the C compiler when you use the macro.)
   </P
><P
>The C preprocessor scans your program sequentially.  Macro definitions
take effect at the place you write them.  Therefore, the following input
to the C preprocessor
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>foo = X;
#define X 4
bar = X;</PRE
></TD
></TR
></TABLE
>   </P
><P
>produces
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>foo = X;
bar = 4;</PRE
></TD
></TR
></TABLE
>   </P
><P
>When the preprocessor expands a macro name, the macro's expansion
replaces the macro invocation, then the expansion is examined for more
macros to expand.  For example,
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define TABLESIZE BUFSIZE
#define BUFSIZE 1024
TABLESIZE
     ==&#62; BUFSIZE
     ==&#62; 1024
     </PRE
></TD
></TR
></TABLE
>   </P
><P
><TT
CLASS="COMMAND"
>TABLESIZE</TT
> is expanded first to produce <TT
CLASS="COMMAND"
>BUFSIZE</TT
>, then that
macro is expanded to produce the final result, <TT
CLASS="COMMAND"
>1024</TT
>.
   </P
><P
>Notice that <TT
CLASS="COMMAND"
>BUFSIZE</TT
> was not defined when <TT
CLASS="COMMAND"
>TABLESIZE</TT
> was
defined.  The <TT
CLASS="COMMAND"
>#define</TT
> for <TT
CLASS="COMMAND"
>TABLESIZE</TT
> uses exactly the
expansion you specify--in this case, <TT
CLASS="COMMAND"
>BUFSIZE</TT
>--and does not
check to see whether it too contains macro names.  Only when you
<I
CLASS="EMPHASIS"
>use</I
> <TT
CLASS="COMMAND"
>TABLESIZE</TT
> is the result of its expansion scanned for
more macro names.
   </P
><P
>This makes a difference if you change the definition of <TT
CLASS="COMMAND"
>BUFSIZE</TT
>
at some point in the source file.  <TT
CLASS="COMMAND"
>TABLESIZE</TT
>, defined as shown,
will always expand using the definition of <TT
CLASS="COMMAND"
>BUFSIZE</TT
> that is
currently in effect:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define BUFSIZE 1020
#define TABLESIZE BUFSIZE
#undef BUFSIZE
#define BUFSIZE 37</PRE
></TD
></TR
></TABLE
>   </P
><P
>Now <TT
CLASS="COMMAND"
>TABLESIZE</TT
> expands (in two stages) to <TT
CLASS="COMMAND"
>37</TT
>.
   </P
><P
>If the expansion of a macro contains its own name, either directly or
via intermediate macros, it is not expanded again when the expansion is
examined for more macros.  This prevents infinite recursion.
<A
HREF="macro-pitfalls.html#SELF-REFERENTIAL-MACROS"
>Section 3.10.5 <I
>Self-Referential Macros</I
></A
>, for the precise details.
   </P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="system-headers.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="function-like-macros.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>System Headers</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
>&nbsp;</TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Function-like Macros</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>