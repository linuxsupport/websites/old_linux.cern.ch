<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Overview</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="The C Preprocessor"
HREF="index.html"><LINK
REL="PREVIOUS"
TITLE="The C Preprocessor"
HREF="index.html"><LINK
REL="NEXT"
TITLE="Initial processing"
HREF="initial-processing.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="CHAPTER"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>The C Preprocessor: Using cpp, the C Preprocessor</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="index.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="initial-processing.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="CHAPTER"
><H1
><A
NAME="OVERVIEW"
></A
>Chapter 1. Overview</H1
><P
>The C preprocessor implements the macro language used to transform C,
C++, and Objective-C programs before they are compiled.  It can also be
useful on its own.
  </P
><P
>The C preprocessor, often known as <I
CLASS="FIRSTTERM"
>cpp</I
>, is a <I
CLASS="FIRSTTERM"
>macro processor</I
>
that is used automatically by the C compiler to transform your program
before compilation.  It is called a macro processor because it allows
you to define <I
CLASS="FIRSTTERM"
>macros</I
>, which are brief abbreviations for longer
constructs.
  </P
><P
>The C preprocessor is intended to be used only with C, C++, and
Objective-C source code.  In the past, it has been abused as a general
text processor.  It will choke on input which does not obey C's lexical
rules.  For example, apostrophes will be interpreted as the beginning of
character constants, and cause errors.  Also, you cannot rely on it
preserving characteristics of the input which are not significant to
C-family languages.  If a Makefile is preprocessed, all the hard tabs
will be removed, and the Makefile will not work.
  </P
><P
>Having said that, you can often get away with using cpp on things which
are not C.  Other Algol-ish programming languages are often safe
(Pascal, Ada, etc.) So is assembly, with caution.  <TT
CLASS="COMMAND"
>-traditional-cpp</TT
>
mode preserves more white space, and is otherwise more permissive.  Many
of the problems can be avoided by writing C or C++ style comments
instead of native language comments, and keeping macros simple.
  </P
><P
>Wherever possible, you should use a preprocessor geared to the language
you are writing in.  Modern versions of the GNU assembler have macro
facilities.  Most high level programming languages have their own
conditional compilation and inclusion mechanism.  If all else fails,
try a true general text processor, such as GNU M4.
  </P
><P
>C preprocessors vary in some details.  This manual discusses the GNU C
preprocessor, which provides a small superset of the features of ISO
Standard C.  In its default mode, the GNU C preprocessor does not do a
few things required by the standard.  These are features which are
rarely, if ever, used, and may cause surprising changes to the meaning
of a program which does not expect them.  To get strict ISO Standard C,
you should use the <TT
CLASS="COMMAND"
>-std=c89</TT
> or <TT
CLASS="COMMAND"
>-std=c99</TT
> options, depending
on which version of the standard you want.  To get all the mandatory
diagnostics, you must also use <TT
CLASS="COMMAND"
>-pedantic</TT
>.  <A
HREF="invocation.html"
>Chapter 12 <I
>Invocation</I
></A
>.
  </P
><P
>This manual describes the behavior of the ISO preprocessor.  To
minimize gratuitous differences, where the ISO preprocessor's
behavior does not conflict with traditional semantics, the
traditional preprocessor should behave the same way.  The various
differences that do exist are detailed in the section <A
HREF="traditional-mode.html"
>Chapter 10 <I
>Traditional Mode</I
></A
>.
  </P
><P
>For clarity, unless noted otherwise, references to <TT
CLASS="COMMAND"
>CPP</TT
> in this
manual refer to GNU CPP.
  </P
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="CHARACTER-SETS"
></A
>1.1. Character sets</H1
><P
>Source code character set processing in C and related languages is
rather complicated.  The C standard discusses two character sets, but
there are really at least four.
   </P
><P
>The files input to CPP might be in any character set at all.  CPP's
very first action, before it even looks for line boundaries, is to
convert the file into the character set it uses for internal
processing.  That set is what the C standard calls the <I
CLASS="FIRSTTERM"
>source</I
>
character set.  It must be isomorphic with ISO 10646, also known as
Unicode.  CPP uses the UTF-8 encoding of Unicode.
   </P
><P
>At present, GNU CPP does not implement conversion from arbitrary file
encodings to the source character set.  Use of any encoding other than
plain ASCII or UTF-8, except in comments, will cause errors.  Use of
encodings that are not strict supersets of ASCII, such as Shift JIS,
may cause errors even if non-ASCII characters appear only in comments.
We plan to fix this in the near future.
   </P
><P
>All preprocessing work (the subject of the rest of this manual) is
carried out in the source character set.  If you request textual
output from the preprocessor with the <TT
CLASS="COMMAND"
>-E</TT
> option, it will be
in UTF-8.
   </P
><P
>After preprocessing is complete, string and character constants are
converted again, into the <I
CLASS="FIRSTTERM"
>execution</I
> character set.  This
character set is under control of the user; the default is UTF-8,
matching the source character set.  Wide string and character
constants have their own character set, which is not called out
specifically in the standard.  Again, it is under control of the user.
The default is UTF-16 or UTF-32, whichever fits in the target's
<TT
CLASS="COMMAND"
>wchar_t</TT
> type, in the target machine's byte
order.<A
NAME="AEN82"
HREF="#FTN.AEN82"
>[1]</A
>  Octal and hexadecimal escape sequences do not undergo
conversion; <I
CLASS="WORDASWORD"
>'\x12'</I
> has the value 0x12 regardless of the currently
selected execution character set.  All other escapes are replaced by
the character in the source character set that they represent, then
converted to the execution character set, just like unescaped
characters.
   </P
><P
>GCC does not permit the use of characters outside the ASCII range, nor
<TT
CLASS="COMMAND"
>\u</TT
> and <TT
CLASS="COMMAND"
>\U</TT
> escapes, in identifiers.  We hope this will
change eventually, but there are problems with the standard semantics
of such "extended identifiers" which must be resolved through the
ISO C and C++ committees first.
   </P
></DIV
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN82"
HREF="overview.html#AEN82"
>[1]</A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>UTF-16 does not meet the requirements of the C
standard for a wide character set, but the choice of 16-bit
<TT
CLASS="COMMAND"
>wchar_t</TT
> is enshrined in some system ABIs so we cannot fix
this.</P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="initial-processing.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>The C Preprocessor</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
>&nbsp;</TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Initial processing</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>