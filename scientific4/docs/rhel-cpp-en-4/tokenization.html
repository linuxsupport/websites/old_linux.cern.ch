<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Tokenization</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="The C Preprocessor"
HREF="index.html"><LINK
REL="UP"
TITLE="Overview"
HREF="overview.html"><LINK
REL="PREVIOUS"
TITLE="Initial processing"
HREF="initial-processing.html"><LINK
REL="NEXT"
TITLE="The preprocessing language"
HREF="the-preprocessing-language.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>The C Preprocessor: Using cpp, the C Preprocessor</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="initial-processing.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 1. Overview</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="the-preprocessing-language.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="TOKENIZATION"
></A
>1.3. Tokenization</H1
><P
>After the textual transformations are finished, the input file is
converted into a sequence of <I
CLASS="FIRSTTERM"
>preprocessing tokens</I
>.  These mostly
correspond to the syntactic tokens used by the C compiler, but there are
a few differences.  White space separates tokens; it is not itself a
token of any kind.  Tokens do not have to be separated by white space,
but it is often necessary to avoid ambiguities.
   </P
><P
>When faced with a sequence of characters that has more than one possible
tokenization, the preprocessor is greedy.  It always makes each token,
starting from the left, as big as possible before moving on to the next
token.  For instance, <TT
CLASS="COMMAND"
>a+++++b</TT
> is interpreted as
<TT
CLASS="COMMAND"
>a ++ ++ + b</TT
>, not as <TT
CLASS="COMMAND"
>a ++ + ++ b</TT
>, even though the
latter tokenization could be part of a valid C program and the former
could not.
   </P
><P
>Once the input file is broken into tokens, the token boundaries never
change, except when the <TT
CLASS="COMMAND"
>##</TT
> preprocessing operator is used to paste
tokens together.  <A
HREF="concatenation.html"
>Section 3.5 <I
>Concatenation</I
></A
>.  For example,
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define foo() bar
foo()baz
     ==&#62; bar baz
<I
CLASS="EMPHASIS"
>not</I
>
     ==&#62; barbaz
     </PRE
></TD
></TR
></TABLE
>   </P
><P
>The compiler does not re-tokenize the preprocessor's output.  Each
preprocessing token becomes one compiler token.
   </P
><P
>Preprocessing tokens fall into five broad classes: identifiers,
preprocessing numbers, string literals, punctuators, and other.  An
<I
CLASS="FIRSTTERM"
>identifier</I
> is the same as an identifier in C: any sequence of
letters, digits, or underscores, which begins with a letter or
underscore.  Keywords of C have no significance to the preprocessor;
they are ordinary identifiers.  You can define a macro whose name is a
keyword, for instance.  The only identifier which can be considered a
preprocessing keyword is <TT
CLASS="COMMAND"
>defined</TT
>.  <A
HREF="conditional-syntax.html#DEFINED"
>Section 4.2.3 <I
>Defined</I
></A
>.
   </P
><P
>This is mostly true of other languages which use the C preprocessor.
However, a few of the keywords of C++ are significant even in the
preprocessor.  <A
HREF="predefined-macros.html#C---NAMED-OPERATORS"
>Section 3.7.4 <I
>C++ Named Operators</I
></A
>.
   </P
><P
>In the 1999 C standard, identifiers may contain letters which are not
part of the "basic source character set," at the implementation's
discretion (such as accented Latin letters, Greek letters, or Chinese
ideograms).  This may be done with an extended character set, or the
<TT
CLASS="COMMAND"
>\u</TT
> and <TT
CLASS="COMMAND"
>\U</TT
> escape sequences.  GCC does not presently
implement either feature in the preprocessor or the compiler.
   </P
><P
>As an extension, GCC treats <TT
CLASS="COMMAND"
>$</TT
> as a letter.  This is for
compatibility with some systems, such as VMS, where <TT
CLASS="COMMAND"
>$</TT
> is commonly
used in system-defined function and object names.  <TT
CLASS="COMMAND"
>$</TT
> is not a
letter in strictly conforming mode, or if you specify the <TT
CLASS="COMMAND"
>-$</TT
>
option.  <A
HREF="invocation.html"
>Chapter 12 <I
>Invocation</I
></A
>.
   </P
><P
>A <I
CLASS="FIRSTTERM"
>preprocessing number</I
> has a rather bizarre definition.  The
category includes all the normal integer and floating point constants
one expects of C, but also a number of other things one might not
initially recognize as a number.  Formally, preprocessing numbers begin
with an optional period, a required decimal digit, and then continue
with any sequence of letters, digits, underscores, periods, and
exponents.  Exponents are the two-character sequences <TT
CLASS="COMMAND"
>e+</TT
>,
<TT
CLASS="COMMAND"
>e-</TT
>, <TT
CLASS="COMMAND"
>E+</TT
>, <TT
CLASS="COMMAND"
>E-</TT
>, <TT
CLASS="COMMAND"
>p+</TT
>, <TT
CLASS="COMMAND"
>p-</TT
>, <TT
CLASS="COMMAND"
>P+</TT
>, and
<TT
CLASS="COMMAND"
>P-</TT
>.  (The exponents that begin with <TT
CLASS="COMMAND"
>p</TT
> or <TT
CLASS="COMMAND"
>P</TT
> are new
to C99.  They are used for hexadecimal floating-point constants.)
   </P
><P
>The purpose of this unusual definition is to isolate the preprocessor
from the full complexity of numeric constants.  It does not have to
distinguish between lexically valid and invalid floating-point numbers,
which is complicated.  The definition also permits you to split an
identifier at any position and get exactly two tokens, which can then be
pasted back together with the <TT
CLASS="COMMAND"
>##</TT
> operator.
   </P
><P
>It's possible for preprocessing numbers to cause programs to be
misinterpreted.  For example, <TT
CLASS="COMMAND"
>0xE+12</TT
> is a preprocessing number
which does not translate to any valid numeric constant, therefore a
syntax error.  It does not mean <TT
CLASS="COMMAND"
>0xE + 12</TT
>, which is what you
might have intended.
   </P
><P
><I
CLASS="FIRSTTERM"
>String literals</I
> are string constants, character constants, and
header file names (the argument of <TT
CLASS="COMMAND"
>#include</TT
>).<A
NAME="AEN239"
HREF="#FTN.AEN239"
>[1]</A
>  String constants and character
constants are straightforward: <I
CLASS="WORDASWORD"
>"&#8230;"</I
> or <I
CLASS="WORDASWORD"
>'&#8230;'</I
>.  In
either case embedded quotes should be escaped with a backslash:
<I
CLASS="WORDASWORD"
>'\''</I
> is the character constant for <TT
CLASS="COMMAND"
>'</TT
>.  There is no limit on
the length of a character constant, but the value of a character
constant that contains more than one character is
implementation-defined.  <A
HREF="implementation-details.html"
>Chapter 11 <I
>Implementation Details</I
></A
>.
   </P
><P
>Header file names either look like string constants, <I
CLASS="WORDASWORD"
>"&#8230;"</I
>, or are
written with angle brackets instead, <I
CLASS="WORDASWORD"
>&#60;&#8230;&#62;</I
>.  In either case,
backslash is an ordinary character.  There is no way to escape the
closing quote or angle bracket.  The preprocessor looks for the header
file in different places depending on which form you use.  <A
HREF="include-operation.html"
>Section 2.2 <I
>Include Operation</I
></A
>.
   </P
><P
>No string literal may extend past the end of a line.  Older versions
of GCC accepted multi-line string constants.  You may use continued
lines instead, or string constant concatenation.  <A
HREF="differences-from-previous-versions.html"
>Section 11.4 <I
>Differences from previous versions</I
></A
>.
   </P
><P
><I
CLASS="FIRSTTERM"
>Punctuators</I
> are all the usual bits of punctuation which are
meaningful to C and C++.  All but three of the punctuation characters in
ASCII are C punctuators.  The exceptions are <TT
CLASS="COMMAND"
>@</TT
>, <TT
CLASS="COMMAND"
>$</TT
>, and
<TT
CLASS="COMMAND"
>`</TT
>.  In addition, all the two- and three-character operators are
punctuators.  There are also six <I
CLASS="FIRSTTERM"
>digraphs</I
>, which the C++ standard
calls <I
CLASS="FIRSTTERM"
>alternative tokens</I
>, which are merely alternate ways to spell
other punctuators.  This is a second attempt to work around missing
punctuation in obsolete systems.  It has no negative side effects,
unlike trigraphs, but does not cover as much ground.  The digraphs and
their corresponding normal punctuators are:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>Digraph:        &#60;%  %&#62;  &#60;:  :&#62;  %:  %:%:
Punctuator:      {   }   [   ]   #    ##</PRE
></TD
></TR
></TABLE
>   </P
><P
>Any other single character is considered "other." It is passed on to
the preprocessor's output unmolested.  The C compiler will almost
certainly reject source code containing "other" tokens.  In ASCII, the
only other characters are <TT
CLASS="COMMAND"
>@</TT
>, <TT
CLASS="COMMAND"
>$</TT
>, <TT
CLASS="COMMAND"
>`</TT
>, and control
characters other than NUL (all bits zero).  (Note that <TT
CLASS="COMMAND"
>$</TT
> is
normally considered a letter.)  All characters with the high bit set
(numeric range 0x7F-0xFF) are also "other" in the present
implementation.  This will change when proper support for international
character sets is added to GCC.
   </P
><P
>NUL is a special case because of the high probability that its
appearance is accidental, and because it may be invisible to the user
(many terminals do not display NUL at all).  Within comments, NULs are
silently ignored, just as any other character would be.  In running
text, NUL is considered white space.  For example, these two directives
have the same meaning.
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define X^@1
#define X 1</PRE
></TD
></TR
></TABLE
>      </P
><P
>(where <TT
CLASS="COMMAND"
>^@</TT
> is ASCII NUL).  Within string or character constants,
NULs are preserved.  In the latter two cases the preprocessor emits a
warning message.
   </P
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN239"
HREF="tokenization.html#AEN239"
>[1]</A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>The C
standard uses the term <I
CLASS="FIRSTTERM"
>string literal</I
> to refer only to what we are
calling <I
CLASS="FIRSTTERM"
>string constants</I
>.</P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="initial-processing.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="the-preprocessing-language.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Initial processing</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="overview.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>The preprocessing language</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>