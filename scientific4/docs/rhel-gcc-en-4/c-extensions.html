<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Extensions to the C Language Family</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="PREVIOUS"
TITLE="Locale-specific behavior"
HREF="locale-specific-behavior-implementation.html"><LINK
REL="NEXT"
TITLE="Locally Declared Labels"
HREF="local-labels.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="CHAPTER"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Using the GNU Compiler Collection (GCC)</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="locale-specific-behavior-implementation.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="local-labels.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="CHAPTER"
><H1
><A
NAME="C-EXTENSIONS"
></A
>Chapter 6. Extensions to the C Language Family</H1
><P
>&#13;

GNU C provides several language features not found in ISO standard C.
(The <TT
CLASS="COMMAND"
>-pedantic</TT
> option directs GCC to print a warning message if
any of these features is used.)  To test for the availability of these
features in conditional compilation, check for a predefined macro
<TT
CLASS="COMMAND"
>__GNUC__</TT
>, which is always defined under GCC.
  </P
><P
>These extensions are available in C and Objective-C.  Most of them are
also available in C++.  <A
HREF="c---extensions.html"
>Chapter 7 <I
>Extensions to the C++ Language</I
></A
>, for extensions that apply <I
CLASS="EMPHASIS"
>only</I
> to C++.
  </P
><P
>Some features that are in ISO C99 but not C89 or C++ are also, as
extensions, accepted by GCC in C89 mode and in C++.
  </P
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="STATEMENT-EXPRS"
></A
>6.1. Statements and Declarations in Expressions</H1
><P
>&#13;


A compound statement enclosed in parentheses may appear as an expression
in GNU C.  This allows you to use loops, switches, and local variables
within an expression.
   </P
><P
>Recall that a compound statement is a sequence of statements surrounded
by braces; in this construct, parentheses go around the braces.  For
example:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>({ int y = foo (); int z;
   if (y &#62; 0) z = y;
   else z = - y;
   z; })</PRE
></TD
></TR
></TABLE
>    </P
><P
>is a valid (though slightly more complex than necessary) expression
for the absolute value of <TT
CLASS="COMMAND"
>foo ()</TT
>.
   </P
><P
>The last thing in the compound statement should be an expression
followed by a semicolon; the value of this subexpression serves as the
value of the entire construct.  (If you use some other kind of statement
last within the braces, the construct has type <TT
CLASS="COMMAND"
>void</TT
>, and thus
effectively no value.)
   </P
><P
>This feature is especially useful in making macro definitions "safe" (so
that they evaluate each operand exactly once).  For example, the
"maximum" function is commonly defined as a macro in standard C as
follows:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define max(a,b) ((a) &#62; (b) ? (a) : (b))</PRE
></TD
></TR
></TABLE
>   </P
><P
>But this definition computes either <TT
CLASS="VARNAME"
>a</TT
> or <TT
CLASS="VARNAME"
>b</TT
> twice, with bad
results if the operand has side effects.  In GNU C, if you know the
type of the operands (here taken as <TT
CLASS="COMMAND"
>int</TT
>), you can define
the macro safely as follows:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define maxint(a,b) \
  ({int _a = (a), _b = (b); _a &#62; _b ? _a : _b; })</PRE
></TD
></TR
></TABLE
>   </P
><P
>Embedded statements are not allowed in constant expressions, such as
the value of an enumeration constant, the width of a bit-field, or
the initial value of a static variable.
   </P
><P
>If you don't know the type of the operand, you can still do this, but you
must use <TT
CLASS="COMMAND"
>typeof</TT
> (<A
HREF="typeof.html"
>Section 6.6 <I
>Referring to a Type with <TT
CLASS="COMMAND"
>typeof</TT
></I
></A
>).
   </P
><P
>In G++, the result value of a statement expression undergoes array and
function pointer decay, and is returned by value to the enclosing
expression. For instance, if <TT
CLASS="COMMAND"
>A</TT
> is a class, then
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>        A a;

        ({a;}).Foo ()</PRE
></TD
></TR
></TABLE
>    </P
><P
>will construct a temporary <TT
CLASS="COMMAND"
>A</TT
> object to hold the result of the
statement expression, and that will be used to invoke <TT
CLASS="COMMAND"
>Foo</TT
>.
Therefore the <TT
CLASS="COMMAND"
>this</TT
> pointer observed by <TT
CLASS="COMMAND"
>Foo</TT
> will not be the
address of <TT
CLASS="COMMAND"
>a</TT
>.
   </P
><P
>Any temporaries created within a statement within a statement expression
will be destroyed at the statement's end.  This makes statement
expressions inside macros slightly different from function calls.  In
the latter case temporaries introduced during argument evaluation will
be destroyed at the end of the statement that includes the function
call.  In the statement expression case they will be destroyed during
the statement expression.  For instance,
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define macro(a)  ({__typeof__(a) b = (a); b + 3; })
template&#60;typename T&#62; T function(T a) { T b = a; return b + 3; }

void foo ()
{
  macro (X ());
  function (X ());
}</PRE
></TD
></TR
></TABLE
>   </P
><P
>will have different places where temporaries are destroyed.  For the
<TT
CLASS="COMMAND"
>macro</TT
> case, the temporary <TT
CLASS="COMMAND"
>X</TT
> will be destroyed just after
the initialization of <TT
CLASS="COMMAND"
>b</TT
>.  In the <TT
CLASS="COMMAND"
>function</TT
> case that
temporary will be destroyed when the function returns.
   </P
><P
>These considerations mean that it is probably a bad idea to use
statement-expressions of this form in header files that are designed to
work with C++.  (Note that some versions of the GNU C Library contained
header files using statement-expression that lead to precisely this
bug.)
   </P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="locale-specific-behavior-implementation.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="local-labels.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Locale-specific behavior</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
>&nbsp;</TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Locally Declared Labels</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>