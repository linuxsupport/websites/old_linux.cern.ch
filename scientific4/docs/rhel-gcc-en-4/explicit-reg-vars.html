<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Variables in Specified Registers</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Extensions to the C Language Family"
HREF="c-extensions.html"><LINK
REL="PREVIOUS"
TITLE="Controlling Names Used in Assembler Code"
HREF="asm-labels.html"><LINK
REL="NEXT"
TITLE="Alternate Keywords"
HREF="alternate-keywords.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Using the GNU Compiler Collection (GCC)</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="asm-labels.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 6. Extensions to the C Language Family</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="alternate-keywords.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="EXPLICIT-REG-VARS"
></A
>6.38. Variables in Specified Registers</H1
><P
>&#13;


GNU C allows you to put a few global variables into specified hardware
registers.  You can also specify the register in which an ordinary
register variable should be allocated.
   </P
><P
></P
><UL
><LI
STYLE="list-style-type: disc"
><P
>Global register variables reserve registers throughout the program.
This may be useful in programs such as programming language
interpreters which have a couple of global variables that are accessed
very often.
     </P
></LI
><LI
STYLE="list-style-type: disc"
><P
>Local register variables in specific registers do not reserve the
registers.  The compiler's data flow analysis is capable of determining
where the specified registers contain live values, and where they are
available for other uses.  Stores into local register variables may be deleted
when they appear to be dead according to dataflow analysis.  References
to local register variables may be deleted or moved or simplified.
     </P
><P
>These local variables are sometimes convenient for use with the extended
<TT
CLASS="COMMAND"
>asm</TT
> feature (<A
HREF="extended-asm.html"
>Section 6.35 <I
>Assembler Instructions with C Expression Operands</I
></A
>), if you want to write one
output of the assembler instruction directly into a particular register.
(This will work provided the register you specify fits the constraints
specified for that operand in the <TT
CLASS="COMMAND"
>asm</TT
>.)
     </P
></LI
></UL
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="GLOBAL-REG-VARS"
></A
>6.38.1. Defining Global Register Variables</H2
><P
>&#13;
You can define a global register variable in GNU C like this:
    </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>register int *foo asm ("a5");</PRE
></TD
></TR
></TABLE
>     </P
><P
>Here <TT
CLASS="COMMAND"
>a5</TT
> is the name of the register which should be used.  Choose a
register which is normally saved and restored by function calls on your
machine, so that library routines will not clobber it.
    </P
><P
>Naturally the register name is cpu-dependent, so you would need to
conditionalize your program according to cpu type.  The register
<TT
CLASS="COMMAND"
>a5</TT
> would be a good choice on a 68000 for a variable of pointer
type.  On machines with register windows, be sure to choose a "global"
register that is not affected magically by the function call mechanism.
    </P
><P
>In addition, operating systems on one type of cpu may differ in how they
name the registers; then you would need additional conditionals.  For
example, some 68000 operating systems call this register <TT
CLASS="COMMAND"
>%a5</TT
>.
    </P
><P
>Eventually there may be a way of asking the compiler to choose a register
automatically, but first we need to figure out how it should choose and
how to enable you to guide the choice.  No solution is evident.
    </P
><P
>Defining a global register variable in a certain register reserves that
register entirely for this use, at least within the current compilation.
The register will not be allocated for any other purpose in the functions
in the current compilation.  The register will not be saved and restored by
these functions.  Stores into this register are never deleted even if they
would appear to be dead, but references may be deleted or moved or
simplified.
    </P
><P
>It is not safe to access the global register variables from signal
handlers, or from more than one thread of control, because the system
library routines may temporarily use the register for other things (unless
you recompile them specially for the task at hand).
    </P
><P
>It is not safe for one function that uses a global register variable to
call another such function <TT
CLASS="COMMAND"
>foo</TT
> by way of a third function
<TT
CLASS="COMMAND"
>lose</TT
> that was compiled without knowledge of this variable (i.e. in a
different source file in which the variable wasn't declared).  This is
because <TT
CLASS="COMMAND"
>lose</TT
> might save the register and put some other value there.
For example, you can't expect a global register variable to be available in
the comparison-function that you pass to <TT
CLASS="COMMAND"
>qsort</TT
>, since <TT
CLASS="COMMAND"
>qsort</TT
>
might have put something else in that register.  (If you are prepared to
recompile <TT
CLASS="COMMAND"
>qsort</TT
> with the same global register variable, you can
solve this problem.)
    </P
><P
>If you want to recompile <TT
CLASS="COMMAND"
>qsort</TT
> or other source files which do not
actually use your global register variable, so that they will not use that
register for any other purpose, then it suffices to specify the compiler
option <TT
CLASS="COMMAND"
>-ffixed-<TT
CLASS="VARNAME"
>reg</TT
></TT
>.  You need not actually add a global
register declaration to their source code.
    </P
><P
>A function which can alter the value of a global register variable cannot
safely be called from a function compiled without this variable, because it
could clobber the value the caller expects to find there on return.
Therefore, the function which is the entry point into the part of the
program that uses the global register variable must explicitly save and
restore the value which belongs to its caller.
    </P
><P
>&#13;


On most machines, <TT
CLASS="COMMAND"
>longjmp</TT
> will restore to each global register
variable the value it had at the time of the <TT
CLASS="COMMAND"
>setjmp</TT
>.  On some
machines, however, <TT
CLASS="COMMAND"
>longjmp</TT
> will not change the value of global
register variables.  To be portable, the function that called <TT
CLASS="COMMAND"
>setjmp</TT
>
should make other arrangements to save the values of the global register
variables, and to restore them in a <TT
CLASS="COMMAND"
>longjmp</TT
>.  This way, the same
thing will happen regardless of what <TT
CLASS="COMMAND"
>longjmp</TT
> does.
    </P
><P
>All global register variable declarations must precede all function
definitions.  If such a declaration could appear after function
definitions, the declaration would be too late to prevent the register from
being used for other purposes in the preceding functions.
    </P
><P
>Global register variables may not have initial values, because an
executable file has no means to supply initial contents for a register.
    </P
><P
>On the SPARC, there are reports that g3 &#8230; g7 are suitable
registers, but certain library functions, such as <TT
CLASS="COMMAND"
>getwd</TT
>, as well
as the subroutines for division and remainder, modify g3 and g4.  g1 and
g2 are local temporaries.
    </P
><P
>On the 68000, a2 &#8230; a5 should be suitable, as should d2 &#8230; d7.
Of course, it will not do to use more than a few of those.
    </P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="LOCAL-REG-VARS"
></A
>6.38.2. Specifying Registers for Local Variables</H2
><P
>&#13;

You can define a local register variable with a specified register
like this:
    </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>register int *foo asm ("a5");</PRE
></TD
></TR
></TABLE
>    </P
><P
>Here <TT
CLASS="COMMAND"
>a5</TT
> is the name of the register which should be used.  Note
that this is the same syntax used for defining global register
variables, but for a local variable it would appear within a function.
    </P
><P
>Naturally the register name is cpu-dependent, but this is not a
problem, since specific registers are most often useful with explicit
assembler instructions (<A
HREF="extended-asm.html"
>Section 6.35 <I
>Assembler Instructions with C Expression Operands</I
></A
>).  Both of these things
generally require that you conditionalize your program according to
cpu type.
    </P
><P
>In addition, operating systems on one type of cpu may differ in how they
name the registers; then you would need additional conditionals.  For
example, some 68000 operating systems call this register <TT
CLASS="COMMAND"
>%a5</TT
>.
    </P
><P
>Defining such a register variable does not reserve the register; it
remains available for other uses in places where flow control determines
the variable's value is not live.  However, these registers are made
unavailable for use in the reload pass; excessive use of this feature
leaves the compiler too few available registers to compile certain
functions.
    </P
><P
>This option does not guarantee that GCC will generate code that has
this variable in the register you specify at all times.  You may not
code an explicit reference to this register in an <TT
CLASS="COMMAND"
>asm</TT
> statement
and assume it will always refer to this variable.
    </P
><P
>Stores into local register variables may be deleted when they appear to be dead
according to dataflow analysis.  References to local register variables may
be deleted or moved or simplified.
    </P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="asm-labels.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="alternate-keywords.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Controlling Names Used in Assembler Code</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="c-extensions.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Alternate Keywords</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>