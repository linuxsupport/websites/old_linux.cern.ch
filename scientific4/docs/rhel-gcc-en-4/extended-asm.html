<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Assembler Instructions with C Expression Operands</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Extensions to the C Language Family"
HREF="c-extensions.html"><LINK
REL="PREVIOUS"
TITLE="An Inline Function is As Fast As a Macro"
HREF="inline.html"><LINK
REL="NEXT"
TITLE="Constraints for asmOperands"
HREF="constraints.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Using the GNU Compiler Collection (GCC)</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="inline.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 6. Extensions to the C Language Family</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="constraints.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="EXTENDED-ASM"
></A
>6.35. Assembler Instructions with C Expression Operands</H1
><P
>&#13;


In an assembler instruction using <TT
CLASS="COMMAND"
>asm</TT
>, you can specify the
operands of the instruction using C expressions.  This means you need not
guess which registers or memory locations will contain the data you want
to use.
   </P
><P
>You must specify an assembler instruction template much like what
appears in a machine description, plus an operand constraint string for
each operand.
   </P
><P
>For example, here is how to use the 68881's <TT
CLASS="COMMAND"
>fsinx</TT
> instruction:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm ("fsinx %1,%0" : "=f" (result) : "f" (angle));</PRE
></TD
></TR
></TABLE
>    </P
><P
>Here <TT
CLASS="COMMAND"
>angle</TT
> is the C expression for the input operand while
<TT
CLASS="COMMAND"
>result</TT
> is that of the output operand.  Each has <TT
CLASS="COMMAND"
>"f"</TT
> as its
operand constraint, saying that a floating point register is required.
The <TT
CLASS="COMMAND"
>=</TT
> in <TT
CLASS="COMMAND"
>=f</TT
> indicates that the operand is an output; all
output operands' constraints must use <TT
CLASS="COMMAND"
>=</TT
>.  The constraints use the
same language used in the machine description (<A
HREF="constraints.html"
>Section 6.36 <I
>Constraints for <TT
CLASS="COMMAND"
>asm</TT
>Operands</I
></A
>).
   </P
><P
>Each operand is described by an operand-constraint string followed by
the C expression in parentheses.  A colon separates the assembler
template from the first output operand and another separates the last
output operand from the first input, if any.  Commas separate the
operands within each group.  The total number of operands is currently
limited to 30; this limitation may be lifted in some future version of
GCC.
   </P
><P
>If there are no output operands but there are input operands, you must
place two consecutive colons surrounding the place where the output
operands would go.
   </P
><P
>As of GCC version 3.1, it is also possible to specify input and output
operands using symbolic names which can be referenced within the
assembler code.  These names are specified inside square brackets
preceding the constraint string, and can be referenced inside the
assembler code using <TT
CLASS="COMMAND"
>%[<TT
CLASS="VARNAME"
>name</TT
>]</TT
> instead of a percentage sign
followed by the operand number.  Using named operands the above example
could look like:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm ("fsinx %[angle],%[output]"
     : [output] "=f" (result)
     : [angle] "f" (angle));</PRE
></TD
></TR
></TABLE
>   </P
><P
>Note that the symbolic operand names have no relation whatsoever to
other C identifiers.  You may use any name you like, even those of
existing C symbols, but you must ensure that no two operands within the same
assembler construct use the same symbolic name.
   </P
><P
>Output operand expressions must be lvalues; the compiler can check this.
The input operands need not be lvalues.  The compiler cannot check
whether the operands have data types that are reasonable for the
instruction being executed.  It does not parse the assembler instruction
template and does not know what it means or even whether it is valid
assembler input.  The extended <TT
CLASS="COMMAND"
>asm</TT
> feature is most often used for
machine instructions the compiler itself does not know exist.  If
the output expression cannot be directly addressed (for example, it is a
bit-field), your constraint must allow a register.  In that case, GCC
will use the register as the output of the <TT
CLASS="COMMAND"
>asm</TT
>, and then store
that register into the output.
   </P
><P
>The ordinary output operands must be write-only; GCC will assume that
the values in these operands before the instruction are dead and need
not be generated.  Extended asm supports input-output or read-write
operands.  Use the constraint character <TT
CLASS="COMMAND"
>+</TT
> to indicate such an
operand and list it with the output operands.  You should only use
read-write operands when the constraints for the operand (or the
operand in which only some of the bits are to be changed) allow a
register.
   </P
><P
>You may, as an alternative, logically split its function into two
separate operands, one input operand and one write-only output
operand.  The connection between them is expressed by constraints
which say they need to be in the same location when the instruction
executes.  You can use the same C expression for both operands, or
different expressions.  For example, here we write the (fictitious)
<TT
CLASS="COMMAND"
>combine</TT
> instruction with <TT
CLASS="COMMAND"
>bar</TT
> as its read-only source
operand and <TT
CLASS="COMMAND"
>foo</TT
> as its read-write destination:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm ("combine %2,%0" : "=r" (foo) : "0" (foo), "g" (bar));</PRE
></TD
></TR
></TABLE
>   </P
><P
>The constraint <TT
CLASS="COMMAND"
>"0"</TT
> for operand 1 says that it must occupy the
same location as operand 0.  A number in constraint is allowed only in
an input operand and it must refer to an output operand.
   </P
><P
>Only a number in the constraint can guarantee that one operand will be in
the same place as another.  The mere fact that <TT
CLASS="COMMAND"
>foo</TT
> is the value
of both operands is not enough to guarantee that they will be in the
same place in the generated assembler code.  The following would not
work reliably:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm ("combine %2,%0" : "=r" (foo) : "r" (foo), "g" (bar));</PRE
></TD
></TR
></TABLE
>   </P
><P
>Various optimizations or reloading could cause operands 0 and 1 to be in
different registers; GCC knows no reason not to do so.  For example, the
compiler might find a copy of the value of <TT
CLASS="COMMAND"
>foo</TT
> in one register and
use it for operand 1, but generate the output operand 0 in a different
register (copying it afterward to <TT
CLASS="COMMAND"
>foo</TT
>'s own address).  Of course,
since the register for operand 1 is not even mentioned in the assembler
code, the result will not work, but GCC can't tell that.
   </P
><P
>As of GCC version 3.1, one may write <TT
CLASS="COMMAND"
>[<TT
CLASS="VARNAME"
>name</TT
>]</TT
> instead of
the operand number for a matching constraint.  For example:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm ("cmoveq %1,%2,%[result]"
     : [result] "=r"(result)
     : "r" (test), "r"(new), "[result]"(old));</PRE
></TD
></TR
></TABLE
>    </P
><P
>Some instructions clobber specific hard registers.  To describe this,
write a third colon after the input operands, followed by the names of
the clobbered hard registers (given as strings).  Here is a realistic
example for the VAX:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm volatile ("movc3 %0,%1,%2"
              : /* no outputs */
              : "g" (from), "g" (to), "g" (count)
              : "r0", "r1", "r2", "r3", "r4", "r5");</PRE
></TD
></TR
></TABLE
>    </P
><P
>You may not write a clobber description in a way that overlaps with an
input or output operand.  For example, you may not have an operand
describing a register class with one member if you mention that register
in the clobber list.  Variables declared to live in specific registers
(<A
HREF="explicit-reg-vars.html"
>Section 6.38 <I
>Variables in Specified Registers</I
></A
>), and used as asm input or output operands must
have no part mentioned in the clobber description.
There is no way for you to specify that an input
operand is modified without also specifying it as an output
operand.  Note that if all the output operands you specify are for this
purpose (and hence unused), you will then also need to specify
<TT
CLASS="COMMAND"
>volatile</TT
> for the <TT
CLASS="COMMAND"
>asm</TT
> construct, as described below, to
prevent GCC from deleting the <TT
CLASS="COMMAND"
>asm</TT
> statement as unused.
   </P
><P
>If you refer to a particular hardware register from the assembler code,
you will probably have to list the register after the third colon to
tell the compiler the register's value is modified.  In some assemblers,
the register names begin with <TT
CLASS="COMMAND"
>%</TT
>; to produce one <TT
CLASS="COMMAND"
>%</TT
> in the
assembler code, you must write <TT
CLASS="COMMAND"
>%%</TT
> in the input.
   </P
><P
>If your assembler instruction can alter the condition code register, add
<TT
CLASS="COMMAND"
>cc</TT
> to the list of clobbered registers.  GCC on some machines
represents the condition codes as a specific hardware register;
<TT
CLASS="COMMAND"
>cc</TT
> serves to name this register.  On other machines, the
condition code is handled differently, and specifying <TT
CLASS="COMMAND"
>cc</TT
> has no
effect.  But it is valid no matter what the machine.
   </P
><P
>If your assembler instructions access memory in an unpredictable
fashion, add <TT
CLASS="COMMAND"
>memory</TT
> to the list of clobbered registers.  This
will cause GCC to not keep memory values cached in registers across the
assembler instruction and not optimize stores or loads to that memory.
You will also want to add the <TT
CLASS="COMMAND"
>volatile</TT
> keyword if the memory
affected is not listed in the inputs or outputs of the <TT
CLASS="COMMAND"
>asm</TT
>, as
the <TT
CLASS="COMMAND"
>memory</TT
> clobber does not count as a side-effect of the
<TT
CLASS="COMMAND"
>asm</TT
>.  If you know how large the accessed memory is, you can add
it as input or output but if this is not known, you should add
<TT
CLASS="COMMAND"
>memory</TT
>.  As an example, if you access ten bytes of a string, you
can use a memory input like:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>{"m"( ({ struct { char x[10]; } *p = (void *)ptr ; *p; }) )}.</PRE
></TD
></TR
></TABLE
>    </P
><P
>Note that in the following example the memory input is necessary,
otherwise GCC might optimize the store to <TT
CLASS="COMMAND"
>x</TT
> away:
<TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>int foo ()
{
  int x = 42;
  int *y = &#38;x;
  int result;
  asm ("magic stuff accessing an 'int' pointed to by '%1'"
        "=&#38;d" (r) : "a" (y), "m" (*y));
  return result;
}</PRE
></TD
></TR
></TABLE
>    </P
><P
>You can put multiple assembler instructions together in a single
<TT
CLASS="COMMAND"
>asm</TT
> template, separated by the characters normally used in assembly
code for the system.  A combination that works in most places is a newline
to break the line, plus a tab character to move to the instruction field
(written as <TT
CLASS="COMMAND"
>\n\t</TT
>).  Sometimes semicolons can be used, if the
assembler allows semicolons as a line-breaking character.  Note that some
assembler dialects use semicolons to start a comment.
The input operands are guaranteed not to use any of the clobbered
registers, and neither will the output operands' addresses, so you can
read and write the clobbered registers as many times as you like.  Here
is an example of multiple instructions in a template; it assumes the
subroutine <TT
CLASS="COMMAND"
>_foo</TT
> accepts arguments in registers 9 and 10:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm ("movl %0,r9\n\tmovl %1,r10\n\tcall _foo"
     : /* no outputs */
     : "g" (from), "g" (to)
     : "r9", "r10");</PRE
></TD
></TR
></TABLE
>    </P
><P
>Unless an output operand has the <TT
CLASS="COMMAND"
>&#38;</TT
> constraint modifier, GCC
may allocate it in the same register as an unrelated input operand, on
the assumption the inputs are consumed before the outputs are produced.
This assumption may be false if the assembler code actually consists of
more than one instruction.  In such a case, use <TT
CLASS="COMMAND"
>&#38;</TT
> for each output
operand that may not overlap an input.  <A
HREF="constraints.html#MODIFIERS"
>Section 6.36.3 <I
>Constraint Modifier Characters</I
></A
>.
   </P
><P
>If you want to test the condition code produced by an assembler
instruction, you must include a branch and a label in the <TT
CLASS="COMMAND"
>asm</TT
>
construct, as follows:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm ("clr %0\n\tfrob %1\n\tbeq 0f\n\tmov #1,%0\n0:"
     : "g" (result)
     : "g" (input));</PRE
></TD
></TR
></TABLE
>    </P
><P
>This assumes your assembler supports local labels, as the GNU assembler
and most Unix assemblers do.
   </P
><P
>Speaking of labels, jumps from one <TT
CLASS="COMMAND"
>asm</TT
> to another are not
supported.  The compiler's optimizers do not know about these jumps, and
therefore they cannot take account of them when deciding how to
optimize.
   </P
><P
>Usually the most convenient way to use these <TT
CLASS="COMMAND"
>asm</TT
> instructions is to
encapsulate them in macros that look like functions.  For example,
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define sin(x)       \
({ double __value, __arg = (x);   \
   asm ("fsinx %1,%0": "=f" (__value): "f" (__arg));  \
   __value; })</PRE
></TD
></TR
></TABLE
>   </P
><P
>Here the variable <TT
CLASS="COMMAND"
>__arg</TT
> is used to make sure that the instruction
operates on a proper <TT
CLASS="COMMAND"
>double</TT
> value, and to accept only those
arguments <TT
CLASS="COMMAND"
>x</TT
> which can convert automatically to a <TT
CLASS="COMMAND"
>double</TT
>.
   </P
><P
>Another way to make sure the instruction operates on the correct data
type is to use a cast in the <TT
CLASS="COMMAND"
>asm</TT
>.  This is different from using a
variable <TT
CLASS="COMMAND"
>__arg</TT
> in that it converts more different types.  For
example, if the desired type were <TT
CLASS="COMMAND"
>int</TT
>, casting the argument to
<TT
CLASS="COMMAND"
>int</TT
> would accept a pointer with no complaint, while assigning the
argument to an <TT
CLASS="COMMAND"
>int</TT
> variable named <TT
CLASS="COMMAND"
>__arg</TT
> would warn about
using a pointer unless the caller explicitly casts it.
   </P
><P
>If an <TT
CLASS="COMMAND"
>asm</TT
> has output operands, GCC assumes for optimization
purposes the instruction has no side effects except to change the output
operands.  This does not mean instructions with a side effect cannot be
used, but you must be careful, because the compiler may eliminate them
if the output operands aren't used, or move them out of loops, or
replace two with one if they constitute a common subexpression.  Also,
if your instruction does have a side effect on a variable that otherwise
appears not to change, the old value of the variable may be reused later
if it happens to be found in a register.
   </P
><P
>You can prevent an <TT
CLASS="COMMAND"
>asm</TT
> instruction from being deleted, moved
significantly, or combined, by writing the keyword <TT
CLASS="COMMAND"
>volatile</TT
> after
the <TT
CLASS="COMMAND"
>asm</TT
>.  For example:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#define get_and_set_priority(new)              \
({ int __old;                                  \
   asm volatile ("get_and_set_priority %0, %1" \
                 : "=g" (__old) : "g" (new));  \
   __old; })</PRE
></TD
></TR
></TABLE
>   </P
><P
>If you write an <TT
CLASS="COMMAND"
>asm</TT
> instruction with no outputs, GCC will know
the instruction has side-effects and will not delete the instruction or
move it outside of loops.
   </P
><P
>The <TT
CLASS="COMMAND"
>volatile</TT
> keyword indicates that the instruction has
important side-effects.  GCC will not delete a volatile <TT
CLASS="COMMAND"
>asm</TT
> if
it is reachable.  (The instruction can still be deleted if GCC can
prove that control-flow will never reach the location of the
instruction.)  In addition, GCC will not reschedule instructions
across a volatile <TT
CLASS="COMMAND"
>asm</TT
> instruction.  For example:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>*(volatile int *)addr = foo;
asm volatile ("eieio" : : );</PRE
></TD
></TR
></TABLE
>   </P
><P
>Assume <TT
CLASS="COMMAND"
>addr</TT
> contains the address of a memory mapped device
register.  The PowerPC <TT
CLASS="COMMAND"
>eieio</TT
> instruction (Enforce In-order
Execution of I/O) tells the CPU to make sure that the store to that
device register happens before it issues any other I/O.
   </P
><P
>Note that even a volatile <TT
CLASS="COMMAND"
>asm</TT
> instruction can be moved in ways
that appear insignificant to the compiler, such as across jump
instructions.  You can't expect a sequence of volatile <TT
CLASS="COMMAND"
>asm</TT
>
instructions to remain perfectly consecutive.  If you want consecutive
output, use a single <TT
CLASS="COMMAND"
>asm</TT
>.  Also, GCC will perform some
optimizations across a volatile <TT
CLASS="COMMAND"
>asm</TT
> instruction; GCC does not
"forget everything" when it encounters a volatile <TT
CLASS="COMMAND"
>asm</TT
>
instruction the way some other compilers do.
   </P
><P
>An <TT
CLASS="COMMAND"
>asm</TT
> instruction without any operands or clobbers (an "old
style" <TT
CLASS="COMMAND"
>asm</TT
>) will be treated identically to a volatile
<TT
CLASS="COMMAND"
>asm</TT
> instruction.
   </P
><P
>It is a natural idea to look for a way to give access to the condition
code left by the assembler instruction.  However, when we attempted to
implement this, we found no way to make it work reliably.  The problem
is that output operands might need reloading, which would result in
additional following "store" instructions.  On most machines, these
instructions would alter the condition code before there was time to
test it.  This problem doesn't arise for ordinary "test" and
"compare" instructions because they don't have any output operands.
   </P
><P
>For reasons similar to those described above, it is not possible to give
an assembler instruction access to the condition code left by previous
instructions.
   </P
><P
>If you are writing a header file that should be includable in ISO C
programs, write <TT
CLASS="COMMAND"
>__asm__</TT
> instead of <TT
CLASS="COMMAND"
>asm</TT
>.  <A
HREF="alternate-keywords.html"
>Section 6.39 <I
>Alternate Keywords</I
></A
>.
   </P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="AEN10346"
></A
>6.35.1. Size of an <TT
CLASS="COMMAND"
>asm</TT
></H2
><P
>Some targets require that GCC track the size of each instruction used in
order to generate correct code.  Because the final length of an
<TT
CLASS="COMMAND"
>asm</TT
> is only known by the assembler, GCC must make an estimate as
to how big it will be.  The estimate is formed by counting the number of
statements in the pattern of the <TT
CLASS="COMMAND"
>asm</TT
> and multiplying that by the
length of the longest instruction on that processor.  Statements in the
<TT
CLASS="COMMAND"
>asm</TT
> are identified by newline characters and whatever statement
separator characters are supported by the assembler; on most processors
this is the `<TT
CLASS="COMMAND"
>;</TT
>' character.
    </P
><P
>Normally, GCC's estimate is perfectly adequate to ensure that correct
code is generated, but it is possible to confuse the compiler if you use
pseudo instructions or assembler macros that expand into multiple real
instructions or if you use assembler directives that expand to more
space in the object file than would be needed for a single instruction.
If this happens then the assembler will produce a diagnostic saying that
a label is unreachable.
    </P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="AEN10355"
></A
>6.35.2. i386 floating point asm operands</H2
><P
>There are several rules on the usage of stack-like regs in
asm_operands insns.  These rules apply only to the operands that are
stack-like regs:
    </P
><P
></P
><OL
TYPE="1"
><LI
><P
>Given a set of input regs that die in an asm_operands, it is
necessary to know which are implicitly popped by the asm, and
which must be explicitly popped by gcc.
      </P
><P
>An input reg that is implicitly popped by the asm must be
explicitly clobbered, unless it is constrained to match an
output operand.
      </P
></LI
><LI
><P
>For any input reg that is implicitly popped by an asm, it is
necessary to know how to adjust the stack to compensate for the pop.
If any non-popped input is closer to the top of the reg-stack than
the implicitly popped reg, it would not be possible to know what the
stack looked like--it's not clear how the rest of the stack "slides
up".
      </P
><P
>All implicitly popped input regs must be closer to the top of
the reg-stack than any input that is not implicitly popped.
      </P
><P
>It is possible that if an input dies in an insn, reload might
use the input reg for an output reload.  Consider this example:
      </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="90%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm ("foo" : "=t" (a) : "f" (b));</PRE
></TD
></TR
></TABLE
>           </P
><P
>This asm says that input B is not popped by the asm, and that
the asm pushes a result onto the reg-stack, i.e., the stack is one
deeper after the asm than it was before.  But, it is possible that
reload will think that it can use the same reg for both the input and
the output, if input B dies in this insn.
      </P
><P
>If any input operand uses the <TT
CLASS="COMMAND"
>f</TT
> constraint, all output reg
constraints must use the <TT
CLASS="COMMAND"
>&#38;</TT
> earlyclobber.
      </P
><P
>The asm above would be written as
      </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="90%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm ("foo" : "=&#38;t" (a) : "f" (b));</PRE
></TD
></TR
></TABLE
>           </P
></LI
><LI
><P
>Some operands need to be in particular places on the stack.  All
output operands fall in this category--there is no other way to
know which regs the outputs appear in unless the user indicates
this in the constraints.
      </P
><P
>Output operands must specifically indicate which reg an output
appears in after an asm.  <TT
CLASS="COMMAND"
>=f</TT
> is not allowed: the operand
constraints must select a class with a single reg.
      </P
></LI
><LI
><P
>Output operands may not be "inserted" between existing stack regs.
Since no 387 opcode uses a read/write operand, all output operands
are dead before the asm_operands, and are pushed by the asm_operands.
It makes no sense to push anywhere but the top of the reg-stack.
      </P
><P
>Output operands must start at the top of the reg-stack: output
operands may not "skip" a reg.
      </P
></LI
><LI
><P
>Some asm statements may need extra stack space for internal
calculations.  This can be guaranteed by clobbering stack registers
unrelated to the inputs and outputs.
      </P
></LI
></OL
><P
>Here are a couple of reasonable asms to want to write.  This asm
takes one input, which is internally popped, and produces two outputs.
    </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm ("fsincos" : "=t" (cos), "=u" (sin) : "0" (inp));</PRE
></TD
></TR
></TABLE
>    </P
><P
>This asm takes two inputs, which are popped by the <TT
CLASS="COMMAND"
>fyl2xp1</TT
> opcode,
and replaces them with one output.  The user must code the <TT
CLASS="COMMAND"
>st(1)</TT
>
clobber for reg-stack.c to know that <TT
CLASS="COMMAND"
>fyl2xp1</TT
> pops both inputs.
    </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>asm ("fyl2xp1" : "=t" (result) : "0" (x), "u" (y) : "st(1)");</PRE
></TD
></TR
></TABLE
>     </P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="inline.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="constraints.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>An Inline Function is As Fast As a Macro</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="c-extensions.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Constraints for <TT
CLASS="COMMAND"
>asm</TT
>Operands</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>