<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>An Inline Function is As Fast As a Macro</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Extensions to the C Language Family"
HREF="c-extensions.html"><LINK
REL="PREVIOUS"
TITLE="Specifying Attributes of Types"
HREF="type-attributes.html"><LINK
REL="NEXT"
TITLE="Assembler Instructions with C Expression Operands"
HREF="extended-asm.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Using the GNU Compiler Collection (GCC)</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="type-attributes.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 6. Extensions to the C Language Family</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="extended-asm.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="INLINE"
></A
>6.34. An Inline Function is As Fast As a Macro</H1
><P
>&#13;


By declaring a function <TT
CLASS="COMMAND"
>inline</TT
>, you can direct GCC to
integrate that function's code into the code for its callers.  This
makes execution faster by eliminating the function-call overhead; in
addition, if any of the actual argument values are constant, their known
values may permit simplifications at compile time so that not all of the
inline function's code needs to be included.  The effect on code size is
less predictable; object code may be larger or smaller with function
inlining, depending on the particular case.  Inlining of functions is an
optimization and it really "works" only in optimizing compilation.  If
you don't use <TT
CLASS="COMMAND"
>-O</TT
>, no function is really inline.
   </P
><P
>Inline functions are included in the ISO C99 standard, but there are
currently substantial differences between what GCC implements and what
the ISO C99 standard requires.
   </P
><P
>To declare a function inline, use the <TT
CLASS="COMMAND"
>inline</TT
> keyword in its
declaration, like this:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>inline int
inc (int *a)
{
  (*a)++;
}</PRE
></TD
></TR
></TABLE
>   </P
><P
>(If you are writing a header file to be included in ISO C programs, write
<TT
CLASS="COMMAND"
>__inline__</TT
> instead of <TT
CLASS="COMMAND"
>inline</TT
>.  <A
HREF="alternate-keywords.html"
>Section 6.39 <I
>Alternate Keywords</I
></A
>.)
You can also make all "simple enough" functions inline with the option
<TT
CLASS="COMMAND"
>-finline-functions</TT
>.
   </P
><P
>Note that certain usages in a function definition can make it unsuitable
for inline substitution.  Among these usages are: use of varargs, use of
alloca, use of variable sized data types (<A
HREF="variable-length.html"
>Section 6.14 <I
>Arrays of Variable Length</I
></A
>),
use of computed goto (<A
HREF="labels-as-values.html"
>Section 6.3 <I
>Labels as Values</I
></A
>), use of nonlocal goto,
and nested functions (<A
HREF="nested-functions.html"
>Section 6.4 <I
>Nested Functions</I
></A
>).  Using <TT
CLASS="COMMAND"
>-Winline</TT
>
will warn when a function marked <TT
CLASS="COMMAND"
>inline</TT
> could not be substituted,
and will give the reason for the failure.
   </P
><P
>Note that in C and Objective-C, unlike C++, the <TT
CLASS="COMMAND"
>inline</TT
> keyword
does not affect the linkage of the function.
   </P
><P
>&#13;


GCC automatically inlines member functions defined within the class
body of C++ programs even if they are not explicitly declared
<TT
CLASS="COMMAND"
>inline</TT
>.  (You can override this with <TT
CLASS="COMMAND"
>-fno-default-inline</TT
>;
<A
HREF="c---dialect-options.html"
>Section 4.5 <I
>Options Controlling C++ Dialect</I
></A
>.)
   </P
><P
>&#13;When a function is both inline and <TT
CLASS="COMMAND"
>static</TT
>, if all calls to the
function are integrated into the caller, and the function's address is
never used, then the function's own assembler code is never referenced.
In this case, GCC does not actually output assembler code for the
function, unless you specify the option <TT
CLASS="COMMAND"
>-fkeep-inline-functions</TT
>.
Some calls cannot be integrated for various reasons (in particular,
calls that precede the function's definition cannot be integrated, and
neither can recursive calls within the definition).  If there is a
nonintegrated call, then the function is compiled to assembler code as
usual.  The function must also be compiled as usual if the program
refers to its address, because that can't be inlined.
   </P
><P
>When an inline function is not <TT
CLASS="COMMAND"
>static</TT
>, then the compiler must assume
that there may be calls from other source files; since a global symbol can
be defined only once in any program, the function must not be defined in
the other source files, so the calls therein cannot be integrated.
Therefore, a non-<TT
CLASS="COMMAND"
>static</TT
> inline function is always compiled on its
own in the usual fashion.
   </P
><P
>If you specify both <TT
CLASS="COMMAND"
>inline</TT
> and <TT
CLASS="COMMAND"
>extern</TT
> in the function
definition, then the definition is used only for inlining.  In no case
is the function compiled on its own, not even if you refer to its
address explicitly.  Such an address becomes an external reference, as
if you had only declared the function, and had not defined it.
   </P
><P
>This combination of <TT
CLASS="COMMAND"
>inline</TT
> and <TT
CLASS="COMMAND"
>extern</TT
> has almost the
effect of a macro.  The way to use it is to put a function definition in
a header file with these keywords, and put another copy of the
definition (lacking <TT
CLASS="COMMAND"
>inline</TT
> and <TT
CLASS="COMMAND"
>extern</TT
>) in a library file.
The definition in the header file will cause most calls to the function
to be inlined.  If any uses of the function remain, they will refer to
the single copy in the library.
   </P
><P
>Since GCC eventually will implement ISO C99 semantics for
inline functions, it is best to use <TT
CLASS="COMMAND"
>static inline</TT
> only
to guarantee compatibility.  (The
existing semantics will remain available when <TT
CLASS="COMMAND"
>-std=gnu89</TT
> is
specified, but eventually the default will be <TT
CLASS="COMMAND"
>-std=gnu99</TT
> and
that will implement the C99 semantics, though it does not do so yet.)
   </P
><P
>GCC does not inline any functions when not optimizing unless you specify
the <TT
CLASS="COMMAND"
>always_inline</TT
> attribute for the function, like this:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>/* Prototype.  */
inline void foo (const char) __attribute__((always_inline));</PRE
></TD
></TR
></TABLE
>    </P
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="type-attributes.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="extended-asm.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Specifying Attributes of Types</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="c-extensions.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Assembler Instructions with C Expression Operands</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>