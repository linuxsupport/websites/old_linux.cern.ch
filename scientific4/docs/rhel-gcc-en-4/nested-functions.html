<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Nested Functions</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Extensions to the C Language Family"
HREF="c-extensions.html"><LINK
REL="PREVIOUS"
TITLE="Labels as Values"
HREF="labels-as-values.html"><LINK
REL="NEXT"
TITLE="Constructing Function Calls"
HREF="constructing-calls.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Using the GNU Compiler Collection (GCC)</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="labels-as-values.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 6. Extensions to the C Language Family</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="constructing-calls.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="NESTED-FUNCTIONS"
></A
>6.4. Nested Functions</H1
><P
>&#13;

A <I
CLASS="FIRSTTERM"
>nested function</I
> is a function defined inside another function.
(Nested functions are not supported for GNU C++.)  The nested function's
name is local to the block where it is defined.  For example, here we
define a nested function named <TT
CLASS="COMMAND"
>square</TT
>, and call it twice:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>foo (double a, double b)
{
  double square (double z) { return z * z; }

  return square (a) + square (b);
}
     </PRE
></TD
></TR
></TABLE
>   </P
><P
>The nested function can access all the variables of the containing
function that are visible at the point of its definition.  This is
called <I
CLASS="FIRSTTERM"
>lexical scoping</I
>.  For example, here we show a nested
function which uses an inherited variable named <TT
CLASS="COMMAND"
>offset</TT
>:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>bar (int *array, int offset, int size)
{
  int access (int *array, int index)
    { return array[index + offset]; }
  int i;
  /* &#8230; */
  for (i = 0; i &#60; size; i++)
    /* &#8230; */ access (array, i) /* &#8230; */
}
     </PRE
></TD
></TR
></TABLE
>   </P
><P
>Nested function definitions are permitted within functions in the places
where variable definitions are allowed; that is, in any block, before
the first statement in the block.
   </P
><P
>It is possible to call the nested function from outside the scope of its
name by storing its address or passing the address to another function:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>hack (int *array, int size)
{
  void store (int index, int value)
    { array[index] = value; }

  intermediate (store, size);
}</PRE
></TD
></TR
></TABLE
>   </P
><P
>Here, the function <TT
CLASS="COMMAND"
>intermediate</TT
> receives the address of
<TT
CLASS="COMMAND"
>store</TT
> as an argument.  If <TT
CLASS="COMMAND"
>intermediate</TT
> calls <TT
CLASS="COMMAND"
>store</TT
>,
the arguments given to <TT
CLASS="COMMAND"
>store</TT
> are used to store into <TT
CLASS="COMMAND"
>array</TT
>.
But this technique works only so long as the containing function
(<TT
CLASS="COMMAND"
>hack</TT
>, in this example) does not exit.
   </P
><P
>If you try to call the nested function through its address after the
containing function has exited, all hell will break loose.  If you try
to call it after a containing scope level has exited, and if it refers
to some of the variables that are no longer in scope, you may be lucky,
but it's not wise to take the risk.  If, however, the nested function
does not refer to anything that has gone out of scope, you should be
safe.
   </P
><P
>GCC implements taking the address of a nested function using a technique
called <I
CLASS="FIRSTTERM"
>trampolines</I
>.  A paper describing them is available as
   </P
><P
>http://people.debian.org/~aaronl/Usenix88-lexic.pdf.
   </P
><P
>A nested function can jump to a label inherited from a containing
function, provided the label was explicitly declared in the containing
function (<A
HREF="local-labels.html"
>Section 6.2 <I
>Locally Declared Labels</I
></A
>).  Such a jump returns instantly to the
containing function, exiting the nested function which did the
<TT
CLASS="COMMAND"
>goto</TT
> and any intermediate functions as well.  Here is an example:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>bar (int *array, int offset, int size)
{
  __label__ failure;
  int access (int *array, int index)
    {
      if (index &#62; size)
        goto failure;
      return array[index + offset];
    }
  int i;
  /* &#8230; */
  for (i = 0; i &#60; size; i++)
    /* &#8230; */ access (array, i) /* &#8230; */
  /* &#8230; */
  return 0;

 /* Control comes here from <TT
CLASS="COMMAND"
>access</TT
>
    if it detects an error.  */
 failure:
  return -1;
}
     </PRE
></TD
></TR
></TABLE
>   </P
><P
>A nested function always has internal linkage.  Declaring one with
<TT
CLASS="COMMAND"
>extern</TT
> is erroneous.  If you need to declare the nested function
before its definition, use <TT
CLASS="COMMAND"
>auto</TT
> (which is otherwise meaningless
for function declarations).
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>bar (int *array, int offset, int size)
{
  __label__ failure;
  auto int access (int *, int);
  /* &#8230; */
  int access (int *array, int index)
    {
      if (index &#62; size)
        goto failure;
      return array[index + offset];
    }
  /* &#8230; */
}</PRE
></TD
></TR
></TABLE
>   </P
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="labels-as-values.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="constructing-calls.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Labels as Values</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="c-extensions.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Constructing Function Calls</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>