<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Using Precompiled Headers</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="GCC Command Options"
HREF="invoking-gcc.html"><LINK
REL="PREVIOUS"
TITLE="Environment Variables Affecting GCC"
HREF="environment-variables.html"><LINK
REL="NEXT"
TITLE="Running Protoize"
HREF="running-protoize.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Using the GNU Compiler Collection (GCC)</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="environment-variables.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 4. GCC Command Options</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="running-protoize.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="PRECOMPILED-HEADERS"
></A
>4.20. Using Precompiled Headers</H1
><P
>&#13;
Often large projects have many header files that are included in every
source file.  The time the compiler takes to process these header files
over and over again can account for nearly all of the time required to
build the project.  To make builds faster, GCC allows users to
`precompile' a header file; then, if builds can use the precompiled
header file they will be much faster.
   </P
><P
><I
CLASS="EMPHASIS"
>Caution:</I
> There are a few known situations where GCC will
crash when trying to use a precompiled header.  If you have trouble
with a precompiled header, you should remove the precompiled header
and compile without it.  In addition, please use GCC's on-line
defect-tracking system to report any problems you encounter with
precompiled headers. 
   </P
><P
>To create a precompiled header file, simply compile it as you would any
other file, if necessary using the <TT
CLASS="COMMAND"
>-x</TT
> option to make the driver
treat it as a C or C++ header file.  You will probably want to use a
tool like <TT
CLASS="COMMAND"
>make</TT
> to keep the precompiled header up-to-date when
the headers it contains change.
   </P
><P
>A precompiled header file will be searched for when <TT
CLASS="COMMAND"
>#include</TT
> is
seen in the compilation.  As it searches for the included file
() the
compiler looks for a precompiled header in each directory just before it
looks for the include file in that directory.  The name searched for is
the name specified in the <TT
CLASS="COMMAND"
>#include</TT
> with <TT
CLASS="COMMAND"
>.gch</TT
> appended.  If
the precompiled header file can't be used, it is ignored.
   </P
><P
>For instance, if you have <TT
CLASS="COMMAND"
>#include "all.h"</TT
>, and you have
<TT
CLASS="COMMAND"
>all.h.gch</TT
> in the same directory as <TT
CLASS="COMMAND"
>all.h</TT
>, then the
precompiled header file will be used if possible, and the original
header will be used otherwise.
   </P
><P
>Alternatively, you might decide to put the precompiled header file in a
directory and use <TT
CLASS="COMMAND"
>-I</TT
> to ensure that directory is searched
before (or instead of) the directory containing the original header.
Then, if you want to check that the precompiled header file is always
used, you can put a file of the same name as the original header in this
directory containing an <TT
CLASS="COMMAND"
>#error</TT
> command.
   </P
><P
>This also works with <TT
CLASS="COMMAND"
>-include</TT
>.  So yet another way to use
precompiled headers, good for projects not designed with precompiled
header files in mind, is to simply take most of the header files used by
a project, include them from another header file, precompile that header
file, and <TT
CLASS="COMMAND"
>-include</TT
> the precompiled header.  If the header files
have guards against multiple inclusion, they will be skipped because
they've already been included (in the precompiled header).
   </P
><P
>If you need to precompile the same header file for different
languages, targets, or compiler options, you can instead make a
<I
CLASS="EMPHASIS"
>directory</I
> named like <TT
CLASS="COMMAND"
>all.h.gch</TT
>, and put each precompiled
header in the directory.  (It doesn't matter what you call the files
in the directory, every precompiled header in the directory will be
considered.)  The first precompiled header encountered in the
directory that is valid for this compilation will be used; they're
searched in no particular order.
   </P
><P
>There are many other possibilities, limited only by your imagination,
good sense, and the constraints of your build system.
   </P
><P
>A precompiled header file can be used only when these conditions apply:
   </P
><P
></P
><UL
><LI
STYLE="list-style-type: disc"
><P
>Only one precompiled header can be used in a particular compilation.
     </P
></LI
><LI
STYLE="list-style-type: disc"
><P
>A precompiled header can't be used once the first C token is seen.  You
can have preprocessor directives before a precompiled header; you can
even include a precompiled header from inside another header, so long as
there are no C tokens before the <TT
CLASS="COMMAND"
>#include</TT
>.
     </P
></LI
><LI
STYLE="list-style-type: disc"
><P
>The precompiled header file must be produced for the same language as
the current compilation.  You can't use a C precompiled header for a C++
compilation.
     </P
></LI
><LI
STYLE="list-style-type: disc"
><P
>The precompiled header file must be produced by the same compiler
version and configuration as the current compilation is using.
The easiest way to guarantee this is to use the same compiler binary
for creating and using precompiled headers.
     </P
></LI
><LI
STYLE="list-style-type: disc"
><P
>Any macros defined before the precompiled header (including with
<TT
CLASS="COMMAND"
>-D</TT
>) must either be defined in the same way as when the
precompiled header was generated, or must not affect the precompiled
header, which usually means that the they don't appear in the
precompiled header at all.
     </P
></LI
><LI
STYLE="list-style-type: disc"
><P
>Certain command-line options must be defined in the same way as when the
precompiled header was generated.  At present, it's not clear which
options are safe to change and which are not; the safest choice is to
use exactly the same options when generating and using the precompiled
header.
     </P
></LI
></UL
><P
>For all of these but the last, the compiler will automatically ignore
the precompiled header if the conditions aren't met.  For the last item,
some option changes will cause the precompiled header to be rejected,
but not all incompatible option combinations have yet been found. 
   </P
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="environment-variables.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="running-protoize.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Environment Variables Affecting GCC</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="invoking-gcc.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Running Protoize</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>