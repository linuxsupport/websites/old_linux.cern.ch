<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Where's the Template?</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Extensions to the C++ Language"
HREF="c---extensions.html"><LINK
REL="PREVIOUS"
TITLE="#pragma interface and implementation"
HREF="c---interface.html"><LINK
REL="NEXT"
TITLE="Extracting the function pointer from a bound pointer to member function"
HREF="bound-member-functions.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Using the GNU Compiler Collection (GCC)</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="c---interface.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 7. Extensions to the C++ Language</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="bound-member-functions.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="TEMPLATE-INSTANTIATION"
></A
>7.6. Where's the Template?</H1
><P
>&#13;C++ templates are the first language feature to require more
intelligence from the environment than one usually finds on a UNIX
system.  Somehow the compiler and linker have to make sure that each
template instance occurs exactly once in the executable if it is needed,
and not at all otherwise.  There are two basic approaches to this
problem, which are referred to as the Borland model and the Cfront model.
   </P
><P
></P
><DIV
CLASS="VARIABLELIST"
><DL
><DT
>Borland model</DT
><DD
><P
>Borland C++ solved the template instantiation problem by adding the code
equivalent of common blocks to their linker; the compiler emits template
instances in each translation unit that uses them, and the linker
collapses them together.  The advantage of this model is that the linker
only has to consider the object files themselves; there is no external
complexity to worry about.  This disadvantage is that compilation time
is increased because the template code is being compiled repeatedly.
Code written for this model tends to include definitions of all
templates in the header file, since they must be seen to be
instantiated.
      </P
></DD
><DT
>Cfront model</DT
><DD
><P
>The AT&#38;T C++ translator, Cfront, solved the template instantiation
problem by creating the notion of a template repository, an
automatically maintained place where template instances are stored.  A
more modern version of the repository works as follows: As individual
object files are built, the compiler places any template definitions and
instantiations encountered in the repository.  At link time, the link
wrapper adds in the objects in the repository and compiles any needed
instances that were not previously emitted.  The advantages of this
model are more optimal compilation speed and the ability to use the
system linker; to implement the Borland model a compiler vendor also
needs to replace the linker.  The disadvantages are vastly increased
complexity, and thus potential for error; for some code this can be
just as transparent, but in practice it can been very difficult to build
multiple programs in one directory and one program in multiple
directories.  Code written for this model tends to separate definitions
of non-inline member templates into a separate file, which should be
compiled separately.
      </P
></DD
></DL
></DIV
><P
>When used with GNU ld version 2.8 or later on an ELF system such as
GNU/Linux, G++ supports the
Borland model.  On other systems, G++ implements neither automatic
model.
   </P
><P
>A future version of G++ will support a hybrid model whereby the compiler
will emit any instantiations for which the template definition is
included in the compile, and store template definitions and
instantiation context information into the object file for the rest.
The link wrapper will extract that information as necessary and invoke
the compiler to produce the remaining instantiations.  The linker will
then combine duplicate instantiations.
   </P
><P
>In the mean time, you have the following options for dealing with
template instantiations:
   </P
><P
></P
><OL
TYPE="1"
><LI
><P
>Compile your template-using code with <TT
CLASS="COMMAND"
>-frepo</TT
>.  The compiler will
generate files with the extension <TT
CLASS="COMMAND"
>.rpo</TT
> listing all of the
template instantiations used in the corresponding object files which
could be instantiated there; the link wrapper, <TT
CLASS="COMMAND"
>collect2</TT
>, will
then update the <TT
CLASS="COMMAND"
>.rpo</TT
> files to tell the compiler where to place
those instantiations and rebuild any affected object files.  The
link-time overhead is negligible after the first pass, as the compiler
will continue to place the instantiations in the same files.
     </P
><P
>This is your best option for application code written for the Borland
model, as it will just work.  Code written for the Cfront model will
need to be modified so that the template definitions are available at
one or more points of instantiation; usually this is as simple as adding
<TT
CLASS="COMMAND"
>#include &#60;tmethods.cc&#62;</TT
> to the end of each template header.
     </P
><P
>For library code, if you want the library to provide all of the template
instantiations it needs, just try to link all of its object files
together; the link will fail, but cause the instantiations to be
generated as a side effect.  Be warned, however, that this may cause
conflicts if multiple libraries try to provide the same instantiations.
For greater control, use explicit instantiation as described in the next
option.
     </P
></LI
><LI
><P
>Compile your code with <TT
CLASS="COMMAND"
>-fno-implicit-templates</TT
> to disable the
implicit generation of template instances, and explicitly instantiate
all the ones you use.  This approach requires more knowledge of exactly
which instances you need than do the others, but it's less
mysterious and allows greater control.  You can scatter the explicit
instantiations throughout your program, perhaps putting them in the
translation units where the instances are used or the translation units
that define the templates themselves; you can put all of the explicit
instantiations you need into one big file; or you can create small files
like
     </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="90%"
><TR
><TD
><PRE
CLASS="SCREEN"
>#include "Foo.h"
#include "Foo.cc"

template class Foo&#60;int&#62;;
template ostream&#38; operator &#60;&#60;
                (ostream&#38;, const Foo&#60;int&#62;&#38;);</PRE
></TD
></TR
></TABLE
>          </P
><P
>for each of the instances you need, and create a template instantiation
library from those.
     </P
><P
>If you are using Cfront-model code, you can probably get away with not
using <TT
CLASS="COMMAND"
>-fno-implicit-templates</TT
> when compiling files that don't
<TT
CLASS="COMMAND"
>#include</TT
> the member template definitions.
     </P
><P
>If you use one big file to do the instantiations, you may want to
compile it without <TT
CLASS="COMMAND"
>-fno-implicit-templates</TT
> so you get all of the
instances required by your explicit instantiations (but not by any
other files) without having to specify them as well.
     </P
><P
>G++ has extended the template instantiation syntax given in the ISO
standard to allow forward declaration of explicit instantiations
(with <TT
CLASS="COMMAND"
>extern</TT
>), instantiation of the compiler support data for a
template class (i.e. the vtable) without instantiating any of its
members (with <TT
CLASS="COMMAND"
>inline</TT
>), and instantiation of only the static data
members of a template class, without the support data or member
functions (with (<TT
CLASS="COMMAND"
>static</TT
>):
     </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="90%"
><TR
><TD
><PRE
CLASS="SCREEN"
>extern template int max (int, int);
inline template class Foo&#60;int&#62;;
static template class Foo&#60;int&#62;;</PRE
></TD
></TR
></TABLE
>          </P
></LI
><LI
><P
>Do nothing.  Pretend G++ does implement automatic instantiation
management.  Code written for the Borland model will work fine, but
each translation unit will contain instances of each of the templates it
uses.  In a large program, this can lead to an unacceptable amount of code
duplication.
     </P
></LI
></OL
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="c---interface.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="bound-member-functions.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>#pragma interface and implementation</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="c---extensions.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Extracting the function pointer from a bound pointer to member function</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>