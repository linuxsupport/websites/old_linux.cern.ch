<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>The GDB Agent Expression Mechanism</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="PREVIOUS"
TITLE="File-I/O remote protocol extension"
HREF="file-i-o-remote-protocol-extension.html"><LINK
REL="NEXT"
TITLE="Bytecode Descriptions"
HREF="bytecode-descriptions.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="APPENDIX"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Debugging with gdb</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="file-i-o-remote-protocol-extension.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="bytecode-descriptions.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="APPENDIX"
><H1
><A
NAME="AGENT-EXPRESSIONS"
></A
>Appendix E. The GDB Agent Expression Mechanism</H1
><P
>In some applications, it is not feasable for the debugger to interrupt
the program's execution long enough for the developer to learn anything
helpful about its behavior.  If the program's correctness depends on its
real-time behavior, delays introduced by a debugger might cause the
program to fail, even when the code itself is correct.  It is useful to
be able to observe the program's behavior without interrupting it.
  </P
><P
>Using GDB's <TT
CLASS="COMMAND"
>trace</TT
> and <TT
CLASS="COMMAND"
>collect</TT
> commands, the user can
specify locations in the program, and arbitrary expressions to evaluate
when those locations are reached.  Later, using the <TT
CLASS="COMMAND"
>tfind</TT
>
command, she can examine the values those expressions had when the
program hit the trace points.  The expressions may also denote objects
in memory -- structures or arrays, for example -- whose values GDB
should record; while visiting a particular tracepoint, the user may
inspect those objects as if they were in memory at that moment.
However, because GDB records these values without interacting with the
user, it can do so quickly and unobtrusively, hopefully not disturbing
the program's behavior.
  </P
><P
>When GDB is debugging a remote target, the GDB <I
CLASS="FIRSTTERM"
>agent</I
> code running
on the target computes the values of the expressions itself.  To avoid
having a full symbolic expression evaluator on the agent, GDB translates
expressions in the source language into a simpler bytecode language, and
then sends the bytecode to the agent; the agent then executes the
bytecode, and records the values for GDB to retrieve later.
  </P
><P
>The bytecode language is simple; there are forty-odd opcodes, the bulk
of which are the usual vocabulary of C operands (addition, subtraction,
shifts, and so on) and various sizes of literals and memory reference
operations.  The bytecode interpreter operates strictly on machine-level
values -- various sizes of integers and floating point numbers -- and
requires no information about types or symbols; thus, the interpreter's
internal data structures are simple, and each bytecode requires only a
few native machine instructions to implement it.  The interpreter is
small, and strict limits on the memory and time required to evaluate an
expression are easy to determine, making it suitable for use by the
debugging agent in real-time applications.
  </P
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="GENERAL-BYTECODE-DESIGN"
></A
>E.1. General Bytecode Design</H1
><P
>The agent represents bytecode expressions as an array of bytes.  Each
instruction is one byte long (thus the term <I
CLASS="FIRSTTERM"
>bytecode</I
>).  Some
instructions are followed by operand bytes; for example, the <TT
CLASS="COMMAND"
>goto</TT
>
instruction is followed by a destination for the jump.
   </P
><P
>The bytecode interpreter is a stack-based machine; most instructions pop
their operands off the stack, perform some operation, and push the
result back on the stack for the next instruction to consume.  Each
element of the stack may contain either a integer or a floating point
value; these values are as many bits wide as the largest integer that
can be directly manipulated in the source language.  Stack elements
carry no record of their type; bytecode could push a value as an
integer, then pop it as a floating point value.  However, GDB will not
generate code which does this.  In C, one might define the type of a
stack element as follows:
<TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>union agent_val {
  LONGEST l;
  DOUBLEST d;
};</PRE
></TD
></TR
></TABLE
>      </P
><P
>where <TT
CLASS="COMMAND"
>LONGEST</TT
> and <TT
CLASS="COMMAND"
>DOUBLEST</TT
> are <TT
CLASS="COMMAND"
>typedef</TT
> names for
the largest integer and floating point types on the machine.
   </P
><P
>By the time the bytecode interpreter reaches the end of the expression,
the value of the expression should be the only value left on the stack.
For tracing applications, <TT
CLASS="COMMAND"
>trace</TT
> bytecodes in the expression will
have recorded the necessary data, and the value on the stack may be
discarded.  For other applications, like conditional breakpoints, the
value may be useful.
   </P
><P
>Separate from the stack, the interpreter has two registers:
   </P
><P
></P
><DIV
CLASS="VARIABLELIST"
><DL
><DT
><TT
CLASS="COMMAND"
>pc</TT
></DT
><DD
><P
>The address of the next bytecode to execute.
      </P
></DD
><DT
><TT
CLASS="COMMAND"
>start</TT
></DT
><DD
><P
>The address of the start of the bytecode expression, necessary for
interpreting the <TT
CLASS="COMMAND"
>goto</TT
> and <TT
CLASS="COMMAND"
>if_goto</TT
> instructions.
      </P
></DD
></DL
></DIV
><P
>Neither of these registers is directly visible to the bytecode language
itself, but they are useful for defining the meanings of the bytecode
operations.
   </P
><P
>There are no instructions to perform side effects on the running
program, or call the program's functions; we assume that these
expressions are only used for unobtrusive debugging, not for patching
the running code.
   </P
><P
>Most bytecode instructions do not distinguish between the various sizes
of values, and operate on full-width values; the upper bits of the
values are simply ignored, since they do not usually make a difference
to the value computed.  The exceptions to this rule are:
   </P
><P
></P
><DIV
CLASS="VARIABLELIST"
><DL
><DT
>memory reference instructions (<TT
CLASS="COMMAND"
>ref</TT
><TT
CLASS="VARNAME"
>n</TT
>)</DT
><DD
><P
>There are distinct instructions to fetch different word sizes from
memory.  Once on the stack, however, the values are treated as full-size
integers.  They may need to be sign-extended; the <TT
CLASS="COMMAND"
>ext</TT
> instruction
exists for this purpose.
      </P
></DD
><DT
>the sign-extension instruction (<TT
CLASS="COMMAND"
>ext</TT
> <TT
CLASS="VARNAME"
>n</TT
>)</DT
><DD
><P
>These clearly need to know which portion of their operand is to be
extended to occupy the full length of the word.
      </P
></DD
></DL
></DIV
><P
>If the interpreter is unable to evaluate an expression completely for
some reason (a memory location is inaccessible, or a divisor is zero,
for example), we say that interpretation "terminates with an error".
This means that the problem is reported back to the interpreter's caller
in some helpful way.  In general, code using agent expressions should
assume that they may attempt to divide by zero, fetch arbitrary memory
locations, and misbehave in other ways.
   </P
><P
>Even complicated C expressions compile to a few bytecode instructions;
for example, the expression <TT
CLASS="COMMAND"
>x + y * z</TT
> would typically produce
code like the following, assuming that <TT
CLASS="COMMAND"
>x</TT
> and <TT
CLASS="COMMAND"
>y</TT
> live in
registers, and <TT
CLASS="COMMAND"
>z</TT
> is a global variable holding a 32-bit
<TT
CLASS="COMMAND"
>int</TT
>:
<TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>reg 1
reg 2
const32 <I
CLASS="WORDASWORD"
>address of z</I
>
ref32
ext 32
mul
add
end</PRE
></TD
></TR
></TABLE
>      </P
><P
>In detail, these mean:
   </P
><P
></P
><DIV
CLASS="VARIABLELIST"
><DL
><DT
><TT
CLASS="COMMAND"
>reg 1</TT
></DT
><DD
><P
>Push the value of register 1 (presumably holding <TT
CLASS="COMMAND"
>x</TT
>) onto the
stack.
      </P
></DD
><DT
><TT
CLASS="COMMAND"
>reg 2</TT
></DT
><DD
><P
>Push the value of register 2 (holding <TT
CLASS="COMMAND"
>y</TT
>).
      </P
></DD
><DT
><TT
CLASS="COMMAND"
>const32 <TT
CLASS="LITERAL"
>address of z</TT
></TT
></DT
><DD
><P
>Push the address of <TT
CLASS="COMMAND"
>z</TT
> onto the stack.
      </P
></DD
><DT
><TT
CLASS="COMMAND"
>ref32</TT
></DT
><DD
><P
>Fetch a 32-bit word from the address at the top of the stack; replace
the address on the stack with the value.  Thus, we replace the address
of <TT
CLASS="COMMAND"
>z</TT
> with <TT
CLASS="COMMAND"
>z</TT
>'s value.
      </P
></DD
><DT
><TT
CLASS="COMMAND"
>ext 32</TT
></DT
><DD
><P
>Sign-extend the value on the top of the stack from 32 bits to full
length.  This is necessary because <TT
CLASS="COMMAND"
>z</TT
> is a signed integer.
      </P
></DD
><DT
><TT
CLASS="COMMAND"
>mul</TT
></DT
><DD
><P
>Pop the top two numbers on the stack, multiply them, and push their
product.  Now the top of the stack contains the value of the expression
<TT
CLASS="COMMAND"
>y * z</TT
>.
      </P
></DD
><DT
><TT
CLASS="COMMAND"
>add</TT
></DT
><DD
><P
>Pop the top two numbers, add them, and push the sum.  Now the top of the
stack contains the value of <TT
CLASS="COMMAND"
>x + y * z</TT
>.
      </P
></DD
><DT
><TT
CLASS="COMMAND"
>end</TT
></DT
><DD
><P
>Stop executing; the value left on the stack top is the value to be
recorded.
      </P
></DD
></DL
></DIV
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="file-i-o-remote-protocol-extension.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="bytecode-descriptions.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>File-I/O remote protocol extension</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
>&nbsp;</TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Bytecode Descriptions</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>