<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Type and range checking</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Using gdb with Different Languages"
HREF="languages.html"><LINK
REL="PREVIOUS"
TITLE="Displaying the language"
HREF="show.html"><LINK
REL="NEXT"
TITLE="Supported languages"
HREF="support.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Debugging with gdb</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="show.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 14. Using gdb with Different Languages</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="support.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="CHECKS"
></A
>14.3. Type and range checking</H1
><A
NAME="AEN6597"
></A
><BLOCKQUOTE
CLASS="BLOCKQUOTE"
><P
><I
CLASS="EMPHASIS"
>Warning:</I
> In this release, the gdb commands for type and range
checking are included, but they do not yet have any effect.  This
section documents the intended facilities.
    </P
></BLOCKQUOTE
><P
>Some languages are designed to guard you against making seemingly common
errors through a series of compile- and run-time checks.  These include
checking the type of arguments to functions and operators, and making
sure mathematical overflows are caught at run time.  Checks such as
these help to ensure a program's correctness once it has been compiled
by eliminating type mismatches, and providing active checks for range
errors when your program is running.
   </P
><P
>gdb can check for conditions like the above if you wish.
Although gdb does not check the statements in your program, it
can check expressions entered directly into gdb for evaluation via
the <TT
CLASS="COMMAND"
>print</TT
> command, for example.  As with the working language,
gdb can also decide whether or not to check automatically based on
your program's source language. 
Refer to <A
HREF="support.html"
>Section 14.4 <I
>Supported languages</I
></A
>,
for the default settings of supported languages.
   </P
><P
>&#13;
   </P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="TYPE-CHECKING"
></A
>14.3.1. An overview of type checking</H2
><P
>Some languages, such as Modula-2, are strongly typed, meaning that the
arguments to operators and functions have to be of the correct type,
otherwise an error occurs.  These checks prevent type mismatch
errors from ever causing any run-time problems.  For example,
    </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>1 + 2 =&#62; 3
but     error--&#62; 1 + 2.3</PRE
></TD
></TR
></TABLE
>
       </P
><P
>The second example fails because the <TT
CLASS="COMMAND"
>CARDINAL</TT
> 1 is not
type-compatible with the <TT
CLASS="COMMAND"
>REAL</TT
> 2.3.
    </P
><P
>For the expressions you use in gdb commands, you can tell the
gdb type checker to skip checking;
to treat any mismatches as errors and abandon the expression;
or to only issue warnings when type mismatches occur,
but evaluate the expression anyway.  When you choose the last of
these, gdb evaluates expressions like the second example above, but
also issues a warning.
    </P
><P
>Even if you turn type checking off, there may be other reasons
related to type that prevent gdb from evaluating an expression.
For instance, gdb does not know how to add an <TT
CLASS="COMMAND"
>int</TT
> and
a <TT
CLASS="COMMAND"
>struct foo</TT
>.  These particular type errors have nothing to do
with the language in use, and usually arise from expressions, such as
the one described above, which make little sense to evaluate anyway.
    </P
><P
>Each language defines to what degree it is strict about type.  For
instance, both Modula-2 and C require the arguments to arithmetical
operators to be numbers.  In C, enumerated types and pointers can be
represented as numbers, so that they are valid arguments to mathematical
operators. 
Refer to <A
HREF="support.html"
>Section 14.4 <I
>Supported languages</I
></A
>, for further
details on specific languages.
    </P
><P
>gdb provides some additional commands for controlling the type checker:
    </P
><P
>&#13;    </P
><P
></P
><DIV
CLASS="VARIABLELIST"
><DL
><DT
><TT
CLASS="COMMAND"
>set check type auto</TT
></DT
><DD
><P
>Set type checking on or off based on the current working language. Refer to <A
HREF="support.html"
>Section 14.4 <I
>Supported languages</I
></A
>, for the default settings for
each language.
       </P
></DD
><DT
><TT
CLASS="COMMAND"
>set check type on</TT
>, <TT
CLASS="COMMAND"
>set check type off</TT
></DT
><DD
><P
>Set type checking on or off, overriding the default setting for the
current working language.  Issue a warning if the setting does not
match the language default.  If any type mismatches occur in
evaluating an expression while type checking is on, gdb prints a
message and aborts evaluation of the expression.
       </P
></DD
><DT
><TT
CLASS="COMMAND"
>set check type warn</TT
></DT
><DD
><P
>Cause the type checker to issue warnings, but to always attempt to
evaluate the expression.  Evaluating the expression may still
be impossible for other reasons.  For example, gdb cannot add
numbers and structures.
       </P
></DD
><DT
><TT
CLASS="COMMAND"
>show type</TT
></DT
><DD
><P
>Show the current setting of the type checker, and whether or not gdb
is setting it automatically.
       </P
></DD
></DL
></DIV
><P
>&#13;
    </P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="RANGE-CHECKING"
></A
>14.3.2. An overview of range checking</H2
><P
>In some languages (such as Modula-2), it is an error to exceed the
bounds of a type; this is enforced with run-time checks.  Such range
checking is meant to ensure program correctness by making sure
computations do not overflow, or indices on an array element access do
not exceed the bounds of the array.
    </P
><P
>For expressions you use in gdb commands, you can tell
gdb to treat range errors in one of three ways: ignore them,
always treat them as errors and abandon the expression, or issue
warnings but evaluate the expression anyway.
    </P
><P
>A range error can result from numerical overflow, from exceeding an
array index bound, or when you type a constant that is not a member
of any type.  Some languages, however, do not treat overflows as an
error.  In many implementations of C, mathematical overflow causes the
result to "wrap around" to lower values--for example, if <TT
CLASS="VARNAME"
>m</TT
> is
the largest integer value, and <TT
CLASS="VARNAME"
>s</TT
> is the smallest, then
    </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
><TT
CLASS="VARNAME"
>m</TT
> + 1 =&#62; <TT
CLASS="VARNAME"
>s</TT
></PRE
></TD
></TR
></TABLE
>               </P
><P
>This, too, is specific to individual languages, and in some cases
specific to individual compilers or machines. 
Refer to <A
HREF="support.html"
>Section 14.4 <I
>Supported languages</I
></A
>, for further details on specific languages.
    </P
><P
>gdb provides some additional commands for controlling the range checker:
    </P
><P
>&#13;    </P
><P
></P
><DIV
CLASS="VARIABLELIST"
><DL
><DT
><TT
CLASS="COMMAND"
>set check range auto</TT
></DT
><DD
><P
>Set range checking on or off based on the current working language. Refer to <A
HREF="support.html"
>Section 14.4 <I
>Supported languages</I
></A
>, for the default settings for
each language.
       </P
></DD
><DT
><TT
CLASS="COMMAND"
>set check range on</TT
>, <TT
CLASS="COMMAND"
>set check range off</TT
></DT
><DD
><P
>Set range checking on or off, overriding the default setting for the
current working language.  A warning is issued if the setting does not
match the language default.  If a range error occurs and range checking is on,
then a message is printed and evaluation of the expression is aborted.
       </P
></DD
><DT
><TT
CLASS="COMMAND"
>set check range warn</TT
></DT
><DD
><P
>Output messages when the gdb range checker detects a range error,
but attempt to evaluate the expression anyway.  Evaluating the
expression may still be impossible for other reasons, such as accessing
memory that the process does not own (a typical example from many Unix
systems).
       </P
></DD
><DT
><TT
CLASS="COMMAND"
>show range</TT
></DT
><DD
><P
>Show the current setting of the range checker, and whether or not it is
being set automatically by gdb.
       </P
></DD
></DL
></DIV
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="show.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="support.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Displaying the language</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="languages.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Supported languages</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>