<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Registers</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Examining Data"
HREF="data.html"><LINK
REL="PREVIOUS"
TITLE="Convenience variables"
HREF="convenience-vars.html"><LINK
REL="NEXT"
TITLE="Floating point hardware"
HREF="floating-point-hardware.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Debugging with gdb</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="convenience-vars.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 10. Examining Data</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="floating-point-hardware.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="REGISTERS"
></A
>10.10. Registers</H1
><P
>You can refer to machine register contents, in expressions, as variables
with names starting with <TT
CLASS="COMMAND"
>$</TT
>.  The names of registers are different
for each machine; use <TT
CLASS="COMMAND"
>info registers</TT
> to see the names used on
your machine.
   </P
><P
></P
><DIV
CLASS="VARIABLELIST"
><P
><B
></B
></P
><DL
><DT
><TT
CLASS="COMMAND"
>info registers</TT
></DT
><DD
><P
>Print the names and values of all registers except floating-point
and vector registers (in the selected stack frame).
      </P
><P
>      </P
></DD
><DT
><TT
CLASS="COMMAND"
>info all-registers</TT
></DT
><DD
><P
>Print the names and values of all registers, including floating-point
and vector registers (in the selected stack frame).
      </P
></DD
><DT
><TT
CLASS="COMMAND"
>info registers <TT
CLASS="VARNAME"
>regname</TT
> &#8230;</TT
></DT
><DD
><P
>Print the <I
CLASS="FIRSTTERM"
>relativized</I
> value of each specified register <TT
CLASS="VARNAME"
>regname</TT
>.
As discussed in detail below, register values are normally relative to
the selected stack frame.  <TT
CLASS="VARNAME"
>regname</TT
> may be any register name valid on
the machine you are using, with or without the initial <TT
CLASS="COMMAND"
>$</TT
>.
      </P
></DD
></DL
></DIV
><P
>gdb has four "standard" register names that are available (in
expressions) on most machines--whenever they do not conflict with an
architecture's canonical mnemonics for registers.  The register names
<TT
CLASS="COMMAND"
>$pc</TT
> and <TT
CLASS="COMMAND"
>$sp</TT
> are used for the program counter register and
the stack pointer.  <TT
CLASS="COMMAND"
>$fp</TT
> is used for a register that contains a
pointer to the current stack frame, and <TT
CLASS="COMMAND"
>$ps</TT
> is used for a
register that contains the processor status.  For example,
you could print the program counter in hex with
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>p/x $pc</PRE
></TD
></TR
></TABLE
>      </P
><P
>or print the instruction to be executed next with
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>x/i $pc</PRE
></TD
></TR
></TABLE
>   </P
><P
>or add four to the stack pointer<A
NAME="AEN5070"
HREF="#FTN.AEN5070"
>[1]</A
> with
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>set $sp += 4</PRE
></TD
></TR
></TABLE
>   </P
><P
>Whenever possible, these four standard register names are available on
your machine even though the machine has different canonical mnemonics,
so long as there is no conflict.  The <TT
CLASS="COMMAND"
>info registers</TT
> command
shows the canonical names.  For example, on the SPARC, <TT
CLASS="COMMAND"
>info
registers</TT
> displays the processor status register as <TT
CLASS="COMMAND"
>$psr</TT
> but you
can also refer to it as <TT
CLASS="COMMAND"
>$ps</TT
>; and on x86-based machines <TT
CLASS="COMMAND"
>$ps</TT
>
is an alias for the eflags register.
   </P
><P
>gdb always considers the contents of an ordinary register as an
integer when the register is examined in this way.  Some machines have
special registers which can hold nothing but floating point; these
registers are considered to have floating point values.  There is no way
to refer to the contents of an ordinary register as floating point value
(although you can <I
CLASS="EMPHASIS"
>print</I
> it as a floating point value with
<TT
CLASS="COMMAND"
>print/f $<TT
CLASS="VARNAME"
>regname</TT
></TT
>).
   </P
><P
>Some registers have distinct "raw" and "virtual" data formats.  This
means that the data format in which the register contents are saved by
the operating system is not the same one that your program normally
sees.  For example, the registers of the 68881 floating point
coprocessor are always saved in "extended" (raw) format, but all C
programs expect to work with "double" (virtual) format.  In such
cases, gdb normally works with the virtual format only (the format
that makes sense for your program), but the <TT
CLASS="COMMAND"
>info registers</TT
> command
prints the data in both formats.
   </P
><P
>Normally, register values are relative to the selected stack frame
(refer to <A
HREF="selection.html"
>Section 8.3 <I
>Selecting a frame</I
></A
>).  This means that you get the
value that the register would contain if all stack frames farther in
were exited and their saved registers restored.  In order to see the
true contents of hardware registers, you must select the innermost
frame (with <TT
CLASS="COMMAND"
>frame 0</TT
>).
   </P
><P
>However, gdb must deduce where registers are saved, from the machine
code generated by your compiler.  If some registers are not saved, or if
gdb is unable to locate the saved registers, the selected stack
frame makes no difference.
   </P
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN5070"
HREF="registers.html#AEN5070"
>[1]</A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>This is a way of removing
one word from the stack, on machines where stacks grow downward in
memory (most machines, nowadays).  This assumes that the innermost
stack frame is selected; setting <TT
CLASS="COMMAND"
>$sp</TT
> is not allowed when other
stack frames are selected.  To pop entire frames off the stack,
regardless of machine architecture, use <TT
CLASS="COMMAND"
>return</TT
>;
(refer to <A
HREF="returning.html"
>Section 16.4 <I
>Returning from a function</I
></A
>.</P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="convenience-vars.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="floating-point-hardware.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Convenience variables</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="data.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Floating point hardware</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>