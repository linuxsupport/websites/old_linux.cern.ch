<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Program variables</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Examining Data"
HREF="data.html"><LINK
REL="PREVIOUS"
TITLE="Examining Data"
HREF="data.html"><LINK
REL="NEXT"
TITLE="Artificial arrays"
HREF="arrays.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Debugging with gdb</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="data.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 10. Examining Data</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="arrays.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="VARIABLES"
></A
>10.2. Program variables</H1
><P
>The most common kind of expression to use is the name of a variable
in your program.
   </P
><P
>Variables in expressions are understood in the selected stack frame
(refer to <A
HREF="selection.html"
>Section 8.3 <I
>Selecting a frame</I
></A
>); they must be either:
   </P
><P
></P
><UL
><LI
STYLE="list-style-type: disc"
><P
>global (or file-static)
     </P
></LI
></UL
><P
>or
   </P
><P
></P
><UL
><LI
STYLE="list-style-type: disc"
><P
>visible according to the scope rules of the
programming language from the point of execution in that frame
     </P
></LI
></UL
><P
>This means that in the function
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>foo (a)
     int a;
{
  bar (a);
  {
    int b = test ();
    bar (b);
  }
}</PRE
></TD
></TR
></TABLE
>   </P
><P
>you can examine and use the variable <TT
CLASS="COMMAND"
>a</TT
> whenever your program is
executing within the function <TT
CLASS="COMMAND"
>foo</TT
>, but you can only use or
examine the variable <TT
CLASS="COMMAND"
>b</TT
> while your program is executing inside
the block where <TT
CLASS="COMMAND"
>b</TT
> is declared.
   </P
><P
>There is an exception: you can refer to a variable or function whose
scope is a single source file even if the current execution point is not
in this file.  But it is possible to have more than one such variable or
function with the same name (in different source files).  If that
happens, referring to that name has unpredictable effects.  If you wish,
you can specify a static variable in a particular function or file,
using the colon-colon notation:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
><TT
CLASS="VARNAME"
>file</TT
>::<TT
CLASS="VARNAME"
>variable</TT
>
<TT
CLASS="VARNAME"
>function</TT
>::<TT
CLASS="VARNAME"
>variable</TT
></PRE
></TD
></TR
></TABLE
>   </P
><P
>Here <TT
CLASS="VARNAME"
>file</TT
> or <TT
CLASS="VARNAME"
>function</TT
> is the name of the context for the
static <TT
CLASS="VARNAME"
>variable</TT
>.  In the case of file names, you can use quotes to
make sure gdb parses the file name as a single word--for example,
to print a global value of <TT
CLASS="COMMAND"
>x</TT
> defined in <TT
CLASS="COMMAND"
>f2.c</TT
>:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>(gdb) p 'f2.c'::x</PRE
></TD
></TR
></TABLE
>       </P
><P
>This use of <TT
CLASS="COMMAND"
>::</TT
> is very rarely in conflict with the very similar
use of the same notation in C<I
CLASS="WORDASWORD"
>++</I
>.  gdb also supports use of the C<I
CLASS="WORDASWORD"
>++</I
>
scope resolution operator in gdb expressions.
   </P
><P
>   </P
><A
NAME="AEN4043"
></A
><BLOCKQUOTE
CLASS="BLOCKQUOTE"
><P
><I
CLASS="EMPHASIS"
>Warning:</I
> Occasionally, a local variable may appear to have the
wrong value at certain points in a function--just after entry to a new
scope, and just before exit.
    </P
></BLOCKQUOTE
><P
>You may see this problem when you are stepping by machine instructions.
This is because, on most machines, it takes more than one instruction to
set up a stack frame (including local variable definitions); if you are
stepping by machine instructions, variables may appear to have the wrong
values until the stack frame is completely built.  On exit, it usually
also takes more than one machine instruction to destroy a stack frame;
after you begin stepping through that group of instructions, local
variable definitions may be gone.
   </P
><P
>This may also happen when the compiler does significant optimizations.
To be sure of always seeing accurate values, turn off all optimization
when compiling.
   </P
><P
>Another possible effect of compiler optimizations is to optimize
unused variables out of existence, or assign variables to registers (as
opposed to memory addresses).  Depending on the support for such cases
offered by the debug info format used by the compiler, gdb
might not be able to display values for such local variables.  If that
happens, gdb will print a message like this:
   </P
><P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>No symbol "foo" in current context.</PRE
></TD
></TR
></TABLE
>      </P
><P
>To solve such problems, either recompile without optimizations, or use a
different debug info format, if the compiler supports several such
formats.  For example, gcc, the gnu C/C<I
CLASS="WORDASWORD"
>++</I
> compiler
usually supports the <TT
CLASS="COMMAND"
>-gstabs+</TT
> option.  <TT
CLASS="COMMAND"
>-gstabs+</TT
>
produces debug info in a format that is superior to formats such as
COFF.  You may be able to use DWARF 2 (<TT
CLASS="COMMAND"
>-gdwarf-2</TT
>), which is also
an effective form for debug info.  .
   </P
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="data.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="arrays.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Examining Data</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="data.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Artificial arrays</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>