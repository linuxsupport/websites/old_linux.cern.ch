<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Bandwidth and Processing Power</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="PREVIOUS"
TITLE="Additional Resources"
HREF="s1-resource-addres.html"><LINK
REL="NEXT"
TITLE="Processing Power"
HREF="s1-bandwidth-processing.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="CHAPTER"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Introduction to System Administration</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-resource-addres.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-bandwidth-processing.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="CHAPTER"
><H1
><A
NAME="CH-BANDWIDTH"
></A
>Chapter 3. Bandwidth and Processing Power</H1
><P
>Of the two resources discussed in this chapter, one (bandwidth) is
      often hard for the new system administrator to understand, while the other
      (processing power) is usually a much easier concept to grasp.</P
><P
>Additionally, it may seem that these two resources are not that
      closely related &#8212; why group them together?</P
><P
>The reason for addressing both resources together is that these
      resources are based on the hardware that tie directly into a computer's
      ability to move and process data.  As such, their relationship is often
      interrelated.</P
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-BANDWIDTH-BANDWIDTH"
>3.1. Bandwidth</A
></H1
><P
>At its most basic, bandwidth is the capacity for data transfer
      &#8212; in other words, how much data can be moved from one point to
      another in a given amount of time.  Having point-to-point data
      communication implies two things:</P
><P
></P
><UL
><LI
><P
>A set of electrical conductors used to make low-level
            communication possible</P
></LI
><LI
><P
>A protocol to facilitate the efficient and reliable
            communication of data</P
></LI
></UL
><P
>There are two types of system components that meet these
        requirements:</P
><P
></P
><UL
><LI
><P
>Buses</P
></LI
><LI
><P
>Datapaths</P
></LI
></UL
><P
>The following sections explore each in more detail.</P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-BANDWIDTH-BUSES"
>3.1.1. Buses</A
></H2
><P
>As stated above, buses enable point-to-point communication and use
          some sort of protocol to ensure that all communication takes place in
          a controlled manner.  However, buses have other distinguishing
          features:</P
><P
></P
><UL
><LI
><P
>Standardized electrical characteristics (such as the number of
              conductors, voltage levels, signaling speeds, etc.)</P
></LI
><LI
><P
>Standardized mechanical characteristics (such as the type of
              connector, card size, physical layout, etc.)</P
></LI
><LI
><P
>Standardized protocol</P
></LI
></UL
><P
>The word "standardized" is important because buses are the primary
          way in which different system components are connected
          together.</P
><P
>In many cases, buses allow the interconnection of hardware made by
          multiple manufacturers; without standardization, this would not be
          possible.  However, even in situations where a bus is proprietary to
          one manufacturer, standardization is important because it allows that
          manufacturer to more easily implement different components by using a
          common interface &#8212; the bus itself.</P
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S2-BANDWIDTH-BUSES-EXAMPLES"
>3.1.1.1. Examples of Buses</A
></H3
><P
>No matter where in a computer system you look, there are buses.
            Here are a few of the more common ones:</P
><P
></P
><UL
><LI
><P
>Mass storage buses (ATA and SCSI)</P
></LI
><LI
><P
>Networks<A
NAME="AEN1990"
HREF="#FTN.AEN1990"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
>
                    (Ethernet and Token Ring)</P
></LI
><LI
><P
>Memory buses (PC133 and <SPAN
CLASS="TRADEMARK"
>Rambus</SPAN
>&reg;)</P
></LI
><LI
><P
>Expansion buses (PCI, ISA, USB)</P
></LI
></UL
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-BANDWIDTH-DATAPATHS"
>3.1.2. Datapaths</A
></H2
><P
>Datapaths can be harder to identify but, like buses, they are
          everywhere.  Also like buses, datapaths enable point-to-point
          communication.  However, unlike buses, datapaths:</P
><P
></P
><UL
><LI
><P
>Use a simpler protocol (if any)</P
></LI
><LI
><P
>Have little (if any) mechanical standardization</P
></LI
></UL
><P
>The reason for these differences is that datapaths are normally
          internal to some system component and are not used to facilitate the
          ad-hoc interconnection of different components.  As such, datapaths
          are highly optimized for a particular situation, where speed and low
          cost are preferred over slower and more expensive general-purpose
          flexibility.</P
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-BANDWIDTH-DATAPATH-EXAMPLES"
>3.1.2.1. Examples of Datapaths</A
></H3
><P
>Here are some typical datapaths:</P
><P
></P
><UL
><LI
><P
>CPU to on-chip cache datapath</P
></LI
><LI
><P
>Graphics processor to video memory datapath</P
></LI
></UL
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-BANDWIDTH-PROBLEMS"
>3.1.3. Potential Bandwidth-Related Problems</A
></H2
><P
>There are two ways in which bandwidth-related problems may occur
          (for either buses or datapaths):</P
><P
></P
><OL
TYPE="1"
><LI
><P
>The bus or datapath may represent a shared resource.  In this
              situation, high levels of contention for the bus reduces the
              effective bandwidth available for all devices on the bus.</P
><P
>A SCSI bus with several highly-active disk drives would be a
              good example of this.  The highly-active disk drives saturate the
              SCSI bus, leaving little bandwidth available for any other device
              on the same bus.  The end result is that all I/O to any of the
              devices on this bus is slow, even if each device on the bus is not
              overly active.</P
></LI
><LI
><P
>The bus or datapath may be a dedicated resource with a fixed
              number of devices attached to it.  In this case, the electrical
              characteristics of the bus (and to some extent the nature of the
              protocol being used) limit the available bandwidth.  This is
              usually more the case with datapaths than with buses.  This is one
              reason why graphics adapters tend to perform more slowly when
              operating at higher resolutions and/or color depths &#8212; for
              every screen refresh, there is more data that must be passed along
              the datapath connecting video memory and the graphics
              processor.</P
></LI
></OL
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-BANDWITH-SOLUTIONS"
>3.1.4. Potential Bandwidth-Related Solutions</A
></H2
><P
>Fortunately, bandwidth-related problems can be addressed.  In
          fact, there are several approaches you can take:</P
><P
></P
><UL
><LI
><P
>Spread the load</P
></LI
><LI
><P
>Reduce the load</P
></LI
><LI
><P
>Increase the capacity</P
></LI
></UL
><P
>The following sections explore each approach in more
          detail.</P
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S2-BANDWIDTH-SOLUTIONS-SPREAD"
>3.1.4.1. Spread the Load</A
></H3
><P
>The first approach is to more evenly distribute the bus
            activity.  In other words, if one bus is overloaded and another is
            idle, perhaps the situation would be improved by moving some of the
            load to the idle bus.</P
><P
>As a system administrator, this is the first approach you should
            consider, as often there are additional buses already present in
            your system.  For example, most PCs include at least two ATA
            <I
CLASS="FIRSTTERM"
>channels</I
> (which is just another name for a
            bus).  If you have two ATA disk drives and two ATA channels, why
            should both drives be on the same channel?</P
><P
>Even if your system configuration does not include additional
            buses, spreading the load might still be a reasonable approach.  The
            hardware expenditures to do so would be less expensive than
            replacing an existing bus with higher-capacity hardware.</P
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S2-BANDWIDTH-SOLUTIONS-REDUCE"
>3.1.4.2. Reduce the Load</A
></H3
><P
>At first glance, reducing the load and spreading the load appear
            to be different sides of the same coin.  After all, when one spreads
            the load, it acts to reduce the load (at least on the overloaded
            bus), correct?</P
><P
>While this viewpoint is correct, it is not the same as reducing
            the load <I
CLASS="EMPHASIS"
>globally</I
>.  The key here is to
            determine if there is some aspect of the system load that is causing
            this particular bus to be overloaded.  For example, is a network
            heavily loaded due to activities that are unnecessary?  Perhaps a
            small temporary file is the recipient of heavy read/write I/O.  If
            that temporary file resides on a networked file server, a great deal
            of network traffic could be eliminated by working with the file
            locally.</P
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S2-BANDWIDTH-SOLUTIONS-INCREASE"
>3.1.4.3. Increase the Capacity</A
></H3
><P
>The obvious solution to insufficient bandwidth is to increase it
            somehow.  However, this is usually an expensive proposition.
            Consider, for example, a SCSI controller and its overloaded bus.  To
            increase its bandwidth, the SCSI controller (and likely all devices
            attached to it) would need to be replaced with faster hardware.  If
            the SCSI controller is a separate card, this would be a relatively
            straightforward process, but if the SCSI controller is part of the
            system's motherboard, it becomes much more difficult to justify the
            economics of such a change.</P
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-BANDWIDTH-SUMMARY"
>3.1.5. In Summary&#8230;</A
></H2
><P
>All system administrators should be aware of bandwidth, and how
          system configuration and usage impacts available
          bandwidth.  Unfortunately, it is not always apparent what is a
          bandwidth-related problem and what is not.  Sometimes, the problem is
          not the bus itself, but one of the components attached to the bus.</P
><P
>For example, consider a SCSI adapter that is connected to a PCI
          bus.  If there are performance problems with SCSI disk I/O, it might
          be the result of a poorly-performing SCSI adapter, even though the
          SCSI and PCI buses themselves are nowhere near their bandwidth
          capabilities.</P
></DIV
></DIV
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN1990"
HREF="ch-bandwidth.html#AEN1990"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>Instead of an intra-system bus,
                    networks can be thought of as an
                    <I
CLASS="EMPHASIS"
>inter</I
>-system bus.</P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-resource-addres.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-bandwidth-processing.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Additional Resources</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
>&nbsp;</TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Processing Power</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>