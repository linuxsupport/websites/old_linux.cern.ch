<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Managing Storage</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="PREVIOUS"
TITLE="Additional Resources"
HREF="s1-memory-addres.html"><LINK
REL="NEXT"
TITLE="Storage Addressing Concepts"
HREF="s1-storage-data-addr.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="CHAPTER"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Introduction to System Administration</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-memory-addres.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-storage-data-addr.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="CHAPTER"
><H1
><A
NAME="CH-STORAGE"
></A
>Chapter 5. Managing Storage</H1
><P
>If there is one thing that takes up the majority of a system
      administrator's day, it would have to be storage management.  It seems
      that disks are always running out of free space, becoming overloaded with
      too much I/O activity, or failing unexpectedly.  Therefore, it is vital to
      have a solid working knowledge of disk storage in order to be a successful
      system administrator.</P
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-STORAGE-HARDWARE"
>5.1. An Overview of Storage Hardware</A
></H1
><P
>Before managing storage, it is first necessary to understand the
        hardware on which data is stored.  Unless you have at least some
        knowledge about mass storage device operation, you may find yourself in
        a situation where you have a storage-related problem, but you lack the
        background knowledge necessary to interpret what you are seeing.  By
        gaining some insight into how the underlying hardware operates, you
        should be able to more easily determine whether your computer's storage
        subsystem is operating properly.</P
><P
>The vast majority of all mass-storage devices use some sort of
        rotating media and supports the random access of data on that media.
        This means that the following components are present in some form within
        nearly every mass storage device:</P
><P
></P
><UL
><LI
><P
>Disk platters</P
></LI
><LI
><P
>Data reading/writing device</P
></LI
><LI
><P
>Access arms</P
></LI
></UL
><P
>The following sections explore each of these components in more
        detail.</P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-STORAGE-HARDWARE-PLATTERS"
>5.1.1. Disk Platters</A
></H2
><P
>The rotating media used by nearly all mass storage devices are in
          the form of one or more flat, circularly-shaped platters.  The platter
          may be composed of any number of different materials, such aluminum,
          glass, and polycarbonate.</P
><P
>The surface of each platter is treated in such a way as to enable
          data storage.  The exact nature of the treatment depends on the data
          storage technology to be used.  The most common data storage
          technology is based on the property of magnetism; in these cases the
          platters are covered with a compound that exhibits good magnetic
          characteristics.</P
><P
>Another common data storage technology is based on optical
          principles; in these cases, the platters are covered with materials
          whose optical properties can be modified, thereby allowing data to be
          stored optically<A
NAME="AEN3100"
HREF="#FTN.AEN3100"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
>.</P
><P
>No matter what data storage technology is in use, the disk
          platters are spun, causing their entire surface to sweep past another
          component &#8212; the data reading/writing device.</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-STORAGE-HARDWARE-HEADS"
>5.1.2. Data reading/writing device</A
></H2
><P
>The data reading/writing device is the component that takes the
          bits and bytes on which a computer system operates and turns them into
          the magnetic or optical variations necessary to interact with the
          materials coating the surface of the disk platters.</P
><P
>Sometimes the conditions under which these devices must operate
          are challenging.  For instance, in magnetically-based mass storage the
          read/write devices (known as <I
CLASS="FIRSTTERM"
>heads</I
>) must be
          very close to the surface of the platter.  However, if the head and
          the surface of the disk platter were to touch, the resulting friction
          would do severe damage to both the head and the platter.  Therefore,
          the surfaces of both the head and the platter are carefully polished,
          and the head uses air pressure developed by the spinning platters to
          float over the platter's surface, "flying" at an altitude less than
          the thickness of a human hair.  This is why magnetic disk drives are
          sensitive to shock, sudden temperature changes, and any airborne
          contamination.</P
><P
>The challenges faced by optical heads are somewhat different than
          for magnetic heads &#8212; here, the head assembly must remain at a
          relatively constant distance from the surface of the platter.
          Otherwise, the lenses used to focus on the platter does not produce a
          sufficiently sharp image.</P
><P
>In either case, the heads use a very small amount of the platter's
          surface area for data storage.  As the platter spins below the heads,
          this surface area takes the form of a very thin circular line.</P
><P
>If this was how mass storage devices worked, it would mean that
          over 99% of the platter's surface area would be wasted.  Additional
          heads could be mounted over the platter, but to fully utilize the
          platter's surface area more than a thousand heads would be necessary.
          What is required is some method of moving the head over the surface of
          the platter.</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-STORAGE-HARDWARE-ARMS"
>5.1.3. Access Arms</A
></H2
><P
>By using a head attached to an arm that is capable of sweeping
          over the platter's entire surface, it is possible to fully utilize the
          platter for data storage.  However, the access arm must be capable of
          two things:</P
><P
></P
><UL
><LI
><P
>Moving very quickly</P
></LI
><LI
><P
>Moving very precisely</P
></LI
></UL
><P
>The access arm must move as quickly as possible, because the time
          spent moving the head from one position to another is wasted time.
          That is because no data can be read or written until the access arm
          stops moving<A
NAME="AEN3128"
HREF="#FTN.AEN3128"
><SPAN
CLASS="footnote"
>[2]</SPAN
></A
>.</P
><P
>The access arm must be able to move with great precision because,
          as stated earlier, the surface area used by the heads is very small.
          Therefore, to efficiently use the platter's storage capacity, it is
          necessary to move the heads only enough to ensure that any data
          written in the new position does not overwrite data written at a
          previous position.  This has the affect of conceptually dividing the
          platter's surface into a thousand or more concentric "rings" or
          <I
CLASS="FIRSTTERM"
>tracks</I
>.  Movement of the access arm from one
          track to another is often referred to as
          <I
CLASS="FIRSTTERM"
>seeking</I
>, and the time it takes the access arms
          to move from one track to another is known as the <I
CLASS="FIRSTTERM"
>seek
          time</I
>.</P
><P
>Where there are multiple platters (or one platter with both
          surfaces used for data storage), the arms for each surface are
          stacked, allowing the same track on each surface to be accessed
          simultaneously.  If the tracks for each surface could be visualized
          with the access stationary over a given track, they would appear to be
          stacked one on top of another, making up a cylindrical shape;
          therefore, the set of tracks accessible at a certain position of the
          access arms are known as a <I
CLASS="FIRSTTERM"
>cylinder</I
>.</P
></DIV
></DIV
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN3100"
HREF="ch-storage.html#AEN3100"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>Some optical devices &#8212; notably
          CD-ROM drives &#8212; use somewhat different approaches to data
          storage; these differences are pointed out at the appropriate points
          within the chapter.</P
></TD
></TR
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN3128"
HREF="ch-storage.html#AEN3128"
><SPAN
CLASS="footnote"
>[2]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>In some optical devices (such as CD-ROM
          drives), the access arm is continually moving, causing the head
          assembly to describe a spiral path over the surface of the platter.
          This is a fundamental difference in how the storage medium is used and
          reflects the CD-ROM's origins as a medium for music storage, where
          continuous data retrieval is a more common operation than searching
          for a specific data point.</P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-memory-addres.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-storage-data-addr.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Additional Resources</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
>&nbsp;</TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Storage Addressing Concepts</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>