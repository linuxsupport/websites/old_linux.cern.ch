<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Managing User Resources</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Managing User Accounts and Resource Access"
HREF="ch-acctsgrps.html"><LINK
REL="PREVIOUS"
TITLE="Managing User Accounts and Resource Access"
HREF="ch-acctsgrps.html"><LINK
REL="NEXT"
TITLE="Red Hat Enterprise Linux-Specific Information"
HREF="s1-acctsgrps-rhlspec.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Introduction to System Administration</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="ch-acctsgrps.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 6. Managing User Accounts and Resource Access</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-acctsgrps-rhlspec.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-ACCTSGRPS-RES"
>6.2. Managing User Resources</A
></H1
><P
>Creating user accounts is only part of a system administrator's
        job. Management of user resources is also essential. Therefore, three
        points must be considered:</P
><P
></P
><UL
><LI
><P
>Who can access shared data</P
></LI
><LI
><P
>Where users access this data</P
></LI
><LI
><P
>What barriers are in place to prevent abuse of resources</P
></LI
></UL
><P
>The following sections briefly review each of these topics.</P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-ACCTSGRPS-REC-WHO"
>6.2.1. Who Can Access Shared Data</A
></H2
><P
>A user's access to a given application, file, or directory is
          determined by the permissions applied to that application, file, or
          directory.</P
><P
>In addition, it is often helpful if different permissions can be
          applied to different classes of users.  For example, shared temporary
          storage should be capable of preventing the accidental (or malicious)
          deletions of a user's files by all other users, while still permitting
          the file's owner full access.</P
><P
>Another example is the access assigned to a user's home directory.
          Only the owner of the home directory should be able to create or view
          files there.  Other users should be denied all access (unless the user
          wishes otherwise). This increases user privacy and prevents possible
          misappropriation of personal files.</P
><P
>But there are many situations where multiple users may need access
          to the same resources on a machine. In this case, careful creation of
          shared groups may be necessary.</P
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-ACCTSGRPS-REC-WHO-GROUPS"
>6.2.1.1. Shared Groups and Data</A
></H3
><P
>As mentioned in the introduction, groups are logical constructs
            that can be used to cluster user accounts together for a specific
            purpose.</P
><P
>When managing users within an organization, it is wise to
            identify what data should be accessed by certain departments, what
            data should be denied to others, and what data should be shared by
            all.  Determining this aids in the creation of an appropriate group
            structure, along with permissions appropriate for the shared
            data.</P
><P
>For instance, assume that that the accounts receivable
            department must maintain a list of accounts that are delinquent on
            their payments.  They must also share that list with the collections
            department.  If both accounts receivable and collections personnel
            are made members of a group called
            <SAMP
CLASS="COMPUTEROUTPUT"
>accounts</SAMP
>, this information can then
            be placed in a shared directory (owned by the
            <SAMP
CLASS="COMPUTEROUTPUT"
>accounts</SAMP
> group) with group read and
            write permissions on the directory.</P
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-ACCTSGRPS-REC-WHO-STRUCT"
>6.2.1.2. Determining Group Structure</A
></H3
><P
>Some of the challenges facing system administrators when
            creating shared groups are:</P
><P
></P
><UL
><LI
><P
>What groups to create</P
></LI
><LI
><P
>Who to put in a given group</P
></LI
><LI
><P
>What type of permissions should these shared resources
                have</P
></LI
></UL
><P
>A common-sense approach to these questions is helpful.  One
            possibility is to mirror your organization's structure when creating
            groups.  For example, if there is a finance department, create a
            group called <SAMP
CLASS="COMPUTEROUTPUT"
>finance</SAMP
>, and make all
            finance personnel members of that group.  If the financial
            information is too sensitive for the company at large, but vital for
            senior officials within the organization, then grant the senior
            officials group-level permission to access the directories and data
            used by the finance department by adding all senior officials to the
            <SAMP
CLASS="COMPUTEROUTPUT"
>finance</SAMP
> group.</P
><P
>It is also good to be cautious when granting permissions to
            users.  This way, sensitive information is less likely to fall into
            the wrong hands.</P
><P
>By approaching the creation of your organization's group
            structure in this manner, the need for access to shared data within
            the organization can be safely and effectively met.</P
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S1-ACCTSGRPS-RES-WHERE"
>6.2.2. Where Users Access Shared Data</A
></H2
><P
>When sharing data among users, it is a common practice to have a
          central server (or group of servers) that make certain directories
          available to other machines on the network. This way data is stored in
          one place; synchronizing data between multiple machines is not
          necessary.</P
><P
>Before taking this approach, you must first determine what systems
          are to access the centrally-stored data.  As you do this, take note of
          the operating systems used by the systems.  This information has a
          bearing on your ability to implement such an approach, as your storage
          server must be capable of serving its data to each of the operating
          systems in use at your organization.</P
><P
>Unfortunately, once data is shared between multiple computers on a
          network, the potential for conflicts in file ownership can
          arise.</P
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S2-ACCTSGRPS-RES-WHERE-UID"
>6.2.2.1. Global Ownership Issues</A
></H3
><P
>There are benefits if data is stored centrally and is accessed
            by multiple computers over a network.  However, assume for a moment
            that each of those computers has a locally-maintained list of user
            accounts.  What if the list of users on each of these systems are
            not consistent with the list of users on the central server?  Even
            worse, what if the list of users on each of these systems are not
            even consistent with each other?</P
><P
>Much of this depends on how users and access permissions are
            implemented on each system, but in some cases it is possible that
            user A on one system may actually be known as user B on another
            system.  This becomes a real problem when data is shared between
            these systems, as data that user A is allowed to access from one
            system can also be read by user B from another system.</P
><P
>For this reason, many organizations use some sort of central
            user database.  This assures that there are no overlaps between user
            lists on different systems.</P
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-ACCTSGRPS-REC-WHO-HOME"
>6.2.2.2. Home Directories</A
></H3
><P
>Another issue facing system administrators is whether or not
            users should have centrally-stored home directories.</P
><P
>The primary advantage of centralizing home directories on a
            network-attached server is that if a user logs into any machine on
            the network, they will be able to access the files in their home
            directory.</P
><P
>The disadvantage is that if the network goes down, users across
            the entire organization will be unable to get to their files.  In
            some situations (such as organizations that make widespread use of
            laptops), having centralized home directories may not be desirable.
            But if it makes sense for your organization, deploying centralized
            home directories can make a system administrator's life much
            easier.</P
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S1-ACCTSGRPS-REC-BAR"
>6.2.3. What Barriers Are in Place To Prevent Abuse of Resources</A
></H2
><P
>The careful organization of groups and assignment of permissions
          for shared resources is one of the most important things a system
          administrator can do to prevent resource abuse among users within an
          organization. In this way, those who should not have access to
          sensitive resources are denied access.</P
><P
>But no matter how your organization does things, the best guard
          against abuse of resources is always sustained vigilance on the part
          of the system administrator.  Keeping your eyes open is often the only
          way to avoid having an unpleasant surprise waiting for you at your
          desk one morning.</P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="ch-acctsgrps.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-acctsgrps-rhlspec.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Managing User Accounts and Resource Access</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="ch-acctsgrps.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Red Hat Enterprise Linux-Specific Information</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>