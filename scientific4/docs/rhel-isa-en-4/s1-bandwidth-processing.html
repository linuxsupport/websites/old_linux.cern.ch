<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Processing Power</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Bandwidth and Processing Power"
HREF="ch-bandwidth.html"><LINK
REL="PREVIOUS"
TITLE="Bandwidth and Processing Power"
HREF="ch-bandwidth.html"><LINK
REL="NEXT"
TITLE="Red Hat Enterprise Linux-Specific Information"
HREF="s1-bandwidth-rhlspec.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Introduction to System Administration</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="ch-bandwidth.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 3. Bandwidth and Processing Power</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-bandwidth-rhlspec.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-BANDWIDTH-PROCESSING"
>3.2. Processing Power</A
></H1
><P
>Often known as CPU power, CPU cycles, and various other names,
        processing power is the ability of a computer to manipulate data.
        Processing power varies with the architecture (and clock speed) of the
        CPU &#8212; usually CPUs with higher clock speeds and those supporting
        larger word sizes have more processing power than slower CPUs supporting
        smaller word sizes.</P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-BANDWIDTH-PROCESSING-FEATURES"
>3.2.1. Facts About Processing Power</A
></H2
><P
>Here are the two main facts about processing power that you should
          keep in mind:</P
><P
></P
><UL
><LI
><P
>Processing power is fixed</P
></LI
><LI
><P
>Processing power cannot be stored</P
></LI
></UL
><P
>Processing power is fixed, in that the CPU can only go so fast.
          For example, if you need to add two numbers together (an operation
          that takes only one machine instruction on most architectures), a
          particular CPU can do it at one speed, and one speed only.  With few
          exceptions, it is not even possible to <I
CLASS="EMPHASIS"
>slow</I
> the
          rate at which a CPU processes instructions, much less increase
          it.</P
><P
>Processing power is also fixed in another way: it is finite.  That
          is, there are limits to the types of CPUs that can be plugged into any
          given computer.  Some systems are capable of supporting a wide range
          of CPUs of differing speeds, while others may not be upgradeable at
          all<A
NAME="AEN2103"
HREF="#FTN.AEN2103"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
>.</P
><P
>Processing power cannot be stored for later use.  In other words,
          if a CPU can process 100 million instructions in one second, one
          second of idle time equals 100 million instructions worth of
          processing that have been wasted.</P
><P
>If we take these facts and examine them from a slightly different
          perspective, a CPU "produces" a stream of executed instructions at a
          fixed rate.  And if the CPU "produces" executed instructions, that
          means that something else must "consume" them.  The next section
          defines these consumers.</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-BANDWIDTH-PROCESSING-CONSUMERS"
>3.2.2. Consumers of Processing Power</A
></H2
><P
>There are two main consumers of processing power:</P
><P
></P
><UL
><LI
><P
>Applications</P
></LI
><LI
><P
>The operating system itself</P
></LI
></UL
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-BANDWIDTH-PROCESSING-CONSUMERS-APPS"
>3.2.2.1. Applications</A
></H3
><P
>The most obvious consumers of processing power are the
            applications and programs you want the computer to run for you.
            From a spreadsheet to a database, applications are the reason you
            have a computer.</P
><P
>A single-CPU system can only do one thing at any given time.
            Therefore, if your application is running, everything else on the
            system is not.  And the opposite is, of course, true &#8212; if
            something other than your application is running, then your
            application is doing nothing.</P
><P
>But how is it that many different applications can seemingly run
            at once under a modern operating system?  The answer is that these
            are multitasking operating systems.  In other words, they create the
            illusion that many different things are going on simultaneously when
            in fact that is not possible.  The trick is to give each process a
            fraction of a second's worth of time running on the CPU before
            giving the CPU to another process for the next fraction of a second.
            If these <I
CLASS="FIRSTTERM"
>context switches</I
> happen frequently
            enough, the illusion of multiple applications running simultaneously
            is achieved.</P
><P
>Of course, applications do other things than manipulate data
            using the CPU.  They may wait for user input as well as performing
            I/O to devices such as disk drives and graphics displays.  When
            these events take place, the application no longer needs the CPU.
            At these times, the CPU can be used for other processes running
            other applications without slowing the waiting application at
            all.</P
><P
>In addition, the CPU can be used by another consumer of
            processing power: the operating system itself.</P
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-BANDWIDTH-PROCESSING-CONSUMERS-OS"
>3.2.2.2. The Operating System</A
></H3
><P
>It is difficult to determine how much processing power is
            consumed by the operating system.  The reason for this is that
            operating systems use a mixture of process-level and system-level
            code to perform their work.  While, for example, it is easy to use a
            process monitor to determine what the process running a
            <I
CLASS="FIRSTTERM"
>daemon</I
> or <I
CLASS="FIRSTTERM"
>service</I
> is
            doing, it is not so easy to determine how much processing power is
            being consumed by system-level I/O-related processing (which is
            normally done within the context of the process requesting the
            I/O.)</P
><P
>In general, it is possible to divide this kind of operating
            system overhead into two types:</P
><P
></P
><UL
><LI
><P
>Operating system housekeeping</P
></LI
><LI
><P
>Process-related activities</P
></LI
></UL
><P
>Operating system housekeeping includes activities such as
            process scheduling and memory management, while process-related
            activities include any processes that support the operating system
            itself, such as processes handling system-wide event logging or I/O
            cache flushing.</P
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-BANDWIDTH-PROCESSING-IMPROVING"
>3.2.3. Improving a CPU Shortage</A
></H2
><P
>When there is insufficient processing power available for the work
          needing to be done, you have two options:</P
><P
></P
><UL
><LI
><P
>Reducing the load</P
></LI
><LI
><P
>Increasing the capacity</P
></LI
></UL
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-BANDWIDTH-PROCESSING-IMPROVING-LOAD"
>3.2.3.1. Reducing the Load</A
></H3
><P
>Reducing the CPU load is something that can be done with no
            expenditure of money.  The trick is to identify those aspects of the
            system load under your control that can be cut back.  There are
            three areas to focus on:</P
><P
></P
><UL
><LI
><P
>Reducing operating system overhead</P
></LI
><LI
><P
>Reducing application overhead</P
></LI
><LI
><P
>Eliminating applications entirely</P
></LI
></UL
><DIV
CLASS="SECT4"
><H4
CLASS="SECT4"
><A
NAME="S4-BANDWIDTH-PROCESSING-IMPROVING-LOAD-OS"
>3.2.3.1.1. Reducing Operating System Overhead</A
></H4
><P
>To reduce operating system overhead, you must examine your
              current system load and determine what aspects of it result in
              inordinate amounts of overhead.  These areas could include:</P
><P
></P
><UL
><LI
><P
>Reducing the need for frequent process scheduling</P
></LI
><LI
><P
>Reducing the amount of I/O performed</P
></LI
></UL
><P
>Do not expect miracles; in a reasonably-well configured
              system, it is unlikely to notice much of a performance increase by
              trying to reduce operating system overhead.  This is due to the
              fact that a reasonably-well configured system, by definition,
              results in a minimal amount of overhead.  However, if your system
              is running with too little RAM for instance, you may be able to
              reduce overhead by alleviating the RAM shortage.</P
></DIV
><DIV
CLASS="SECT4"
><H4
CLASS="SECT4"
><A
NAME="S4-BANDWIDTH-PROCESSING-IMPROVING-LOAD-APP"
>3.2.3.1.2. Reducing Application Overhead</A
></H4
><P
>Reducing application overhead means making sure the
              application has everything it needs to run well.  Some
              applications exhibit wildly different behaviors under different
              environments &#8212; an application may become highly
              compute-bound while processing certain types of data, but not
              others, for example.</P
><P
>The point to keep in mind here is that you must understand the
              applications running on your system if you are to enable them to
              run as efficiently as possible.  Often this entails working with
              your users, and/or your organization's developers, to help uncover
              ways in which the applications can be made to run more
              efficiently.</P
></DIV
><DIV
CLASS="SECT4"
><H4
CLASS="SECT4"
><A
NAME="S4-BANDWIDTH-PROCESSING-IMPROVING-LOAD-APPD"
>3.2.3.1.3. Eliminating  Applications Entirely</A
></H4
><P
>Depending on your organization, this approach might not be
              available to you, as it often is not a system administrator's
              responsibility to dictate which applications will and will not be
              run.  However, if you can identify any applications that are known
              "CPU hogs", you might be able to influence the powers-that-be to
              retire them.</P
><P
>Doing this will likely involve more than just yourself.  The
              affected users should certainly be a part of this process; in many
              cases they may have the knowledge and the political power to make
              the necessary changes to the application lineup.</P
><DIV
CLASS="TIP"
><P
></P
><TABLE
CLASS="TIP"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/tip.png"
HSPACE="5"
ALT="Tip"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Tip</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>Keep in mind that an application may not need to be
                eliminated from every system in your organization.  You might be
                able to move a particularly CPU-hungry application from an
                overloaded system to another system that is nearly idle.</P
></TD
></TR
></TABLE
></DIV
></DIV
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-BANDWIDTH-PROCESSING-IMPROVING-CAPACITY"
>3.2.3.2. Increasing the Capacity</A
></H3
><P
>Of course, if it is not possible to reduce the demand for
            processing power, you must find ways of increasing the processing
            power that is available.  To do so costs money, but it can be
            done.</P
><DIV
CLASS="SECT4"
><H4
CLASS="SECT4"
><A
NAME="S4-BANDWIDTH-PROCESSING-IMPROVE-CAPACITY-UPG"
>3.2.3.2.1. Upgrading the CPU</A
></H4
><P
>The most straightforward approach is to determine if your
              system's CPU can be upgraded.  The first step is to determine if
              the current CPU can be removed.  Some systems (primarily laptops)
              have CPUs that are soldered in place, making an upgrade
              impossible.  The rest, however, have socketed CPUs, making
              upgrades possible &#8212; at least in theory.</P
><P
>Next, you must do some research to determine if a faster CPU
              exists for your system configuration.  For example, if you
              currently have a 1GHz CPU, and a 2GHz unit of the same type
              exists, an upgrade might be possible.</P
><P
>Finally, you must determine the maximum clock speed supported
              by your system.  To continue the example above, even if a 2GHz CPU
              of the proper type exists, a simple CPU swap is not an option if
              your system only supports processors running at 1GHz or
              below.</P
><P
>Should you find that you cannot install a faster CPU in your
              system, your options may be limited to changing motherboards or
              even the more expensive forklift upgrade mentioned earlier.</P
><P
>However, some system configurations make a slightly different
              approach possible.  Instead of replacing the current CPU, why not
              just add another one?</P
></DIV
><DIV
CLASS="SECT4"
><H4
CLASS="SECT4"
><A
NAME="S4-BANDWIDTH-PROCESSING-IMPROVE-CAPACITY-SMP"
>3.2.3.2.2. Is Symmetric Multiprocessing Right for You?</A
></H4
><P
>Symmetric multiprocessing (also known as SMP) makes it
              possible for a computer system to have more than one CPU sharing
              all system resources.  This means that, unlike a uniprocessor
              system, an SMP system may actually have more than one process
              running at the same time.</P
><P
>At first glance, this seems like any system administrator's
              dream.  First and foremost, SMP makes it possible to increase a
              system's CPU power even if CPUs with faster clock speeds are not
              available &#8212; just by adding another CPU.  However, this
              flexibility comes with some caveats.</P
><P
>The first caveat is that not all systems are capable of SMP
              operation.  Your system must have a motherboard designed to
              support multiple processors.  If it does not, a motherboard
              upgrade (at the least) would be required.</P
><P
>The second caveat is that SMP increases system overhead.  This
              makes sense if you stop to think about it; with more CPUs to
              schedule work for, the operating system requires more CPU cycles
              for overhead.  Another aspect to this is that with multiple CPUs,
              there can be more contention for system resources.  Because of
              these factors, upgrading a dual-processor system to a
              quad-processor unit does not result in a 100% increase in
              available CPU power.  In fact, depending on the actual hardware,
              the workload, and the processor architecture, it is possible to
              reach a point where the addition of another processor could
              actually <I
CLASS="EMPHASIS"
>reduce</I
> system performance.</P
><P
>Another point to keep in mind is that SMP does not help
              workloads consisting of one monolithic application with a single
              stream of execution.  In other words, if a large compute-bound
              simulation program runs as one process and without threads, it
              will not run any faster on an SMP system than on a
              single-processor machine.  In fact, it may even run somewhat
              slower, due to the increased overhead SMP brings.  For these
              reasons, many system administrators feel that when it comes to CPU
              power, single stream processing power is the way to go.  It
              provides the most CPU power with the fewest restrictions on its
              use.</P
><P
>While this discussion seems to indicate that SMP is never a
              good idea, there are circumstances in which it makes sense.  For
              example, environments running multiple highly compute-bound
              applications are good candidates for SMP.  The reason for this is
              that applications that do nothing but compute for long periods of
              time keep contention between active processes (and therefore, the
              operating system overhead) to a minimum, while the processes
              themselves keep every CPU busy.</P
><P
>One other thing to keep in mind about SMP is that the
              performance of an SMP system tends to degrade more gracefully as
              the system load increases.  This does make SMP systems popular in
              server and multi-user environments, as the ever-changing process
              mix can impact the system-wide load less on a multi-processor
              machine.</P
></DIV
></DIV
></DIV
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN2103"
HREF="s1-bandwidth-processing.html#AEN2103"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>This situation leads to what is humorously termed
          as a <I
CLASS="FIRSTTERM"
>forklift upgrade</I
>, which means a complete
          replacement of a computer.</P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="ch-bandwidth.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-bandwidth-rhlspec.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Bandwidth and Processing Power</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="ch-bandwidth.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Red Hat Enterprise Linux-Specific Information</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>