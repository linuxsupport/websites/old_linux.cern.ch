<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Basic Virtual Memory Concepts</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Physical and Virtual Memory"
HREF="ch-memory.html"><LINK
REL="PREVIOUS"
TITLE="The Storage Spectrum"
HREF="s1-memory-spectrum.html"><LINK
REL="NEXT"
TITLE="Virtual Memory: The Details"
HREF="s1-memory-virt-details.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Introduction to System Administration</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-memory-spectrum.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 4. Physical and Virtual Memory</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-memory-virt-details.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-MEMORY-CONCEPTS"
>4.3. Basic Virtual Memory Concepts</A
></H1
><P
>While the technology behind the construction of the various
        modern-day storage technologies is truly impressive, the average system
        administrator does not need to be aware of the details.  In fact, there
        is really only one fact that system administrators should always keep in
        mind:</P
><P
>There is never enough RAM.</P
><P
>While this truism might at first seem humorous, many operating
        system designers have spent a great deal of time trying to reduce the
        impact of this very real shortage.  They have done so by implementing
        <I
CLASS="FIRSTTERM"
>virtual memory</I
> &#8212; a way of combining RAM
        with slower storage to give a system the appearance of having more RAM
        than is actually installed.</P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-MEMORY-VIRT-SIMPLE"
>4.3.1. Virtual Memory in Simple Terms</A
></H2
><P
>Let us start with a hypothetical application.  The machine code
          making up this application is 10000 bytes in size.  It also requires
          another 5000 bytes for data storage and I/O buffers.  This means that,
          to run this application, there must be 15000 bytes of RAM available;
          even one byte less, and the application would not be able to
          run.</P
><P
>This 15000 byte requirement is known as the application's
          <I
CLASS="FIRSTTERM"
>address space</I
>.  It is the number of unique
          addresses needed to hold both the application and its data.  In the
          first computers, the amount of available RAM had to be greater than
          the address space of the largest application to be run; otherwise, the
          application would fail with an "out of memory" error.</P
><P
>A later approach known as <I
CLASS="FIRSTTERM"
>overlaying</I
>
          attempted to alleviate the problem by allowing programmers to dictate
          which parts of their application needed to be memory-resident at any
          given time.  In this way, code only required once for initialization
          purposes could be written over (overlayed) with code that would be
          used later.  While overlays did ease memory shortages, it was a very
          complex and error-prone process.  Overlays also failed to address the
          issue of system-wide memory shortages at runtime.  In other words, an
          overlayed program may require less memory to run than a program that
          is not overlayed, but if the system still does not have sufficient
          memory for the overlayed program, the end result is the same &#8212;
          an out of memory error.</P
><P
>With virtual memory, the concept of an application's address space
          takes on a different meaning.  Rather than concentrating on how
          <I
CLASS="EMPHASIS"
>much</I
> memory an application needs to run, a
          virtual memory operating system continually attempts to find the
          answer to the question, "how <I
CLASS="EMPHASIS"
>little</I
> memory does
          an application need to run?"</P
><P
>While it at first appears that our hypothetical application
          requires the full 15000 bytes to run, think back to our discussion in
          <A
HREF="ch-memory.html#S1-MEMORY-LOCAL-SEQ"
>Section 4.1 <I
>Storage Access Patterns</I
></A
> &#8212; memory access tends to be
          sequential and localized.  Because of this, the amount of memory
          required to execute the application at any given time is less than
          15000 bytes &#8212; usually a lot less.  Consider the types of memory
          accesses required to execute a single machine instruction:</P
><P
></P
><UL
><LI
><P
>The instruction is read from memory.</P
></LI
><LI
><P
>The data required by the instruction is read from
              memory.</P
></LI
><LI
><P
>After the instruction completes, the results of the
              instruction are written back to memory.</P
></LI
></UL
><P
>The actual number of bytes necessary for each memory access varies
          according to the CPU's architecture, the actual instruction, and the
          data type.  However, even if one instruction required 100 bytes of
          memory for each type of memory access, the 300 bytes required is still
          much less than the application's entire 15000-byte address space.  If
          a way could be found to keep track of an application's memory
          requirements as the application runs, it would be possible to keep the
          application running while using less memory than its address space
          would otherwise dictate.</P
><P
>But that leaves one question:</P
><P
>If only part of the application is in memory at any given time,
          where is the rest of it?</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-MEMORY-VIRT-WHERE"
>4.3.2. Backing Store &#8212; the Central Tenet of Virtual Memory</A
></H2
><P
>The short answer to this question is that the rest of the
          application remains on disk.  In other words, disk acts as the
          <I
CLASS="FIRSTTERM"
>backing store</I
> for RAM; a slower, larger storage
          medium acting as a "backup" for a much faster, smaller storage medium.
          This might at first seem to be a very large performance problem in the
          making &#8212; after all, disk drives are so much slower than
          RAM.</P
><P
>While this is true, it is possible to take advantage of the
          sequential and localized access behavior of applications and eliminate
          most of the performance implications of using disk drives as backing
          store for RAM.  This is done by structuring the virtual memory
          subsystem so that it attempts to ensure that those parts of the
          application currently needed &#8212; or likely to be needed in the
          near future &#8212; are kept in RAM only for as long as they are
          actually needed.</P
><P
>In many respects this is similar to the relationship between cache
          and RAM: making the a small amount of fast storage combined with a
          large amount of slow storage act just like a large amount of fast
          storage.</P
><P
>With this in mind, let us explore the process in more
          detail.</P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-memory-spectrum.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-memory-virt-details.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>The Storage Spectrum</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="ch-memory.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Virtual Memory: The Details</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>