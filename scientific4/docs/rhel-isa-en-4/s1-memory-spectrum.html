<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>The Storage Spectrum</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Physical and Virtual Memory"
HREF="ch-memory.html"><LINK
REL="PREVIOUS"
TITLE="Physical and Virtual Memory"
HREF="ch-memory.html"><LINK
REL="NEXT"
TITLE="Basic Virtual Memory Concepts"
HREF="s1-memory-concepts.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Introduction to System Administration</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="ch-memory.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 4. Physical and Virtual Memory</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-memory-concepts.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-MEMORY-SPECTRUM"
>4.2. The Storage Spectrum</A
></H1
><P
>Present-day computers actually use a variety of storage
        technologies.  Each technology is geared toward a specific function,
        with speeds and capacities to match.</P
><P
>These technologies are:</P
><P
></P
><UL
><LI
><P
>CPU registers</P
></LI
><LI
><P
>Cache memory</P
></LI
><LI
><P
>RAM</P
></LI
><LI
><P
>Hard drives</P
></LI
><LI
><P
>Off-line backup storage (tape, optical disk, etc.)</P
></LI
></UL
><P
>In terms of capabilities and cost, these technologies form a
        spectrum.  For example, CPU registers are:</P
><P
></P
><UL
><LI
><P
>Very fast (access times of a few nanoseconds)</P
></LI
><LI
><P
>Low capacity (usually less than 200 bytes)</P
></LI
><LI
><P
>Very limited expansion capabilities (a change in CPU
            architecture would be required)</P
></LI
><LI
><P
>Expensive (more than one dollar/byte)</P
></LI
></UL
><P
>However, at the other end of the spectrum, off-line backup storage
        is:</P
><P
></P
><UL
><LI
><P
>Very slow (access times may be measured in days, if the backup
            media must be shipped long distances)</P
></LI
><LI
><P
>Very high capacity (10s - 100s of gigabytes)</P
></LI
><LI
><P
>Essentially unlimited expansion capabilities (limited only by
            the floorspace needed to house the backup media)</P
></LI
><LI
><P
>Very inexpensive (fractional cents/byte)</P
></LI
></UL
><P
>By using different technologies with different capabilities, it is
        possible to fine-tune system design for maximum performance at the
        lowest possible cost.  The following sections explore each technology in
        the storage spectrum.</P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-MEMORY-"
>4.2.1. CPU Registers</A
></H2
><P
>Every present-day CPU design includes registers for a variety of
          purposes, from storing the address of the currently-executed
          instruction to more general-purpose data storage and manipulation.
          CPU registers run at the same speed as the rest of the CPU; otherwise,
          they would be a serious bottleneck to overall system performance.  The
          reason for this is that nearly all operations performed by the CPU
          involve the registers in one way or another.</P
><P
>The number of CPU registers (and their uses) are strictly
          dependent on the architectural design of the CPU itself.  There is no
          way to change the number of CPU registers, short of migrating to a CPU
          with a different architecture.  For these reasons, the number of CPU
          registers can be considered a constant, as they are changeable only
          with great pain and expense.</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-MEMORY-CACHE"
>4.2.2. Cache Memory</A
></H2
><P
>The purpose of cache memory is to act as a buffer between the very
          limited, very high-speed CPU registers and the relatively slower and
          much larger main system memory &#8212; usually referred to as
          RAM<A
NAME="AEN2607"
HREF="#FTN.AEN2607"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
>.  Cache memory has an operating speed
          similar to the CPU itself so, when the CPU accesses data in cache, the
          CPU is not kept waiting for the data.</P
><P
>Cache memory is configured such that, whenever data is to be read
          from RAM, the system hardware first checks to determine if the desired
          data is in cache.  If the data is in cache, it is quickly retrieved,
          and used by the CPU.  However, if the data is not in cache, the data
          is read from RAM and, while being transferred to the CPU, is also
          placed in cache (in case it is needed again later).  From the
          perspective of the CPU, all this is done transparently, so that the
          only difference between accessing data in cache and accessing data in
          RAM is the amount of time it takes for the data to be returned.</P
><P
>In terms of storage capacity, cache is much smaller than RAM.
          Therefore, not every byte in RAM can have its own unique location in
          cache.  As such, it is necessary to split cache up into sections that
          can be used to cache different areas of RAM, and to have a mechanism
          that allows each area of cache to cache different areas of RAM at
          different times.  Even with the difference in size between cache and
          RAM, given the sequential and localized nature of storage access, a
          small amount of cache can effectively speed access to a large amount
          of RAM.</P
><P
>When writing data from the CPU, things get a bit more complicated.
          There are two different approaches that can be used.  In both cases,
          the data is first written to cache.  However, since the purpose of
          cache is to function as a very fast copy of the contents of selected
          portions of RAM, any time a piece of data changes its value, that new
          value must be written to both cache memory and RAM.  Otherwise, the
          data in cache and the data in RAM would no longer match.</P
><P
>The two approaches differ in how this is done.  One approach,
          known as <I
CLASS="FIRSTTERM"
>write-through</I
> caching, immediately
          writes the modified data to RAM.  <I
CLASS="FIRSTTERM"
>Write-back</I
>
          caching, however, delays the writing of modified data back to RAM.
          The reason for doing this is to reduce the number of times a
          frequently-modified piece of data must be written back to RAM.</P
><P
>Write-through cache is a bit simpler to implement; for this reason
          it is most common.  Write-back cache is a bit trickier to implement;
          in addition to storing the actual data, it is necessary to maintain
          some sort of mechanism capable of flagging the cached data as clean
          (the data in cache is the same as the data in RAM), or dirty (the data
          in cache has been modified, meaning that the data in RAM is no longer
          current).  It is also necessary to implement a way of periodically
          flushing dirty cache entries back to RAM.</P
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-MEMORY-CACHE-LEVELS"
>4.2.2.1. Cache Levels</A
></H3
><P
>Cache subsystems in present-day computer designs may be
            multi-level; that is, there might be more than one set of cache
            between the CPU and main memory.  The cache levels are often
            numbered, with lower numbers being closer to the CPU.  Many systems
            have two cache levels:</P
><P
></P
><UL
><LI
><P
>L1 cache is often located directly on the CPU chip itself
                and runs at the same speed as the CPU</P
></LI
><LI
><P
>L2 cache is often part of the CPU module, runs at CPU speeds
                (or nearly so), and is usually a bit larger and slower than L1
                cache</P
></LI
></UL
><P
>Some systems (normally high-performance servers) also have L3
            cache, which is usually part of the system motherboard.  As might be
            expected, L3 cache would be larger (and most likely slower) than L2
            cache.</P
><P
>In either case, the goal of all cache subsystems &#8212; whether
            single- or multi-level &#8212; is to reduce the average access time
            to the RAM.</P
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-MEMORY-RAM"
>4.2.3. Main Memory &#8212; RAM</A
></H2
><P
>RAM makes up the bulk of electronic storage on present-day
          computers.  It is used as storage for both data and programs while
          those data and programs are in use.  The speed of RAM in most systems
          today lies between the speed of cache memory and that of hard drives,
          and is much closer to the former than the latter.</P
><P
>The basic operation of RAM is actually quite straightforward.  At
          the lowest level, there are the RAM chips &#8212; integrated circuits that
          do the actual "remembering."  These chips have four types of
          connections to the outside world:</P
><P
></P
><UL
><LI
><P
>Power connections (to operate the circuitry within the
              chip)</P
></LI
><LI
><P
>Data connections (to enable the transfer of data into or out
              of the chip)</P
></LI
><LI
><P
>Read/Write connections (to control whether data is to be
              stored into or retrieved from the chip)</P
></LI
><LI
><P
>Address connections (to determine where in the chip the data
              should be read/written)</P
></LI
></UL
><P
>Here are the steps required to store data in RAM:</P
><P
></P
><OL
TYPE="1"
><LI
><P
>The data to be stored is presented to the data
              connections.</P
></LI
><LI
><P
>The address at which the data is to be stored is presented to
              the address connections.</P
></LI
><LI
><P
>The read/write connection is set to write mode.</P
></LI
></OL
><P
>Retrieving data is just as straightforward:</P
><P
></P
><OL
TYPE="1"
><LI
><P
>The address of the desired data is presented to the address
              connections.</P
></LI
><LI
><P
>The read/write connection is set to read mode.</P
></LI
><LI
><P
>The desired data is read from the data connections.</P
></LI
></OL
><P
>While these steps seem simple, they take place at very high
          speeds, with the time spent on each step measured in
          nanoseconds.</P
><P
>Nearly all RAM chips created today are sold as
          <I
CLASS="FIRSTTERM"
>modules</I
>.  Each module consists of a number of
          individual RAM chips attached to a small circuit board.  The
          mechanical and electrical layout of the module adheres to various
          industry standards, making it possible to purchase memory from a
          variety of vendors.</P
><DIV
CLASS="NOTE"
><P
></P
><TABLE
CLASS="NOTE"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/note.png"
HSPACE="5"
ALT="Note"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Note</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>The main benefit to a system using industry-standard RAM modules
            is that it tends to keep the cost of RAM low, due to the ability to
            purchase the modules from more than just the system
            manufacturer.</P
><P
>Although most computers use industry-standard RAM modules, there
            are exceptions.  Most notable are laptops (and even here some
            standardization is starting to take hold) and high-end servers.
            However, even in these instances, it is likely that third-party RAM
            modules are available, assuming the system is relatively popular and
            is not a completely new design.</P
></TD
></TR
></TABLE
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-MEMORY-DRIVES"
>4.2.4. Hard Drives</A
></H2
><P
>All the technologies discussed so far are
          <I
CLASS="FIRSTTERM"
>volatile</I
> in nature.  In other words, data
          contained in volatile storage is lost when the power is turned
          off.</P
><P
>Hard drives, on the other hand, are
          <I
CLASS="FIRSTTERM"
>non-volatile</I
> &#8212; the data they contain
          remains there, even after the power is removed.  Because of this, hard
          drives occupy a special place in the storage spectrum.  Their
          non-volatile nature makes them ideal for storing programs and data for
          longer-term use.  Another unique aspect to hard drives is that, unlike
          RAM and cache memory, it is not possible to execute programs directly
          when they are stored on hard drives; instead, they must first be read
          into RAM.</P
><P
>Also different from cache and RAM is the speed of data storage and
          retrieval; hard drives are at least an order of magnitude slower than
          the all-electronic technologies used for cache and RAM.  The
          difference in speed is due mainly to their electromechanical nature.
          There are four distinct phases taking place during each data transfer
          to or from a hard drive.  The following list illustrates these phases,
          along with the time it would take a typical high-performance drive, on
          average, to complete each:</P
><P
></P
><UL
><LI
><P
>Access arm movement (5.5 milliseconds)</P
></LI
><LI
><P
>Disk rotation (.1 milliseconds)</P
></LI
><LI
><P
>Heads reading/writing data (.00014 milliseconds)</P
></LI
><LI
><P
>Data transfer to/from the drive's electronics (.003 Milliseconds)</P
></LI
></UL
><P
>Of these, only the last phase is not dependent on any mechanical
        operation.</P
><DIV
CLASS="NOTE"
><P
></P
><TABLE
CLASS="NOTE"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/note.png"
HSPACE="5"
ALT="Note"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Note</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>Although there is much more to learn about hard drives, disk
            storage technologies are discussed in more depth in <A
HREF="ch-storage.html"
>Chapter 5 <I
>Managing Storage</I
></A
>.  For the time being, it is only necessary to
            keep in mind the huge speed difference between RAM and disk-based
            technologies and that their storage capacity usually exceeds that of
            RAM by a factor of at least 10, and often by 100 or more.</P
></TD
></TR
></TABLE
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-MEMORY-BACKUP"
>4.2.5. Off-Line Backup Storage</A
></H2
><P
>Off-line backup storage takes a step beyond hard drive storage in
          terms of capacity (higher) and speed (slower).  Here, capacities are
          effectively limited only by your ability to procure and store the
          removable media.</P
><P
>The actual technologies used in these devices varies widely.  Here
          are the more popular types:</P
><P
></P
><UL
><LI
><P
>Magnetic tape</P
></LI
><LI
><P
>Optical disk</P
></LI
></UL
><P
>Of course, having removable media means that access times become
          even longer, particularly when the desired data is on media not
          currently loaded in the storage device.  This situation is alleviated
          somewhat by the use of robotic devices capable of automatically
          loading and unloading media, but the media storage capacities of such
          devices are still finite.  Even in the best of cases, access times are
          measured in seconds, which is much longer than the relatively slow
          multi-millisecond access times typical for a high-performance hard
          drive.</P
><P
>Now that we have briefly studied the various storage technologies
          in use today, let us explore basic virtual memory concepts.</P
></DIV
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN2607"
HREF="s1-memory-spectrum.html#AEN2607"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>While "RAM" is an acronym for "Random Access
          Memory," and a term that could easily apply to any storage technology
          allowing the non-sequential access of stored data, when system
          administrators talk about RAM they invariably mean main system
          memory.</P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="ch-memory.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-memory-concepts.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Physical and Virtual Memory</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="ch-memory.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Basic Virtual Memory Concepts</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>