<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Virtual Memory: The Details</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Physical and Virtual Memory"
HREF="ch-memory.html"><LINK
REL="PREVIOUS"
TITLE="Basic Virtual Memory Concepts"
HREF="s1-memory-concepts.html"><LINK
REL="NEXT"
TITLE="Virtual Memory Performance Implications"
HREF="s1-memory-concepts-perf.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Introduction to System Administration</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-memory-concepts.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 4. Physical and Virtual Memory</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-memory-concepts-perf.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-MEMORY-VIRT-DETAILS"
>4.4. Virtual Memory: The Details</A
></H1
><P
>First, we must introduce a new concept: <I
CLASS="FIRSTTERM"
>virtual address
        space</I
>.  Virtual address space is the maximum amount of
        address space available to an application.  The virtual address space
        varies according to the system's architecture and operating system.
        Virtual address space depends on the architecture because it is the
        architecture that defines how many bits are available for addressing
        purposes.  Virtual address space also depends on the operating system
        because the manner in which the operating system was implemented may
        introduce additional limits over and above those imposed by the
        architecture.</P
><P
>The word "virtual" in virtual address space means this is the total
        number of uniquely-addressable memory locations available to an
        application, but <I
CLASS="EMPHASIS"
>not</I
> the amount of physical memory
        either installed in the system, or dedicated to the application at any
        given time.</P
><P
>In the case of our example application, its virtual address space is
        15000 bytes.</P
><P
>To implement virtual memory, it is necessary for the computer system
        to have special memory management hardware.  This hardware is often
        known as an <I
CLASS="FIRSTTERM"
>MMU</I
> (Memory Management Unit).
        Without an MMU, when the CPU accesses RAM, the actual RAM locations
        never change &#8212; memory address 123 is always the same physical
        location within RAM.</P
><P
>However, with an MMU, memory addresses go through a translation step
        prior to each memory access.  This means that memory address 123 might
        be directed to physical address 82043 at one time, and physical address
        20468 another time.  As it turns out, the overhead of individually
        tracking the virtual to physical translations for billions of bytes of
        memory would be too great.  Instead, the MMU divides RAM into
        <I
CLASS="FIRSTTERM"
>pages</I
> &#8212; contiguous sections of memory of a
        set size that are handled by the MMU as single entities.</P
><P
>Keeping track of these pages and their address translations might
        sound like an unnecessary and confusing additional step.  However, it is
        crucial to implementing virtual memory.  For that reason, consider the
        following point.</P
><P
>Taking our hypothetical application with the 15000 byte virtual
        address space, assume that the application's first instruction accesses
        data stored at address 12374.  However, also assume that our computer
        only has 12288 bytes of physical RAM.  What happens when the CPU
        attempts to access address 12374?</P
><P
>What happens is known as a <I
CLASS="FIRSTTERM"
>page fault</I
>.</P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-MEMORY-CONCEPTS-PAGING"
>4.4.1. Page Faults</A
></H2
><P
>A page fault is the sequence of events occurring when a program
          attempts to access data (or code) that is in its address space, but is
          not currently located in the system's RAM.  The operating system must
          handle page faults by somehow making the accessed data memory
          resident, allowing the program to continue operation as if the page
          fault had never occurred.</P
><P
>In the case of our hypothetical application, the CPU first
          presents the desired address (12374) to the MMU.  However, the MMU has
          no translation for this address.  So, it interrupts the CPU and causes
          software, known as a page fault handler, to be executed.  The page
          fault handler then determines what must be done to resolve this page
          fault.  It can:</P
><P
></P
><UL
><LI
><P
>Find where the desired page resides on disk and read it in
              (this is normally the case if the page fault is for a page of
              code)</P
></LI
><LI
><P
>Determine that the desired page is already in RAM (but not
              allocated to the current process) and reconfigure the MMU to point
              to it</P
></LI
><LI
><P
>Point to a special page containing only zeros, and allocate a
              new page for the process only if the process ever attempts to
              write to the special page (this is called a <I
CLASS="FIRSTTERM"
>copy on
              write</I
> page, and is often used for pages containing
              zero-initialized data)</P
></LI
><LI
><P
>Get the desired page from somewhere else (which is discussed
              in more detail later)
            </P
></LI
></UL
><P
>While the first three actions are relatively straightforward, the
          last one is not.  For that, we need to cover some additional
          topics.</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-MEMORY-CONCEPTS-WSET"
>4.4.2. The Working Set</A
></H2
><P
>The group of physical memory pages currently dedicated to a
          specific process is known as the <I
CLASS="FIRSTTERM"
>working set</I
>
          for that process.  The number of pages in the working set can grow and
          shrink, depending on the overall availability of pages on a
          system-wide basis.</P
><P
>The working set grows as a process page faults.  The working set
          shrinks as fewer and fewer free pages exist.  To keep from running out
          of memory completely, pages must be removed from process's working
          sets and turned into free pages, available for later use.  The
          operating system shrinks processes' working sets by:</P
><P
></P
><UL
><LI
><P
>Writing modified pages to a dedicated area on a mass storage
              device (usually known as <I
CLASS="FIRSTTERM"
>swapping</I
> or
              <I
CLASS="FIRSTTERM"
>paging</I
> space)</P
></LI
><LI
><P
>Marking unmodified pages as being free (there is no need to
              write these pages out to disk as they have not changed)</P
></LI
></UL
><P
>To determine appropriate working sets for all processes, the
          operating system must track usage information for all pages.  In this
          way, the operating system determines which pages are actively being
          used (and must remain memory resident) and which pages are not (and
          therefore, can be removed from memory.)  In most cases, some sort of
          least-recently used algorithm determines which pages are eligible for
          removal from process working sets.</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-MEMORY-CONCEPTS-SWAPPING"
>4.4.3. Swapping</A
></H2
><P
>While swapping (writing modified pages out to the system swap
          space) is a normal part of a system's operation, it is possible to
          experience too much swapping.  The reason to be wary of excessive
          swapping is that the following situation can easily occur, over and
          over again:</P
><P
></P
><UL
><LI
><P
>Pages from a process are swapped</P
></LI
><LI
><P
>The process becomes runnable and attempts to access a swapped
              page</P
></LI
><LI
><P
>The page is faulted back into memory (most likely forcing some
              other processes' pages to be swapped out)</P
></LI
><LI
><P
>A short time later, the page is swapped out again</P
></LI
></UL
><P
>If this sequence of events is widespread, it is known as
          <I
CLASS="FIRSTTERM"
>thrashing</I
> and is indicative of insufficient RAM
          for the present workload.  Thrashing is extremely detrimental to
          system performance, as the CPU and I/O loads that can be generated in
          such a situation quickly outweigh the load imposed by a system's real
          work.  In extreme cases, the system may actually do no useful work,
          spending all its resources moving pages to and from memory.</P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-memory-concepts.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-memory-concepts-perf.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Basic Virtual Memory Concepts</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="ch-memory.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Virtual Memory Performance Implications</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>