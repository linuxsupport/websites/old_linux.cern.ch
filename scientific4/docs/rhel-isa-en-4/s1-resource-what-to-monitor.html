<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>What to Monitor?</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Resource Monitoring"
HREF="ch-resource.html"><LINK
REL="PREVIOUS"
TITLE="Monitoring System Capacity"
HREF="s1-resource-capacity.html"><LINK
REL="NEXT"
TITLE="Red Hat Enterprise Linux-Specific Information"
HREF="s1-resource-rhlspec.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Introduction to System Administration</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-resource-capacity.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 2. Resource Monitoring</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-resource-rhlspec.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-RESOURCE-WHAT-TO-MONITOR"
>2.4. What to Monitor?</A
></H1
><P
>As stated earlier, the resources present in every system are CPU
        power, bandwidth, memory, and storage.  At first glance, it would seem
        that monitoring would need only consist of examining these four
        different things.</P
><P
>Unfortunately, it is not that simple.  For example, consider a disk
        drive.  What things might you want to know about its performance?</P
><P
></P
><UL
><LI
><P
>How much free space is available?</P
></LI
><LI
><P
>How many I/O operations on average does it perform each
            second?</P
></LI
><LI
><P
>How long on average does it take each I/O operation to be
            completed?</P
></LI
><LI
><P
>How many of those I/O operations are reads?  How many are
            writes?</P
></LI
><LI
><P
>What is the average amount of data read/written with each
            I/O?</P
></LI
></UL
><P
>There are more ways of studying disk drive performance; these points
        have only scratched the surface.  The main concept to keep in mind is
        that there are many different types of data for each resource.</P
><P
>The following sections explore the types of utilization information
        that would be helpful for each of the major resource types.</P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-RESOURCE-WHAT-TO-MONITOR-CPU"
>2.4.1. Monitoring CPU Power</A
></H2
><P
>In its most basic form, monitoring CPU power can be no more
          difficult than determining if CPU utilization ever reaches 100%.  If
          CPU utilization stays below 100%, no matter what the system is doing,
          there is additional processing power available for more work.</P
><P
>However, it is a rare system that does not reach 100% CPU
          utilization at least some of the time.  At that point it is important
          to examine more detailed CPU utilization data.  By doing so, it
          becomes possible to start determining where the majority of your
          processing power is being consumed.  Here are some of the more popular
          CPU utilization statistics:</P
><P
></P
><DIV
CLASS="VARIABLELIST"
><DL
><DT
>User Versus System</DT
><DD
><P
>The percentage of time spent performing user-level
                processing versus system-level processing can point out whether
                a system's load is primarily due to running applications or due
                to operating system overhead.  High user-level percentages tend
                to be good (assuming users are not experiencing unsatisfactory
                performance), while high system-level percentages tend to point
                toward problems that will require further investigation.</P
></DD
><DT
>Context Switches</DT
><DD
><P
>A context switch happens when the CPU stops running one
                process and starts running another.  Because each context switch
                requires the operating system to take control of the CPU,
                excessive context switches and high levels of system-level CPU
                consumption tend to go together.</P
></DD
><DT
>Interrupts</DT
><DD
><P
>As the name implies, interrupts are situations where the
                processing being performed by the CPU is abruptly changed.
                Interrupts generally occur due to hardware activity (such as an
                I/O device completing an I/O operation) or due to software (such
                as software interrupts that control application processing).
                Because interrupts must be serviced at a system level, high
                interrupt rates lead to higher system-level CPU
                consumption.</P
></DD
><DT
>Runnable Processes</DT
><DD
><P
>A process may be in different states.  For example, it may
                be:</P
><P
></P
><UL
><LI
><P
>Waiting for an I/O operation to complete</P
></LI
><LI
><P
>Waiting for the memory management subsystem to handle a
                    page fault</P
></LI
></UL
><P
>In these cases, the process has no need for the CPU.</P
><P
>However, eventually the process state changes, and the
                process becomes runnable.  As the name implies, a runnable
                process is one that is capable of getting work done as soon as
                it is scheduled to receive CPU time.  However, if more than one
                process is runnable at any given time, all but
                one<A
NAME="AEN1016"
HREF="#FTN.AEN1016"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
> of the runnable processes must wait
                for their turn at the CPU.  By monitoring the number of runnable
                processes, it is possible to determine how CPU-bound your system
                is.</P
></DD
></DL
></DIV
><P
>Other performance metrics that reflect an impact on CPU
          utilization tend to include different services the operating system
          provides to processes.  They may include statistics on memory
          management, I/O processing, and so on.  These statistics also reveal
          that, when system performance is monitored, there are no boundaries
          between the different statistics.  In other words, CPU utilization
          statistics may end up pointing to a problem in the I/O subsystem, or
          memory utilization statistics may reveal an application design
          flaw.</P
><P
>Therefore, when monitoring system performance, it is not possible
          to examine any one statistic in complete isolation; only by examining
          the overall picture it it possible to extract meaningful information
          from any performance statistics you gather.</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-RESOURCE-WHAT-TO-MONITOR-BANDWIDTH"
>2.4.2. Monitoring Bandwidth</A
></H2
><P
>Monitoring bandwidth is more difficult than the other resources
          described here.  The reason for this is due to the fact that
          performance statistics tend to be device-based, while most of the
          places where bandwidth is important tend to be the buses that connect
          devices.  In those instances where more than one device shares a
          common bus, you might see reasonable statistics for each device, but
          the aggregate load those devices place on the bus would be much
          greater.</P
><P
>Another challenge to monitoring bandwidth is that there can be
          circumstances where statistics for the devices themselves may not be
          available.  This is particularly true for system expansion buses and
          datapaths<A
NAME="AEN1034"
HREF="#FTN.AEN1034"
><SPAN
CLASS="footnote"
>[2]</SPAN
></A
>.  However, even though 100%
          accurate bandwidth-related statistics may not always be available,
          there is often enough information to make some level of analysis
          possible, particularly when related statistics are taken into
          account.</P
><P
>Some of the more common bandwidth-related statistics are:</P
><P
></P
><DIV
CLASS="VARIABLELIST"
><DL
><DT
>Bytes received/sent</DT
><DD
><P
>Network interface statistics provide an indication of the
                bandwidth utilization of one of the more visible buses &#8212;
                the network.</P
></DD
><DT
>Interface counts and rates</DT
><DD
><P
>These network-related statistics can give indications of
                excessive collisions, transmit and receive errors, and more.
                Through the use of these statistics (particularly if the
                statistics are available for more than one system on your
                network), it is possible to perform a modicum of network
                troubleshooting even before the more common network diagnostic
                tools are used.</P
></DD
><DT
>Transfers per Second</DT
><DD
><P
>Normally collected for block I/O devices, such as disk and
                high-performance tape drives, this statistic is a good way of
                determining whether a particular device's bandwidth limit is
                being reached.  Due to their electromechanical nature, disk and
                tape drives can only perform so many I/O operations every
                second; their performance degrades rapidly as this limit is
                reached.</P
></DD
></DL
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-RESOURCE-WHAT-TO-MONITOR-MEMORY"
>2.4.3. Monitoring Memory</A
></H2
><P
>If there is one area where a wealth of performance statistics can
          be found, it is in the area of monitoring memory utilization.  Due to
          the inherent complexity of today's demand-paged virtual memory
          operating systems, memory utilization statistics are many and varied.
          It is here that the majority of a system administrator's work with
          resource management takes place.</P
><P
>The following statistics represent a cursory overview of
          commonly-found memory management statistics:</P
><P
></P
><DIV
CLASS="VARIABLELIST"
><DL
><DT
>Page Ins/Page Outs</DT
><DD
><P
>These statistics make it possible to gauge the flow of pages
                from system memory to attached mass storage devices (usually
                disk drives).  High rates for both of these statistics can mean
                that the system is short of physical memory and is
                <I
CLASS="FIRSTTERM"
>thrashing</I
>, or spending more system
                resources on moving pages into and out of memory than on
                actually running applications.</P
></DD
><DT
>Active/Inactive Pages</DT
><DD
><P
>These statistics show how heavily memory-resident pages are
                used.  A lack of inactive pages can point toward a shortage of
                physical memory.</P
></DD
><DT
>Free, Shared, Buffered, and Cached Pages</DT
><DD
><P
>These statistics provide additional detail over the more
              simplistic active/inactive page statistics.  By using these
              statistics, it is possible to determine the overall mix of memory
              utilization.</P
></DD
><DT
>Swap Ins/Swap Outs</DT
><DD
><P
>These statistics show the system's overall swapping
                behavior.  Excessive rates here can point to physical memory
                shortages.</P
></DD
></DL
></DIV
><P
>Successfully monitoring memory utilization requires a good
          understanding of how demand-paged virtual memory operating systems
          work.  While such a subject alone could take up an entire book, the
          basic concepts are discussed in <A
HREF="ch-memory.html"
>Chapter 4 <I
>Physical and Virtual Memory</I
></A
>.  This
          chapter, along with time spent actually monitoring a system, gives you
          the the necessary building blocks to learn more about this
          subject.</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-RESOURCE-WHAT-TO-MONITOR-STORAGE"
>2.4.4. Monitoring Storage</A
></H2
><P
>Monitoring storage normally takes place at two different
          levels:</P
><P
></P
><UL
><LI
><P
>Monitoring for sufficient disk space</P
></LI
><LI
><P
>Monitoring for storage-related performance problems</P
></LI
></UL
><P
>The reason for this is that it is possible to have dire problems
          in one area and no problems whatsoever in the other.  For example, it
          is possible to cause a disk drive to run out of disk space without
          once causing any kind of performance-related problems.  Likewise, it
          is possible to have a disk drive that has 99% free space, yet is being
          pushed past its limits in terms of performance.</P
><P
>However, it is more likely that the average system experiences
          varying degrees of resource shortages in both areas.  Because of this,
          it is also likely that &#8212; to some extent &#8212; problems in one
          area impact the other.  Most often this type of interaction takes the
          form of poorer and poorer I/O performance as a disk drive nears 0%
          free space although, in cases of extreme I/O loads, it might be
          possible to slow I/O throughput to such a level that applications no
          longer run properly.</P
><P
>In any case, the following statistics are useful for monitoring
          storage:</P
><P
></P
><DIV
CLASS="VARIABLELIST"
><DL
><DT
>Free Space</DT
><DD
><P
>Free space is probably the one resource all system
                administrators watch closely; it would be a rare administrator
                that never checks on free space (or has some automated way of
                doing so).</P
></DD
><DT
>File System-Related Statistics</DT
><DD
><P
>These statistics (such as number of files/directories,
                average file size, etc.) provide additional detail over a single
                free space percentage.  As such, these statistics make it
                possible for system administrators to configure the system to
                give the best performance, as the I/O load imposed by a file
                system full of many small files is not the same as that imposed
                by a file system filled with a single massive file.</P
></DD
><DT
>Transfers per Second</DT
><DD
><P
>This statistic is a good way of determining whether a
                particular device's bandwidth limitations are being
                reached.</P
></DD
><DT
>Reads/Writes per Second</DT
><DD
><P
>A slightly more detailed breakdown of transfers per second,
                these statistics allow the system administrator to more fully
                understand the nature of the I/O loads a storage device is
                experiencing.  This can be critical, as some storage
                technologies have widely different performance characteristics
                for read versus write operations.</P
></DD
></DL
></DIV
></DIV
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN1016"
HREF="s1-resource-what-to-monitor.html#AEN1016"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>Assuming a single-processor computer
                system.</P
></TD
></TR
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN1034"
HREF="s1-resource-what-to-monitor.html#AEN1034"
><SPAN
CLASS="footnote"
>[2]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>More information on buses, datapaths, and
          bandwidth is available in <A
HREF="ch-bandwidth.html"
>Chapter 3 <I
>Bandwidth and Processing Power</I
></A
>.</P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-resource-capacity.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-resource-rhlspec.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Monitoring System Capacity</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="ch-resource.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Red Hat Enterprise Linux-Specific Information</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>