<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Storage Addressing Concepts</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Managing Storage"
HREF="ch-storage.html"><LINK
REL="PREVIOUS"
TITLE="Managing Storage"
HREF="ch-storage.html"><LINK
REL="NEXT"
TITLE="Mass Storage Device Interfaces"
HREF="s1-storage-interface.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Introduction to System Administration</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="ch-storage.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 5. Managing Storage</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-storage-interface.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-STORAGE-DATA-ADDR"
>5.2. Storage Addressing Concepts</A
></H1
><P
>The configuration of disk platters, heads, and access arms makes it
        possible to position the head over any part of any surface of any
        platter in the mass storage device.  However, this is not sufficient; to
        use this storage capacity, we must have some method of giving addresses
        to uniform-sized parts of the available storage.</P
><P
>There is one final aspect to this process that is required.
        Consider all the tracks in the many cylinders present in a typical mass
        storage device.  Because the tracks have varying diameters, their
        circumference also varies.  Therefore, if storage was addressed only to
        the track level, each track would have different amounts of data &#8212;
        track #0 (being near the center of the platter) might hold 10,827 bytes,
        while track #1,258 (near the outside edge of the platter) might hold
        15,382 bytes.</P
><P
>The solution is to divide each track into multiple
        <I
CLASS="FIRSTTERM"
>sectors</I
> or <I
CLASS="FIRSTTERM"
>blocks</I
> of
        consistently-sized (often 512 bytes) segments of storage.  The result is
        that each track contains a set number<A
NAME="AEN3147"
HREF="#FTN.AEN3147"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
> of sectors.</P
><P
>A side effect of this is that every track contains unused space
        &#8212; the space between the sectors.  Despite the constant number of
        sectors in each track, the amount of unused space varies &#8212;
        relatively little unused space in the inner tracks, and a great deal
        more unused space in the outer tracks.  In either case, this unused
        space is wasted, as data cannot be stored on it.</P
><P
>However, the advantage offsetting this wasted space is that
        effectively addressing the storage on a mass storage device is now
        possible.  In fact, there are two methods of addressing &#8212;
        geometry-based addressing and block-based addressing.</P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-STORAGE-DATA-ADDR-GEO"
>5.2.1. Geometry-Based Addressing</A
></H2
><P
>The term <I
CLASS="FIRSTTERM"
>geometry-based addressing</I
> refers
          to the fact that mass storage devices actually store data at a
          specific physical spot on the storage medium.  In the case of the
          devices being described here, this refers to three specific items that
          define a specific point on the device's disk platters:</P
><P
></P
><UL
><LI
><P
>Cylinder</P
></LI
><LI
><P
>Head</P
></LI
><LI
><P
>Sector</P
></LI
></UL
><P
>The following sections describe how a hypothetical address can
          describe a specific physical location on the storage medium.</P
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-STORAGE-DATA-ADDR-GEO-CYL"
>5.2.1.1. Cylinder</A
></H3
><P
>As stated earlier, the cylinder denotes a specific position of
            the access arm (and therefore, the read/write heads).  By specifying
            a particular cylinder, we are eliminating all other cylinders,
            reducing our search to only one track for each surface in the mass
            storage device.</P
><DIV
CLASS="TABLE"
><A
NAME="TB-STORAGE-CYL"
></A
><TABLE
BORDER="1"
BGCOLOR="#DCDCDC"
CELLSPACING="0"
CELLPADDING="4"
CLASS="CALSTABLE"
><THEAD
><TR
><TH
>Cylinder</TH
><TH
>Head</TH
><TH
>Sector</TH
></TR
></THEAD
><TBODY
><TR
><TD
>1014</TD
><TD
><VAR
CLASS="REPLACEABLE"
>X</VAR
></TD
><TD
><VAR
CLASS="REPLACEABLE"
>X</VAR
></TD
></TR
></TBODY
></TABLE
><P
><B
>Table 5-1. Storage Addressing</B
></P
></DIV
><P
>In <A
HREF="s1-storage-data-addr.html#TB-STORAGE-CYL"
>Table 5-1</A
>, the first part of a
            geometry-based address has been filled in.  Two more components to
            this address &#8212; the head and sector &#8212; remain
            undefined.</P
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-STORAGE-DATA-ADDR-GEO-HEAD"
>5.2.1.2. Head</A
></H3
><P
>Although in the strictest sense we are selecting a particular
            disk platter, because each surface has a read/write head dedicated
            to it, it is easier to think in terms of interacting with a specific
            head.  In fact, the device's underlying electronics actually select
            one head and &#8212; deselecting the rest &#8212; only interact with
            the selected head for the duration of the I/O operation.  All other
            tracks that make up the current cylinder have now been
            eliminated.</P
><DIV
CLASS="TABLE"
><A
NAME="TB-STORAGE-HEAD"
></A
><TABLE
BORDER="1"
BGCOLOR="#DCDCDC"
CELLSPACING="0"
CELLPADDING="4"
CLASS="CALSTABLE"
><THEAD
><TR
><TH
>Cylinder</TH
><TH
>Head</TH
><TH
>Sector</TH
></TR
></THEAD
><TBODY
><TR
><TD
>1014</TD
><TD
>2</TD
><TD
><VAR
CLASS="REPLACEABLE"
>X</VAR
></TD
></TR
></TBODY
></TABLE
><P
><B
>Table 5-2. Storage Addressing</B
></P
></DIV
><P
>In <A
HREF="s1-storage-data-addr.html#TB-STORAGE-HEAD"
>Table 5-2</A
>, the first two parts of a
            geometry-based address have been filled in.  One final component to
            this address &#8212; the sector &#8212; remains undefined.</P
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-STORAGE-DATA-ADDR-GEO-SECTOR"
>5.2.1.3. Sector</A
></H3
><P
>By specifying a particular sector, we have completed the
            addressing, and have uniquely identified the desired block of
            data.</P
><DIV
CLASS="TABLE"
><A
NAME="TB-STORAGE-SECTOR"
></A
><TABLE
BORDER="1"
BGCOLOR="#DCDCDC"
CELLSPACING="0"
CELLPADDING="4"
CLASS="CALSTABLE"
><THEAD
><TR
><TH
>Cylinder</TH
><TH
>Head</TH
><TH
>Sector</TH
></TR
></THEAD
><TBODY
><TR
><TD
>1014</TD
><TD
>2</TD
><TD
>12</TD
></TR
></TBODY
></TABLE
><P
><B
>Table 5-3. Storage Addressing</B
></P
></DIV
><P
>In <A
HREF="s1-storage-data-addr.html#TB-STORAGE-SECTOR"
>Table 5-3</A
>, the complete
            geometry-based address has been filled in.  This address identifies
            the location of one specific block out of all the other blocks on
            this device.</P
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-STORAGE-DATA-ADDR-GEO-PROBS"
>5.2.1.4. Problems with Geometry-Based Addressing</A
></H3
><P
>While geometry-based addressing is straightforward, there is an
            area of ambiguity that can cause problems.  The ambiguity is in
            numbering the cylinders, heads, and sectors.</P
><P
>It is true that each geometry-based address uniquely identifies
            one specific data block, but that only applies if the numbering
            scheme for the cylinders, heads, and sectors is not changed.  If the
            numbering scheme changes (such as when the hardware/software
            interacting with the storage device changes), then the mapping
            between geometry-based addresses and their corresponding data blocks
            can change, making it impossible to access the desired data.</P
><P
>Because of this potential for ambiguity, a different approach to
            addressing was developed.  The next section describes it in more
            detail.</P
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-STORAGE-DATA-ADDR-LBA"
>5.2.2. Block-Based Addressing</A
></H2
><P
><I
CLASS="FIRSTTERM"
>Block-based addressing</I
> is much more
          straightforward than geometry-based addressing.  With block-based
          addressing, every data block is given a unique number.  This number is
          passed from the computer to the mass storage device, which then
          internally performs the conversion to the geometry-based address
          required by the device's control circuitry.</P
><P
>Because the conversion to a geometry-based address is always done
          by the device itself, it is always consistent, eliminating the problem
          inherent with giving the device geometry-based addressing.</P
></DIV
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN3147"
HREF="s1-storage-data-addr.html#AEN3147"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>While early mass
        storage devices used the same number of sectors for every track, later
        devices divided the range of cylinders into different "zones," with each
        zone having a different number of sectors per track.  The reason for
        this is to take advantage of the additional space between sectors in the
        outer cylinders, where there is more unused space between
        sectors.</P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="ch-storage.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-storage-interface.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Managing Storage</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="ch-storage.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Mass Storage Device Interfaces</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>