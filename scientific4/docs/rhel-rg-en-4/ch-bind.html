<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Berkeley Internet Name Domain (BIND)</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Network Services Reference"
HREF="pt-network-services-reference.html"><LINK
REL="PREVIOUS"
TITLE="Additional Resources"
HREF="s1-email-additional-resources.html"><LINK
REL="NEXT"
TITLE="/etc/named.conf"
HREF="s1-bind-namedconf.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="CHAPTER"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Reference Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-email-additional-resources.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-bind-namedconf.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="CHAPTER"
><H1
><A
NAME="CH-BIND"
></A
>Chapter 12. Berkeley Internet Name Domain (BIND)</H1
><P
>      On most modern networks, including the Internet, users locate other
      computers by name. This frees users from the daunting task of remembering
      the numerical network address of network resources. The most effective way
      to configure a network to allow such name-based connections is to set up a
      <I
CLASS="FIRSTTERM"
>Domain Name Service</I
> (<I
CLASS="FIRSTTERM"
>DNS</I
>) or a
      <I
CLASS="FIRSTTERM"
>nameserver</I
>, which resolves hostnames on the network
      to numerical addresses and vice versa.
    </P
><P
>      This chapter reviews the nameserver included in Red Hat Enterprise Linux, the
      <I
CLASS="FIRSTTERM"
>Berkeley Internet Name Domain</I
>
      (<I
CLASS="FIRSTTERM"
>BIND</I
>) DNS server, with an emphasis on the
      structure of its configuration files and how it may be administered both
      locally and remotely.
    </P
><P
>      For instructions on configuring BIND using the graphical
      <B
CLASS="APPLICATION"
>Domain Name Service Configuration Tool</B
>
      (<TT
CLASS="COMMAND"
>redhat-config-bind</TT
>), refer to the chapter called
      <I
CLASS="CITETITLE"
>BIND Configuration</I
> in the
      <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux System Administration Guide</I
>.
    </P
><DIV
CLASS="WARNING"
><P
></P
><TABLE
CLASS="WARNING"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/warning.png"
HSPACE="5"
ALT="Warning"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Warning</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>	If using the <B
CLASS="APPLICATION"
>Domain Name Service Configuration Tool</B
>, do not manually
	edit any BIND configuration files as all changes are overwritten the
	next time the <B
CLASS="APPLICATION"
>Domain Name Service Configuration Tool</B
> is used.
      </P
></TD
></TR
></TABLE
></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-BIND-INTRODUCTION"
>12.1. Introduction to DNS</A
></H1
><P
>	When hosts on a network connect to one another via a hostname, also
	called a <I
CLASS="FIRSTTERM"
>fully qualified domain name (FQDN)</I
>, 
	DNS is used to associate the names of machines to the IP address for the
	host.
      </P
><P
>	Use of DNS and FQDNs also has advantages for system
	administrators, allowing the flexibility to change the IP address for a
	host without effecting name-based queries to the machine. Conversely,
	administrators can shuffle which machines handle a name-based query.
      </P
><P
>	DNS is normally implemented using centralized servers that are
	authoritative for some domains and refer to other DNS servers for other
	domains.
      </P
><P
>	When a client host requests information from a nameserver, it usually
	connects to port 53. The nameserver then attempts to resolve the FQDN
	based on its resolver library, which may contain authoritative
	information about the host requested or cached data from an earlier
	query. If the nameserver does not already have the answer in its
	resolver library, it queries other nameservers, called <I
CLASS="FIRSTTERM"
>root
	nameservers</I
>, to determine which nameservers are
	authoritative for the FQDN in question. Then, with that information, it
	queries the authoritative nameservers to determine the IP address of the
	requested host. If performing a reverse lookup, the same procedure is
	used, except the query is made with an unknown IP address rather than a
	name.
      </P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-BIND-INTRODUCTION-ZONES"
>12.1.1. Nameserver Zones</A
></H2
><P
>	  On the Internet, the FQDN of a host can be broken down into different
	  sections. These sections are organized into a hierarchy (much like a
	  tree), with a main trunk, primary branches, secondary branches, and so
	  forth. Consider the following FQDN:
	</P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
><SAMP
CLASS="COMPUTEROUTPUT"
>bob.sales.example.com</SAMP
></PRE
></TD
></TR
></TABLE
><P
>	  When looking at how an FQDN is resolved to find the IP address that
	  relates to a particular system, read the name from right to left, with
	  each level of the hierarchy divided by periods
	  (<SAMP
CLASS="COMPUTEROUTPUT"
>.</SAMP
>). In this example,
	  <SAMP
CLASS="COMPUTEROUTPUT"
>com</SAMP
> defines the <I
CLASS="FIRSTTERM"
>top level
	  domain</I
> for this FQDN. The name
	  <SAMP
CLASS="COMPUTEROUTPUT"
>example</SAMP
> is a sub-domain under
	  <SAMP
CLASS="COMPUTEROUTPUT"
>com</SAMP
>, while
	  <SAMP
CLASS="COMPUTEROUTPUT"
>sales</SAMP
> is a sub-domain under
	  <SAMP
CLASS="COMPUTEROUTPUT"
>example</SAMP
>. The name furthest to the
	  left, <SAMP
CLASS="COMPUTEROUTPUT"
>bob</SAMP
>, identifies a specific
	  machine hostname.
	</P
><P
>	  Except for the hostname, each section is a called a
	  <I
CLASS="FIRSTTERM"
>zone</I
>, which defines a specific
	  <I
CLASS="FIRSTTERM"
>namespace</I
>. A namespace controls the naming of
	  the sub-domains to its left. While this example only contains two
	  sub-domains, a FQDN must contain at least one sub-domain but may
	  include many more, depending upon how the namespace is organized.
	</P
><P
>	  Zones are defined on authoritative nameservers through the use of
	  <I
CLASS="FIRSTTERM"
>zone files</I
>, which describe the namespace of
	  that zone, the mail servers to be used for a particular domain or
	  sub-domain, and more. Zone files are stored on <I
CLASS="FIRSTTERM"
>primary
	  nameservers</I
> (also called <I
CLASS="FIRSTTERM"
>master
	  nameservers</I
>), which are truly authoritative and where
	  changes are made to the files, and <I
CLASS="FIRSTTERM"
>secondary
	  nameservers</I
> (also called <I
CLASS="FIRSTTERM"
>slave
	  nameservers</I
>), which receive their zone files from the
	  primary nameservers. Any nameserver can be a primary and secondary
	  nameserver for different zones at the same time, and they may also be
	  considered authoritative for multiple zones. It all depends on how the
	  nameserver is configured.
	</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-BIND-INTRODUCTION-NAMESERVERS"
>12.1.2. Nameserver Types</A
></H2
><P
>	  There are four primary nameserver configuration types:
	</P
><P
></P
><UL
><LI
><P
><I
CLASS="EMPHASIS"
>master</I
> &#8212; Stores original and
	      authoritative zone records for a namespace, and answers queries
	      about the namespace from other nameservers.
	    </P
></LI
><LI
><P
><I
CLASS="EMPHASIS"
>slave</I
> &#8212; Answers queries from other
	      nameservers concerning namespaces for which it is considered an
	      authority. However, slave nameservers get their namespace
	      information from master nameservers.
	    </P
></LI
><LI
><P
><I
CLASS="EMPHASIS"
>caching-only</I
> &#8212; Offers name to IP
	      resolution services but is not authoritative for any
	      zones. Answers for all resolutions are cached in memory for a
	      fixed period of time, which is specified by the retrieved zone
	      record.
	    </P
></LI
><LI
><P
><I
CLASS="EMPHASIS"
>forwarding</I
> &#8212; Forwards requests to a
	      specific list of nameservers for name resolution. If none of the
	      specified nameservers can perform the resolution, the resolution
	      fails.
	    </P
></LI
></UL
><P
>	  A nameserver may be one or more of these types. For example, a
	  nameserver can be a master for some zones, a slave for others, and
	  only offer forwarding resolutions for others.
	</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-BIND-INTRODUCTION-BIND"
>12.1.3. BIND as a Nameserver</A
></H2
><P
>	  BIND performs name resolution services through the
	  <TT
CLASS="COMMAND"
>/usr/sbin/named</TT
> daemon.  BIND also includes an
	  administration utility called <TT
CLASS="COMMAND"
>/usr/sbin/rndc</TT
>. More
	  information about <TT
CLASS="COMMAND"
>rndc</TT
> can be found in <A
HREF="s1-bind-rndc.html"
>Section 12.4 <I
>Using <TT
CLASS="COMMAND"
>rndc</TT
></I
></A
>.
	</P
><P
>	  BIND stores its configuration files in the following locations:
	</P
><P
></P
><UL
><LI
><P
><TT
CLASS="FILENAME"
>/etc/named.conf</TT
> &#8212; The configuration
	      file for the <TT
CLASS="COMMAND"
>named</TT
> daemon.
	    </P
></LI
><LI
><P
><TT
CLASS="FILENAME"
>/var/named/</TT
> directory &#8212; The <TT
CLASS="COMMAND"
>named</TT
>
	      working directory which stores zone, statistic, and cache files.
	    </P
></LI
></UL
><P
>	  The next few sections review the BIND configuration files in more
	  detail.
	</P
></DIV
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-email-additional-resources.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-bind-namedconf.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Additional Resources</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="pt-network-services-reference.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><TT
CLASS="FILENAME"
>/etc/named.conf</TT
></TD
></TR
></TABLE
></DIV
></BODY
></HTML
>