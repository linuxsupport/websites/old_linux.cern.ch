<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Kerberos</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Security Reference"
HREF="pt-security-reference.html"><LINK
REL="PREVIOUS"
TITLE="Additional Resources"
HREF="s1-iptables-additional-resources.html"><LINK
REL="NEXT"
TITLE="Kerberos Terminology"
HREF="s1-kerberos-terminology.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="CHAPTER"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Reference Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-iptables-additional-resources.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-kerberos-terminology.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="CHAPTER"
><H1
><A
NAME="CH-KERBEROS"
></A
>Chapter 19. Kerberos</H1
><P
>      System security and integrity within a network can be unwieldy. It can
      occupy the time of several administrators just to keep track of what
      services are being run on a network and the manner in which these services
      are used. Moreover, authenticating users to network services can prove
      dangerous when the method used by the protocol is inherently insecure, as
      evidenced by the transfer of unencrypted passwords over a network under
      the FTP and Telnet protocols. Kerberos is a way to eliminate the need for
      protocols that allow unsafe methods of authentication, thereby enhancing
      overall network security.
    </P
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-KERBEROS-DEFINITION"
>19.1. What is Kerberos?</A
></H1
><P
>	Kerberos, a network authentication protocol created by MIT, uses
	symmetric-key cryptography<A
NAME="AEN20410"
HREF="#FTN.AEN20410"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
> to authenticate users to
	network services &#8212; eliminating the need to send passwords over the
	network. When users authenticate to network services using Kerberos,
	unauthorized users attempting to gather passwords by monitoring network
	traffic are effectively thwarted.
      </P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-KERBEROS-ADVANTAGES"
>19.1.1. Advantages of Kerberos</A
></H2
><P
>	Most conventional network services use password-based authentication
	schemes. Such schemes require a user to authenticate to a given network
	server by supplying their username and password. Unfortunately, the
	transmission of authentication information for many services is
	unencrypted. For such a scheme to be secure, the network has to be
	inaccessible to outsiders, and all computers and users on the network
	must be trusted and trustworthy.
      </P
><P
>	Even if this is the case, once a network is connected to the Internet,
	it can no longer be assumed that the network is secure. Any attacker who
	gains access to the network can use a simple packet analyzer, also known
	as a packet sniffer, to intercept usernames and passwords sent in this
	manner, compromising user accounts and the integrity of the entire
	security infrastructure.
      </P
><P
>	The primary design goal of Kerberos is to eliminate the transmission of
	unencrypted passwords across the network. If used properly, Kerberos
	effectively eliminates the threat packet sniffers would otherwise pose
	on a network.
      </P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-KERBEROS-IMPLEMENTATIONS"
>19.1.2. Disadvantages of Kerberos</A
></H2
><P
>	  Although Kerberos removes a common and severe security threat, it may be
	  difficult to implement for a variety of reasons:
	</P
><P
></P
><UL
><LI
><P
>Migrating user passwords from a standard UNIX password
	    database, such as <TT
CLASS="FILENAME"
>/etc/passwd</TT
> or
	    <TT
CLASS="FILENAME"
>/etc/shadow</TT
>, to a Kerberos password database
	    can be tedious, as there is no automated mechanism to perform this
	    task. For more information, refer to question number 2.23 in the
	    online Kerberos FAQ:
	    </P
><P
><A
HREF="http://www.nrl.navy.mil/CCS/people/kenh/kerberos-faq.html#pwconvert"
TARGET="_top"
>	    http://www.nrl.navy.mil/CCS/people/kenh/kerberos-faq.html</A
>
	    </P
></LI
><LI
><P
>Kerberos has only partial compatibility with the Pluggable
	    Authentication Modules (PAM) system used by most Red Hat Enterprise Linux servers. For
	    more information about this issue, refer to <A
HREF="s1-kerberos-pam.html"
>Section 19.4 <I
>Kerberos and PAM</I
></A
>.
	  </P
></LI
><LI
><P
>Kerberos assumes that each user is trusted but is using an
	    untrusted host on an untrusted network. Its primary goal is to
	    prevent unencrypted passwords from being sent across that
	    network. However, if anyone other than the proper user has access to
	    the one host that issues tickets used for authentication &#8212;
	    called the <I
CLASS="FIRSTTERM"
>key distribution center</I
>
	    (<I
CLASS="FIRSTTERM"
>KDC</I
>) &#8212; the entire Kerberos
	    authentication system is at risk.
	  </P
></LI
><LI
><P
>For an application to use Kerberos, its source must be modified
	    to make the appropriate calls into the Kerberos
	    libraries. Applications modified in this way are considered to be
	    <I
CLASS="FIRSTTERM"
>kerberized</I
>. For some applications, this can
	    be quite problematic due to the size of the application or its
	    design. For other incompatible applications, changes must be made to
	    the way in which the server and client side communicate. Again, this
	    may require extensive programming.  Closed-source applications that
	    do not have Kerberos support by default are often the most
	    problematic.
	  </P
></LI
><LI
><P
>Kerberos is an all or nothing solution. Once Kerberos is used on
	    the network, any unencrypted passwords transferred to a
	    non-kerberized service is at risk. Thus, the network gains no
	    benefit from the use of Kerberos. To secure a network with Kerberos,
	    one must either use kerberized versions of <I
CLASS="EMPHASIS"
>all</I
>
	    client/server applications which send unencrypted passwords or not
	    use <I
CLASS="EMPHASIS"
>any</I
> such client/server applications at all.
	  </P
></LI
></UL
></DIV
></DIV
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN20410"
HREF="ch-kerberos.html#AEN20410"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>A system where both the client
	and the server share a common key that is used to encrypt and decrypt
	network communication.</P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-iptables-additional-resources.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-kerberos-terminology.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Additional Resources</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="pt-security-reference.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Kerberos Terminology</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>