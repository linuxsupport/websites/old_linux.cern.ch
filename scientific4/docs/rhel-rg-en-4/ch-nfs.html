<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Network File System (NFS)</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Network Services Reference"
HREF="pt-network-services-reference.html"><LINK
REL="PREVIOUS"
TITLE="Additional Resources"
HREF="s1-networkscripts-resources.html"><LINK
REL="NEXT"
TITLE="Starting and Stopping NFS"
HREF="s1-nfs-start.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="CHAPTER"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Reference Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-networkscripts-resources.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-nfs-start.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="CHAPTER"
><H1
><A
NAME="CH-NFS"
></A
>Chapter 9. Network File System (NFS)</H1
><P
>      A <I
CLASS="FIRSTTERM"
>Network File System</I
> (<I
CLASS="FIRSTTERM"
>NFS</I
>)
      allows remote hosts to mount file systems over a network and interact with
      those file systems as though they are mounted locally. This enables system
      administrators to consolidate resources onto centralized servers on the
      network.
    </P
><P
>      This chapter focuses on fundamental NFS concepts and supplemental
      information. For specific instructions regarding the configuration and
      operation of NFS server and client software, refer to the chapter titled
      <I
CLASS="CITETITLE"
>Network File System (NFS) </I
> in the
      <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux System Administration Guide</I
>.
    </P
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-NFS-HOW"
>9.1. How It Works</A
></H1
><P
>	Currently, there are three versions of NFS. NFS version 2 (NFSv2) is
	older and is widely supported. NFS version 3 (NFSv3) has more features,
	including variable size file handling and better error reporting, but is
	not fully compatible with NFSv2 clients. NFS version 4 (NFSv4) includes
	Kerberos security, works through firewalls and on the Internet, no
	longer requires portmapper, supports ACLs, and utilizes stateful
	operations. Red Hat Enterprise Linux supports NFSv2, NFSv3, and NFSv4 clients, and
	when mounting a file system via NFS, Red Hat Enterprise Linux uses NFSv4 by default, if
	the server supports it.
      </P
><P
>	All versions of NFS can use <I
CLASS="FIRSTTERM"
>Transmission Control
	Protocol</I
> (<I
CLASS="FIRSTTERM"
>TCP</I
>) running over an IP
	network, with NFSv4 requiring it. NFSv2 and NFSv3 can use the
	<I
CLASS="FIRSTTERM"
>User Datagram Protocol</I
>
	(<I
CLASS="FIRSTTERM"
>UDP</I
>) running over an IP network to provide a
	stateless network connection between the client and server.
      </P
><P
>	When using NFSv2 or NFSv3 with UDP, the stateless UDP connection under
	normal conditions minimizes network traffic, as the NFS server sends the
	client a cookie after the client is authorized to access the shared
	volume. This cookie is a random value stored on the server's side and is
	passed along with RPC requests from the client. The NFS server can be
	restarted without affecting the clients and the cookie remains
	intact. However, because UDP is stateless, if the server goes down
	unexpectedly, UDP clients continue to saturate the network with requests
	for the server. For this reason, TCP is the preferred protocol when
	connecting to an NFS server.
      </P
><P
>	When using NFSv4, a stateful connection is made, and Kerberos user and
	group authentication with various security levels is optionally
	available. NFSv4 has no interaction with portmapper,
	<TT
CLASS="COMMAND"
>rpc.mountd</TT
>, <TT
CLASS="COMMAND"
>rpc.lockd</TT
>, and
	<TT
CLASS="COMMAND"
>rpc.statd</TT
>, since they have been rolled
	into the kernel. NFSv4 listens on the well known TCP port 2049.
      </P
><DIV
CLASS="NOTE"
><P
></P
><TABLE
CLASS="NOTE"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/note.png"
HSPACE="5"
ALT="Note"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Note</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>	  TCP is the default transport protocol for NFS under Red Hat Enterprise Linux. Refer to
	  the chapter titled <I
CLASS="CITETITLE"
>Network File System (NFS)</I
> in
	  the <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux System Administration Guide</I
> for more information about
	  connecting to NFS servers using TCP. UDP can be used for compatibility
	  purposes as needed, but is not recommended for wide usage.
	</P
></TD
></TR
></TABLE
></DIV
><P
>	The only time NFS performs authentication is when a client system
	attempts to mount the shared NFS resource. To limit access to the NFS
	service, TCP wrappers are used. TCP wrappers read the
	<TT
CLASS="FILENAME"
>/etc/hosts.allow</TT
> and
	<TT
CLASS="FILENAME"
>/etc/hosts.deny</TT
> files to determine if a particular
	client or network is permitted or denied access to the NFS service. For
	more information on configuring access controls with TCP wrappers, refer
	to <A
HREF="ch-tcpwrappers.html"
>Chapter 17 <I
>TCP Wrappers and <TT
CLASS="COMMAND"
>xinetd</TT
></I
></A
>.
      </P
><P
>	After the client is granted access by TCP wrappers, the NFS server
	refers to its configuration file, <TT
CLASS="FILENAME"
>/etc/exports</TT
>, to
	determine whether the client is allowed to access any of the exported
	file systems. Once access is granted, all file and directory operations
	are available to the user.
      </P
><DIV
CLASS="WARNING"
><P
></P
><TABLE
CLASS="WARNING"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/warning.png"
HSPACE="5"
ALT="Warning"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Warning</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>	  If using NFSv2 or NFSv3, which do not support Kerberos
	  authentication, NFS mount privileges are granted to the client host,
	  not the user. Therefore, exported file systems can be accessed by any
	  user on a client host with access permissions. When configuring the
	  NFS shares, be very careful which hosts get read/write permissions
	  (<TT
CLASS="COMMAND"
>rw</TT
>).
	</P
></TD
></TR
></TABLE
></DIV
><DIV
CLASS="IMPORTANT"
><P
></P
><TABLE
CLASS="IMPORTANT"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/important.png"
HSPACE="5"
ALT="Important"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Important</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>	  In order for NFS to work with a default installation of Red Hat Enterprise Linux with a
	  firewall enabled, IPTables with the default TCP port 2049 must be
	  configured. Without an IPTables configuration, NFS does not function
	  properly.
	</P
><P
>	  The NFS initialization script and <TT
CLASS="COMMAND"
>rpc.nfsd</TT
> process
	  now allow binding to any specified port during system start
	  up. However, this can be error prone if the port is unavailable or
	  conflicts with another daemon.
	</P
></TD
></TR
></TABLE
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-NFS-HOW-DAEMONS"
>9.1.1. Required Services</A
></H2
><P
>	  Red Hat Enterprise Linux uses a combination of kernel-level support and daemon processes
	  to provide NFS file sharing. NFSv2 and NFSv3 rely on <I
CLASS="FIRSTTERM"
>Remote
	  Procedure Calls</I
> (<I
CLASS="FIRSTTERM"
>RPC</I
>) to encode and decode
	  requests between clients and servers. RPC services under Linux are
	  controlled by the <TT
CLASS="COMMAND"
>portmap</TT
> service. To share or
	  mount NFS file systems, the following services work together,
	  depending on which version of NFS is implemented:
	</P
><P
></P
><UL
><LI
><P
><TT
CLASS="COMMAND"
>nfs</TT
> &#8212; Starts the appropriate RPC 
	      processes to service requests for shared NFS file systems.
	    </P
></LI
><LI
><P
><TT
CLASS="COMMAND"
>nfslock</TT
> &#8212; An optional service that
	      starts the appropriate RPC processes to allow NFS clients to lock
	      files on the server.
	    </P
></LI
><LI
><P
><TT
CLASS="COMMAND"
>portmap</TT
> &#8212; The RPC service for Linux;
	      it responds to requests for RPC services and sets up connections
	      to the requested RPC service. This is not used with NFSv4.
	    </P
></LI
></UL
><P
>	  The following RPC processes facilitate NFS services:
	</P
><P
></P
><UL
><LI
><P
><TT
CLASS="COMMAND"
>rpc.mountd</TT
> &#8212; This process receives
	      mount requests from NFS clients and verifies the requested file
	      system is currently exported. This process is started automatically
	      by the <TT
CLASS="COMMAND"
>nfs</TT
> service
	      and does not require user configuration. This is not used with NFSv4.
	    </P
></LI
><LI
><P
><TT
CLASS="COMMAND"
>rpc.nfsd</TT
> &#8212; This process is the NFS
	      server. It works with the Linux kernel to meet the dynamic demands
	      of NFS clients, such as providing server threads each time an NFS
	      client connects. This process corresponds to the
	      <TT
CLASS="COMMAND"
>nfs</TT
> service.
	    </P
></LI
><LI
><P
><TT
CLASS="COMMAND"
>rpc.lockd</TT
> &#8212; An optional process that
	      allows NFS clients to lock files on the server. This process
	      corresponds to the <TT
CLASS="COMMAND"
>nfslock</TT
> service. This is not
	      used with NFSv4.
	    </P
></LI
><LI
><P
><TT
CLASS="COMMAND"
>rpc.statd</TT
> &#8212; This process implements the
	      <I
CLASS="FIRSTTERM"
>Network Status Monitor (NSM)</I
> RPC protocol
	      which notifies NFS clients when an NFS server is restarted without
	      being gracefully brought down. This process is started automatically
	      by the <TT
CLASS="COMMAND"
>nfslock</TT
> service and does not require user
	      configuration. This is not used with NFSv4.
	    </P
></LI
><LI
><P
><TT
CLASS="COMMAND"
>rpc.rquotad</TT
> &#8212; This process provides
	      user quota information for remote users. This process is started
	      automatically by the <TT
CLASS="COMMAND"
>nfs</TT
> service and does not
	      require user configuration.
	    </P
></LI
><LI
><P
><TT
CLASS="COMMAND"
>rpc.idmapd</TT
> &#8212; This process provides
	      NFSv4 client and server upcalls which map between on-the-wire
	      NFSv4 names (which are strings in the form of user@domain) and
	      local UIDs and GIDs. For <TT
CLASS="COMMAND"
>idmapd</TT
> to function
	      with NFSv4, the <TT
CLASS="FILENAME"
>/etc/idmapd.conf</TT
> must be
	      configured. This service is required for use with NFSv4.
	    </P
></LI
><LI
><P
><TT
CLASS="COMMAND"
>rpc.svcgssd</TT
> &#8212; This process provides
	      the server transport mechanism for the authentication process
	      (Kerberos Version 5) with NFSv4. This service is required for use
	      with NFSv4.
	    </P
></LI
><LI
><P
><TT
CLASS="COMMAND"
>rpc.gssd</TT
> &#8212; This process provides the
	      client transport mechanism for the authentication process
	      (Kerberos Version 5) with NFSv4. This service is required for use
	      with NFSv4.
	    </P
></LI
></UL
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-NFS-METHODOLOGY-PORTMAP"
>9.1.2. NFS and <TT
CLASS="COMMAND"
>portmap</TT
></A
></H2
><DIV
CLASS="NOTE"
><P
></P
><TABLE
CLASS="NOTE"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/note.png"
HSPACE="5"
ALT="Note"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Note</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>	    The following section only applies to NFSv2 or NFSv3 implementations
	    that require the <TT
CLASS="COMMAND"
>portmap</TT
> service for backward
	    compatibility.
	  </P
></TD
></TR
></TABLE
></DIV
><P
>	  The <TT
CLASS="COMMAND"
>portmap</TT
> service under Linux maps RPC requests
	  to the correct services. RPC processes notify
	  <TT
CLASS="COMMAND"
>portmap</TT
> when they start, revealing the port number
	  they are monitoring and the RPC program numbers they expect to
	  serve. The client system then contacts <TT
CLASS="COMMAND"
>portmap</TT
> on
	  the server with a particular RPC program number. The
	  <TT
CLASS="COMMAND"
>portmap</TT
> service redirects the client to the proper
	  port number so it can communicate with the requested service.
	</P
><P
>	  Because RPC-based services rely on <TT
CLASS="COMMAND"
>portmap</TT
> to make
	  all connections with incoming client requests,
	  <TT
CLASS="COMMAND"
>portmap</TT
> must be available before any of these
	  services start. 
	</P
><P
>	  The <TT
CLASS="COMMAND"
>portmap</TT
> service uses TCP wrappers for access
	  control, and access control rules for <TT
CLASS="COMMAND"
>portmap</TT
>
	  affect <I
CLASS="EMPHASIS"
>all</I
> RPC-based services. Alternatively, it
	  is possible to specify access control rules for each of the NFS RPC
	  daemons. The man pages for <TT
CLASS="COMMAND"
>rpc.mountd</TT
> and
	  <TT
CLASS="COMMAND"
>rpc.statd</TT
> contain information regarding the precise
	  syntax for these rules.
	</P
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-NFS-METHODOLOGY-PORTMAP-RPCINFO"
>9.1.2.1. Troubleshooting NFS and <TT
CLASS="COMMAND"
>portmap</TT
></A
></H3
><P
>	    Because <TT
CLASS="COMMAND"
>portmap</TT
> provides coordination between RPC
	    services and the port numbers used to communicate with them, it is
	    useful to view the status of current RPC services using
	    <TT
CLASS="COMMAND"
>portmap</TT
> when troubleshooting. The
	    <TT
CLASS="COMMAND"
>rpcinfo</TT
> command shows each RPC-based service with
	    port numbers, an RPC program number, a version number, and an IP
	    protocol type (TCP or UDP).
	  </P
><P
>	    To make sure the proper NFS RPC-based services are enabled for
	    <TT
CLASS="COMMAND"
>portmap</TT
>, issue the following command as root:
	  </P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
><TT
CLASS="COMMAND"
>rpcinfo -p</TT
></PRE
></TD
></TR
></TABLE
><P
>	    The following is sample output from this command:
	  </P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
><SAMP
CLASS="COMPUTEROUTPUT"
>   program vers proto   port
    100000    2   tcp    111  portmapper
    100000    2   udp    111  portmapper
    100021    1   udp  32774  nlockmgr
    100021    3   udp  32774  nlockmgr
    100021    4   udp  32774  nlockmgr
    100021    1   tcp  34437  nlockmgr
    100021    3   tcp  34437  nlockmgr
    100021    4   tcp  34437  nlockmgr
    100011    1   udp    819  rquotad
    100011    2   udp    819  rquotad
    100011    1   tcp    822  rquotad
    100011    2   tcp    822  rquotad
    100003    2   udp   2049  nfs
    100003    3   udp   2049  nfs
    100003    2   tcp   2049  nfs
    100003    3   tcp   2049  nfs
    100005    1   udp    836  mountd
    100005    1   tcp    839  mountd
    100005    2   udp    836  mountd
    100005    2   tcp    839  mountd
    100005    3   udp    836  mountd
    100005    3   tcp    839  mountd</SAMP
></PRE
></TD
></TR
></TABLE
><P
>	    The output from this command reveals that the correct NFS services
	    are running. If one of the NFS services does not start up correctly,
	    <TT
CLASS="COMMAND"
>portmap</TT
> is unable to map RPC requests from
	    clients for that service to the correct port. In many cases, if NFS
	    is not present in <TT
CLASS="COMMAND"
>rpcinfo</TT
> output, restarting NFS
	    causes the service to correctly register with
	    <TT
CLASS="COMMAND"
>portmap</TT
> and begin working. For instructions on
	    starting NFS, refer to <A
HREF="s1-nfs-start.html"
>Section 9.2 <I
>Starting and Stopping NFS</I
></A
>.
	  </P
><P
>	    Other useful options are available for the
	    <TT
CLASS="COMMAND"
>rpcinfo</TT
> command. Refer to the
	    <TT
CLASS="COMMAND"
>rpcinfo</TT
> man page for more information.
	  </P
></DIV
></DIV
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-networkscripts-resources.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-nfs-start.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Additional Resources</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="pt-network-services-reference.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Starting and Stopping NFS</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>