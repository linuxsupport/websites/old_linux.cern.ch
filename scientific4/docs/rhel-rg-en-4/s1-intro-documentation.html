<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Finding Appropriate Documentation</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Introduction"
HREF="ch-intro.html"><LINK
REL="PREVIOUS"
TITLE="Architecture-specific Information"
HREF="s1-archinfo.html"><LINK
REL="NEXT"
TITLE="Document Conventions"
HREF="s1-intro-conventions.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Reference Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-archinfo.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Introduction</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-intro-conventions.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-INTRO-DOCUMENTATION"
>3. Finding Appropriate Documentation</A
></H1
><P
>	You need documentation that is appropriate to your level of Linux
        expertise. Otherwise, you might feel overwhelmed or may not find the
        necessary information to answer any questions. The
        <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Reference Guide</I
> deals with the more technical aspects
        and options of a Red Hat Enterprise Linux system. This section helps you decide whether to
        look in this manual for the information you need or to consider other
        Red Hat Enterprise Linux manuals, including online sources, in your search.
      </P
><P
>        Three different categories of people use Red Hat Enterprise Linux, and each of these
        categories require different sets of documentation and informative
        sources. To help you figure out where you should start, determine your
        own experience level:

      <P
></P
><DIV
CLASS="VARIABLELIST"
><DL
><DT
><I
CLASS="EMPHASIS"
>New to Linux</I
></DT
><DD
><P
>This type of user has never used any Linux (or Linux-like)
              operating system before or has had only limited exposure to Linux.
              They may or may not have experience using other operating systems
              (such as Windows).  Is this you? If so, skip ahead to <A
HREF="s1-intro-documentation.html#S2-INTRO-NEWBIE"
>Section 3.1 <I
>Documentation For First-Time Linux Users</I
></A
>.
            </P
></DD
><DT
><I
CLASS="EMPHASIS"
>Some Linux Experience</I
></DT
><DD
><P
>This type of user has installed and successfully used Linux (but
              not Red Hat Enterprise Linux) before or may have equivalent
              experience with other Linux-like operating systems. Does this
              describe you? If so, turn to <A
HREF="s1-intro-documentation.html#S2-INTRO-EXPERIENCED"
>Section 3.2 <I
>For the More Experienced</I
></A
>.
            </P
></DD
><DT
><I
CLASS="EMPHASIS"
>Experienced User</I
></DT
><DD
><P
>This type of user has installed and successfully used Red Hat Enterprise Linux
              before. If this describes you, turn to <A
HREF="s1-intro-documentation.html#S2-INTRO-GURU"
>Section 3.3 <I
>Documentation for Linux Gurus</I
></A
>.
            </P
></DD
></DL
></DIV
>
      </P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-INTRO-NEWBIE"
>3.1. Documentation For First-Time Linux Users</A
></H2
><P
>          For someone new to Linux, the amount of information available on any
          particular subject, such as printing, starting up the system or
          partitioning a hard drive, can be overwhelming. It helps to initially
          step back and gain a decent base of information centered around how Linux
          works before tackling these kinds of advanced issues.
        </P
><P
>          Your first goal should be to obtain some useful documentation. This
          cannot be stressed enough. Without documentation, you only become
          frustrated at your inability to get a Red Hat Enterprise Linux system working the way
          you want.
        </P
><P
>          You should acquire the following types of Linux documentation:
        </P
><P
></P
><UL
><LI
><P
><I
CLASS="EMPHASIS"
>A brief history of Linux</I
> &#8212; Many aspects
              of Linux are the way they are because of historical precedent. The
              Linux culture is also based on past events, needs, or
              requirements. A basic understanding of the history of Linux helps
              you figure out how to solve many potential problems before you
              actually see them.
	    </P
></LI
><LI
><P
><I
CLASS="EMPHASIS"
>An explanation of how Linux works</I
> &#8212;
              While delving into the most arcane aspects of the Linux kernel is
              not necessary, it is a good idea to know something about how
              Linux is put together. This is particularly important if you have
              been working with other operating systems, as some of the assumptions
              you currently hold about how computers work may not transfer from that
              operating system to Linux.
	    </P
></LI
><LI
><P
><I
CLASS="EMPHASIS"
>An introductory command overview (with
              examples)</I
> &#8212; This is probably the most important
              thing to look for in Linux documentation.  The underlying design
              philosophy for Linux is that it is better to use many small commands
              connected together in different ways than it is to have a few
              large (and complex) commands that do the whole job
              themselves. Without examples that illustrate this approach to
              doing things, you may find yourself intimidated by the sheer
              number of commands available on a Red Hat Enterprise Linux system.
	    </P
><P
>Keep in mind that you do not have to memorize all of the
              available Linux commands. Different techniques exist to help you
              find the specific command you need to accomplish a task. You only
              need to know the general way in which Linux functions, what you
              need to accomplish, and how to access the tool that gives you the
              exact instructions you need to execute the command.
            </P
></LI
></UL
><P
>	  The <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Installation Guide</I
> and the
	  <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Step By Step Guide</I
> are excellent references for helping
	  you get a Red Hat Enterprise Linux system successfully installed and initially
	  configured. The <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Introduction to System Administration</I
> is a great place to
	  start for those learning the basics of system administration. Start
	  with these books and use them to build the base of your
	  knowledge of Red Hat Enterprise Linux. Before long, more complicated concepts begin to
	  make sense because you already grasp the general ideas.
	</P
><P
>          Beyond reading the Red Hat Enterprise Linux manuals, several other excellent
          documentation resources are available for little or no cost:
        </P
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-INTRO-NEWBIE-WEBSITES"
>3.1.1. Introduction to Linux Websites</A
></H3
><P
></P
><UL
><LI
><P
><A
HREF="http://www.redhat.com/"
TARGET="_top"
>http://www.redhat.com/</A
>
                &#8212; On the Red Hat website, you find links to the Linux
                Documentation Project (LDP), online versions of the Red Hat Enterprise Linux
                manuals, FAQs (Frequently Asked Questions), a database which can
                help you find a Linux Users Group near you, technical
                information in the Red Hat Support Knowledge Base, and more.
	      </P
></LI
><LI
><P
><A
HREF="http://www.linuxheadquarters.com/"
TARGET="_top"
>http://www.linuxheadquarters.com/</A
>
		&#8212; The Linux Headquarters website features easy to follow,
		step-by-step guides for a variety of Linux tasks.
	      </P
></LI
></UL
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-INTRO-NEWBIE-NEWSGROUPS"
>3.1.2. Introduction to Linux Newsgroups</A
></H3
><P
>	    You can participate in newsgroups by watching the discussions of
	    others attempting to solve problems, or by actively asking or
	    answering questions. Experienced Linux users are known to be
	    extremely helpful when trying to assist new users with various Linux
	    issues &#8212; especially if you are posing questions in the right
	    venue. If you do not have access to a news reader application, you
	    can access this information via the Web at <A
HREF="http://groups.google.com/"
TARGET="_top"
>http://groups.google.com/</A
>. Dozens of
	    Linux-related newsgroups exist, including the following:
	</P
><P
></P
><UL
><LI
><P
><A
HREF="news:linux.help"
TARGET="_top"
>linux.help</A
> &#8212; A
                  great place to get help from fellow Linux users.
                </P
></LI
><LI
><P
><A
HREF="news:linux.redhat"
TARGET="_top"
>linux.redhat</A
> &#8212;
                  This newsgroup primarily covers Red Hat Enterprise Linux-specific issues.
                </P
></LI
><LI
><P
><A
HREF="news:linux.redhat.install"
TARGET="_top"
>linux.redhat.install</A
>
                  &#8212; Pose installation questions to this newsgroup or
                  search it to see how others solved similar problems.
                </P
></LI
><LI
><P
><A
HREF="news:linux.redhat.misc"
TARGET="_top"
>linux.redhat.misc</A
>
                  &#8212; Questions or requests for help that do not really fit
                  into traditional categories go here.
                </P
></LI
><LI
><P
><A
HREF="news:linux.redhat.rpm"
TARGET="_top"
>linux.redhat.rpm</A
> &#8212; A
		good place to go if you are having trouble using RPM to
		accomplish particular objectives.
                </P
></LI
></UL
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-INTRO-EXPERIENCED"
>3.2. For the More Experienced</A
></H2
><P
>          If you have used other Linux distributions, you probably already have a
          basic grasp of the most frequently used commands.  You may have
          installed your own Linux system, and maybe you have even downloaded and
          built software you found on the Internet. After installing Linux,
          however, configuration issues can be very confusing.
        </P
><P
>	  The <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux System Administration Guide</I
> is designed to help explain the
	  various ways a Red Hat Enterprise Linux system can be configured to meet
	  specific objectives. Use this manual to learn about specific
	  configuration options and how to put them into effect.
	</P
><P
>	  When you are installing software that is not covered in the
	  <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux System Administration Guide</I
>, it is often helpful to see what other
	  people in similar circumstances have done. HOWTO documents from the
	  Linux Documentation Project, available at <A
HREF="http://www.redhat.com/mirrors/LDP/HOWTO/HOWTO-INDEX/howtos.html"
TARGET="_top"
>http://www.redhat.com/mirrors/LDP/HOWTO/HOWTO-INDEX/howtos.html</A
>,
	  document particular aspects of Linux, from low-level kernel esoteric
	  changes to using Linux for amateur radio station work.
	</P
><P
>	  If you are concerned with the finer points and specifics of the Red Hat Enterprise Linux
	  system, the <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Reference Guide</I
> is a great resource.
	</P
><P
>	  If you are concerned about security issues, the
	  <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Security Guide</I
> is a great resource &#8212; explaining
	  in concise terms best strategies and practices for securing Red Hat Enterprise Linux.
	</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-INTRO-GURU"
>3.3. Documentation for Linux Gurus</A
></H2
><P
>	  If you are concerned with the finer points and specifics of the Red Hat Enterprise Linux
	  system, the <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Reference Guide</I
> is a great resource.
	</P
><P
>          If you are a long-time Red Hat Enterprise Linux user, you probably already know that one
	  of the best ways to understand a particular program is to read its source
	  code and/or configuration files. A major advantage of Red Hat Enterprise Linux is the
          availability of the source code for anyone to read.
	</P
><P
>	  Obviously, not everyone is a programmer, so the source code may not
	  be helpful for you. However, if you have the knowledge and skills necessary to
	  read it, the source code holds all of the answers.
	</P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-archinfo.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-intro-conventions.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Architecture-specific Information</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="ch-intro.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Document Conventions</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>