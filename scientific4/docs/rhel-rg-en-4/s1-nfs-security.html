<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Securing NFS</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Network File System (NFS)"
HREF="ch-nfs.html"><LINK
REL="PREVIOUS"
TITLE="NFS Client Configuration Files"
HREF="s1-nfs-client-config.html"><LINK
REL="NEXT"
TITLE="Additional Resources"
HREF="s1-nfs-additional-resources.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Reference Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-nfs-client-config.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 9. Network File System (NFS)</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-nfs-additional-resources.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-NFS-SECURITY"
>9.5. Securing NFS</A
></H1
><P
>      NFS is well suited for sharing entire file systems with a large number
      of known hosts in a transparent manner. However, with ease of use comes
      a variety of potential security problems.
    </P
><P
>      The following points should be considered when exporting NFS file
      systems on a server or mounting them on a client. Doing so minimizes
      NFS security risks and better protects data on the server.
    </P
><P
>      For a concise listing of steps administrators can take to secure NFS
      servers, refer the the chapter titled <I
CLASS="CITETITLE"
>Server
	Security</I
> in the <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Security Guide</I
>.
    </P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-NFS-SECURITY-HOSTS"
>9.5.1. Host Access</A
></H2
><P
>	Depending on which version of NFS you plan to implement, depends on your
	existing network environment, and your security concerns. The following
	sections explain the differences between implementing security measures
	with NFSv2, NFSv3, and NFSv4. If at all possible, use of NFSv4 is
	recommended over other versions of NFS.
      </P
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-NFS-SECURITY-HOSTS-NONNFSV4"
>9.5.1.1. Using NFSv2 or NFSv3</A
></H3
><P
>	    NFS controls who can mount an exported file system based on the host
	    making the mount request, not the user that actually uses the file
	    system. Hosts must be given explicit rights to mount the exported file
	    system. Access control is not possible for users, other than through
	    file and directory permissions. In other words, once a file system is
	    exported via NFS, any user on any remote host connected to the NFS
	    server can access the shared data. To limit the potential risks,
	    administrators often allow read-only access or squash user permissions
	    to a common user and group ID. Unfortunately, these solutions prevent
	    the NFS share from being used in the way it was originally intended.
	  </P
><P
>	    Additionally, if an attacker gains control of the DNS server used by
	    the system exporting the NFS file system, the system associated with a
	    particular hostname or fully qualified domain name can be pointed to
	    an unauthorized machine. At this point, the unauthorized machine
	    <I
CLASS="EMPHASIS"
>is</I
> the system permitted to mount the NFS share,
	    since no username or password information is exchanged to provide
	    additional security for the NFS mount.
	  </P
><P
>	    Wildcards should be used sparingly when exporting directories via NFS
	    as it is possible for the scope of the wildcard to encompass more systems
	    than intended.
	  </P
><P
>	    It is also possible to restrict access to the
	    <TT
CLASS="COMMAND"
>portmap</TT
> service via TCP wrappers. Access to ports
	    used by <TT
CLASS="COMMAND"
>portmap</TT
>, <TT
CLASS="COMMAND"
>rpc.mountd</TT
>, and
	    <TT
CLASS="COMMAND"
>rpc.nfsd</TT
> can also be limited by creating
	    firewall rules with <TT
CLASS="COMMAND"
>iptables</TT
>.
	  </P
><P
>	    For more information on securing NFS and <TT
CLASS="COMMAND"
>portmap</TT
>,
	    refer to the chapter titled <I
CLASS="CITETITLE"
>Server Security</I
> in
	    the <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Security Guide</I
>. Additional information about
	    firewalls can be found in <A
HREF="ch-iptables.html"
>Chapter 18 <I
><TT
CLASS="COMMAND"
>iptables</TT
></I
></A
>.
	  </P
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-NFS-SECURITY-HOSTS-NFSV4"
>9.5.1.2. Using NFSv4</A
></H3
><P
>	    The release of NFSv4 brought a revolution to authentication and
	    security to NFS exports. NFSv4 mandates the implementation of
	    the RPCSEC_GSS kernel module, the Kerberos version 5 GSS-API
	    mechanism, SPKM-3, and LIPKEY. With NFSv4, the mandatory
	    security mechanisms are oriented towards authenticating individual
	    users, and not client machines as used in NFSv2 and NFSv3.
	  </P
><DIV
CLASS="NOTE"
><P
></P
><TABLE
CLASS="NOTE"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/note.png"
HSPACE="5"
ALT="Note"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Note</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>	      It is assumed that a Kerberos ticket-granting server (KDC) is
	      installed and configured correctly, prior to configuring an NFSv4
	      server.
	    </P
></TD
></TR
></TABLE
></DIV
><P
>	    NFSv4 includes ACL support based on the Microsoft Windows NT model,
	    not the POSIX model, because of its features and because it is widely
	    deployed. NFSv2 and NFSv3 do not have support for native ACL
	    attributes.
	  </P
><P
>	    Another important security feature of NFSv4 is its removal of the
	    <TT
CLASS="FILENAME"
>rpc.mountd</TT
> daemon. The
	    <TT
CLASS="FILENAME"
>rpc.mountd</TT
> daemon presented possible security
	    holes because of the way it dealt with filehandlers.
	  </P
><P
>	    For more information on the RPCSEC_GSS framework, including how
	    <TT
CLASS="COMMAND"
>rpc.svcgssd</TT
> and <TT
CLASS="COMMAND"
>rpc.gssd</TT
>
	    interoperate, refer to <A
HREF="http://www.citi.umich.edu/projects/nfsv4/gssd/"
TARGET="_top"
>http://www.citi.umich.edu/projects/nfsv4/gssd/</A
>.
	  </P
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-NFS-SECURITY-FILES"
>9.5.2. File Permissions</A
></H2
><P
>	  Once the NFS file system is mounted read/write by a remote host, the
	  only protection each shared file has is its permissions. If two users
	  that share the same user ID value mount the same NFS file system, they
	  can modify each others files. Additionally, anyone logged in as root
	  on the client system can use the <TT
CLASS="COMMAND"
>su -</TT
> command to
	  become a user who could access particular files via the NFS share. For
	  more on NFS and user ID conflicts, refer to the chapter titled
	  <I
CLASS="CITETITLE"
>Managing User Accounts and Resource Access</I
> in
	  the <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Introduction to System Administration</I
>.
	</P
><P
>	  By default, access control lists (ACLs) are supported by NFS under
	  Red Hat Enterprise Linux. It is not recommended that this feature be disabled. For more
	  about this feature, refer to the chapter titled <I
CLASS="CITETITLE"
>Network
	  File System (NFS)</I
> in the <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux System Administration Guide</I
>.
	</P
><P
>	  The default behavior when exporting a file system via NFS is to use
	  <I
CLASS="FIRSTTERM"
>root squashing</I
>. This sets the user ID of anyone
	  accessing the NFS share as the root user on their local machine to a
	  value of the server's <SAMP
CLASS="COMPUTEROUTPUT"
>nfsnobody</SAMP
>
	  account. Never turn off root squashing.
	</P
><P
>	  If exporting an NFS share as read-only, consider using the
	  <TT
CLASS="OPTION"
>all_squash</TT
> option, which makes every user accessing
	  the exported file system take the user ID of the
	  <SAMP
CLASS="COMPUTEROUTPUT"
>nfsnobody</SAMP
> user.
	</P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-nfs-client-config.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-nfs-additional-resources.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>NFS Client Configuration Files</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="ch-nfs.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Additional Resources</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>