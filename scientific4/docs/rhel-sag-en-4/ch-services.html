<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Controlling Access to Services</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Network-Related Configuration"
HREF="pt-network-related-config.html"><LINK
REL="PREVIOUS"
TITLE="Activating the iptables Service"
HREF="s1-basic-firewall-activate-iptables.html"><LINK
REL="NEXT"
TITLE="TCP Wrappers"
HREF="s1-services-tcp-wrappers.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="CHAPTER"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: System Administration Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-basic-firewall-activate-iptables.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-services-tcp-wrappers.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="CHAPTER"
><H1
><A
NAME="CH-SERVICES"
></A
>Chapter 20. Controlling Access to Services</H1
><P
>      Maintaining security on your system is extremely important, and one
      approach for this task is to manage access to system services carefully.
      Your system may need to provide open access to particular services (for
      example, <TT
CLASS="COMMAND"
>httpd</TT
> if you are running a Web server).
      However, if you do not need to provide a service, you should turn it off
      to minimize your exposure to possible bug exploits.
    </P
><P
>      There are several different methods for managing access to system
      services.  Decide which method of management to use based on the
      service, your system's configuration, and your level of Linux expertise.
    </P
><P
>      The easiest way to deny access to a service is to turn it off.  Both the
      services managed by <TT
CLASS="COMMAND"
>xinetd</TT
> and the services in the
      <TT
CLASS="FILENAME"
>/etc/rc.d/init.d</TT
> hierarchy (also known as SysV
      services) can be configured to start or stop using three different
      applications:
    </P
><P
></P
><UL
><LI
><P
><B
CLASS="APPLICATION"
>Services Configuration Tool</B
> &#8212; a graphical application
	  that displays a description of each service, displays whether each
	  service is started at boot time (for runlevels 3, 4, and 5), and
	  allows services to be started, stopped, and restarted.
	</P
></LI
><LI
><P
><B
CLASS="APPLICATION"
>ntsysv</B
> &#8212; a text-based application
	  that allows you to configure which services are started at boot time
	  for each runlevel. Non-<TT
CLASS="COMMAND"
>xinetd</TT
> services can not be
	  started, stopped, or restarted using this program.
	</P
></LI
><LI
><P
><TT
CLASS="COMMAND"
>chkconfig</TT
> &#8212; a command line utility that
	  allows you to turn services on and off for the different
	  runlevels. Non-<TT
CLASS="COMMAND"
>xinetd</TT
> services can not be started,
	  stopped, or restarted using this utility.
	</P
></LI
></UL
><P
>      You may find that these tools are easier to use than the alternatives
      &#8212; editing the numerous symbolic links located in the directories
      below <TT
CLASS="FILENAME"
>/etc/rc.d</TT
> by hand or editing the
      <TT
CLASS="COMMAND"
>xinetd</TT
> configuration files in
      <TT
CLASS="FILENAME"
>/etc/xinetd.d</TT
>.
    </P
><P
>      Another way to manage access to system services is by using
      <TT
CLASS="COMMAND"
>iptables</TT
> to configure an IP firewall.  If you are a new
      Linux user, please realize that <TT
CLASS="COMMAND"
>iptables</TT
> may not be the
      best solution for you.  Setting up <TT
CLASS="COMMAND"
>iptables</TT
> can be
      complicated and is best tackled by experienced Linux system
      administrators.
      </P
><P
>      On the other hand, the benefit of using <TT
CLASS="COMMAND"
>iptables</TT
> is
      flexibility.  For example, if you need a customized solution which
      provides certain hosts access to certain services,
      <TT
CLASS="COMMAND"
>iptables</TT
> can provide it for you.  Refer to the
      <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Reference Guide</I
> and the <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Security Guide</I
> for more
      information about <TT
CLASS="COMMAND"
>iptables</TT
>.
    </P
><P
>      Alternatively, if you are looking for a utility to set general access
      rules for your home machine, and/or if you are new to Linux, try the
      <B
CLASS="APPLICATION"
>Security Level Configuration Tool</B
>
      (<TT
CLASS="COMMAND"
>system-config-securitylevel</TT
>), which allows you to
      select the security level for your system, similar to the
      <B
CLASS="GUILABEL"
>Firewall Configuration</B
> screen in the installation
      program. 
    </P
><P
>      Refer to <A
HREF="ch-basic-firewall.html"
>Chapter 19 <I
>Basic Firewall Configuration</I
></A
> for more information.
      If you need more specific firewall rules, refer to the
      <TT
CLASS="COMMAND"
>iptables</TT
> chapter in the
      <I
CLASS="CITETITLE"
>Red Hat Enterprise Linux Reference Guide</I
>.
    </P
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-SERVICES-RUNLEVELS"
>20.1. Runlevels</A
></H1
><P
>	Before you can configure access to services, you must understand Linux
	runlevels. A runlevel is a state, or <I
CLASS="FIRSTTERM"
>mode</I
>, that
	is defined by the services listed in the directory
	<TT
CLASS="FILENAME"
>/etc/rc.d/rc<VAR
CLASS="REPLACEABLE"
>&#60;x&#62;</VAR
>.d</TT
>,
	where <VAR
CLASS="REPLACEABLE"
>&#60;x&#62;</VAR
> is the number of the
	runlevel.
      </P
><P
>	The following runlevels exist:
      </P
><P
></P
><UL
><LI
><P
>0 &#8212; Halt</P
></LI
><LI
><P
>1 &#8212; Single-user mode</P
></LI
><LI
><P
>2 &#8212; Not used (user-definable)</P
></LI
><LI
><P
>3 &#8212; Full multi-user mode</P
></LI
><LI
><P
>4 &#8212; Not used (user-definable)</P
></LI
><LI
><P
>5 &#8212; Full multi-user mode (with an X-based login screen)</P
></LI
><LI
><P
>6 &#8212; Reboot</P
></LI
></UL
><P
>	If you use a text login screen, you are operating in runlevel 3. If you
	use a graphical login screen, you are operating in runlevel 5.
      </P
><P
>	The default runlevel can be changed by modifying the
	<TT
CLASS="FILENAME"
>/etc/inittab</TT
> file, which contains a line near the top
	of the file similar to the following:
      </P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
><SAMP
CLASS="COMPUTEROUTPUT"
>id:5:initdefault:</SAMP
></PRE
></TD
></TR
></TABLE
><P
>	Change the number in this line to the desired runlevel. The change does
	not take effect until you reboot the system.
      </P
><P
>	To change the runlevel immediately, use the command
	<TT
CLASS="COMMAND"
>telinit</TT
> followed by the runlevel number. You must be
	root to use this command. The <TT
CLASS="COMMAND"
>telinit</TT
> command does
	not change the <TT
CLASS="FILENAME"
>/etc/inittab</TT
> file; it only changes
	the runlevel currently running. When the system is rebooted, it
	continues to boot the runlevel as specified in
	<TT
CLASS="FILENAME"
>/etc/inittab</TT
>.
      </P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-basic-firewall-activate-iptables.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-services-tcp-wrappers.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Activating the <TT
CLASS="COMMAND"
>iptables</TT
> Service</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="pt-network-related-config.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>TCP Wrappers</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>