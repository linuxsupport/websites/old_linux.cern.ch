License: distributable
Source: rhel-sag-en-%{version}.tbz
Release: 3
Name: rhel-sag-en
Group: Documentation
URL: http://www.redhat.com/docs/
Version: 4
Summary: Red Hat Enterprise Linux %{version} System Administration Guide
BuildArch: noarch
Buildroot: %{_tmppath}/%{name}-%{version}-buildroot
Requires: htmlview

%description
The Red Hat Enterprise Linux %{version} System Administration Guide 
contains information on how to customize a Red Hat Enterprise Linux 
system. Step-by-step guides include setting up a network interface
card, configuring shared files and directories, managing software
using RPM, configuring a printer, configuring a Web server, and
gathering system information.

%prep
%define _builddir %(mkdir -p %{buildroot}%{_defaultdocdir} ; echo %{buildroot}%{_defaultdocdir})
%setup -q -c
for i in *
do
if [ -d $i ]; then
   cd $i
   mv -f * ../
   cd ..
   rmdir $i
fi
done

%build
 
%install

mkdir -p $RPM_BUILD_ROOT/usr/share/applications/

cat > $RPM_BUILD_ROOT/usr/share/applications/%{name}.desktop <<'EOF'
[Desktop Entry]
Name=System Administration Guide
Comment=Read about administering Red Hat Enterprise Linux
Exec=htmlview file:%{_defaultdocdir}/%{name}-%{version}/index.html
Icon=/%{_defaultdocdir}/%{name}-%{version}/docs.png
Categories=Documentation;X-Red-Hat-Base;
Type=Application
Encoding=UTF-8
Terminal=false
EOF

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(644,root,root,755)
%{_datadir}/applications/%{name}.desktop
/%{_defaultdocdir}/%{name}-%{version}

%changelog
* Wed Feb 16 2005  <jha@falcon.rdu.redhat.com> 4-2
- rebuilt to fix copyright date and bookdate fields

* Wed Feb 16 2005 John Ha <jha@redhat.com> - 4-1
- build for Red Hat Enterprise Linux 4

* Wed Oct  1 2003 Tammy Fox <tfox@falcon.rdu.redhat.com> 3-2
- tweaks to netboot chapters

* Mon Sep 29 2003 Tammy Fox <tfox@falcon.rdu.redhat.com> 3-1
- build final version

* Fri Jul 25 2003 Tammy Fox <tfox@redhat.com> 2.9.5-1
- build for beta

