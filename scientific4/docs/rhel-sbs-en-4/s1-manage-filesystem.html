<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>A Larger Picture of the File System</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.76b+
"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Managing Files and Directories"
HREF="ch-managers.html"><LINK
REL="PREVIOUS"
TITLE="Managing Files and Directories"
HREF="ch-managers.html"><LINK
REL="NEXT"
TITLE="Managing Files"
HREF="s1-managing-files.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Red Hat Enterprise Linux Step By Step Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="ch-managers.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 3. Managing Files and Directories</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-managing-files.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-MANAGE-FILESYSTEM"
></A
>3.2. A Larger Picture of the File System</H1
><P
>Every operating system has a method of storing data in files
      and directories so that it can keep track of additions,
      modifications, and other changes.  In Red Hat Enterprise Linux, every file is
      stored in a directory.  Directories can also contain
      directories: these <I
CLASS="FIRSTTERM"
>subdirectories</I
> may
      also contain files and other subdirectories.</P
><P
>You might think of the file system as a tree and
      directories as branches. There would be no tree without a root,
      and the same is true for the Red Hat Enterprise Linux file system.  No matter how
      far away the directories branch, everything is connected to the
      root directory, which is represented as a singe forward slash
      (/).</P
><DIV
CLASS="TIP"
><P
></P
><TABLE
CLASS="TIP"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/tip.png"
HSPACE="5"
ALT="Tip"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Tip</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>Red Hat Enterprise Linux uses the term <I
CLASS="FIRSTTERM"
>root</I
> in
	several different ways, which might be confusing to new users.
	There is the root account (the superuser, who has permission
	to do anything), the root account's home directory
	(<TT
CLASS="FILENAME"
>/root</TT
>) and the root directory for the
	entire file system (<TT
CLASS="FILENAME"
>/</TT
>). When you are
	speaking to someone and using the term
	<I
CLASS="FIRSTTERM"
>root</I
>, be sure to know which root is
	being discussed. 
      </P
></TD
></TR
></TABLE
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-FILESYSTEM-PATHS"
></A
>3.2.1. Paths</H2
><P
>	To continue the tree analogy from <A
HREF="s1-manage-filesystem.html"
>Section 3.2 <I
>A Larger Picture of the File System</I
></A
>, imagine navigating the file
	system as climbing around in the branches of the tree. The
	branches you would climb and traverse in order to get from one
	part of the tree to another would be the
	<I
CLASS="FIRSTTERM"
>path</I
> from one location to another.
	There are two kinds of paths, depending on how you describe
	them.  A <I
CLASS="FIRSTTERM"
>relative path</I
> describes the
	route starting from your current location in the tree.  An
	<I
CLASS="FIRSTTERM"
>absolute path</I
> describes the route to
	the new location starting from the tree trunk (the root
	directory).
      </P
><P
>	Graphic file browsers like
	<B
CLASS="APPLICATION"
>Nautilus</B
> use an absolute path to
	display your location in the file system.  At the top of a
	<B
CLASS="APPLICATION"
>Nautilus</B
> browser window is a
	location bar.  This bar indicates your current location
	starting with a forward slash (/) &#8212; this is an
	absolute path.  You can navigate the file system by entering
	the absolute path here.  Press <SPAN
CLASS="KEYCAP"
><KEYCAP
>[Enter]</KEYCAP
></SPAN
> and
	<B
CLASS="APPLICATION"
>Nautilus</B
> moves immediately to the
	new location without navigating through the intervening
	directories one at a time.
      </P
><DIV
CLASS="FIGURE"
><A
NAME="GR-PATHS-NAUTILUS"
></A
><DIV
CLASS="MEDIAOBJECT"
><P
><IMG
SRC="./figs/managers/location.png"></P
></DIV
><P
><B
>Figure 3-1. The <B
CLASS="APPLICATION"
>Nautilus</B
> location
	  bar</B
></P
></DIV
><P
>	Navigating via the shell prompt utilizes either relative or
	absolute paths.  In some instances, relative paths are shorter
	to type than absolute paths. In others, the unambiguous
	absolute path is easier to remember.
      </P
><P
>	There are two special characters used with relative paths.
	These characters are "." and
	"..".  A single period, ".",
	is shorthand for "here". It references your current
	working directory.  Two periods, "..",
	indicates the directory one level up from your current working
	directory. If your current working directory is your home
	directory, <TT
CLASS="FILENAME"
>/home/user/</TT
>,
	".." indicates the next directory up,
	<TT
CLASS="FILENAME"
>/home/</TT
>.
      </P
><P
>	Consider moving from the <TT
CLASS="FILENAME"
>/usr/share/doc/</TT
>
	directory to the <TT
CLASS="FILENAME"
>/tmp/</TT
> directory.  The
	relative path between the two requires a great deal of typing,
	and requires knowledge of the absolute path to your current
	working directory.  The relative path would look like this:
	<TT
CLASS="USERINPUT"
><B
>../../../tmp/</B
></TT
>.  The absolute path is
	much shorter: <TT
CLASS="USERINPUT"
><B
>/tmp/</B
></TT
>. The relative path
	requires you to move up three directories to the
	<TT
CLASS="FILENAME"
>/</TT
> directory before moving to the
	<TT
CLASS="FILENAME"
>/tmp/</TT
> directory.  The absolute path,
	which always starts at the <TT
CLASS="FILENAME"
>/</TT
> directory, is much simpler.
      </P
><P
>	However, the relative path between two closely-related
	directories may be simpler than the absolute path.  Consider
	moving from <TT
CLASS="FILENAME"
>/var/www/html/pics/vacation/</TT
> to
	<TT
CLASS="FILENAME"
>/var/www/html/pics/birthday/</TT
>.   The relative path is:
	<TT
CLASS="USERINPUT"
><B
>../birthday/</B
></TT
>.  The absolute path is:
	<TT
CLASS="USERINPUT"
><B
>/var/www/html/pics/birthday/</B
></TT
>.  Clearly, the relative path is
	shorter in this case.
      </P
><P
>	There is no right or wrong choice: both relative and absolute
	paths point to the same branch of the tree.  Choosing between
	the two is a matter of preference and convenience.  Remember,
	the <B
CLASS="APPLICATION"
>Nautilus</B
>	location bar does not
	recognize the ".." symbol &#8212; you must
	use an absolute path.
      </P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="ch-managers.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-managing-files.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Managing Files and Directories</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="ch-managers.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Managing Files</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>