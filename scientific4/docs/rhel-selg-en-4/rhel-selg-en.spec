License: distributable
Source: rhel-selg-en-%{version}.tbz
Release: 1
Name: rhel-selg-en
Group: Documentation
Version: 4
Summary: The Red Hat %{version} SELinux Guide in HTML format
BuildArchitectures: noarch
Buildroot: %{_tmppath}/%{name}-%{version}-buildroot
Requires: htmlview

%description
The Red Hat %{version} SELinux Policy Writing Guide provides
information for systems administrators, developers, and technical
users who wish to understand how to manage and manipulate SELinux
under Red Hat Enterprise Linux.  SELinux architecture is explained,
the default policy is described, and an approach with methodology for
manipulating and writing policy is covered.

%prep
%define _builddir %(mkdir -p %{buildroot}%{_defaultdocdir} ; echo %{buildroot}%{_defaultdocdir})
%setup -q -c
for i in *
do
if [ -d $i ]; then
   cd $i
   mv -f * ../
   cd ..
   rmdir $i
fi
done

%build
 
%install

mkdir -p $RPM_BUILD_ROOT/usr/share/applications/

cat > $RPM_BUILD_ROOT/usr/share/applications/%{name}-%{version}.desktop <<'EOF'
[Desktop Entry]
Name=RHEL SELinux Policy Writing Guide
Comment=Read in depth about writing policy for SELinux-enabled RHEL
Exec=htmlview file:%{_defaultdocdir}/%{name}-%{version}/index.html
Icon=/%{_defaultdocdir}/%{name}-%{version}/docs.png
Categories=Documentation;X-Red-Hat-Applications;
Type=Application
Encoding=UTF-8
Terminal=false
EOF

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(644,root,root,755)
%{_datadir}/applications/%{name}-%{version}.desktop
%{_defaultdocdir}/%{name}-%{version}

%changelog

* Thu Dec 16 2004 Karsten Wade <kwade@redhat.com>
- Set for nearly final usage, awaiting final version and edit

* Fri Dec 05 2003 Karsten Wade <kwade@redhat.com>
- Changed to RHEL SELinux usage 

* Thu Aug 21 2003 Karsten Wade <kwade@redhat.com>
- Added package description and summary, removed Obsoletes line.

* Wed Aug 20 2003 Dennis Gregorovic <dgregor@redhat.com>
- Added spec file.  Based on rhl-ig-waf-en.spec
