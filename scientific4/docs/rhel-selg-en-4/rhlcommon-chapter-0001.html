<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>SELinux Policy Overview</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Understanding SELinux"
HREF="selg-part-0057.html"><LINK
REL="PREVIOUS"
TITLE="SELinux, an Implementation of Flask"
HREF="selg-sect1-0015.html"><LINK
REL="NEXT"
TITLE="Where is the Policy?"
HREF="rhlcommon-section-0091.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="CHAPTER"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Red Hat SELinux Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="selg-sect1-0015.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="rhlcommon-section-0091.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="CHAPTER"
><H1
><A
NAME="RHLCOMMON-CHAPTER-0001"
></A
>Chapter 2. SELinux Policy Overview</H1
><P
>    This chapter is an overview of SELinux policy, some of its internals, and how
    it works.  This chapter discusses the policy in a more general way, where
    <A
HREF="rhlcommon-chapter-0051.html"
>Chapter 3 <I
>Targeted Policy Overview</I
></A
> focuses on the details of the
    targeted policy as it ships in Red Hat Enterprise Linux.  This chapter starts with a brief
    overview of what policy is and where it resides.  Next, the role of SELinux
    during boot is discussed.  This is followed by discussions on file security
    contexts, object classes and permissions, attributes, types, access vectors,
    macros, users and roles, constraints, and a brief discussion summarizing
    special kernel interfaces.
  </P
><P
>    To see all of the details discussed in this chapter, you must make sure you
    have installed the policy source and binary packages for the targeted
    policy:
  </P
><P
></P
><UL
><LI
><P
><TT
CLASS="FILENAME"
>selinux-policy-targeted-sources-<VAR
CLASS="REPLACEABLE"
>&#60;version&#62;</VAR
></TT
> 
        
      </P
></LI
><LI
><P
><TT
CLASS="FILENAME"
>selinux-policy-targeted-<VAR
CLASS="REPLACEABLE"
>&#60;version&#62;</VAR
></TT
>
      </P
></LI
></UL
><DIV
CLASS="IMPORTANT"
><P
></P
><TABLE
CLASS="IMPORTANT"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/important.png"
HSPACE="5"
ALT="Important"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Important</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>      When you have the policy sources installed, <TT
CLASS="COMMAND"
>rpm</TT
> may
      assume that you have modified the policy and may not automatically load a
      newly installed policy.  This occurs if you have ever loaded the policy
      from source, that is, run <TT
CLASS="COMMAND"
>make load</TT
>, <TT
CLASS="COMMAND"
>make
        reload</TT
>, or <TT
CLASS="COMMAND"
>make install</TT
>. New binary policy
      packages install
      <TT
CLASS="FILENAME"
>policy.<VAR
CLASS="REPLACEABLE"
>&#60;XY&#62;</VAR
></TT
> as, for
      example, <TT
CLASS="FILENAME"
>$SELINUX_POLICY/policy.18.rpmnew</TT
>.
    </P
><P
>      If you have not modified the policy or want to use the binary policy
      package, you can <TT
CLASS="COMMAND"
>mv policy.18.rpmnew policy.18</TT
>, then
      <TT
CLASS="COMMAND"
>touch /.autorelabel</TT
> and reboot.  If you have modified
      the policy and want to load your modifications, you must upgrade the
      policy source package and <TT
CLASS="COMMAND"
>make load</TT
>.  Policy building
      is discussed in <A
HREF="rhlcommon-chapter-0018.html"
>Chapter 7 <I
>Compiling SELinux Policy</I
></A
>.
    </P
><P
>      If you have only built the policy but never loaded it, that is, have only
      run <TT
CLASS="COMMAND"
>make policy</TT
>, you should not run into this
      situation. The binary policy installs cleanly, knowing that you are not
      running a custom policy.
    </P
><P
>      Work is ongoing to improve package installation logic so the entire
      process is automated by <TT
CLASS="COMMAND"
>rpm</TT
>.  Expect this to be
      included in a future update to Red Hat Enterprise Linux 4.
    </P
></TD
></TR
></TABLE
></DIV
><DIV
CLASS="SECTION"
><H1
CLASS="SECTION"
><A
NAME="RHLCOMMON-SECTION-0056"
>2.1. What Is Policy?</A
></H1
><P
>      Policy is the set of rules that guide the SELinux security engine.  It
      defines types for file objects and domains for processes, uses roles to
      limit the domains that can be entered, and has user identities to specify
      the roles that can be attained.  A <I
CLASS="GLOSSTERM"
>domain</I
> is what a
      type is called when it is applied to a process.
      
      
    </P
><P
>      A type is a way of grouping together like items based on their fundamental
      security sameness.  This doesn't necessarily have to do with the unique
      purpose of an application or the content of a document.  For example, an
      object such as a file can have any type of content and be for any purpose,
      but if it belongs to a user and lives in that user's home directory, it is
      considered to be of a specific security type,
      <SAMP
CLASS="COMPUTEROUTPUT"
>user_home_t</SAMP
>.
    </P
><P
>      These object types gain their sameness because they are accessible in the
      same way by the same set of subjects. Similarly, processes tend to be of
      the same type if they have the same permissions as other subjects.  In the
      targeted policy, programs that run in the
      <SAMP
CLASS="COMPUTEROUTPUT"
>unconfined_t</SAMP
> domain have an executable
      with a type such as <SAMP
CLASS="COMPUTEROUTPUT"
>sbin_t</SAMP
>. From an SELinux
      perspective, that means they are all equivalent in terms of what they can
      and cannot do on the system.
    </P
><P
>      For example, the binary executable file object at
      <TT
CLASS="FILENAME"
>/usr/bin/postgres</TT
> has the type of
      <SAMP
CLASS="COMPUTEROUTPUT"
>postgresql_exec_t</SAMP
>.  All of the targeted
      daemons have their own <SAMP
CLASS="COMPUTEROUTPUT"
>*_exec_t</SAMP
> type for
      their executable applications.  In fact, the entire set of PostgreSQL
      executables such as <TT
CLASS="COMMAND"
>createlang</TT
>,
      <TT
CLASS="COMMAND"
>pg_dump</TT
>, and <TT
CLASS="COMMAND"
>pg_restore</TT
> have the
      same type, <SAMP
CLASS="COMPUTEROUTPUT"
>postgresql_exec_t</SAMP
>, and they
      transition to the same domain,
      <SAMP
CLASS="COMPUTEROUTPUT"
>postgresql_t</SAMP
>, upon execution.
    </P
><P
>      The policy defines various rules that say how each domain may access each
      type.  Only what is specifically allowed by the rules is permitted.  By
      default every operation is denied and audited, meaning it is logged in
      <TT
CLASS="FILENAME"
>$AUDIT_LOG</TT
>, such as <TT
CLASS="FILENAME"
>/var/log/messages</TT
>. Policy is
      compiled into binary format for loading into the kernel security server,
      and as the security server hands out decisions, these are cached in the
      AVC for performance.
    </P
><P
>      Policy can be administratively defined, either by modifying the existing
      files or adding local TE and file context files to the policy tree.  Such
      a new policy can be loaded into the kernel in real time.  Otherwise, the
      policy is loaded during boot by <TT
CLASS="COMMAND"
>init</TT
>, as explained in
      <A
HREF="rhlcommon-section-0016.html"
>Section 2.3 <I
>Policy Role in Boot</I
></A
>.  Ultimately, every system
      operation is determined by the policy and the type labeling of the files.
    </P
><DIV
CLASS="IMPORTANT"
><P
></P
><TABLE
CLASS="IMPORTANT"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/important.png"
HSPACE="5"
ALT="Important"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Important</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>        After loading a new policy, it is recommended to restart any services
        that may have new or changed labeling.  For the most part, this is only
        the targeted daemons, as listed in <A
HREF="rhlcommon-chapter-0051.html#RHLCOMMON-SECTION-0003"
>Section 3.1 <I
>What is the Targeted Policy?</I
></A
>.
      </P
></TD
></TR
></TABLE
></DIV
><P
>      SELinux is an implementation of domain-type access control, with role-based
      limiting.  The policy specifies the rules in that environment.  It is
      written in a simple language created specifically for writing security
      policy.  Policy writers use <TT
CLASS="COMMAND"
>m4</TT
> macros to capture common
      sets of low-level rules.  There are a number of <TT
CLASS="COMMAND"
>m4</TT
>
      macros defined in the existing policy, which assist greatly in writing new
      policy.  These rules are preprocessed into many additional rules as part
      of building <TT
CLASS="FILENAME"
>policy.conf</TT
>, which is compiled into the
      binary policy.
    </P
><P
>      The files are divided into various categories in a policy tree at
      <TT
CLASS="FILENAME"
>$SELINUX_SRC/</TT
>.  This is covered in <A
HREF="rhlcommon-section-0010.html"
>Section 3.2 <I
>Files and Directories of the Targeted Policy</I
></A
>.  Access rights are divided
      differently among domains, and no domain is required to act as a master
      for all other domains.  Entering and switching domains is controlled by
      the policy, through login programs, userspace programs such as
      <TT
CLASS="COMMAND"
>newrole</TT
>, or by requiring a new process execution in the
      new domain, called a <I
CLASS="GLOSSTERM"
>transition</I
>.
    </P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="selg-sect1-0015.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="rhlcommon-section-0091.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>SELinux, an Implementation of Flask</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="selg-part-0057.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Where is the Policy?</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>