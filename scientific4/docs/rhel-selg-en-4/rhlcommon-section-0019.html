<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>File System Security Contexts</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="SELinux Policy Overview"
HREF="rhlcommon-chapter-0001.html"><LINK
REL="PREVIOUS"
TITLE="Policy Role in Boot"
HREF="rhlcommon-section-0016.html"><LINK
REL="NEXT"
TITLE="Object Classes and Permissions"
HREF="rhlcommon-section-0049.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECTION"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Red Hat SELinux Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="rhlcommon-section-0016.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 2. SELinux Policy Overview</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="rhlcommon-section-0049.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECTION"
><H1
CLASS="SECTION"
><A
NAME="RHLCOMMON-SECTION-0019"
>2.4. File System Security Contexts</A
></H1
><P
>      This section covers how file system security contexts are defined and
      stored.
    </P
><P
>      SELinux stores file security labels in xattrs<A
NAME="AEN919"
HREF="#FTN.AEN919"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
>.  For more information about xattrs, read the manual pages for
      <TT
CLASS="COMMAND"
>attr(5)</TT
>, <TT
CLASS="COMMAND"
>getfattr(1)</TT
>, and
      <TT
CLASS="COMMAND"
>setfattr(1)</TT
>. Xattrs are stored as name-value property
      pairs associated with files.  SELinux uses the
      <TT
CLASS="PARAMETER"
><I
>security.selinux</I
></TT
> attribute.  The xattrs can be
      stored with files on a disk or in memory with pseudo file systems.
      Currently, most file system types support the API for xattr, which allows for
      
      retrieving attribute information with <TT
CLASS="COMMAND"
>getxattr(2)</TT
>.
    </P
><P
>      Some non-persistent objects can be controlled through the API.  The
      pseudo-tty system controlled through <TT
CLASS="FILENAME"
>/dev/pts</TT
> is
      manipulated through <TT
CLASS="COMMAND"
>setxattr(2)</TT
>, enabling programs such
      as <TT
CLASS="COMMAND"
>sshd</TT
> to change the context of a tty device.
      Information about the tty is exported and available through
      <TT
CLASS="COMMAND"
>getxattr(2)</TT
>.  However, <TT
CLASS="FILENAME"
>libselinux</TT
>
      provides a more useful set of functions layered on top of the xattr API,
      such as <TT
CLASS="COMMAND"
>getfilecon(3)</TT
>,
      <TT
CLASS="COMMAND"
>setfilecon(3)</TT
>, and
      <TT
CLASS="COMMAND"
>setfscreatecon(3)</TT
>.
    </P
><DIV
CLASS="TIP"
><P
></P
><TABLE
CLASS="TIP"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/tip.png"
HSPACE="5"
ALT="Tip"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Tip</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>        It is recommended to use <TT
CLASS="FILENAME"
>libselinux</TT
> when managing
        file attributes in SELinux programmatically.
      </P
></TD
></TR
></TABLE
></DIV
><P
>      There are two approaches to take for storing file security labels on a
      file system, such as ext2 or ext3.  One approach is to label every file
      system object (all files) with an individual security attribute<A
NAME="AEN942"
HREF="#FTN.AEN942"
><SPAN
CLASS="footnote"
>[2]</SPAN
></A
>.  Once these labels are on the file system, the xattrs become
      authoritative for holding the state of security labels on the system.
    </P
><P
>      The other option is to label the entire file system with a single security
      attribute.  This is called <I
CLASS="GLOSSTERM"
>genfs labeling</I
>.  One
      example of this is with ISO9660 file systems, which are used for CD-ROMs
      and <TT
CLASS="FILENAME"
>.iso</TT
> files. This example from
      <TT
CLASS="FILENAME"
>$SELINUX_SRC/genfs_contexts</TT
> defines the context for
      every file on an ISO9660 file system.
    </P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="PROGRAMLISTING"
>genfscon iso9660 /                      system_u:object_r:iso9660_t</PRE
></TD
></TR
></TABLE
><P
>      The file <TT
CLASS="FILENAME"
>genfs_contexts</TT
> has labels to associate with
      the most common mounted file systems that do not support xattrs.
    </P
><P
>      You can set the context at the time of mounting the file system with the
      option <TT
CLASS="OPTION"
>-o
        context=<VAR
CLASS="REPLACEABLE"
>&#60;user&#62;</VAR
>:<VAR
CLASS="REPLACEABLE"
>&#60;role&#62;</VAR
>:<VAR
CLASS="REPLACEABLE"
>&#60;type&#62;</VAR
></TT
>. 
      A complete list of file system types can be found at
      <TT
CLASS="FILENAME"
>$SELINUX_SRC/types/file.te</TT
>.  This option is also
      known as <I
CLASS="FIRSTTERM"
>mountpoint labeling</I
> and is new in the
      2.6.<VAR
CLASS="REPLACEABLE"
>x</VAR
> kernel. Mountpoint labeling occurs in the kernel memory only,
      the labels are not written to disk.  This example overrides the setting in
      <TT
CLASS="FILENAME"
>genfs_contexts</TT
> that would normally mount the file
      system as <SAMP
CLASS="COMPUTEROUTPUT"
>nfs_t</SAMP
>:
    </P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>mount -t nfs -o context=user_u:object_r:user_home_t \
<VAR
CLASS="REPLACEABLE"
>&#60;hostname&#62;</VAR
>:/shares/homes/ /home/</PRE
></TD
></TR
></TABLE
><P
>      The <TT
CLASS="OPTION"
>-o context=</TT
> option is useful when mounting file
      systems that do not support extended attributes, such as a floppy or hard
      disk formatted with VFAT, or systems that are not normally running under
      SELinux, such as an ext3 formatted disk from a non-SELinux workstation.  You
      can also use <TT
CLASS="OPTION"
>-o context=</TT
> on file systems you do not trust,
      such as a floppy.  It also helps in compatibility with xattr-supporting
      file systems on earlier 2.4.<VAR
CLASS="REPLACEABLE"
>&#60;x&#62;</VAR
> kernel versions.
      Even where xattrs are supported, you can save time not having to label
      every file by assigning the entire disk one security context.
    </P
><P
>      Two other options are <TT
CLASS="OPTION"
>-o fscontext=</TT
> and <TT
CLASS="OPTION"
>-o
        defcontext=</TT
>, both of which are mutually exclusive of the
      <TT
CLASS="OPTION"
>context</TT
> option.  This means you can use
      <TT
CLASS="OPTION"
>fscontext</TT
> and <TT
CLASS="OPTION"
>defcontext</TT
> with each
      other, but neither can be used with <TT
CLASS="OPTION"
>context</TT
>.
    </P
><P
>      The <TT
CLASS="OPTION"
>fscontext</TT
> option works for all file systems,
      regardless of their xattr support.  The <TT
CLASS="OPTION"
>fscontext</TT
> option
      sets the overarching file system label to a specific security context.
      This file system label is separate from the individual labels on the
      files.  It represents the entire file system for certain kinds of
      permission checks, such as during mount or file creation. Individual file
      labels are still obtained from the xattrs on the files themselves.  The
      <TT
CLASS="OPTION"
>context</TT
> option actually sets the aggregate context that
      <TT
CLASS="OPTION"
>fscontext</TT
> provides, in addition to supplying the same
      label for individual files.
    </P
><P
>      You can set the default security context for unlabeled files using
      <TT
CLASS="OPTION"
>defcontext</TT
>.  This overrides the value set for unlabeled
      files in the policy and requires a file system that supports xattr
      labeling.  This example might be for a shared volume that gets file drops
      of security quarantined code, so the dropped files are labeled as being
      unsafe and can be controlled specially by policy:
    </P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
>mount -t ext3 defcontext=user_u:object_r:insecure_t \
  /shares/quarantined</PRE
></TD
></TR
></TABLE
><P
>      This all works because SELinux acts as a transparent layer for the mounted
      file system.  After parsing the security options, SELinux only passes normal
      file system specific code to the mounted file system. SELinux is able to
      seamlessly handle the text name-value pairs that most file systems use for
      mount options.  File systems with binary mount option data, such as NFS
      and SMBFS, need to be handled as special cases. Currently, NFSv3 is the
      only one supported.
    </P
><DIV
CLASS="SECTION"
><H2
CLASS="SECTION"
><A
NAME="RHLCOMMON-SECTION.0062"
>2.4.1. Security Contexts and the Kernel</A
></H2
><P
>        SELinux uses LSM hooks in the kernel in key locations, where they
        interject access vector decisions.  For example, there is a hook just
        prior to a file being read by a user, where SELinux steps from the normal
        kernel workflow to request the AVC decision.  This mainly occurs between
        a subject (a process such as <TT
CLASS="COMMAND"
>less</TT
>) and an object (a
        file such as <TT
CLASS="FILENAME"
>/etc/ssh/sshd_config</TT
>) for a specific
        permission need (such as <SAMP
CLASS="COMPUTEROUTPUT"
>read</SAMP
>).
      </P
><P
>        Based on the result read back from the AVC, the hook either continues
        the workflow or returns <SPAN
CLASS="RETURNVALUE"
>EACCES</SPAN
>, that is,
        <SAMP
CLASS="COMPUTEROUTPUT"
>Permission denied</SAMP
>.
      </P
><P
>        The way SELinux implements its label in the xattr is different from
        other labeling schemes.  SELinux stores its labels in human-readable
        strings.  This provides a meaningful label with the file that can help
        in backup, restoration, and moving files between systems.  Standard
        attributes do not provide a label that has continuous meaning for the
        file.
      </P
><P
>        In this example under the targeted policy, the policy does not specify
        anything about files created by <TT
CLASS="FILENAME"
>unconfined_t</TT
> in the
        directory <TT
CLASS="FILENAME"
>/tmp</TT
>, so the files inherit the context
        from the parent directory:
      </P
><DIV
CLASS="INFORMALEXAMPLE"
><P
></P
><A
NAME="AEN1020"
></A
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="PROGRAMLISTING"
>id -Z
root:system_r:unconfined_t
ls -dZ /tmp
drwxrwxrwt  root   root    system_u:object_r:tmp_t       /tmp/
touch /tmp/foo
ls -Z /tmp/foo
-rw-r--r--  root   root    root:object_r:tmp_t           /tmp/foo</PRE
></TD
></TR
></TABLE
><P
></P
></DIV
><P
>        In this example under a different policy, the policy explicitly states
        that files created by <SAMP
CLASS="COMPUTEROUTPUT"
>user_t</SAMP
> in
        <TT
CLASS="FILENAME"
>/tmp</TT
> have a type of
        <SAMP
CLASS="COMPUTEROUTPUT"
>user_tmp_t</SAMP
>:
      </P
><DIV
CLASS="INFORMALEXAMPLE"
><P
></P
><A
NAME="AEN1026"
></A
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="PROGRAMLISTING"
>id -Z
user_u:staff_r:user_t
ls -dZ /tmp
drwxrwxrwt  usera  usera   system_u:object_r:tmp_t       /tmp/
touch /tmp/foo
ls -Z /tmp/foo
-rw-r--r--  usera  usera   root:object_r:user_tmp_t      /tmp/foo</PRE
></TD
></TR
></TABLE
><P
></P
></DIV
><P
>        This finer grained control is implemented via policy using the
        <SAMP
CLASS="COMPUTEROUTPUT"
>tmp_domain()</SAMP
> macro, which defines a
        temporary type per domain.  In this macro, the variable
        <SAMP
CLASS="COMPUTEROUTPUT"
>$1_tmp_t</SAMP
> is expanded by substituting
        the subject's type base, so that <SAMP
CLASS="COMPUTEROUTPUT"
>user_t</SAMP
>
        creates files with a type of
        <SAMP
CLASS="COMPUTEROUTPUT"
>user_tmp_t</SAMP
>.
      </P
><P
>        Having separate types for <TT
CLASS="FILENAME"
>/tmp/</TT
> protects a domain's
        temporary files against tampering or disclosure by other domains.  It
        also protects against misdirection through a malicious symlink.  In the
        targeted policy, the confined daemons have separate types for their
        temporary files, keeping those daemons from interfering with other
        <TT
CLASS="FILENAME"
>/tmp/</TT
> files.
      </P
><P
>        A privileged application can override any stated labeling rule by
        writing a security context to
        <TT
CLASS="FILENAME"
>/proc/self/attr/fscreate</TT
> using
        <TT
CLASS="COMMAND"
>setfscreatecon(3)</TT
>.  This action must still be allowed
        by policy.  The context is then used to label the next newly created
        file object, and the <TT
CLASS="FILENAME"
>fscreate</TT
> is automatically
        reset after the next <TT
CLASS="COMMAND"
>execve</TT
> or through
        <TT
CLASS="COMMAND"
>setfscreatecon(NULL)</TT
>.  This ensures that a program
        starts in a known state without having to be concerned what context was
        left by the previous program in
        <TT
CLASS="FILENAME"
>/proc/self/attr/fscreate</TT
>.
      </P
></DIV
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN919"
HREF="rhlcommon-section-0019.html#AEN919"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>          Extended attributes are also called
          <I
CLASS="GLOSSTERM"
><ABBR
CLASS="ABBREV"
>EA</ABBR
>s</I
>.  To be more concise, the
          term xattr is used in this guide.
        </P
></TD
></TR
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN942"
HREF="rhlcommon-section-0019.html#AEN942"
><SPAN
CLASS="footnote"
>[2]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>These are defined initially for the system in
          <TT
CLASS="FILENAME"
>$SELINUX_SRC/file_contexts/types.fc</TT
>.  This file uses
          regular expression matching to associate the files on a particular
          path with a particular security label.  These contexts are rendered
          into the installed version at
          <TT
CLASS="FILENAME"
>/etc/selinux/targeted/contexts/files/file_contexts</TT
>, and are
          used during installation of the operating system and software
          packages, or for checking or restoring files to their original state.
        </P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="rhlcommon-section-0016.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="rhlcommon-section-0049.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Policy Role in Boot</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="rhlcommon-chapter-0001.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Object Classes and Permissions</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>