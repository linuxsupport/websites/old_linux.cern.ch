<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Introduction to the Red Hat SELinux Guide</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="PREVIOUS"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="NEXT"
TITLE="Prerequisites for This Guide"
HREF="selg-sect1-0006.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="PREFACE"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Red Hat SELinux Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="index.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="selg-sect1-0006.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="PREFACE"
><H1
><A
NAME="SELG-PREFACE-0011"
></A
>Introduction to the Red Hat SELinux Guide</H1
><P
>    Welcome to the Red Hat SELinux Guide.  This guide addresses the complex world of SELinux
    policy, and has the goal of teaching you how to understand, use, administer,
    and troubleshoot SELinux in a Red Hat Enterprise Linux environment.  SELinux, an implementation of
    <I
CLASS="GLOSSTERM"
>mandatory access control</I
> (<ABBR
CLASS="ABBREV"
>MAC</ABBR
>) in
    the Linux kernel, adds the ability to administratively define policies on
    all <I
CLASS="GLOSSTERM"
>subjects</I
> (processes) and
    <I
CLASS="GLOSSTERM"
>objects</I
> (devices, files, and signaled processes).
    These terms are used as an abstract when discussing actors/doers and their
    targets on a system.  This guide commonly refers to processes, the source of
    an operations, and objects, the target of an operation.
  </P
><P
>    This guide opens with a short explanation of SELinux, some assumptions about
    the reader, and an explanation of document conventions.  The first part of
    the guide provides an overview of the technical architecture and how policy
    works, specifically the policy that comes with Red Hat Enterprise Linux called the
    <I
CLASS="GLOSSTERM"
>targeted</I
> policy.  The second part focuses on working
    with SELinux, including maintaining and manipulating your systems, policy
    analysis, and compiling your custom policy.  Working with some of the
    daemons that are confined by the targeted policy is discussed throughout.
    These daemons are collectively called the <I
CLASS="GLOSSTERM"
>targeted
      daemons</I
>.
  </P
><P
>    One powerful way of finding information in this guide is the <A
HREF="generated-index.html"
><I
>Index</I
></A
>.  The <A
HREF="generated-index.html"
><I
>Index</I
></A
>  has direct links to sections on specific
    terminology, and also features lists of various SELinux syntaxes, as well as
    <I
CLASS="CITETITLE"
>what are</I
>/<I
CLASS="CITETITLE"
>what is</I
> and
    <I
CLASS="CITETITLE"
>how to</I
> entries.
  </P
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="RHLCOMMON-SECTION-0072"
>1. What Is SELinux?</A
></H1
><P
>      This section is a very brief overview of SELinux.  More detail is given in
      <A
HREF="selg-part-0057.html"
>Part I <I
>Understanding SELinux</I
></A
> and <A
HREF="rhlcommon-appendix-0005.html"
>Appendix A <I
>Brief Background and History of SELinux</I
></A
>.
  </P
><P
>    <I
CLASS="GLOSSTERM"
>Security-enhanced Linux</I
>
    (<I
CLASS="GLOSSTERM"
><ABBR
CLASS="ABBREV"
>SELinux</ABBR
></I
>) is an implementation of a
    <I
CLASS="GLOSSTERM"
>mandatory access control</I
> mechanism.  This mechanism
    is in the Linux kernel, checking for allowed operations after standard Linux
    <I
CLASS="GLOSSTERM"
>discretionary access controls</I
> are checked.
  </P
><P
>    To understand the benefit of mandatory access control (<ABBR
CLASS="ABBREV"
>MAC</ABBR
>)
    over traditional discretionary access control (<ABBR
CLASS="ABBREV"
>DAC</ABBR
>), you
    need to first understand the limitations of DAC.
  </P
><P
>    Under DAC, ownership of a file object provides potentially crippling or
    risky control over the object. A user can expose a file or directory to a
    security or confidentiality breach with a misconfigured
    <TT
CLASS="COMMAND"
>chmod</TT
> command and an unexpected propagation of access
    rights.  A process started by that user, such as a CGI script, can do
    anything it wants to the files owned by the user.  A compromised Apache HTTP
    server can perform any operation on files in the Web group. Malicious or
    broken software can have root-level access to the entire system, either by
    running as a root process or using <TT
CLASS="COMMAND"
>setuid</TT
> or
    <TT
CLASS="COMMAND"
>setgid</TT
>.
  </P
><P
>    Under DAC, there are really only two major categories of users,
    administrators and non-administrators.  In order for services and programs
    to run with any level of elevated privilege, the choices are few and course
    grained, and typically resolve to just giving full administrator access.
    Solutions such as <ABBR
CLASS="ABBREV"
>ACL</ABBR
>s (<I
CLASS="GLOSSTERM"
>access control
      lists</I
>) can provide some additional security for allowing
    non-administrators expanded privileges, but for the most part a root account
    has complete discretion over the file system.
  </P
><P
>    A MAC or <I
CLASS="GLOSSTERM"
>non-discretionary access control</I
> framework
    allows you to define permissions for how all processes (called
    <I
CLASS="GLOSSTERM"
>subjects</I
>) interact with other parts of the system
    such as files, devices, sockets, ports, and other processes (called
    <I
CLASS="GLOSSTERM"
>objects</I
> in SELinux).  This is done through an
    administratively-defined security policy over all processes and objects.
    These processes and objects are controlled through the kernel, and security
    decisions are made on all available information rather than just user
    identity.  With this model, a process can be granted just the permissions it
    needs to be functional.  This follows the principle of <I
CLASS="GLOSSTERM"
>least
      privilege</I
>.  Under MAC, for example, users who have exposed
    their data using <TT
CLASS="COMMAND"
>chmod</TT
> are protected by the fact that
    their data is a kind only associated with user home directories, and
    confined processes cannot touch those files without permission and purpose
    written into the policy.
  </P
><P
>    SELinux is implemented in the Linux kernel using the <ABBR
CLASS="ABBREV"
>LSM</ABBR
>
    (<I
CLASS="GLOSSTERM"
>Linux Security Modules</I
>) framework.  This is only the
    latest implementation of an ongoing project, as detailed in <A
HREF="rhlcommon-appendix-0005.html"
>Appendix A <I
>Brief Background and History of SELinux</I
></A
>.  To support fine-grained access
    control, SELinux implements two technologies:   <I
CLASS="FIRSTTERM"
><SPAN
CLASS="TRADEMARK"
>Type Enforcement</SPAN
>&trade;</I
>
    (<ABBR
CLASS="ABBREV"
>TE</ABBR
>) and a kind of <I
CLASS="FIRSTTERM"
>role-based access
      control</I
> (<ABBR
CLASS="ABBREV"
>RBAC</ABBR
>), which are discussed in
      <A
HREF="selg-chapter-0013.html"
>Chapter 1 <I
>SELinux Architectural Overview</I
></A
>.  
  </P
><P
>    Type Enforcement involves defining a <I
CLASS="GLOSSTERM"
>type</I
> for every
    subject, that is, process, and object on the system.  These types are
    defined by the SELinux <I
CLASS="GLOSSTERM"
>policy</I
> and are contained in
    security labels on the files themselves, stored in the <I
CLASS="GLOSSTERM"
>extended
      attributes</I
> (<I
CLASS="GLOSSTERM"
><ABBR
CLASS="ABBREV"
>xattrs</ABBR
></I
>) of
    the file.  When a type is associated with a processes, the type is called a
    <I
CLASS="GLOSSTERM"
>domain</I
>, as in, "<TT
CLASS="COMMAND"
>httpd</TT
> is in the domain of
    <SAMP
CLASS="COMPUTEROUTPUT"
>httpd_t</SAMP
>."  This is a terminology difference
    leftover from other models when domains and types were handled separately.
  </P
><P
>    All interactions between subjects and objects are disallowed by default on
    an SELinux system.  The policy specifically allows certain operations.  To
    know what to allow, TE uses a matrix of domains and object types derived
    from the policy.  The matrix is derived from the policy rules.  For example,
    <SAMP
CLASS="COMPUTEROUTPUT"
>allow httpd_t net_conf_t:file { read getattr lock ioctl
      };</SAMP
> gives the domain associated with <TT
CLASS="COMMAND"
>httpd</TT
> the
    permissions to read data out of specific network configuration files such as
    <TT
CLASS="FILENAME"
>/etc/resolv.conf</TT
>. The matrix clearly defines all the
    interactions of processes and the targets of their operations.
  </P
><P
>    Because of this design, SELinux can implement very granular access controls.
    For Red Hat Enterprise Linux 4 the policy has been designed to restrict only a
    specific list of daemons.  All other processes run in an unconfined state.
    This policy is designed to help integrate SELinux into your development and
    production environment.  It is possible to have a much more strict policy,
    which comes with an increase in maintenance complexity.
  </P
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="selg-sect1-0006.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Red Hat Enterprise Linux 4</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
>&nbsp;</TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Prerequisites for This Guide</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>