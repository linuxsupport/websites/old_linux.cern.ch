<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Security Overview</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="A General Introduction to Security"
HREF="pt-general-intro.html"><LINK
REL="PREVIOUS"
TITLE="A General Introduction to Security"
HREF="pt-general-intro.html"><LINK
REL="NEXT"
TITLE="Security Controls"
HREF="s1-sgs-ov-controls.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="CHAPTER"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Security Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="pt-general-intro.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
></TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-sgs-ov-controls.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="CHAPTER"
><H1
><A
NAME="CH-SGS-OV"
></A
>Chapter 1. Security Overview</H1
><P
>Because of the increased reliance on powerful, networked computers to
      help run businesses and keep track of our personal information, industries
      have been formed around the practice of network and computer
      security. Enterprises have solicited the knowledge and skills of security
      experts to properly audit systems and tailor solutions to fit the
      operating requirements of the organization. Because most organizations are
      dynamic in nature, with workers accessing company IT resources locally and
      remotely, the need for secure computing environments has become more
      pronounced.
    </P
><P
>Unfortunately, most organizations (as well as individual users) regard
      security as an afterthought, a process that is overlooked in favor of
      increased power, productivity, and budgetary concerns. Proper security
      implementation is often enacted <I
CLASS="FIRSTTERM"
>postmortem</I
> &#8212;
      after an unauthorized intrusion has already occurred. Security experts
      agree that the right measures taken prior to connecting a site to an
      untrusted network, such as the Internet, is an effective means of
      thwarting most attempts at intrusion.
    </P
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-SGS-OV-CS"
>1.1. What is Computer Security?</A
></H1
><P
>Computer security is a general term that covers a wide area of
	computing and information processing. Industries that depend on computer
	systems and networks to conduct daily business transactions and access
	crucial information regard their data as an important part of their
	overall assets. Several terms and metrics have entered our daily
	business vocabulary, such as total cost of ownership (TCO) and quality
	of service (QoS). In these metrics, industries calculate aspects such as
	data integrity and high-availability as part of their planning and
	process management costs. In some industries, such as electronic
	commerce, the availability and trustworthiness of data can be the
	difference between success and failure.
      </P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-SGS-OV-CS-HOW"
>1.1.1. How did Computer Security Come about?</A
></H2
><P
>Many readers may recall the movie "Wargames," starring Matthew
	  Broderick in his portrayal of a high school student who breaks into
	  the United States Department of Defense (DoD) supercomputer and
	  inadvertently causes a nuclear war threat. In this movie, Broderick
	  uses his modem to dial into the DoD computer (called WOPR) and plays
	  games with the artificially intelligent software controlling all of
	  the nuclear missile silos. The movie was released during the "cold
	  war" between the former Soviet Union and the United States and
	  was considered a success in its theatrical release in 1983. The
	  popularity of the movie inspired many individuals and groups to begin
	  implementing some of the methods that the young protagonist used to
	  crack restricted systems, including what is known as <I
CLASS="FIRSTTERM"
>war
	  dialing</I
> &#8212; a method of searching phone numbers for
	  analog modem connections in a defined area code and phone prefix
	  combination.
	</P
><P
>More than 10 years later, after a four-year, multi-jurisdictional
	  pursuit involving the Federal Bureau of Investigation (FBI) and the
	  aid of computer professionals across the country, infamous computer
	  cracker Kevin Mitnick was arrested and charged with 25 counts of
	  computer and access device fraud that resulted in an estimated US$80
	  Million in losses of intellectual property and source code from Nokia,
	  NEC, Sun Microsystems, Novell, Fujitsu, and Motorola. At the time, the
	  FBI considered it to be the largest computer-related criminal offense
	  in U.S. history. He was convicted and sentenced to a combined 68
	  months in prison for his crimes, of which he served 60 months before
	  his parole on January 21, 2000. Mitnick was further barred from using
	  computers or doing any computer-related consulting until
	  2003. Investigators say that Mitnick was an expert in
	  <I
CLASS="FIRSTTERM"
>social engineering</I
> &#8212; using human beings
	  to gain access to passwords and systems using falsified credentials.
	</P
><P
>Information security has evolved over the years due to the
	  increasing reliance on public networks to disclose personal,
	  financial, and other restricted information. There are numerous
	  instances such as the Mitnick and the Vladimir Levin cases (refer to
	  <A
HREF="ch-sgs-ov.html#S2-SGS-OV-CS-TIMELINE"
>Section 1.1.2 <I
>Computer Security Timeline</I
></A
> for more information) that
	  prompted organizations across all industries to rethink the way they
	  handle information transmission and disclosure. The popularity of the
	  Internet was one of the most important developments that prompted an
	  intensified effort in data security.
	</P
><P
>An ever-growing number of people are using their personal
	  computers to gain access to the resources that the Internet has to
	  offer. From research and information retrieval to electronic mail and
	  commerce transaction, the Internet has been regarded as one of the
	  most important developments of the 20th century.
	</P
><P
>The Internet and its earlier protocols, however, were developed as
	  a <I
CLASS="FIRSTTERM"
>trust-based</I
> system. That is, the Internet
	  Protocol was not designed to be secure in itself. There are no
	  approved security standards built into the TCP/IP communications
	  stack, leaving it open to potentially malicious users and processes
	  across the network. Modern developments have made Internet
	  communication more secure, but there are still several incidents that
	  gain national attention and alert us to the fact that nothing is
	  completely safe.
	</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-SGS-OV-CS-TIMELINE"
>1.1.2. Computer Security Timeline</A
></H2
><P
>Several key events contributed to the birth and rise of computer
	security. The following timeline lists some of the more important events
	that brought attention to computer and information security and its
	importance today.
	</P
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-SGS-OV-CS-TIMELINE-30S"
>1.1.2.1. The 1930s and 1940s</A
></H3
><P
></P
><UL
><LI
><P
>Polish cryptographers invent the Enigma machine in 1918, an
	      electro-mechanical rotor cypher device which converts plain-text
	      messages to an encrypted result. Originally developed to secure
	      banking communications, the German military finds the potential of
	      the device by securing communications during World War II. A
	      brilliant mathematician named Alan Turing develops a method for
	      breaking the codes of Enigma, enabling Allied forces to develop
	      Colossus, a machine often credited to ending the war a year
	      early.
	      </P
></LI
></UL
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-SGS-OV-CS-TIMELINE-60S"
>1.1.2.2. The 1960s</A
></H3
><P
></P
><UL
><LI
><P
>Students at the Massachusetts Institute of Technology (MIT)
		form the Tech Model Railroad Club (TMRC) begin exploring and
		programming the school's PDP-1 mainframe computer system. The
		group eventually coined the term "hacker" in the context it is
		known today.
	      </P
></LI
><LI
><P
>The DoD creates the Advanced Research Projects Agency
	      Network (ARPANet), which gains popularity in research and academic
	      circles as a conduit for the electronic exchange of data and
	      information. This paves the way for the creation of the carrier
	      network known today as the Internet.
	      </P
></LI
><LI
><P
>Ken Thompson develops the UNIX operating system, widely
	      hailed as the most "hacker-friendly" OS because of its accessible
	      developer tools and compilers, and its supportive user
	      community. Around the same time, Dennis Ritchie develops the C
	      programming language, arguably the most popular hacking language
	      in computer history.
	      </P
></LI
></UL
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-SGS-OV-CS-TIMELINE-70S"
>1.1.2.3. The 1970s</A
></H3
><P
></P
><UL
><LI
><P
>Bolt, Beranek, and Newman, a computing research and
	      development contractor for government and industry, develops the
	      Telnet protocol, a public extension of the ARPANet. This opens
	      doors for the public use of data networks which were once
	      restricted to government contractors and academic
	      researchers. Telnet, though, is also arguably the most insecure
	      protocol for public networks, according to several security
	      researchers.
	      </P
></LI
><LI
><P
>Steve Jobs and Steve Wozniak found Apple Computer and begin
		marketing the Personal Computer (PC). The PC is the springboard
		for several malicious users to learn the craft of cracking
		systems remotely using common PC communication hardware such as
		analog modems and war dialers.
	      </P
></LI
><LI
><P
>Jim Ellis and Tom Truscott create USENET, a
	      bulletin-board-style system for electronic communication between
	      disparate users. USENET quickly becomes one of the most popular
	      forums for the exchange of ideas in computing, networking, and, of
	      course, cracking.
	      </P
></LI
></UL
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-SGS-OV-CS-TIMELINE-80S"
>1.1.2.4. The 1980s</A
></H3
><P
></P
><UL
><LI
><P
>IBM develops and markets PCs based on the Intel 8086
	      microprocessor, a relatively inexpensive architecture that brought
	      computing from the office to the home. This serves to commodify
	      the PC as a common and accessible tool that was fairly powerful
	      and easy to use, aiding in the proliferation of such hardware in
	      the homes and offices of malicious users.
	      </P
></LI
><LI
><P
>The Transmission Control Protocol, developed by Vint Cerf,
		is split into two separate parts. The Internet Protocol is born
		from this split, and the combined TCP/IP protocol becomes the
		standard for all Internet communication today.
	      </P
></LI
><LI
><P
>Based on developments in the area of
		<I
CLASS="FIRSTTERM"
>phreaking</I
>, or exploring and hacking the
		telephone system, the magazine <I
CLASS="CITETITLE"
>2600: The Hacker
		Quarterly</I
> is created and begins discussion on topics
		such as cracking computers and computer networks to a broad
		audience.
	      </P
></LI
><LI
><P
>The 414 gang (named after the area code where they lived and
		hacked from) are raided by authorities after a nine-day cracking
		spree where they break into systems from such top-secret
		locations as the Los Alamos National Laboratory, a nuclear
		weapons research facility.
	      </P
></LI
><LI
><P
>The Legion of Doom and the Chaos Computer Club are two
	      pioneering cracker groups that begin exploiting vulnerabilities in
	      computers and electronic data networks.
	      </P
></LI
><LI
><P
>The Computer Fraud and Abuse Act of 1986 is voted into law
	      by congress based on the exploits of Ian Murphy, also known as
	      Captain Zap, who broke into military computers, stole information
	      from company merchandise order databases, and used restricted
	      government telephone switchboards to make phone calls.
	      </P
></LI
><LI
><P
>Based on the Computer Fraud and Abuse Act, the courts
	      convict Robert Morris, a graduate student, for unleashing the
	      Morris Worm to over 6,000 vulnerable computers connected to the
	      Internet. The next most prominent case ruled under this act was
	      Herbert Zinn, a high-school dropout who cracked and misused
	      systems belonging to AT&#38;T and the DoD.
	      </P
></LI
><LI
><P
>Based on concerns that the Morris Worm ordeal could be
	      replicated, the Computer Emergency Response Team (CERT) is created
	      to alert computer users of network security issues.
	      </P
></LI
><LI
><P
>Clifford Stoll writes <I
CLASS="CITETITLE"
>The Cuckoo's
	      Egg</I
>, Stoll's account of investigating crackers who
	      exploit his system.
	      </P
></LI
></UL
></DIV
><DIV
CLASS="SECT3"
><H3
CLASS="SECT3"
><A
NAME="S3-SGS-OV-CS-TIMELINE-90S"
>1.1.2.5. The 1990s</A
></H3
><P
></P
><UL
><LI
><P
>ARPANet is decommissioned. Traffic from that network is
	      transferred to the Internet.
	      </P
></LI
><LI
><P
>Linus Torvalds develops the Linux kernel for use with the
	      GNU operating system; the widespread development and adoption of
	      Linux is largely due to the collaboration of users and developers
	      communicating via the Internet. Because of its roots in UNIX,
	      Linux is most popular among hackers and administrators who found
	      it quite useful for building secure alternatives to legacy servers
	      running proprietary (closed-source) operating systems.
	      </P
></LI
><LI
><P
>The graphical Web browser is created and sparks an
	      exponentially higher demand for public Internet access.
	      </P
></LI
><LI
><P
>Vladimir Levin and accomplices illegally transfer US$10
	      Million in funds to several accounts by cracking into the CitiBank
	      central database. Levin is arrested by Interpol and almost all of
	      the money is recovered.
	      </P
></LI
><LI
><P
>Possibly the most heralded of all crackers is Kevin Mitnick,
	      who hacked into several corporate systems, stealing everything
	      from personal information of celebrities to over 20,000 credit
	      card numbers and source code for proprietary software. He is
	      arrested and convicted of wire fraud charges and serves 5 years in
	      prison.
	      </P
></LI
><LI
><P
>Kevin Poulsen and an unknown accomplice rig radio station
	      phone systems to win cars and cash prizes. He is convicted for
	      computer and wire fraud and is sentenced to 5 years in prison.
	      </P
></LI
><LI
><P
>The stories of cracking and phreaking become legend, and
	      several prospective crackers convene at the annual DefCon
	      convention to celebrate cracking and exchange ideas between peers.
	      </P
></LI
><LI
><P
>A 19-year-old Israeli student is arrested and convicted for
	      coordinating numerous break-ins to US government systems during
	      the Persian-Gulf conflict. Military officials call it "the most
	      organized and systematic attack" on government systems in US
	      history.
	      </P
></LI
><LI
><P
>US Attorney General Janet Reno, in response to escalated
	      security breaches in government systems, establishes the National
	      Infrastructure Protection Center.
	      </P
></LI
><LI
><P
>British communications satellites are taken over and
	      ransomed by unknown offenders. The British government eventually
	      seizes control of the satellites.
	      </P
></LI
></UL
></DIV
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-SGS-OV-CS-EX"
>1.1.3. Security Today</A
></H2
><P
>In February of 2000, a Distributed Denial of Service (DDoS) attack
	  was unleashed on several of the most heavily-trafficked sites on the
	  Internet. The attack rendered yahoo.com, cnn.com, amazon.com, fbi.gov,
	  and several other sites completely unreachable to normal users, as it
	  tied up routers for several hours with large-byte ICMP packet
	  transfers, also called a <I
CLASS="FIRSTTERM"
>ping flood</I
>. The attack
	  was brought on by unknown assailants using specially created, widely
	  available programs that scanned vulnerable network servers, installed
	  client applications called <I
CLASS="FIRSTTERM"
>trojans</I
> on the
	  servers, and timed an attack with every infected server flooding the
	  victim sites and rendering them unavailable. Many blame the attack on
	  fundamental flaws in the way routers and the protocols used are
	  structured to accept all incoming data, no matter where or for what
	  purpose the packets are sent.
	</P
><P
>This brings us to the new millennium, a time where an estimated
	  945 Million people use or have used the Internet worldwide (Computer
	  Industry Almanac, 2004). At the same time:
	</P
><P
></P
><UL
><LI
><P
>On any given day, there are approximately 225 major incidences
	      of security breach reported to the CERT Coordination Center at
	      Carnegie Mellon University.<A
NAME="AEN478"
HREF="#FTN.AEN478"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
>
	    </P
></LI
><LI
><P
>In 2003, the number of CERT reported incidences jumped to
	      137,529 from 82,094 in 2002 and from 52,658 in
	      2001.<A
NAME="AEN483"
HREF="#FTN.AEN483"
><SPAN
CLASS="footnote"
>[2]</SPAN
></A
>
	    </P
></LI
><LI
><P
>The worldwide economic impact of the three most dangerous
	      Internet Viruses of the last three years was estimated at US$13.2
	      Billion.<A
NAME="AEN488"
HREF="#FTN.AEN488"
><SPAN
CLASS="footnote"
>[3]</SPAN
></A
>
	    </P
></LI
></UL
><P
>Computer security has become a quantifiable and justifiable
	  expense for all IT budgets. Organizations that require data integrity
	  and high availability elicit the skills of system administrators,
	  developers, and engineers to ensure 24x7 reliability of their systems,
	  services, and information. Falling victim to malicious users,
	  processes, or coordinated attacks is a direct threat to the success of
	  the organization.
	</P
><P
>Unfortunately, system and network security can be a difficult
	  proposition, requiring an intricate knowledge of how an organization
	  regards, uses, manipulates, and transmits its
	  information. Understanding the way an organization (and the people
	  that make up the organization) conducts business is paramount to
	  implementing a proper security plan.
	</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-SGS-OV-CIA"
>1.1.4. Standardizing Security</A
></H2
><P
>Enterprises in every industry rely on regulations and rules that
	  are set by standards making bodies such as the American Medical
	  Association (AMA) or the Institute of Electrical and Electronics
	  Engineers (IEEE). The same ideals hold true for information
	  security. Many security consultants and vendors agree upon the
	  standard security model known as CIA, or <I
CLASS="FIRSTTERM"
>Confidentiality,
	  Integrity, and Availability</I
>. This three-tiered model is a
	  generally accepted component to assessing risks of sensitive
	  information and establishing security policy. The following describes
	  the CIA model in further detail:</P
><P
></P
><UL
><LI
><P
>Confidentiality &#8212; Sensitive information must be
	      available only to a set of pre-defined individuals. Unauthorized
	      transmission and usage of information should be restricted. For
	      example, confidentiality of information ensures that a customer's
	      personal or financial information is not obtained by an
	      unauthorized individual for malicious purposes such as identity
	      theft or credit fraud.</P
></LI
><LI
><P
>Integrity &#8212; Information should not be altered in ways
	      that render it incomplete or incorrect. Unauthorized users should
	      be restricted from the ability to modify or destroy sensitive
	      information. 
	      </P
></LI
><LI
><P
>Availability &#8212; Information should be accessible to
	      authorized users any time that it is needed. Availability is a
	      warranty that information can be obtained with an agreed-upon
	      frequency and timeliness. This is often measured in terms of
	      percentages and agreed to formally in Service Level Agreements
	      (SLAs) used by network service providers and their enterprise
	      clients.
	      </P
></LI
></UL
></DIV
></DIV
></DIV
><H3
CLASS="FOOTNOTES"
>Notes</H3
><TABLE
BORDER="0"
CLASS="FOOTNOTES"
WIDTH="100%"
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN478"
HREF="ch-sgs-ov.html#AEN478"
><SPAN
CLASS="footnote"
>[1]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>Source: <A
HREF="http://www.cert.org"
TARGET="_top"
>http://www.cert.org</A
></P
></TD
></TR
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN483"
HREF="ch-sgs-ov.html#AEN483"
><SPAN
CLASS="footnote"
>[2]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>Source: <A
HREF="http://www.cert.org/stats/"
TARGET="_top"
>http://www.cert.org/stats/</A
></P
></TD
></TR
><TR
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="5%"
><A
NAME="FTN.AEN488"
HREF="ch-sgs-ov.html#AEN488"
><SPAN
CLASS="footnote"
>[3]</SPAN
></A
></TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
WIDTH="95%"
><P
>Source: <A
HREF="http://www.newsfactor.com/perl/story/16407.html"
TARGET="_top"
>http://www.newsfactor.com/perl/story/16407.html</A
></P
></TD
></TR
></TABLE
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="pt-general-intro.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-sgs-ov-controls.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>A General Introduction to Security</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="pt-general-intro.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Security Controls</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>