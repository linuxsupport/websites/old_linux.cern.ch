<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML
><HEAD
><TITLE
>Investigating the Incident</TITLE
><META
NAME="GENERATOR"
CONTENT="Modular DocBook HTML Stylesheet Version 1.7"><LINK
REL="HOME"
TITLE="Red Hat Enterprise Linux 4"
HREF="index.html"><LINK
REL="UP"
TITLE="Incident Response"
HREF="ch-response.html"><LINK
REL="PREVIOUS"
TITLE="Implementing the Incident Response Plan"
HREF="s1-response-implement.html"><LINK
REL="NEXT"
TITLE="Restoring and Recovering Resources"
HREF="s1-response-restore.html"><LINK
REL="STYLESHEET"
TYPE="text/css"
HREF="rhdocs-man.css"></HEAD
><BODY
CLASS="SECT1"
BGCOLOR="#FFFFFF"
TEXT="#000000"
LINK="#0000FF"
VLINK="#840084"
ALINK="#0000FF"
><DIV
CLASS="NAVHEADER"
><TABLE
SUMMARY="Header navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TH
COLSPAN="3"
ALIGN="center"
>Red Hat Enterprise Linux 4: Security Guide</TH
></TR
><TR
><TD
WIDTH="10%"
ALIGN="left"
VALIGN="bottom"
><A
HREF="s1-response-implement.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="80%"
ALIGN="center"
VALIGN="bottom"
>Chapter 10. Incident Response</TD
><TD
WIDTH="10%"
ALIGN="right"
VALIGN="bottom"
><A
HREF="s1-response-restore.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
></TABLE
><HR
ALIGN="LEFT"
WIDTH="100%"></DIV
><DIV
CLASS="SECT1"
><H1
CLASS="SECT1"
><A
NAME="S1-RESPONSE-INVEST"
>10.4. Investigating the Incident</A
></H1
><P
>	Investigating a computer breach is like investigating a crime
	scene. Detectives collect evidence, note any strange clues, and take
	inventory on loss and damage. An analysis of a computer compromise can
	either be done as the attack is happening or post-mortem.
      </P
><P
>	Although it is unwise to trust any system log files on an exploited
	system, there are other forensic utilities to aid in the analysis.  The
	purpose and features of these tools vary, but they commonly create
	bit-image copies of media, correlate events and processes, show low
	level file system information, and recover deleted files whenever
	possible.
      </P
><P
>	It is also a good idea to record of all of the investigatory actions
	executed on a compromised system by using the <TT
CLASS="COMMAND"
>script</TT
>
	command, as in the following example:
      </P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
><TT
CLASS="COMMAND"
>script -q <VAR
CLASS="REPLACEABLE"
>&#60;file-name&#62;</VAR
></TT
></PRE
></TD
></TR
></TABLE
><P
>	Replace <VAR
CLASS="REPLACEABLE"
>&#60;file-name&#62;</VAR
> with file name for
	the <TT
CLASS="COMMAND"
>script</TT
> log. Always save the log file on media
	other than the hard drive of the compromised system &#8212; a floppy
	disk or CD-ROM works particularly well for this purpose.
      </P
><P
>	By recording all your actions, an audit trail is created that may prove
	valuable if the attacker is ever caught.
      </P
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-RESPONSE-INVEST-IMAGE"
>10.4.1. Collecting an Evidential Image</A
></H2
><P
>Creating a bit-image copy of media is a feasible first step. If
	  performing data forensic work, it is a requirement. It is recommended
	  to make two copies: one for analysis and investigation, and a second
	  to be stored along with the original for evidence in any legal
	  proceedings.</P
><P
> You can use the <TT
CLASS="COMMAND"
>dd</TT
> command that is part of the
	  <TT
CLASS="FILENAME"
>coreutils</TT
> package in Red Hat Enterprise Linux to create a
	  monolithic image of an exploited system as evidence in an
	  investigation or for comparison with trusted images.  Suppose there is
	  a single hard drive from a system you want to image.  Attach that
	  drive as a slave to the system and then use <TT
CLASS="COMMAND"
>dd</TT
> to
	  create the image file, such as the following:</P
><TABLE
CLASS="SCREEN"
BGCOLOR="#DCDCDC"
WIDTH="100%"
><TR
><TD
><PRE
CLASS="SCREEN"
><TT
CLASS="COMMAND"
>dd if=/dev/hdd bs=1k conv=noerror,sync of=/home/evidence/image1</TT
></PRE
></TD
></TR
></TABLE
><P
>	  This command creates a single file named <TT
CLASS="FILENAME"
>image1</TT
>
	  using a 1k block size for speed.  The
	  <TT
CLASS="OPTION"
>conv=noerror,sync</TT
> options force <TT
CLASS="COMMAND"
>dd</TT
>
	  to continue reading and dumping data even if bad sectors are
	  encountered on the suspect drive. It is now possible to study the
	  resulting image file or even attempt to recover deleted files.
	</P
></DIV
><DIV
CLASS="SECT2"
><H2
CLASS="SECT2"
><A
NAME="S2-RESPONSE-INVEST-TOOL"
>10.4.2. Gathering Post-Breach Information</A
></H2
><P
>The topic of digital forensics and analysis itself is quite broad,
	  yet the tools are mostly architecture specific and cannot be applied
	  generically.  However, incident response, analysis, and recovery are
	  important topics. With proper knowledge and experience, Red Hat Enterprise Linux can be
	  an excellent platform for performing these types of analysis, as it
	  includes several utilities for performing post-breach response and
	  restoration.</P
><P
><A
HREF="s1-response-invest.html#TB-RESPONSE-COMMANDS"
>Table 10-1</A
> details some commands for
	  file auditing and management. It also lists some examples that can be
	  used to properly identify files and file attributes (such as
	  permissions and access dates) to allow the collection of further
	  evidence or items for analysis. These tools, when combined with
	  intrusion detection systems, firewalls, hardened services, and other
	  security measures, can help reduce the amount of potential damage when
	  an attack occurs.</P
><DIV
CLASS="NOTE"
><P
></P
><TABLE
CLASS="NOTE"
WIDTH="100%"
BORDER="0"
><TR
><TD
WIDTH="25"
ALIGN="CENTER"
VALIGN="TOP"
><IMG
SRC="./stylesheet-images/note.png"
HSPACE="5"
ALT="Note"></TD
><TH
ALIGN="LEFT"
VALIGN="CENTER"
><B
>Note</B
></TH
></TR
><TR
><TD
>&nbsp;</TD
><TD
ALIGN="LEFT"
VALIGN="TOP"
><P
>For detailed information about each tool, refer to their
	    respective man pages.
	  </P
></TD
></TR
></TABLE
></DIV
><DIV
CLASS="TABLE"
><A
NAME="TB-RESPONSE-COMMANDS"
></A
><TABLE
BORDER="1"
BGCOLOR="#DCDCDC"
CELLSPACING="0"
CELLPADDING="4"
CLASS="CALSTABLE"
><THEAD
><TR
><TH
>Command</TH
><TH
>Function</TH
><TH
>Example</TH
></TR
></THEAD
><TBODY
><TR
><TD
><TT
CLASS="COMMAND"
>dd</TT
></TD
><TD
>Creates a bit-image copy (or
		  <I
CLASS="FIRSTTERM"
>disk dump</I
>) of files and
		  partitions. Combined with a check of the md5sums of each image,
		  administrators can compare a pre-breach image of a partition or
		  file with a breached system to see if the sums match.
		</TD
><TD
><TT
CLASS="COMMAND"
>dd if=/bin/ls of=ls.dd |md5sum ls.dd &#62;ls-sum.txt
		  </TT
>
		</TD
></TR
><TR
><TD
><TT
CLASS="COMMAND"
>grep</TT
></TD
><TD
>Finds useful string (text) information inside files and
		  directories as well as reveals permissions, script changes,
		  file attributes, and more. Used mostly as a piped command of
		  for commands like <TT
CLASS="COMMAND"
>ls</TT
>,
		  <TT
CLASS="COMMAND"
>ps</TT
>, or <TT
CLASS="COMMAND"
>ifconfig</TT
>.</TD
><TD
><TT
CLASS="COMMAND"
>ps auxw |grep /bin</TT
></TD
></TR
><TR
><TD
><TT
CLASS="COMMAND"
>strings</TT
></TD
><TD
>Prints the strings of printable characters within a file. It is
		  most useful for auditing executables for anomalies such as 
		  <TT
CLASS="COMMAND"
>mail</TT
> commands to unknown addresses or logging to
		  a non-standard log file.</TD
><TD
><TT
CLASS="COMMAND"
>strings /bin/ps |grep 'mail'</TT
></TD
></TR
><TR
><TD
><TT
CLASS="COMMAND"
>file</TT
></TD
><TD
>Determines the
		characteristics of files based on format, encoding,
		linked-libraries (if any), and file type (binary, text, and
		more). It is useful for determining whether an executable such
		as <TT
CLASS="COMMAND"
>/bin/ls</TT
> has been modified using static
		libraries, which is a sure sign that the executable has been
		replaced with one installed by a malicious user.</TD
><TD
><TT
CLASS="COMMAND"
>file /bin/ls</TT
></TD
></TR
><TR
><TD
><TT
CLASS="COMMAND"
>find</TT
></TD
><TD
>Searches
		directories for particular files. It is a useful tool for
		searching the directory structure by keyword, date and time of
		access, permissions, and more. It can also be useful for
		administrators that perform general system audits of particular
		directories or files.
		</TD
><TD
><TT
CLASS="COMMAND"
>find -atime +12 -name *log* -perm
		    u+rw</TT
></TD
></TR
><TR
><TD
><TT
CLASS="COMMAND"
>stat</TT
></TD
><TD
>Displays file
		status information, including time last accessed, permissions,
		UID and GID bit settings, and more. It can be useful for
		checking when a breached system executable was last used or
		modified.</TD
><TD
><TT
CLASS="COMMAND"
>stat
		/bin/netstat</TT
></TD
></TR
><TR
><TD
><TT
CLASS="COMMAND"
>md5sum</TT
></TD
><TD
>Calculates the
		128-bit checksum using the md5 hash algorithm. Use this command
		to create a text file that lists all crucial executables that
		are often modified or replaced in a security
		compromise. Redirect the sums to a file to create a simple
		database of checksums and then copy the file onto a read-only
		medium such as CD-ROM.</TD
><TD
><TT
CLASS="COMMAND"
>md5sum
		/usr/bin/gdm &#62;&#62;md5sum.txt</TT
></TD
></TR
></TBODY
></TABLE
><P
><B
>Table 10-1. File Auditing Tools</B
></P
></DIV
></DIV
></DIV
><DIV
CLASS="NAVFOOTER"
><HR
ALIGN="LEFT"
WIDTH="100%"><TABLE
SUMMARY="Footer navigation table"
WIDTH="100%"
BORDER="0"
CELLPADDING="0"
CELLSPACING="0"
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
><A
HREF="s1-response-implement.html"
ACCESSKEY="P"
>Prev</A
></TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="index.html"
ACCESSKEY="H"
>Home</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
><A
HREF="s1-response-restore.html"
ACCESSKEY="N"
>Next</A
></TD
></TR
><TR
><TD
WIDTH="33%"
ALIGN="left"
VALIGN="top"
>Implementing the Incident Response Plan</TD
><TD
WIDTH="34%"
ALIGN="center"
VALIGN="top"
><A
HREF="ch-response.html"
ACCESSKEY="U"
>Up</A
></TD
><TD
WIDTH="33%"
ALIGN="right"
VALIGN="top"
>Restoring and Recovering Resources</TD
></TR
></TABLE
></DIV
></BODY
></HTML
>