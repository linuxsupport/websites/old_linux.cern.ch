<!--#include virtual="/linux/layout/header4.shtml" --> 
<script>setTitle('SLC4: Windows Terminal Services access');</script>

<h2>Using CERN Windows Terminal Services on Scientific Linux CERN</h2>

<h3>Prerequisites</h3>
<ul>
<li>Read <a href="http://terminalservices.web.cern.ch/terminalservices/">CERN Windows Terminal Services documentation</a>.
</li>
<li>WTS registration (<a href="http://terminalservices.web.cern.ch/terminalservices/">WTS registration page</a>)
</li>
<li>rdesktop and optionally tsclient packages.
</ul>
<br>
tsclient and rdesktop packages are most likely already installed on your system and should be updated automatically.<br>
If this is not the case, you can install them manually (as <tt>root</tt>):<br>
<ul>
<li>/usr/bin/yum install tsclient rdesktop
</ul>

<h3>Command line tool: Remote Desktop (rdesktop)</h3>
Example command line to connect to CERN WTS is as follows:
<pre>
rdesktop  -a 16 -u NICElogin -d CERN -g 800x600 cernts.cern.ch
</pre>
Above command will open connection to cernts.cern.ch with screen
resolution of 800x600 pixels, 16bit color depth (highly recommended, visible artifacts otherwise) and
preset your login name to NICElogin, login domain to CERN.
For more options see <tt>man rdesktop</tt>.
<br>
(Please note that the SLC rdesktop clients will use RDP v5 protocol by default)
<br>
<h3>Graphical user interface: Terminal Server Client (tsclient)</h3>
<table border="0">
<tr>
<td><a href="miscshots/wts-start.png"><img src="miscshots/wts-start-mini.png"></a></td>
<td>
<font size="-1">
Start Terminal Server Client by selecting it from G/K Menu -> Internet -> Terminal Server Client or typing
<b>tsclient</b> on command line.
</font>
</td>
</tr>
<tr>
<td><a href="miscshots/wts0.png"><img src="miscshots/wts0-mini.png"></a></td>
<td>
<font size="-1">
When interface starts supply following data in 'General' tab:<br>
<i>Computer</i>: <i><b>cernts.cern.ch</b></i><br>
<i>Protocol</i>: <i><b>RDPv5</b></i><br>
<i>User name</i>: <i><b>NICE login</b></i><br>
<i>Password</i>: <i><b>NICE password</b></i><br>
<i>Domain</i>: <i><b>CERN</b></i><br>
</font>
</td>
</tr>
<tr>
<td><a href="miscshots/wts1.png"><img src="miscshots/wts1-mini.png"></a></td>
<td>
<font size="-1">
In 'Display' tab adjust your settings:<br>
<i>Screen Size</i>: <b><i>800x600</i></b> (or more)<br>
<i>Color Depth</i>: <b><i>High Color (16 bit)</i></b><br>
</font>

</td>
</tr>
<tr>
<td><a href="miscshots/wts2.png"><img src="miscshots/wts2-mini.png"></a></td>
<td>
<font size="-1">
Clicking on 'Connect' will bring you to Windows Terminal Server login screen.
</font>
</td>
</tr>
<tr>
<td><a href="miscshots/wts3.png"><img src="miscshots/wts3-mini.png"></a></td>
<td>
<font size="-1">
After NICE login sequence completes you will be able to use installed (subset of) MS Windows applications.
</font>
</td>
</tr>


</table>
<h3>Support</h3>
In case of problems please report to <a href="mailto: linux.support@cern.ch">linux.support@cern.ch</a>
<br>

<div align="right"><a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a></div>	
<!--#include virtual="/linux/layout/footer.shtml" -->
