<!--#include virtual="/linux/layout/header5.shtml" -->
<script>setTitle('SLC5 - Managing configuration');</script>
<h3>Configuration Management</h3>

<ul>
<li><a href="#intro">Introduction</a>
<li><a href="#config">System Configuration</a>
<li><a href="#compconfig">Configuring Components</a>
<li><a href="#compunconfig">Un-configuring Components</a>
<li><a href="#compupdate">Updating Components</a>
</ul>


<a name="intro"></a><h4>Introduction</h4>

Scientific Linux CERN 5 systems use some of system configuration tools from <a href="http://quattor.org">Quattor</a> toolsuite.
<br>
These tools have been modified to allow standalone operation of the configuration
subsystem without the Quattor CDB (Configuration DataBase). The frontend command for
running configuration tools is <b>/usr/sbin/lcm</b> - Local Configuration Manager.
This system is used to preconfigure machine for CERN site wide defaults.
<br>
<br>
In the default system setup all necessary components are preinstalled
on Scientific Linux CERN 5 system.
<br>
<br>
Individual system settings shall be configured using provided system configuration tools.

<a name="config"></a><h4>System Configuration</h4>
The configuration file for the system is 
<pre>/etc/lcm.conf</pre>

in the default setup this file is empty and 
the LCM (Local Configuration Manager) uses built in defaults.
<br>
In order to see available configuration Components, driven by LCM 
you can run:
<pre>
#/usr/sbin/lcm --list
</pre>

<a name="compconfig"></a><h4>Configuring Components</h4>
In order to configure all components run:
<pre>
# /usr/sbin/lcm --configure --all
</pre>
In order to configure named component (for example global machine pine 
configuration), run:
<pre>
# /usr/sbin/lcm --configure pine
</pre>

<a name="compunconfig"></a><h4>Un-configuring Components</h4>
In order to un-configure component run:
<pre>
# /usr/sbin/lcm --unconfigure <i>component</i>
</pre>
(Please note that most of components do not actually implement the
unconfigure action ...)

<a name="compupdate"></a><h4>Updating installed Components</h4>
In order to update already configured components run:
<pre>
# /usr/sbin/lcm --update
</pre>

<!--#include virtual="/linux/layout/footer.shtml" -->
