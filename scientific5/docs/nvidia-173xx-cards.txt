    NVIDIA GPU product                                        Device PCI ID
    ------------------------------------------------------    ---------------
    GeForce 6800 Ultra                                        0x0040
    GeForce 6800                                              0x0041
    GeForce 6800 LE                                           0x0042
    GeForce 6800 XE                                           0x0043
    GeForce 6800 XT                                           0x0044
    GeForce 6800 GT                                           0x0045
    GeForce 6800 GT                                           0x0046
    GeForce 6800 GS                                           0x0047
    GeForce 6800 XT                                           0x0048
    GeForce 7800 GTX                                          0x0090
    GeForce 7800 GTX                                          0x0091
    GeForce 7800 GT                                           0x0092
    GeForce 7800 GS                                           0x0093
    GeForce 7800 SLI                                          0x0095
    GeForce Go 7800                                           0x0098
    GeForce Go 7800 GTX                                       0x0099
    GeForce 6800 GS                                           0x00C0
    GeForce 6800                                              0x00C1
    GeForce 6800 LE                                           0x00C2
    GeForce 6800 XT                                           0x00C3
    GeForce Go 6800                                           0x00C8
    GeForce Go 6800 Ultra                                     0x00C9
    GeForce 6800                                              0x00F0
    GeForce 6600 GT                                           0x00F1
    GeForce 6600                                              0x00F2
    GeForce 6200                                              0x00F3
    GeForce 6600 LE                                           0x00F4
    GeForce 7800 GS                                           0x00F5
    GeForce 6800 GS                                           0x00F6
    GeForce 6800 Ultra                                        0x00F9
    GeForce PCX 5750                                          0x00FA
    GeForce PCX 5900                                          0x00FB
    GeForce PCX 5300                                          0x00FC
    GeForce 6600 GT                                           0x0140
    GeForce 6600                                              0x0141
    GeForce 6600 LE                                           0x0142
    GeForce 6600 VE                                           0x0143
    GeForce Go 6600                                           0x0144
    GeForce 6610 XL                                           0x0145
    GeForce Go 6600 TE/6200 TE                                0x0146
    GeForce 6700 XL                                           0x0147
    GeForce Go 6600                                           0x0148
    GeForce Go 6600 GT                                        0x0149
    GeForce 6200                                              0x014F
    GeForce 6500                                              0x0160
    GeForce 6200 TurboCache(TM)                               0x0161
    GeForce 6200SE TurboCache(TM)                             0x0162
    GeForce 6200 LE                                           0x0163
    GeForce Go 6200                                           0x0164
    GeForce Go 6400                                           0x0166
    GeForce Go 6200                                           0x0167
    GeForce Go 6400                                           0x0168
    GeForce 6250                                              0x0169
    GeForce 7100 GS                                           0x016A
    GeForce 8800 GTX                                          0x0191
    GeForce 8800 GTS                                          0x0193
    GeForce 8800 Ultra                                        0x0194
    Tesla C870                                                0x0197
    GeForce 7350 LE                                           0x01D0
    GeForce 7300 LE                                           0x01D1
    GeForce 7300 SE/7200 GS                                   0x01D3
    GeForce Go 7200                                           0x01D6
    GeForce Go 7300                                           0x01D7
    GeForce Go 7400                                           0x01D8
    GeForce 7500 LE                                           0x01DD
    GeForce 7300 GS                                           0x01DF
    GeForce 6800                                              0x0211
    GeForce 6800 LE                                           0x0212
    GeForce 6800 GT                                           0x0215
    GeForce 6800 XT                                           0x0218
    GeForce 6200                                              0x0221
    GeForce 6200 A-LE                                         0x0222
    GeForce 6150                                              0x0240
    GeForce 6150 LE                                           0x0241
    GeForce 6100                                              0x0242
    GeForce Go 6150                                           0x0244
    GeForce Go 6100                                           0x0247
    GeForce 7900 GTX                                          0x0290
    GeForce 7900 GT/GTO                                       0x0291
    GeForce 7900 GS                                           0x0292
    GeForce 7950 GX2                                          0x0293
    GeForce 7950 GX2                                          0x0294
    GeForce 7950 GT                                           0x0295
    GeForce Go 7950 GTX                                       0x0297
    GeForce Go 7900 GS                                        0x0298
    GeForce Go 7900 GTX                                       0x0299
    GeForce 7600 GT                                           0x02E0
    GeForce 7600 GS                                           0x02E1
    GeForce 7900 GS                                           0x02E3
    GeForce 7950 GT                                           0x02E4
    GeForce FX 5800 Ultra                                     0x0301
    GeForce FX 5800                                           0x0302
    GeForce FX 5600 Ultra                                     0x0311
    GeForce FX 5600                                           0x0312
    GeForce FX 5600XT                                         0x0314
    GeForce FX Go5600                                         0x031A
    GeForce FX Go5650                                         0x031B
    GeForce FX 5200                                           0x0320
    GeForce FX 5200 Ultra                                     0x0321
    GeForce FX 5200                                           0x0322
    GeForce FX 5200LE                                         0x0323
    GeForce FX Go5200                                         0x0324
    GeForce FX Go5250                                         0x0325
    GeForce FX 5500                                           0x0326
    GeForce FX 5100                                           0x0327
    GeForce FX Go5200 32M/64M                                 0x0328
    GeForce FX Go53xx                                         0x032C
    GeForce FX Go5100                                         0x032D
    GeForce FX 5900 Ultra                                     0x0330
    GeForce FX 5900                                           0x0331
    GeForce FX 5900XT                                         0x0332
    GeForce FX 5950 Ultra                                     0x0333
    GeForce FX 5900ZT                                         0x0334
    GeForce FX 5700 Ultra                                     0x0341
    GeForce FX 5700                                           0x0342
    GeForce FX 5700LE                                         0x0343
    GeForce FX 5700VE                                         0x0344
    GeForce FX Go5700                                         0x0347
    GeForce FX Go5700                                         0x0348
    GeForce 7650 GS                                           0x0390
    GeForce 7600 GT                                           0x0391
    GeForce 7600 GS                                           0x0392
    GeForce 7300 GT                                           0x0393
    GeForce 7600 LE                                           0x0394
    GeForce 7300 GT                                           0x0395
    GeForce Go 7600                                           0x0398
    GeForce Go 7600 GT                                        0x0399
    GeForce 6150SE nForce 430                                 0x03D0
    GeForce 6100 nForce 405                                   0x03D1
    GeForce 6100 nForce 400                                   0x03D2
    GeForce 6100 nForce 420                                   0x03D5
    GeForce 8600 GTS                                          0x0400
    GeForce 8600 GT                                           0x0401
    GeForce 8600 GT                                           0x0402
    GeForce 8600 GS                                           0x0403
    GeForce 8400 GS                                           0x0404
    GeForce 9500M GS                                          0x0405
    GeForce 8600M GT                                          0x0407
    GeForce 9650M GS                                          0x0408
    GeForce 8700M GT                                          0x0409
    GeForce 8400 SE                                           0x0420
    GeForce 8500 GT                                           0x0421
    GeForce 8400 GS                                           0x0422
    GeForce 8300 GS                                           0x0423
    GeForce 8400 GS                                           0x0424
    GeForce 8600M GS                                          0x0425
    GeForce 8400M GT                                          0x0426
    GeForce 8400M GS                                          0x0427
    GeForce 8400M G                                           0x0428
    GeForce 9300M G                                           0x042E
    GeForce 7150M / nForce 630M                               0x0531
    GeForce 7000M / nForce 610M                               0x0533
    GeForce 7050 PV / NVIDIA nForce 630a                      0x053A
    GeForce 7050 PV / NVIDIA nForce 630a                      0x053B
    GeForce 7025 / NVIDIA nForce 630a                         0x053E
    GeForce 8800 GTS 512                                      0x0600
    GeForce 8800 GT                                           0x0602
    GeForce 9800 GX2                                          0x0604
    GeForce 8800 GS                                           0x0606
    GeForce 8800M GTS                                         0x0609
    GeForce 8800M GTX                                         0x060C
    GeForce 8800 GS                                           0x060D
    GeForce 9600 GSO                                          0x0610
    GeForce 8800 GT                                           0x0611
    GeForce 9800 GTX                                          0x0612
    GeForce 9600 GT                                           0x0622
    GeForce 9600M GT                                          0x0647
    GeForce 9600M GS                                          0x0648
    GeForce 9600M GT                                          0x0649
    GeForce 9500M G                                           0x064B
    GeForce 8400 GS                                           0x06E4
    GeForce 9300M GS                                          0x06E5
    GeForce 9200M GS                                          0x06E8
    GeForce 9300M GS                                          0x06E9
    GeForce 7150 / NVIDIA nForce 630i                         0x07E0
    GeForce 7100 / NVIDIA nForce 630i                         0x07E1
    GeForce 7050 / NVIDIA nForce 610i                         0x07E3
    GeForce 9100M G                                           0x0844
    GeForce 8300                                              0x0848
    GeForce 8200                                              0x0849
    nForce 730a                                               0x084A
    GeForce 8200                                              0x084B
    GeForce 8100 / nForce 720a                                0x084F

    NVIDIA QUADRO GPUS


    NVIDIA GPU product                                        Device PCI ID
    ------------------------------------------------------    ---------------
    Quadro FX 4000                                            0x004E
    Quadro FX 4500                                            0x009D
   Quadro FX Go1400                                          0x00CC
    Quadro FX 3450/4000 SDI                                   0x00CD
    Quadro FX 1400                                            0x00CE
    Quadro FX 4400/Quadro FX 3400                             0x00F8
    Quadro FX 330                                             0x00FC
    Quadro NVS 280 PCI-E/Quadro FX 330                        0x00FD
    Quadro FX 1300                                            0x00FE
    Quadro NVS 440                                            0x014A
    Quadro FX 540M                                            0x014C
    Quadro FX 550                                             0x014D
    Quadro FX 540                                             0x014E
    Quadro NVS 285                                            0x0165
    Quadro FX 5600                                            0x019D
    Quadro FX 4600                                            0x019E
    Quadro NVS 110M                                           0x01D7
    Quadro NVS 110M                                           0x01DA
    Quadro NVS 120M                                           0x01DB
    Quadro FX 350M                                            0x01DC
    Quadro FX 350                                             0x01DE
    Quadro NVS 210S / NVIDIA GeForce 6150LE                   0x0245
    Quadro FX 2500M                                           0x029A
    Quadro FX 1500M                                           0x029B
    Quadro FX 5500                                            0x029C
    Quadro FX 3500                                            0x029D
    Quadro FX 1500                                            0x029E
    Quadro FX 4500 X2                                         0x029F
    Quadro FX 2000                                            0x0308
    Quadro FX 1000                                            0x0309
    Quadro FX Go700                                           0x031C
    Quadro NVS 55/280 PCI                                     0x032A
    Quadro FX 500/FX 600                                      0x032B
    Quadro FX 3000                                            0x0338
    Quadro FX 700                                             0x033F
    Quadro FX Go1000                                          0x034C
    Quadro FX 1100                                            0x034E
    Quadro FX 560                                             0x039E
    Quadro FX 370                                             0x040A
    Quadro NVS 320M                                           0x040B
    Quadro FX 570M                                            0x040C
    Quadro FX 1600M                                           0x040D
    Quadro FX 570                                             0x040E
    Quadro FX 1700                                            0x040F
    Quadro NVS 140M                                           0x0429
    Quadro NVS 130M                                           0x042A
    Quadro NVS 135M                                           0x042B
    Quadro FX 360M                                            0x042D
    Quadro NVS 290                                            0x042F
    Quadro FX 3700                                            0x061A
    Quadro FX 3600M                                           0x061C

