<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>2.4. Considerations for Configuring HA Services</title><link rel="stylesheet" href="./Common_Content/css/default.css" type="text/css"/><meta name="generator" content="publican"/><meta name="package" content="Red_Hat_Enterprise_Linux-Cluster_Administration-5-en-US-3-13"/><link rel="home" href="index.html" title="Cluster Administration"/><link rel="up" href="ch-before-config-CA.html" title="Chapter 2. Before Configuring a Red Hat Cluster"/><link rel="prev" href="s2-acpi-disable-boot-CA.html" title="2.3.3. Disabling ACPI Completely in the grub.conf File"/><link rel="next" href="s1-max-luns-CA.html" title="2.5. Configuring max_luns"/></head><body class=""><p id="title"><a class="left" href="http://www.redhat.com"><img src="Common_Content/images/image_left.png" alt="Product Site"/></a><a class="right" href="http://www.redhat.com/docs"><img src="Common_Content/images/image_right.png" alt="Documentation Site"/></a></p><ul class="docnav"><li class="previous"><a accesskey="p" href="s2-acpi-disable-boot-CA.html"><strong>Prev</strong></a></li><li class="next"><a accesskey="n" href="s1-max-luns-CA.html"><strong>Next</strong></a></li></ul><div class="section" lang="en-US"><div class="titlepage"><div><div><h2 class="title" id="s1-clust-svc-ov-CA">2.4. Considerations for Configuring HA Services</h2></div></div></div><a id="d0e2090" class="indexterm"/><a id="d0e2095" class="indexterm"/><div class="para">
			You can create a cluster to suit your needs for high availability by configuring HA (high-availability) services. The key component for HA service management in a Red Hat cluster, <code class="command">rgmanager</code>, implements cold failover for off-the-shelf applications. In a Red Hat cluster, an application is configured with other cluster resources to form an HA service that can fail over from one cluster node to another with no apparent interruption to cluster clients. HA-service failover can occur if a cluster node fails or if a cluster system administrator moves the service from one cluster node to another (for example, for a planned outage of a cluster node).
		</div><div class="para">
			To create an HA service, you must configure it in the cluster configuration file. An HA service comprises cluster <em class="firstterm">resources</em>. Cluster resources are building blocks that you create and manage in the cluster configuration file — for example, an IP address, an application initialization script, or a Red Hat GFS shared partition.
		</div><div class="para">
			An HA service can run on only one cluster node at a time to maintain data integrity. You can specify failover priority in a failover domain. Specifying failover priority consists of assigning a priority level to each node in a failover domain. The priority level determines the failover order — determining which node that an HA service should fail over to. If you do not specify failover priority, an HA service can fail over to any node in its failover domain. Also, you can specify if an HA service is restricted to run only on nodes of its associated failover domain. (When associated with an unrestricted failover domain, an HA service can start on any cluster node in the event no member of the failover domain is available.)
		</div><div class="para">
			<a class="xref" href="s1-clust-svc-ov-CA.html#fig-ha-svc-example-webserver-CA" title="Figure 2.1. Web Server Cluster Service Example">Figure 2.1, “Web Server Cluster Service Example”</a> shows an example of an HA service that is a web server named "content-webserver". It is running in cluster node B and is in a failover domain that consists of nodes A, B, and D. In addition, the failover domain is configured with a failover priority to fail over to node D before node A and to restrict failover to nodes only in that failover domain. The HA service comprises these cluster resources:
		</div><div class="itemizedlist"><ul><li><div class="para">
					IP address resource — IP address 10.10.10.201.
				</div></li><li><div class="para">
					An application resource named "httpd-content" — a web server application init script <code class="filename">/etc/init.d/httpd</code> (specifying <code class="command">httpd</code>).
				</div></li><li><div class="para">
					A file system resource — Red Hat GFS named "gfs-content-webserver".
				</div></li></ul></div><div class="figure" id="fig-ha-svc-example-webserver-CA"><div class="figure-contents"><div class="mediaobject"><img src="./images/ha-svc-example-webserver.png" alt="Web Server Cluster Service Example"/><div class="longdesc"><div class="para">
						Web Server Cluster Service Example
					</div></div></div></div><h6>Figure 2.1. Web Server Cluster Service Example</h6></div><br class="figure-break"/><div class="para">
			Clients access the HA service through the IP address 10.10.10.201, enabling interaction with the web server application, httpd-content. The httpd-content application uses the gfs-content-webserver file system. If node B were to fail, the content-webserver HA service would fail over to node D. If node D were not available or also failed, the service would fail over to node A. Failover would occur with minimal service interruption to the cluster clients. For example, in an HTTP service, certain state information may be lost (like session data). The HA service would be accessible from another cluster node via the same IP address as it was before failover.
		</div><div class="note"><h2>Note</h2><div class="para">
				For more information about HA services and failover domains, refer to <em class="citetitle">Red Hat Cluster Suite Overview</em>. For information about configuring failover domains, refer to <a class="xref" href="s1-config-failover-domain-conga-CA.html" title="3.7. Configuring a Failover Domain">Section 3.7, “Configuring a Failover Domain”</a> (using <span class="application"><strong>Conga</strong></span>) or <a class="xref" href="s1-config-failover-domain-CA.html" title="5.6. Configuring a Failover Domain">Section 5.6, “Configuring a Failover Domain”</a> (using <code class="command">system-config-cluster</code>).
			</div></div><div class="para"> An HA service is a group of cluster resources
configured into a coherent entity that provides specialized services
to clients. An HA service is represented as a resource tree in the
cluster configuration file,
<code class="filename">/etc/cluster/cluster.conf</code> (in each cluster
node). In the cluster configuration file, each resource tree is an XML
representation that specifies each resource, its attributes, and its
relationship among other resources in the resource tree (parent,
child, and sibling relationships).</div><div class="note"><h2>Note</h2><div class="para">
	Because an HA service consists of resources organized into a
	hierarchical tree, a service is sometimes referred to as a
	<em class="firstterm">resource tree</em> or <em class="firstterm">resource
	group</em>. Both phrases are synonymous with
	<span class="emphasis"><em>HA service</em></span>.
      </div></div><div class="para">
       At the root of each resource tree is a special type of resource
       — a <em class="firstterm">service resource</em>. Other types of resources comprise
       the rest of a service, determining its
       characteristics. Configuring an HA service consists of
       creating a service resource, creating subordinate cluster
       resources, and organizing them into a coherent entity that
       conforms to hierarchical restrictions of the service.
    </div><div class="para">
			Red Hat Cluster supports the following HA services:
		</div><div class="itemizedlist"><ul><li><div class="para">
					Apache
				</div></li><li><div class="para">
					Application (Script)
				</div></li><li><div class="para">
					LVM (HA LVM)
				</div></li><li><div class="para">
					MySQL
				</div></li><li><div class="para">
					NFS
				</div></li><li><div class="para">
					Open LDAP
				</div></li><li><div class="para">
					Oracle
				</div></li><li><div class="para">
					PostgreSQL 8
				</div></li><li><div class="para">
					Samba
				</div></li><li><div class="para">
					SAP
				</div></li><li><div class="para">
					Tomcat 5
				</div></li></ul></div><div class="para">
			There are two major considerations to take into account when configuring an HA service:
		</div><div class="itemizedlist"><ul><li><div class="para">
					The types of resources needed to create a service
				</div></li><li><div class="para">
					Parent, child, and sibling relationships among resources
				</div></li></ul></div><div class="para">
			The types of resources and the hierarchy of resources depend on the type of service you are configuring.
		</div><a id="d0e2232" class="indexterm"/><a id="d0e2235" class="indexterm"/><div class="para">
			The types of cluster resources are listed in <a class="xref" href="ap-ha-resource-params-CA.html" title="Appendix C. HA Resource Parameters">Appendix C, <i>HA Resource Parameters</i></a>. Information about parent, child, and sibling relationships among resources is described in <a class="xref" href="ap-ha-resource-behavior-CA.html" title="Appendix D. HA Resource Behavior">Appendix D, <i>HA Resource Behavior</i></a>.
		</div></div><ul class="docnav"><li class="previous"><a accesskey="p" href="s2-acpi-disable-boot-CA.html"><strong>Prev</strong>2.3.3. Disabling ACPI Completely in the grub.conf...</a></li><li class="up"><a accesskey="u" href="#"><strong>Up</strong></a></li><li class="home"><a accesskey="h" href="index.html"><strong>Home</strong></a></li><li class="next"><a accesskey="n" href="s1-max-luns-CA.html"><strong>Next</strong>2.5. Configuring max_luns</a></li></ul></body></html>