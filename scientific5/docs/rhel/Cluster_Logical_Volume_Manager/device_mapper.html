<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Appendix A. The Device Mapper</title><link rel="stylesheet" href="./Common_Content/css/default.css" type="text/css"/><meta name="generator" content="publican"/><meta name="package" content="Red_Hat_Enterprise_Linux-Cluster_Logical_Volume_Manager-5-en-US-3-3"/><link rel="home" href="index.html" title="Cluster Logical Volume Manager"/><link rel="up" href="index.html" title="Cluster Logical Volume Manager"/><link rel="prev" href="LVM_GUI.html" title="Chapter 7. LVM Administration with the LVM GUI"/><link rel="next" href="dmsetup.html" title="A.2. The dmsetup Command"/></head><body class=""><p id="title"><a class="left" href="http://www.redhat.com"><img src="Common_Content/images/image_left.png" alt="Product Site"/></a><a class="right" href="http://www.redhat.com/docs"><img src="Common_Content/images/image_right.png" alt="Documentation Site"/></a></p><ul class="docnav"><li class="previous"><a accesskey="p" href="LVM_GUI.html"><strong>Prev</strong></a></li><li class="next"><a accesskey="n" href="dmsetup.html"><strong>Next</strong></a></li></ul><div class="appendix" lang="en-US"><div class="titlepage"><div><div><h1 id="device_mapper" class="title">The Device Mapper</h1></div></div></div><div class="para">
		The Device Mapper is a kernel driver that provides a framework for volume management. It provides a generic way of creating mapped devices, which may be used as logical volumes. It does not specifically know about volume groups or metadata formats.
	</div><div class="para">
		The Device Mapper provides the foundation for a number of higher-level technologies. In addition to LVM, Device-Mapper multipath and the <code class="command">dmraid</code> command use the Device Mapper. The application interface to the Device Mapper is the <code class="command">ioctl</code> system call. The user interface is the <code class="command">dmsetup</code> command.
	</div><div class="para">
		LVM logical volumes are activated using the Device Mapper. Each logical volume is translated into a mapped device. Each segment translates into a line in the mapping table that describes the device. The Device Mapper supports a variety of mapping targets, including linear mapping, striped mapping, and error mapping. So, for example, two disks may be concatenated into one logical volume with a pair of linear mappings, one for each disk. When LVM2 creates a volume, it creates an underlying device-mapper device that can be queried with the <code class="command">dmsetup</code> command. For information about the format of devices in a mapping table, see <a class="xref" href="device_mapper.html#dm-mappings" title="A.1. Device Table Mappings">Section A.1, “Device Table Mappings”</a>. For information about using the <code class="command">dmsetup</code> command to query a device, see <a class="xref" href="dmsetup.html" title="A.2. The dmsetup Command">Section A.2, “The dmsetup Command”</a>.
	</div><div class="section" lang="en-US"><div class="titlepage"><div><div><h2 class="title" id="dm-mappings">A.1. Device Table Mappings</h2></div></div></div><div class="para">
			A mapped device is defined by a table that specifies how to map each range of logical sectors of the device using a supported Device Table mapping. The table for a mapped device is constructed from a list of lines of the form:
		</div><pre class="screen">
<em class="parameter"><code>start length mapping</code></em> [<em class="parameter"><code>mapping_parameters...</code></em>]
</pre><div class="para">
			In the first line of a Device Mapper table, the <em class="parameter"><code>start</code></em> parameter must equal 0. The <em class="parameter"><code>start</code></em> + <em class="parameter"><code>length</code></em> parameters on one line must equal the <em class="parameter"><code>start</code></em> on the next line. Which mapping parameters are specified in a line of the mapping table depends on which <em class="parameter"><code>mapping</code></em> type is specified on the line.
		</div><div class="para">
			Sizes in the Device Mapper are always specified in sectors (512 bytes).
		</div><div class="para">
			When a device is specified as a mapping parameter in the Device Mapper, it can be referenced by the device name in the filesystem (for example, <code class="literal">/dev/hda</code>) or by the major and minor numbers in the format <em class="parameter"><code>major</code></em>:<em class="parameter"><code>minor</code></em>. The major:minor format is preferred because it avoids pathname lookups.
		</div><div class="para">
			The following shows a sample mapping table for a device. In this table there are four linear targets:
		</div><pre class="screen">
0 35258368 linear 8:48 65920
35258368 35258368 linear 8:32 65920
70516736 17694720 linear 8:16 17694976
88211456 17694720 linear 8:16 256
</pre><div class="para">
			The first 2 parameters of each line are the segment starting block and the length of the segment. The next keyword is the mapping target, which in all of the cases in this example is <code class="literal">linear</code>. The rest of the line consists of the parameters for a <code class="literal">linear</code> target.
		</div><div class="para">
			The following subsections describe the format of the following mappings:
		</div><div class="itemizedlist"><ul><li><div class="para">
					linear
				</div></li><li><div class="para">
					striped
				</div></li><li><div class="para">
					mirror
				</div></li><li><div class="para">
					snapshot and snapshot-origin
				</div></li><li><div class="para">
					error
				</div></li><li><div class="para">
					zero
				</div></li><li><div class="para">
					multipath
				</div></li><li><div class="para">
					crypt
				</div></li></ul></div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="linear-map">A.1.1. The linear Mapping Target</h3></div></div></div><div class="para">
				A linear mapping target maps a continuous range of blocks onto another block device. The format of a linear target is as follows:
			</div><pre class="screen">
<em class="parameter"><code>start length</code></em> linear <em class="parameter"><code>device offset</code></em>
</pre><div class="variablelist"><dl><dt><span class="term"><em class="parameter"><code>start</code></em></span></dt><dd><div class="para">
							starting block in virtual device
						</div></dd><dt><span class="term"><em class="parameter"><code>length</code></em></span></dt><dd><div class="para">
							length of this segment
						</div></dd><dt><span class="term"><em class="parameter"><code>device</code></em></span></dt><dd><div class="para">
							block device, referenced by the device name in the filesystem or by the major and minor numbers in the format <em class="parameter"><code>major</code></em>:<em class="parameter"><code>minor</code></em>
						</div></dd><dt><span class="term"><em class="parameter"><code>offset</code></em></span></dt><dd><div class="para">
							starting offset of the mapping on the device
						</div></dd></dl></div><div class="para">
				The following example shows a linear target with a starting block in the virtual device of 0, a segment length of 1638400, a major:minor number pair of 8:2, and a starting offset for the device of 41146992.
			</div><pre class="screen">
0 16384000 linear 8:2 41156992
</pre><div class="para">
				The following example shows a linear target with the device parameter specified as the device <code class="filename">/dev/hda</code>.
			</div><pre class="screen">
0 20971520 linear /dev/hda 384
</pre></div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="striped-map">A.1.2. The striped Mapping Target</h3></div></div></div><div class="para">
				The striped mapping target supports striping across physical devices. It takes as arguments the number of stripes and the striping chunk size followed by a list of pairs of device name and sector. The format of a striped target is as follows:
			</div><pre class="screen">
<em class="parameter"><code>start length</code></em> striped <em class="parameter"><code>#stripes chunk_size device1 offset1 ... deviceN offsetN</code></em>
</pre><div class="para">
				There is one set of <em class="parameter"><code>device</code></em> and <em class="parameter"><code>offset</code></em> parameters for each stripe.
			</div><div class="variablelist"><dl><dt><span class="term"><em class="parameter"><code>start</code></em></span></dt><dd><div class="para">
							starting block in virtual device
						</div></dd><dt><span class="term"><em class="parameter"><code>length</code></em></span></dt><dd><div class="para">
							length of this segment
						</div></dd><dt><span class="term"><em class="parameter"><code>#stripes</code></em></span></dt><dd><div class="para">
							number of stripes for the virtual device
						</div></dd><dt><span class="term"><em class="parameter"><code>chunk_size</code></em></span></dt><dd><div class="para">
							number of sectors written to each stripe before switching to the next; must be power of 2 at least as big as the kernel page size
						</div></dd><dt><span class="term"><em class="parameter"><code>device</code></em></span></dt><dd><div class="para">
							block device, referenced by the device name in the filesystem or by the major and minor numbers in the format <em class="parameter"><code>major</code></em>:<em class="parameter"><code>minor</code></em>.
						</div></dd><dt><span class="term"><em class="parameter"><code>offset</code></em></span></dt><dd><div class="para">
							starting offset of the mapping on the device
						</div></dd></dl></div><div class="para">
				The following example shows a striped target with three stripes and a chunk size of 128:
			</div><pre class="screen">
0 73728 striped 3 128 8:9 384 8:8 384 8:7 9789824
</pre><div class="variablelist"><dl><dt><span class="term">0</span></dt><dd><div class="para">
							starting block in virtual device
						</div></dd><dt><span class="term">73728</span></dt><dd><div class="para">
							length of this segment
						</div></dd><dt><span class="term">striped 3 128</span></dt><dd><div class="para">
							stripe across three devices with chunk size of 128 blocks
						</div></dd><dt><span class="term">8:9</span></dt><dd><div class="para">
							major:minor numbers of first device
						</div></dd><dt><span class="term">384</span></dt><dd><div class="para">
							starting offset of the mapping on the first device
						</div></dd><dt><span class="term">8:8</span></dt><dd><div class="para">
							major:minor numbers of second device
						</div></dd><dt><span class="term">384</span></dt><dd><div class="para">
							starting offset of the mapping on the second device
						</div></dd><dt><span class="term">8:7</span></dt><dd><div class="para">
							major:minor numbers of of third device
						</div></dd><dt><span class="term">9789824</span></dt><dd><div class="para">
							starting offset of the mapping on the third device
						</div></dd></dl></div><div class="para">
				The following example shows a striped target for 2 stripes with 256 KiB chunks, with the device parameters specified by the device names in the file system rather than by the major and minor numbers.
			</div><pre class="screen">
0 65536 striped 2 512 /dev/hda 0 /dev/hdb 0
</pre></div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="mirror-map">A.1.3. The mirror Mapping Target</h3></div></div></div><div class="para">
				The mirror mapping target supports the mapping of a mirrored logical device. The format of a mirrored target is as follows:
			</div><pre class="screen">
<em class="parameter"><code>start length</code></em> mirror <em class="parameter"><code>log_type #logargs logarg1 ... logargN #devs device1 offset1 ... deviceN offsetN</code></em>
</pre><div class="variablelist"><dl><dt><span class="term"><em class="parameter"><code>start</code></em></span></dt><dd><div class="para">
							starting block in virtual device
						</div></dd><dt><span class="term"><em class="parameter"><code>length</code></em></span></dt><dd><div class="para">
							length of this segment
						</div></dd><dt><span class="term"><em class="parameter"><code>log_type</code></em></span></dt><dd><div class="para">
							The possible log types and their arguments are as follows:
						</div><div class="variablelist"><dl><dt><span class="term"><code class="literal">core</code></span></dt><dd><div class="para">
										The mirror is local and the mirror log is kept in core memory. This log type takes 1 - 3 arguments:
									</div><div class="para">
										<em class="replaceable"><code>regionsize</code></em> [[<code class="literal">no</code>]<code class="literal">sync</code>] [<code class="literal">block_on_error</code>]
									</div></dd><dt><span class="term"><code class="literal">disk</code></span></dt><dd><div class="para">
										The mirror is local and the mirror log is kept on disk. This log type takes 2 - 4 arguments:
									</div><div class="para">
										<em class="replaceable"><code>logdevice regionsize</code></em> [[<code class="literal">no</code>]<code class="literal">sync</code>] [<code class="literal">block_on_error</code>]
									</div></dd><dt><span class="term"><code class="literal">clustered_core</code></span></dt><dd><div class="para">
										The mirror is clustered and the mirror log is kept in core memory. This log type takes 2 - 4 arguments:
									</div><div class="para">
										<em class="replaceable"><code>regionsize UUID</code></em> [[<code class="literal">no</code>]<code class="literal">sync</code>] [<code class="literal">block_on_error</code>]
									</div></dd><dt><span class="term"><code class="literal">clustered_disk</code></span></dt><dd><div class="para">
										The mirror is clustered and the mirror log is kept on disk. This log type takes 3 - 5 arguments:
									</div><div class="para">
										<em class="replaceable"><code>logdevice regionsize UUID</code></em> [[<code class="literal">no</code>]<code class="literal">sync</code>] [<code class="literal">block_on_error</code>]
									</div></dd></dl></div><div class="para">
							LVM maintains a small log which it uses to keep track of which regions are in sync with the mirror or mirrors. The <em class="replaceable"><code>regionsize</code></em> argument specifies the size of these regions.
						</div><div class="para">
							In a clustered environment, the <em class="replaceable"><code>UUID</code></em> argument is a unique identifier associated with the mirror log device so that the log state can be maintained throughout the cluster.
						</div><div class="para">
							The optional <code class="literal">[no]sync</code> argument can be used to specify the mirror as "in-sync" or "out-of-sync". The <code class="literal">block_on_error</code> argument is used to tell the mirror to respond to errors rather than ignoring them.
						</div></dd><dt><span class="term"><em class="parameter"><code>#log_args</code></em></span></dt><dd><div class="para">
							number of log arguments that will be specified in the mapping
						</div></dd><dt><span class="term"><em class="parameter"><code>logargs</code></em></span></dt><dd><div class="para">
							the log arguments for the mirror; the number of log arguments provided is specified by the <em class="parameter"><code>#log-args</code></em> parameter and the valid log arguments are determined by the <em class="parameter"><code>log_type</code></em> parameter.
						</div></dd><dt><span class="term"><em class="parameter"><code>#devs</code></em></span></dt><dd><div class="para">
							the number of legs in the mirror; a device and an offset is specifed for each leg.
						</div></dd><dt><span class="term"><em class="parameter"><code>device</code></em></span></dt><dd><div class="para">
							block device for each mirror leg, referenced by the device name in the filesystem or by the major and minor numbers in the format <em class="parameter"><code>major</code></em>:<em class="parameter"><code>minor</code></em>. A block device and offset is specified for each mirror leg, as indicated by the <em class="parameter"><code>#devs</code></em> parameter.
						</div></dd><dt><span class="term"><em class="parameter"><code>offset</code></em></span></dt><dd><div class="para">
							starting offset of the mapping on the device. A block device and offset is specified for each mirror leg, as indicated by the <em class="parameter"><code>#devs</code></em> parameter.
						</div></dd></dl></div><div class="para">
				The following example shows a mirror mapping target for a clustered mirror with a mirror log kept on disk.
			</div><pre class="screen">
0 52428800 mirror clustered_disk 4 253:2 1024 <em class="replaceable"><code>UUID</code></em> block_on_error 3 253:3 0 253:4 0 253:5 0
</pre><div class="variablelist"><dl><dt><span class="term">0</span></dt><dd><div class="para">
							starting block in virtual device
						</div></dd><dt><span class="term">52428800</span></dt><dd><div class="para">
							length of this segment
						</div></dd><dt><span class="term">mirror clustered_disk</span></dt><dd><div class="para">
							mirror target with a log type specifying that mirror is clustered and the mirror log is maintained on disk
						</div></dd><dt><span class="term">4</span></dt><dd><div class="para">
							4 mirror log arguments will follow
						</div></dd><dt><span class="term">253:2</span></dt><dd><div class="para">
							major:minor numbers of log device
						</div></dd><dt><span class="term">1024</span></dt><dd><div class="para">
							region size the mirror log uses to keep track of what is in sync
						</div></dd><dt><span class="term"><em class="parameter"><code>UUID</code></em></span></dt><dd><div class="para">
							UUID of mirror log device to maintain log information throughout a cluster
						</div></dd><dt><span class="term"><code class="literal">block_on_error</code></span></dt><dd><div class="para">
							mirror should respond to errors
						</div></dd><dt><span class="term">3</span></dt><dd><div class="para">
							number of legs in mirror
						</div></dd><dt><span class="term">253:3 0 253:4 0 253:5 0</span></dt><dd><div class="para">
							major:minor numbers and offset for devices constituting each leg of mirror
						</div></dd></dl></div></div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="snapshot-map">A.1.4. The snapshot and snapshot-origin Mapping Targets</h3></div></div></div><div class="para">
				When you create the first LVM snapshot of a volume, four Device Mapper devices are used:
			</div><div class="orderedlist"><ol><li><div class="para">
						A device with a <code class="literal">linear</code> mapping containing the original mapping table of the source volume.
					</div></li><li><div class="para">
						A device with a <code class="literal">linear</code> mapping used as the copy-on-write (COW) device for the source volume; for each write, the original data is saved in the COW device of each snapshot to keep its visible content unchanged (until the COW device fills up).
					</div></li><li><div class="para">
						A device with a <code class="literal">snapshot</code> mapping combining #1 and #2, which is the visible snapshot volume
					</div></li><li><div class="para">
						The "original" volume (which uses the device number used by the original source volume), whose table is replaced by a "snapshot-origin" mapping from device #1.
					</div></li></ol></div><div class="para">
				A fixed naming scheme is used to create these devices, For example, you might use the following commands to create an LVM volume named <code class="literal">base</code> and a snapshot volume named <code class="literal">snap</code> based on that volume.
			</div><pre class="screen">
# <strong class="userinput"><code>lvcreate -L 1G -n base volumeGroup</code></strong>
# <strong class="userinput"><code>lvcreate -L 100M --snapshot -n snap volumeGroup/base</code></strong>
</pre><div class="para">
				This yields four devices, which you can view with the following commands:
			</div><pre class="screen">
# <strong class="userinput"><code>dmsetup table|grep volumeGroup</code></strong>
volumeGroup-base-real: 0 2097152 linear 8:19 384
volumeGroup-snap-cow: 0 204800 linear 8:19 2097536
volumeGroup-snap: 0 2097152 snapshot 254:11 254:12 P 16
volumeGroup-base: 0 2097152 snapshot-origin 254:11

# <strong class="userinput"><code>ls -lL /dev/mapper/volumeGroup-*</code></strong>
brw-------  1 root root 254, 11 29 ago 18:15 /dev/mapper/volumeGroup-base-real
brw-------  1 root root 254, 12 29 ago 18:15 /dev/mapper/volumeGroup-snap-cow
brw-------  1 root root 254, 13 29 ago 18:15 /dev/mapper/volumeGroup-snap
brw-------  1 root root 254, 10 29 ago 18:14 /dev/mapper/volumeGroup-base
</pre><div class="para">
				The format for the <code class="literal">snapshot-origin</code> target is as follows:
			</div><pre class="screen">
<em class="parameter"><code>start length</code></em> snapshot-origin <em class="parameter"><code>origin</code></em>
</pre><div class="variablelist"><dl><dt><span class="term"><em class="parameter"><code>start</code></em></span></dt><dd><div class="para">
							starting block in virtual device
						</div></dd><dt><span class="term"><em class="parameter"><code>length</code></em></span></dt><dd><div class="para">
							length of this segment
						</div></dd><dt><span class="term"><em class="parameter"><code>origin</code></em></span></dt><dd><div class="para">
							base volume of snapshot
						</div></dd></dl></div><div class="para">
				The <code class="literal">snapshot-origin</code> will normally have one or more snapshots based on it. Reads will be mapped directly to the backing device. For each write, the original data will be saved in the COW device of each snapshot to keep its visible content unchanged until the COW device fills up.
			</div><div class="para">
				The format for the <code class="literal">snapshot</code> target is as follows:
			</div><pre class="screen">
<em class="parameter"><code>start length</code></em> snapshot <em class="parameter"><code>origin COW-device</code></em> P|N <em class="parameter"><code>chunksize</code></em>
</pre><div class="variablelist"><dl><dt><span class="term"><em class="parameter"><code>start</code></em></span></dt><dd><div class="para">
							starting block in virtual device
						</div></dd><dt><span class="term"><em class="parameter"><code>length</code></em></span></dt><dd><div class="para">
							length of this segment
						</div></dd><dt><span class="term"><em class="parameter"><code>origin</code></em></span></dt><dd><div class="para">
							base volume of snapshot
						</div></dd><dt><span class="term"><em class="parameter"><code>COW-device</code></em></span></dt><dd><div class="para">
							Device on which changed chunks of data are stored
						</div></dd><dt><span class="term">P|N</span></dt><dd><div class="para">
							P (Persistent) or N (Not persistent); indicates whether snapshot will survive after reboot. For transient snapshots (N) less metadata must be saved on disk; they can be kept in memory by the kernel.
						</div></dd><dt><span class="term"><em class="parameter"><code>chunksize</code></em></span></dt><dd><div class="para">
							Size in sectors of changed chunks of data that will be stored on the COW device
						</div></dd></dl></div><div class="para">
				The following example shows a <code class="literal">snapshot-origin</code> target with an origin device of 254:11.
			</div><pre class="screen">
0 2097152 snapshot-origin 254:11
</pre><div class="para">
				The following example shows a <code class="literal">snapshot</code> target with an origin device of 254:11 and a COW device of 254:12. This snapshot device is persistent across reboots and the chunk size for the data stored on the COW device is 16 sectors.
			</div><pre class="screen">
0 2097152 snapshot 254:11 254:12 P 16
</pre></div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="error-map">A.1.5. The error Mapping Target</h3></div></div></div><div class="para">
				With an error mapping target, any I/O operation to the mapped sector fails.
			</div><div class="para">
				An error mapping target can be used for testing. To test how a device behaves in failure, you can create a device mapping with a bad sector in the middle of a device, or you can swap out the leg of a mirror and replace the leg with an error target.
			</div><div class="para">
				An error target can be used in place of a failing device, as a way of avoiding tiemouts and retries on the actual device. It can serve as an intermediate target while you rearrange LVM metadata during failures.
			</div><div class="para">
				The <code class="literal">error</code> mapping target takes no additional parameters besides the <em class="replaceable"><code>start</code></em> and <em class="replaceable"><code>length</code></em> parameters.
			</div><div class="para">
				The following example shows an <code class="literal">error</code> target.
			</div><pre class="screen">
0 65536 error
</pre></div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="zero-map">A.1.6. The zero Mapping Target</h3></div></div></div><div class="para">
				The <code class="literal">zero</code> mapping target is a block device equivalent of <code class="filename">/dev/zero</code>. A read operation to this mapping returns blocks of zeros. Data written to this mapping is discarded, but the write succeeds. The <code class="literal">zero</code> mapping target takes no additional parameters besides the <em class="replaceable"><code>start</code></em> and <em class="replaceable"><code>length</code></em> parameters.
			</div><div class="para">
				The following example shows a <code class="literal">zero</code> target for a 16Tb Device.
			</div><pre class="screen">
0 65536 zero
</pre></div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="multipath-map">A.1.7. The multipath Mapping Target</h3></div></div></div><div class="para">
				The multipath mapping target supports the mapping of a multipathed device. The format for the <code class="literal">multipath</code> target is as follows:
			</div><pre class="screen">
<em class="parameter"><code>start length</code></em> <code class="literal"> multipath </code> <em class="parameter"><code>#features [feature1 ... featureN] #handlerargs [handlerarg1 ... handlerargN] #pathgroups pathgroup pathgroupargs1 ... pathgroupargsN</code></em>
</pre><div class="para">
				There is one set of <em class="parameter"><code>pathgroupargs</code></em> parameters for each path group.
			</div><div class="variablelist"><dl><dt><span class="term"><em class="parameter"><code>start</code></em></span></dt><dd><div class="para">
							starting block in virtual device
						</div></dd><dt><span class="term"><em class="parameter"><code>length</code></em></span></dt><dd><div class="para">
							length of this segment
						</div></dd><dt><span class="term"><em class="parameter"><code>#features</code></em></span></dt><dd><div class="para">
							The number of multipath features, followed by those features. If this parameter is zero, then there is no <em class="parameter"><code>feature</code></em> parameter and the next device mapping parameter is <em class="parameter"><code>#handlerargs</code></em>. Currently there is one supported multipath feature, <code class="literal">queue_if_no_path</code>. This indicates that this multipathed device is currently set to queue I/O operations if there is no path available.
						</div><div class="para">
							For example, if the <code class="literal">no_path_retry</code> option in the <code class="literal">multipath.conf</code> file has been set to queue I/O operations only until all paths have been marked as failed after a set number of attempts have been made to use the paths, the mapping would appear as follows until all the path checkers have failed the specified number of checks.
						</div><pre class="screen">
0 71014400 multipath 1 queue_if_no_path 0 2 1 round-robin 0 2 1 66:128 \
1000 65:64 1000 round-robin 0 2 1 8:0 1000 67:192 1000
</pre><div class="para">
							After all the path checkers have failed the specified number of checks, the mapping would appear as follows.
						</div><pre class="screen">
0 71014400 multipath 0 0 2 1 round-robin 0 2 1 66:128 1000 65:64 1000 \
round-robin 0 2 1 8:0 1000 67:192 1000
</pre></dd><dt><span class="term"><em class="parameter"><code>#handlerargs</code></em></span></dt><dd><div class="para">
							The number of hardware handler arguments, followed by those arguments. A hardware handler specifies a module that will be used to perform hardware-specific actions when switching path groups or handling I/O errors. If this is set to 0, then the next parameter is <em class="parameter"><code>#pathgroups</code></em>.
						</div></dd><dt><span class="term"><em class="parameter"><code>#pathgroups</code></em></span></dt><dd><div class="para">
							The number of path groups. A path group is the set of paths over which a multipathed device will load balance. There is one set of <em class="parameter"><code>pathgroupargs</code></em> parameters for each path group.
						</div></dd><dt><span class="term"><em class="parameter"><code>pathgroup</code></em></span></dt><dd><div class="para">
							The next path group to try.
						</div></dd><dt><span class="term"><em class="parameter"><code>pathgroupsargs</code></em></span></dt><dd><div class="para">
							Each path group consists of the following arguments:
						</div><pre class="screen">
<em class="parameter"><code>pathselector #selectorargs #paths #pathargs device1 ioreqs1 ... deviceN ioreqsN </code></em>
</pre><div class="para">
							There is one set of path arguments for each path in the path group.
						</div><div class="variablelist"><dl><dt><span class="term"><em class="parameter"><code>pathselector</code></em></span></dt><dd><div class="para">
										Specifies the algorithm in use to determine what path in this path group to use for the next I/O operation.
									</div></dd><dt><span class="term"><em class="parameter"><code>#selectorargs</code></em></span></dt><dd><div class="para">
										The number of path selector arguments which follow this argument in the multipath mapping. Currently, the value of this argument is always 0.
									</div></dd><dt><span class="term"><em class="parameter"><code>#paths</code></em></span></dt><dd><div class="para">
										The number of paths in this path group.
									</div></dd><dt><span class="term"><em class="parameter"><code>#pathargs</code></em></span></dt><dd><div class="para">
										The number of path arguments specified for each path in this group. Currently this number is always 1, the <em class="parameter"><code>ioreqs</code></em> argument.
									</div></dd><dt><span class="term"><em class="parameter"><code>device</code></em></span></dt><dd><div class="para">
										The block device number of the path, referenced by the major and minor numbers in the format <em class="parameter"><code>major</code></em>:<em class="parameter"><code>minor</code></em>
									</div></dd><dt><span class="term"><em class="parameter"><code>ioreqs</code></em></span></dt><dd><div class="para">
										The number of I/O requests to route to this path before switching to the next path in the current group.
									</div></dd></dl></div></dd></dl></div><div class="para">
				<a class="xref" href="device_mapper.html#multipath_map_figure" title="Figure A.1. Multipath Mapping Target">Figure A.1, “Multipath Mapping Target”</a> shows the format of a multipath target with two path groups.
			</div><div class="figure" id="multipath_map_figure"><div class="figure-contents"><div class="mediaobject"><img src="./images/devicemap/multipathmap.png" alt="Multipath Mapping Target"/></div></div><h6>Figure A.1. Multipath Mapping Target</h6></div><br class="figure-break"/><div class="para">
				The following example shows a pure failover target definition for the same multipath device. In this target there are four path groups, with only one open path per path group so that the multipathed device will use only one path at a time.
			</div><pre class="screen">
0 71014400 multipath 0 0 4 1 round-robin 0 1 1 66:112 1000 \
round-robin 0 1 1 67:176 1000 round-robin 0 1 1 68:240 1000 \
round-robin 0 1 1 65:48 1000
</pre><div class="para">
				The following example shows a full spread (multibus) target definition for the same multipathed device. In this target there is only one path group, which includes all of the paths. In this setup, multipath spreads the load evenly out to all of the paths.
			</div><pre class="screen">
0 71014400 multipath 0 0 1 1 round-robin 0 4 1 66:112 1000 \
 67:176 1000 68:240 1000 65:48 1000
</pre><div class="para">
				For further information about multipathing, see the <em class="citetitle">Using Device Mapper Multipath</em> document.
			</div></div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="crypt-map">A.1.8. The crypt Mapping Target</h3></div></div></div><div class="para">
				The <code class="literal">crypt</code> target encrypts the data passing through the specified device. It uses the kernel Crypto API.
			</div><div class="para">
				The format for the <code class="literal">crypt</code> target is as follows:
			</div><pre class="screen">
<em class="parameter"><code>start length</code></em> crypt <em class="parameter"><code>cipher key IV-offset device offset</code></em>
</pre><div class="variablelist"><dl><dt><span class="term"><em class="parameter"><code>start</code></em></span></dt><dd><div class="para">
							starting block in virtual device
						</div></dd><dt><span class="term"><em class="parameter"><code>length</code></em></span></dt><dd><div class="para">
							length of this segment
						</div></dd><dt><span class="term"><em class="parameter"><code>cipher</code></em></span></dt><dd><div class="para">
							Cipher consists of <em class="parameter"><code>cipher[-chainmode]-ivmode[:iv options]</code></em>.
						</div><div class="variablelist"><dl><dt><span class="term"><em class="parameter"><code>cipher</code></em></span></dt><dd><div class="para">
										Ciphers available are listed in <code class="filename">/proc/crypto</code> (for example, <code class="literal">aes</code>).
									</div></dd><dt><span class="term"><em class="parameter"><code>chainmode</code></em></span></dt><dd><div class="para">
										Always use <code class="literal">cbc</code>. Do not use <code class="literal">ebc</code>; it does not use an initial vector (IV).
									</div></dd><dt><span class="term"><em class="parameter"><code>ivmode[:iv options]</code></em></span></dt><dd><div class="para">
										IV is an initial vector used to vary the encryption. The IV mode is <code class="literal">plain</code> or <code class="literal">essiv:hash</code>. An <em class="parameter"><code>ivmode</code></em> of <code class="literal">-plain</code> uses the sector number (plus IV offset) as the IV. An <em class="parameter"><code>ivmode</code></em> of <code class="literal">-essiv</code> is an enhancement avoiding a watermark weakness
									</div></dd></dl></div></dd><dt><span class="term"><em class="parameter"><code>key</code></em></span></dt><dd><div class="para">
							Encryption key, supplied in hex
						</div></dd><dt><span class="term"><em class="parameter"><code>IV-offset</code></em></span></dt><dd><div class="para">
							Initial Vector (IV) offset
						</div></dd><dt><span class="term"><em class="parameter"><code>device</code></em></span></dt><dd><div class="para">
							block device, referenced by the device name in the filesystem or by the major and minor numbers in the format <em class="parameter"><code>major</code></em>:<em class="parameter"><code>minor</code></em>
						</div></dd><dt><span class="term"><em class="parameter"><code>offset</code></em></span></dt><dd><div class="para">
							starting offset of the mapping on the device
						</div></dd></dl></div><div class="para">
				The following is an example of a <code class="literal">crypt</code> target.
			</div><pre class="screen">
0 2097152 crypt aes-plain 0123456789abcdef0123456789abcdef 0 /dev/hda 0
</pre></div></div></div><ul class="docnav"><li class="previous"><a accesskey="p" href="LVM_GUI.html"><strong>Prev</strong>Chapter 7. LVM Administration with the LVM GUI</a></li><li class="up"><a accesskey="u" href="#"><strong>Up</strong></a></li><li class="home"><a accesskey="h" href="index.html"><strong>Home</strong></a></li><li class="next"><a accesskey="n" href="dmsetup.html"><strong>Next</strong>A.2. The dmsetup Command</a></li></ul></body></html>