<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>24.5. Mail Delivery Agents</title><link rel="stylesheet" href="./Common_Content/css/default.css" type="text/css"/><meta name="generator" content="publican"/><meta name="package" content="Red_Hat_Enterprise_Linux-Deployment_Guide-5-en-US-3-15"/><link rel="start" href="index.html" title="Deployment Guide"/><link rel="up" href="ch-email.html" title="Chapter 24. Email"/><link rel="prev" href="s1-email-switchmail.html" title="24.4. Mail Transport Agent (MTA) Configuration"/><link rel="next" href="s2-email-procmail-recipes.html" title="24.5.2. Procmail Recipes"/></head><body class=""><p id="title"><a class="left" href="http://www.redhat.com"><img src="Common_Content/images/image_left.png" alt="Product Site"/></a><a class="right" href="http://www.redhat.com/docs"><img src="Common_Content/images/image_right.png" alt="Documentation Site"/></a></p><ul class="docnav"><li class="previous"><a accesskey="p" href="s1-email-switchmail.html"><strong>Prev</strong></a></li><li class="next"><a accesskey="n" href="s2-email-procmail-recipes.html"><strong>Next</strong></a></li></ul><div class="section" lang="en-US"><div class="titlepage"><div><div><h2 class="title" id="s1-email-mda">24.5. Mail Delivery Agents</h2></div></div></div><div class="para">
			Red Hat Enterprise Linux includes two primary MDAs, Procmail and <code class="command">mail</code>. Both of the applications are considered LDAs and both move email from the MTA's spool file into the user's mailbox. However, Procmail provides a robust filtering system.
		</div><div class="para">
			This section details only Procmail. For information on the <code class="command">mail</code> command, consult its man page.
		</div><a id="d0e39678" class="indexterm"/><a id="d0e39683" class="indexterm"/><div class="para">
			Procmail delivers and filters email as it is placed in the mail spool file of the localhost. It is powerful, gentle on system resources, and widely used. Procmail can play a critical role in delivering email to be read by email client applications.
		</div><div class="para">
			Procmail can be invoked in several different ways. Whenever an MTA places an email into the mail spool file, Procmail is launched. Procmail then filters and files the email for the MUA and quits. Alternatively, the MUA can be configured to execute Procmail any time a message is received so that messages are moved into their correct mailboxes. By default, the presence of <code class="filename">/etc/procmailrc</code> or of a <code class="filename">.procmailrc</code> file (also called an <em class="firstterm">rc</em> file) in the user's home directory invokes Procmail whenever an MTA receives a new message.
		</div><div class="para">
			Whether Procmail acts upon an email message depends upon whether the message matches a specified set of conditions or <em class="firstterm">recipes</em> in the <code class="filename">rc</code> file. If a message matches a recipe, then the email is placed in a specified file, is deleted, or is otherwise processed.
		</div><div class="para">
			When Procmail starts, it reads the email message and separates the body from the header information. Next, Procmail looks for <code class="filename">/etc/procmailrc</code> and <code class="filename">rc</code> files in the <code class="filename">/etc/procmailrcs</code> directory for default, system-wide, Procmail environmental variables and recipes. Procmail then searches for a <code class="filename">.procmailrc</code> file in the user's home directory. Many users also create additional <code class="filename">rc</code> files for Procmail that are referred to within the <code class="filename">.procmailrc</code> file in their home directory.
		</div><div class="para">
			By default, no system-wide <code class="filename">rc</code> files exist in the <code class="filename">/etc/</code> directory and no <code class="filename">.procmailrc</code> files exist in any user's home directory. Therefore, to use Procmail, each user must construct a <code class="filename">.procmailrc</code> file with specific environment variables and rules.
		</div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="s2-email-procmail-configuration">24.5.1. Procmail Configuration</h3></div></div></div><a id="d0e39744" class="indexterm"/><a id="d0e39749" class="indexterm"/><div class="para">
				The Procmail configuration file contains important environmental variables. These variables specify things such as which messages to sort and what to do with the messages that do not match any recipes.
			</div><div class="para">
				These environmental variables usually appear at the beginning of <code class="filename">.procmailrc</code> in the following format:
			</div><pre class="screen"><code class="computeroutput"><em class="replaceable"><code>&lt;env-variable&gt;</code></em>="<em class="replaceable"><code>&lt;value&gt;</code></em>"</code>
</pre><div class="para">
				In this example, <code class="command"><em class="replaceable"><code>&lt;env-variable&gt;</code></em></code> is the name of the variable and <code class="command"><em class="replaceable"><code>&lt;value&gt;</code></em></code> defines the variable.
			</div><div class="para">
				There are many environment variables not used by most Procmail users and many of the more important environment variables are already defined by a default value. Most of the time, the following variables are used:
			</div><div class="itemizedlist"><ul><li><div class="para">
						<code class="command">DEFAULT</code> — Sets the default mailbox where messages that do not match any recipes are placed.
					</div><div class="para">
						The default <code class="command">DEFAULT</code> value is the same as <code class="command">$ORGMAIL</code>.
					</div></li><li><div class="para">
						<code class="command">INCLUDERC</code> — Specifies additional <code class="filename">rc</code> files containing more recipes for messages to be checked against. This breaks up the Procmail recipe lists into individual files that fulfill different roles, such as blocking spam and managing email lists, that can then be turned off or on by using comment characters in the user's <code class="filename">.procmailrc</code> file.
					</div><div class="para">
						For example, lines in a user's <code class="filename">.procmailrc</code> file may look like this:
					</div><pre class="screen"><code class="computeroutput">MAILDIR=$HOME/Msgs INCLUDERC=$MAILDIR/lists.rc INCLUDERC=$MAILDIR/spam.rc</code>
</pre><div class="para">
						If the user wants to turn off Procmail filtering of their email lists but leave spam control in place, they would comment out the first <code class="command">INCLUDERC</code> line with a hash mark character (<code class="command">#</code>).
					</div></li><li><div class="para">
						<code class="command">LOCKSLEEP</code> — Sets the amount of time, in seconds, between attempts by Procmail to use a particular lockfile. The default is eight seconds.
					</div></li><li><div class="para">
						<code class="command">LOCKTIMEOUT</code> — Sets the amount of time, in seconds, that must pass after a lockfile was last modified before Procmail assumes that the lockfile is old and can be deleted. The default is 1024 seconds.
					</div></li><li><div class="para">
						<code class="command">LOGFILE</code> — The file to which any Procmail information or error messages are written.
					</div></li><li><div class="para">
						<code class="command">MAILDIR</code> — Sets the current working directory for Procmail. If set, all other Procmail paths are relative to this directory.
					</div></li><li><div class="para">
						<code class="command">ORGMAIL</code> — Specifies the original mailbox, or another place to put the messages if they cannot be placed in the default or recipe-required location.
					</div><div class="para">
						By default, a value of <code class="command">/var/spool/mail/$LOGNAME</code> is used.
					</div></li><li><div class="para">
						<code class="command">SUSPEND</code> — Sets the amount of time, in seconds, that Procmail pauses if a necessary resource, such as swap space, is not available.
					</div></li><li><div class="para">
						<code class="command">SWITCHRC</code> — Allows a user to specify an external file containing additional Procmail recipes, much like the <code class="command">INCLUDERC</code> option, except that recipe checking is actually stopped on the referring configuration file and only the recipes on the <code class="command">SWITCHRC</code>-specified file are used.
					</div></li><li><div class="para">
						<code class="command">VERBOSE</code> — Causes Procmail to log more information. This option is useful for debugging.
					</div></li></ul></div><div class="para">
				Other important environmental variables are pulled from the shell, such as <code class="command">LOGNAME</code>, which is the login name; <code class="command">HOME</code>, which is the location of the home directory; and <code class="command">SHELL</code>, which is the default shell.
			</div><div class="para">
				A comprehensive explanation of all environments variables, as well as their default values, is available in the <code class="filename">procmailrc</code> man page.
			</div></div></div><ul class="docnav"><li class="previous"><a accesskey="p" href="s1-email-switchmail.html"><strong>Prev</strong>24.4. Mail Transport Agent (MTA) Configuration</a></li><li class="up"><a accesskey="u" href="#"><strong>Up</strong></a></li><li class="home"><a accesskey="h" href="index.html"><strong>Home</strong></a></li><li class="next"><a accesskey="n" href="s2-email-procmail-recipes.html"><strong>Next</strong>24.5.2. Procmail Recipes</a></li></ul></body></html>