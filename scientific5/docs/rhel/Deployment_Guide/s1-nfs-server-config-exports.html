<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>19.7. The /etc/exports Configuration File</title><link rel="stylesheet" href="./Common_Content/css/default.css" type="text/css"/><meta name="generator" content="publican"/><meta name="package" content="Red_Hat_Enterprise_Linux-Deployment_Guide-5-en-US-3-15"/><link rel="start" href="index.html" title="Deployment Guide"/><link rel="up" href="ch-nfs.html" title="Chapter 19. Network File System (NFS)"/><link rel="prev" href="s2-nfs-hostname-formats.html" title="19.6.3. Hostname Formats"/><link rel="next" href="s1-nfs-security.html" title="19.8. Securing NFS"/></head><body class=""><p id="title"><a class="left" href="http://www.redhat.com"><img src="Common_Content/images/image_left.png" alt="Product Site"/></a><a class="right" href="http://www.redhat.com/docs"><img src="Common_Content/images/image_right.png" alt="Documentation Site"/></a></p><ul class="docnav"><li class="previous"><a accesskey="p" href="s2-nfs-hostname-formats.html"><strong>Prev</strong></a></li><li class="next"><a accesskey="n" href="s1-nfs-security.html"><strong>Next</strong></a></li></ul><div class="section" lang="en-US"><div class="titlepage"><div><div><h2 class="title" id="s1-nfs-server-config-exports">19.7. The <code class="filename">/etc/exports</code> Configuration File</h2></div></div></div><a id="d0e23882" class="indexterm"/><div class="para">
			The <code class="filename">/etc/exports</code> file controls which file systems are exported to remote hosts and specifies options. Blank lines are ignored, comments can be made by starting a line with the hash mark (<code class="command">#</code>), and long lines can be wrapped with a backslash (<code class="command">\</code>). Each exported file system should be on its own individual line, and any lists of authorized hosts placed after an exported file system must be separated by space characters. Options for each of the hosts must be placed in parentheses directly after the host identifier, without any spaces separating the host and the first parenthesis. Valid host types are <code class="command">gss/krb5</code> <code class="command">gss/krb5i</code> and <code class="command">gss/krb5p</code>.
		</div><div class="para">
			A line for an exported file system has the following structure:
		</div><pre class="screen">
<code class="command"><em class="replaceable"><code>&lt;export&gt;</code></em> <em class="replaceable"><code>&lt;host1&gt;</code></em>(<em class="replaceable"><code>&lt;options&gt;</code></em>) <em class="replaceable"><code>&lt;hostN&gt;</code></em>(<em class="replaceable"><code>&lt;options&gt;</code></em>)...</code>
</pre><div class="para">
			In this structure, replace <em class="replaceable"><code>&lt;export&gt;</code></em> with the directory being exported, replace <em class="replaceable"><code>&lt;host1&gt;</code></em> with the host or network to which the export is being shared, and replace <em class="replaceable"><code>&lt;options&gt;</code></em> with the options for that host or network. Additional hosts can be specified in a space separated list.
		</div><div class="para">
			The following methods can be used to specify host names:
		</div><div class="itemizedlist"><ul><li><div class="para">
					<span class="emphasis"><em>single host</em></span> — Where one particular host is specified with a fully qualified domain name, hostname, or IP address.
				</div></li><li><div class="para">
					<span class="emphasis"><em>wildcards</em></span> — Where a <code class="command">*</code> or <code class="command">?</code> character is used to take into account a grouping of fully qualified domain names that match a particular string of letters. Wildcards should not be used with IP addresses; however, it is possible for them to work accidentally if reverse DNS lookups fail.
				</div><div class="para">
					Be careful when using wildcards with fully qualified domain names, as they tend to be more exact than expected. For example, the use of <code class="command">*.example.com</code> as a wildcard allows sales.example.com to access an exported file system, but not bob.sales.example.com. To match both possibilities both <code class="command">*.example.com</code> and <code class="command">*.*.example.com</code> must be specified.
				</div></li><li><div class="para">
					<span class="emphasis"><em>IP networks</em></span> — Allows the matching of hosts based on their IP addresses within a larger network. For example, <code class="command">192.168.0.0/28</code> allows the first 16 IP addresses, from 192.168.0.0 to 192.168.0.15, to access the exported file system, but not 192.168.0.16 and higher.
				</div></li><li><div class="para">
					<span class="emphasis"><em>netgroups</em></span> — Permits an NIS netgroup name, written as <code class="command">@<em class="replaceable"><code>&lt;group-name&gt;</code></em></code>, to be used. This effectively puts the NIS server in charge of access control for this exported file system, where users can be added and removed from an NIS group without affecting <code class="filename">/etc/exports</code>.
				</div></li></ul></div><div class="para">
			In its simplest form, the <code class="filename">/etc/exports</code> file only specifies the exported directory and the hosts permitted to access it, as in the following example:
		</div><pre class="screen">
<code class="command">/exported/directory bob.example.com</code>
</pre><div class="para">
			In the example, <code class="computeroutput">bob.example.com</code> can mount <code class="filename">/exported/directory/</code>. Because no options are specified in this example, the following default NFS options take effect:
		</div><div class="itemizedlist"><ul><li><div class="para">
					<code class="option">ro</code> — Mounts of the exported file system are read-only. Remote hosts are not able to make changes to the data shared on the file system. To allow hosts to make changes to the file system, the read/write (<code class="option">rw</code>) option must be specified.
				</div></li><li><div class="para">
					<code class="option">wdelay</code> — Causes the NFS server to delay writing to the disk if it suspects another write request is imminent. This can improve performance by reducing the number of times the disk must be accessed by separate write commands, reducing write overhead. The <code class="option">no_wdelay</code> option turns off this feature, but is only available when using the <code class="option">sync</code> option.
				</div></li><li><div class="para">
					<code class="option">root_squash</code> — Prevents root users connected remotely from having root privileges and assigns them the user ID for the user <code class="computeroutput">nfsnobody</code>. This effectively "squashes" the power of the remote root user to the lowest local user, preventing unauthorized alteration of files on the remote server. Alternatively, the <code class="option">no_root_squash</code> option turns off root squashing. To squash every remote user, including root, use the <code class="option">all_squash</code> option. To specify the user and group IDs to use with remote users from a particular host, use the <code class="option">anonuid</code> and <code class="option">anongid</code> options, respectively. In this case, a special user account can be created for remote NFS users to share and specify <code class="option">(anonuid=<em class="replaceable"><code>&lt;uid-value&gt;</code></em>,anongid=<em class="replaceable"><code>&lt;gid-value&gt;</code></em>)</code>, where <code class="option"><em class="replaceable"><code>&lt;uid-value&gt;</code></em></code> is the user ID number and <code class="option"><em class="replaceable"><code>&lt;gid-value&gt;</code></em></code> is the group ID number.
				</div></li></ul></div><div class="important"><h2>Important</h2><div class="para">
				By default, <em class="firstterm">access control lists</em> (<em class="firstterm">ACLs</em>) are supported by NFS under Red Hat Enterprise Linux. To disable this feature, specify the <code class="command">no_acl</code> option when exporting the file system.
			</div></div><div class="para">
			Each default for every exported file system must be explicitly overridden. For example, if the <code class="option">rw</code> option is not specified, then the exported file system is shared as read-only. The following is a sample line from <code class="filename">/etc/exports</code> which overrides two default options:
		</div><pre class="screen">
<code class="command">/another/exported/directory 192.168.0.3(rw,sync)</code>
</pre><div class="para">
			In this example <code class="command">192.168.0.3</code> can mount <code class="filename">/another/exported/directory/</code> read/write and all transfers to disk are committed to the disk before the write request by the client is completed.
		</div><div class="para">
			Additionally, other options are available where no default value is specified. These include the ability to disable sub-tree checking, allow access from insecure ports, and allow insecure file locks (necessary for certain early NFS client implementations). Refer to the <code class="filename">exports</code> man page for details on these lesser used options.
		</div><div class="warning"><h2>Warning</h2><div class="para">
				The format of the <code class="filename">/etc/exports</code> file is very precise, particularly in regards to use of the space character. Remember to always separate exported file systems from hosts and hosts from one another with a space character. However, there should be no other space characters in the file except on comment lines.
			</div><div class="para">
				For example, the following two lines do not mean the same thing:
			</div><pre class="screen">
<code class="command">/home bob.example.com(rw) /home bob.example.com (rw)</code>
</pre><div class="para">
				The first line allows only users from <code class="filename">bob.example.com</code> read/write access to the <code class="filename">/home</code> directory. The second line allows users from <code class="filename">bob.example.com</code> to mount the directory as read-only (the default), while the rest of the world can mount it read/write.
			</div></div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="s1-nfs-server-config-exportfs">19.7.1. The <code class="command">exportfs</code> Command</h3></div></div></div><a id="d0e24147" class="indexterm"/><div class="para">
				Every file system being exported to remote users via NFS, as well as the access level for those file systems, are listed in the <code class="filename">/etc/exports</code> file. When the <code class="command">nfs</code> service starts, the <code class="command">/usr/sbin/exportfs</code> command launches and reads this file, passes control to <code class="command">rpc.mountd</code> (if NFSv2 or NFSv3) for the actual mounting process, then to <code class="command">rpc.nfsd</code> where the file systems are then available to remote users.
			</div><div class="para">
				When issued manually, the <code class="command">/usr/sbin/exportfs</code> command allows the root user to selectively export or unexport directories without restarting the NFS service. When given the proper options, the <code class="command">/usr/sbin/exportfs</code> command writes the exported file systems to <code class="filename">/var/lib/nfs/xtab</code>. Since <code class="command">rpc.mountd</code> refers to the <code class="filename">xtab</code> file when deciding access privileges to a file system, changes to the list of exported file systems take effect immediately.
			</div><div class="para">
				The following is a list of commonly used options available for <code class="command">/usr/sbin/exportfs</code>:
			</div><div class="itemizedlist"><ul><li><div class="para">
						<code class="option">-r</code> — Causes all directories listed in <code class="filename">/etc/exports</code> to be exported by constructing a new export list in <code class="filename">/etc/lib/nfs/xtab</code>. This option effectively refreshes the export list with any changes that have been made to <code class="filename">/etc/exports</code>.
					</div></li><li><div class="para">
						<code class="option">-a</code> — Causes all directories to be exported or unexported, depending on what other options are passed to <code class="command">/usr/sbin/exportfs</code>. If no other options are specified, <code class="command">/usr/sbin/exportfs</code> exports all file systems specified in <code class="filename">/etc/exports</code>.
					</div></li><li><div class="para">
						<code class="option">-o <em class="replaceable"><code>file-systems</code></em></code> — Specifies directories to be exported that are not listed in <code class="filename">/etc/exports</code>. Replace <em class="replaceable"><code>file-systems</code></em> with additional file systems to be exported. These file systems must be formatted in the same way they are specified in <code class="filename">/etc/exports</code>. Refer to <a href="s1-nfs-server-config-exports.html" title="19.7. The /etc/exports Configuration File">Section 19.7, “The <code class="filename">/etc/exports</code> Configuration File”</a> for more information on <code class="filename">/etc/exports</code> syntax. This option is often used to test an exported file system before adding it permanently to the list of file systems to be exported.
					</div></li><li><div class="para">
						<code class="option">-i</code> — Ignores <code class="filename">/etc/exports</code>; only options given from the command line are used to define exported file systems.
					</div></li><li><div class="para">
						<code class="option">-u</code> — Unexports all shared directories. The command <code class="command">/usr/sbin/exportfs -ua</code> suspends NFS file sharing while keeping all NFS daemons up. To re-enable NFS sharing, type <code class="command">exportfs -r</code>.
					</div></li><li><div class="para">
						<code class="option">-v</code> — Verbose operation, where the file systems being exported or unexported are displayed in greater detail when the <code class="command">exportfs</code> command is executed.
					</div></li></ul></div><div class="para">
				If no options are passed to the <code class="command">/usr/sbin/exportfs</code> command, it displays a list of currently exported file systems.
			</div><div class="para">
				For more information about the <code class="command">/usr/sbin/exportfs</code> command, refer to the <code class="command">exportfs</code> man page.
			</div><div class="section" lang="en-US"><div class="titlepage"><div><div><h4 class="title" id="s3-nfs-server-config-exportfs-nfsv4">19.7.1.1. Using <code class="command">exportfs</code> with NFSv4</h4></div></div></div><a id="d0e24297" class="indexterm"/><div class="para">
					The <code class="command">exportfs</code> command is used in maintaining the NFS table of exported file systems. When typed in a terminal with no arguments, the <code class="command">exportfs</code> command shows all the exported directories.
				</div><div class="para">
					Since NFSv4 no longer utilizes the <code class="command">rpc.mountd</code> protocol as was used in NFSv2 and NFSv3, the mounting of file systems has changed.
				</div><div class="para">
					An NFSv4 client now has the ability to see all of the exports served by the NFSv4 server as a single file system, called the NFSv4 pseudo-file system. On Red Hat Enterprise Linux, the pseudo-file system is identified as a single, real file system, identified at export with the <code class="option">fsid=0</code> option.
				</div><div class="para">
					For example, the following commands could be executed on an NFSv4 server:
				</div><div class="para">
					
<pre class="screen">
<code class="computeroutput">mkdir /exports mkdir /exports/opt mkdir /exports/etc mount --bind /usr/local/opt /exports/opt mount --bind /usr/local/etc /exports/etc exportfs -o fsid=0,insecure,no_subtree_check gss/krb5p:/exports exportfs -o rw,nohide,insecure,no_subtree_check gss/krb5p:/exports/opt exportfs -o rw,nohide,insecure,no_subtree_check gss/krb5p:/exports/etc</code>
</pre>
				</div><div class="para">
					In this example, clients are provided with multiple file systems to mount, by using the <code class="option">--bind</code> option which creates unbreakeable links.
				</div><div class="para">
					Because of the pseudo-file systems feature, NFS version 2, 3 and 4 export configurations are not always compatible. For example, given the following directory tree:
				</div><pre class="screen">
<code class="command"> /home /home/sam /home/john /home/joe</code>
</pre><div class="para">
					and the export:
				</div><pre class="screen"><code class="command"> /home *(rw,fsid=0,sync)</code>
</pre><div class="para">
					Using NFS version 2,3 and 4 the following would work:
				</div><pre class="screen"><code class="command"> mount server:/home /mnt/home ls /mnt/home/joe</code>
</pre><div class="para">
					Using v4 the following would work:
				</div><pre class="screen"><code class="command"> mount -t nfs4 server:/ /mnt/home ls /mnt/home/joe</code>
</pre><div class="para">
					The difference being "<code class="command">server:/home</code>" and "<code class="command">server:/</code>". To make the exports configurations compatible for all version, one needs to export (read only) the root filesystem with an <code class="command">fsid=0</code>. The <code class="command">fsid=0</code> signals the NFS server that this export is the root.
				</div><pre class="screen"><code class="command"> / *(ro,fsid=0) /home *(rw,sync,nohide)</code>
</pre><div class="para">
					Now with these exports, both "<code class="command">mount server:/home /mnt/home</code>" and "<code class="command">mount -t nfs server:/home /mnt/home</code>" will work as expected.
				</div></div></div></div><ul class="docnav"><li class="previous"><a accesskey="p" href="s2-nfs-hostname-formats.html"><strong>Prev</strong>19.6.3. Hostname Formats</a></li><li class="up"><a accesskey="u" href="#"><strong>Up</strong></a></li><li class="home"><a accesskey="h" href="index.html"><strong>Home</strong></a></li><li class="next"><a accesskey="n" href="s1-nfs-security.html"><strong>Next</strong>19.8. Securing NFS</a></li></ul></body></html>