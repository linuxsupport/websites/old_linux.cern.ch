<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>31.4. Fonts</title><link rel="stylesheet" href="./Common_Content/css/default.css" type="text/css"/><meta name="generator" content="publican"/><meta name="package" content="Red_Hat_Enterprise_Linux-Deployment_Guide-5-en-US-3-15"/><link rel="start" href="index.html" title="Deployment Guide"/><link rel="up" href="ch-x.html" title="Chapter 31. The X Window System"/><link rel="prev" href="s1-x-server-configuration.html" title="31.3. X Server Configuration Files"/><link rel="next" href="s2-x-fonts-core.html" title="31.4.2. Core X Font System"/></head><body class=""><p id="title"><a class="left" href="http://www.redhat.com"><img src="Common_Content/images/image_left.png" alt="Product Site"/></a><a class="right" href="http://www.redhat.com/docs"><img src="Common_Content/images/image_right.png" alt="Documentation Site"/></a></p><ul class="docnav"><li class="previous"><a accesskey="p" href="s1-x-server-configuration.html"><strong>Prev</strong></a></li><li class="next"><a accesskey="n" href="s2-x-fonts-core.html"><strong>Next</strong></a></li></ul><div class="section" lang="en-US"><div class="titlepage"><div><div><h2 class="title" id="s1-x-fonts">31.4. Fonts</h2></div></div></div><a id="d0e49006" class="indexterm"/><div class="para">
			Red Hat Enterprise Linux uses two subsystems to manage and display fonts under X: <em class="firstterm">Fontconfig</em> and <code class="command">xfs</code>.
		</div><div class="para">
			The newer Fontconfig font subsystem simplifies font management and provides advanced display features, such as anti-aliasing. This system is used automatically for applications programmed using the Qt 3 or GTK+ 2 graphical toolkit.
		</div><div class="para">
			For compatibility, Red Hat Enterprise Linux includes the original font subsystem, called the core X font subsystem. This system, which is over 15 years old, is based around the <em class="firstterm">X Font Server</em> (<em class="firstterm">xfs</em>).
		</div><div class="para">
			This section discusses how to configure fonts for X using both systems.
		</div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="s2-x-fonts-fontconfig">31.4.1. Fontconfig</h3></div></div></div><a id="d0e49036" class="indexterm"/><a id="d0e49043" class="indexterm"/><a id="d0e49050" class="indexterm"/><a id="d0e49057" class="indexterm"/><div class="para">
				The Fontconfig font subsystem allows applications to directly access fonts on the system and use Xft or other rendering mechanisms to render Fontconfig fonts with advanced anti-aliasing. Graphical applications can use the Xft library with Fontconfig to draw text to the screen.
			</div><div class="para">
				Over time, the Fontconfig/Xft font subsystem replaces the core X font subsystem.
			</div><div class="important"><h2>Important</h2><div class="para">
					The Fontconfig font subsystem does not yet work for <span><strong class="application">OpenOffice.org</strong></span>, which uses its own font rendering technology.
				</div></div><div class="para">
				It is important to note that Fontconfig uses the <code class="filename">/etc/fonts/fonts.conf</code> configuration file, which should not be edited by hand.
			</div><div class="note"><h2>Tip</h2><div class="para">
					Due to the transition to the new font system, GTK+ 1.2 applications are not affected by any changes made via the <span><strong class="application">Font Preferences</strong></span> dialog (accessed by selecting System (on the panel) &gt; <span><strong class="guimenuitem">Preferences</strong></span> &gt; <span><strong class="guimenuitem">Fonts</strong></span>). For these applications, a font can be configured by adding the following lines to the file <code class="filename">~/.gtkrc.mine</code>:
				</div><pre class="screen">
<code class="command">style "user-font" { fontset = "<em class="replaceable"><code>&lt;font-specification&gt;</code></em>" } widget_class "*" style "user-font"</code>
</pre><div class="para">
					Replace <em class="replaceable"><code>&lt;font-specification&gt;</code></em> with a font specification in the style used by traditional X applications, such as <code class="command">-adobe-helvetica-medium-r-normal--*-120-*-*-*-*-*-*</code>. A full list of core fonts can be obtained by running <code class="command">xlsfonts</code> or created interactively using the <code class="command">xfontsel</code> command.
				</div></div><div class="section" lang="en-US"><div class="titlepage"><div><div><h4 class="title" id="s3-x-fonts-fontconfig-add">31.4.1.1. Adding Fonts to Fontconfig</h4></div></div></div><a id="d0e49123" class="indexterm"/><div class="para">
					Adding new fonts to the Fontconfig subsystem is a straightforward process.
				</div><div class="orderedlist"><ol><li><div class="para">
							To add fonts system-wide, copy the new fonts into the <code class="filename">/usr/share/fonts/</code> directory. It is a good idea to create a new subdirectory, such as <code class="filename">local/</code> or similar, to help distinguish between user-installed and default fonts.
						</div><div class="para">
							To add fonts for an individual user, copy the new fonts into the <code class="filename">.fonts/</code> directory in the user's home directory.
						</div></li><li><div class="para">
							Use the <code class="command">fc-cache</code> command to update the font information cache, as in the following example:
						</div><pre class="screen">
<code class="command">fc-cache <em class="replaceable"><code>&lt;path-to-font-directory&gt;</code></em></code>
</pre><div class="para">
							In this command, replace <em class="replaceable"><code>&lt;path-to-font-directory&gt;</code></em> with the directory containing the new fonts (either <code class="filename">/usr/share/fonts/local/</code> or <code class="filename">/home/<em class="replaceable"><code>&lt;user&gt;</code></em>/.fonts/</code>).
						</div></li></ol></div><div class="note"><h2>Tip</h2><div class="para">
						Individual users may also install fonts graphically, by typing <code class="filename">fonts:///</code> into the <span><strong class="application">Nautilus</strong></span> address bar, and dragging the new font files there.
					</div></div><div class="important"><h2>Important</h2><div class="para">
						If the font file name ends with a <code class="filename">.gz</code> extension, it is compressed and cannot be used until uncompressed. To do this, use the <code class="command">gunzip</code> command or double-click the file and drag the font to a directory in <span><strong class="application">Nautilus</strong></span>.
					</div></div></div></div></div><ul class="docnav"><li class="previous"><a accesskey="p" href="s1-x-server-configuration.html"><strong>Prev</strong>31.3. X Server Configuration Files</a></li><li class="up"><a accesskey="u" href="#"><strong>Up</strong></a></li><li class="home"><a accesskey="h" href="index.html"><strong>Home</strong></a></li><li class="next"><a accesskey="n" href="s2-x-fonts-core.html"><strong>Next</strong>31.4.2. Core X Font System</a></li></ul></body></html>