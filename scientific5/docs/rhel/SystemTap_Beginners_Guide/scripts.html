<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>3.2. SystemTap Scripts</title><link rel="stylesheet" href="./Common_Content/css/default.css" type="text/css"/><meta name="generator" content="publican"/><link rel="start" href="index.html" title="SystemTap Beginners Guide"/><link rel="up" href="understanding-how-systemtap-works.html" title="Chapter 3. Understanding How SystemTap Works"/><link rel="prev" href="understanding-how-systemtap-works.html" title="Chapter 3. Understanding How SystemTap Works"/><link rel="next" href="systemtapscript-handler.html" title="3.2.2. Systemtap Handler/Body"/></head><body><p id="title"><a href="http://www.redhat.com/docs"><strong>3.2. SystemTap Scripts</strong></a></p><ul class="docnav"><li class="previous"><a accesskey="p" href="understanding-how-systemtap-works.html"><strong>Prev</strong></a></li><li class="next"><a accesskey="n" href="systemtapscript-handler.html"><strong>Next</strong></a></li></ul><div class="section" lang="en-US"><div class="titlepage"><div><div><h2 class="title" id="scripts">3.2. SystemTap Scripts</h2></div></div></div><a id="d0e1745" class="indexterm"/><a id="d0e1750" class="indexterm"/><p>
		For the most part, SystemTap scripts are the foundation of each SystemTap session. SystemTap scripts instruct SystemTap on what type of information to collect, and what to do once that information is collected.
	</p><a id="d0e1757" class="indexterm"/><a id="d0e1764" class="indexterm"/><a id="d0e1771" class="indexterm"/><a id="d0e1778" class="indexterm"/><a id="d0e1785" class="indexterm"/><a id="d0e1792" class="indexterm"/><p>
		As stated in <a href="understanding-how-systemtap-works.html" title="Chapter 3. Understanding How SystemTap Works">Chapter 3, <i xmlns:xlink="http://www.w3.org/1999/xlink">Understanding How SystemTap Works</i></a>, SystemTap scripts are made up of two components: <span class="emphasis"><em>events</em></span> and <span class="emphasis"><em>handlers</em></span>. Once a SystemTap session is underway, SystemTap monitors the operating system for the specified events and executes the handlers as they occur.
	</p><div class="note"><h2>Note</h2><a id="d0e1812" class="indexterm"/><a id="d0e1819" class="indexterm"/><a id="d0e1826" class="indexterm"/><p>
			An event and its corresponding handler is collectively called a <span class="emphasis"><em>probe</em></span>. A SystemTap script can have multiple probes.
		</p><p>
			A probe's handler is commonly referred to as a <span class="emphasis"><em>probe body</em></span>.
		</p></div><p>
		In terms of application development, using events and handlers is similar to instrumenting the code by inserting diagnostic print statements in a program's sequence of commands. These diagnostic print statements allow you to view a history of commands executed once the program is run.
	</p><p>
		SystemTap scripts allow insertion of the instrumentation code without recompilation of the code and allows more flexibility with regard to handlers. Events serve as the triggers for handlers to run; handlers can be specified to record specified data and print it in a certain manner.
	</p><h5 class="formalpara" id="scriptformats">Format</h5><a id="d0e1850" class="indexterm"/><a id="d0e1857" class="indexterm"/><a id="d0e1864" class="indexterm"/><a id="d0e1871" class="indexterm"/>
			SystemTap scripts use the file extension <code class="filename">.stp</code>, and contains probes written in the following format:
		<pre class="screen">probe	<em class="replaceable"><code>event</code></em> {<em class="replaceable"><code>statements</code></em>}</pre><p>
		SystemTap supports multiple events per probe; multiple events are delimited by a comma (<code class="command">,</code>). If multiple events are specified in a single probe, SystemTap will execute the handler when any of the specified events occur.
	</p><a id="d0e1896" class="indexterm"/><a id="d0e1903" class="indexterm"/><a id="d0e1910" class="indexterm"/><p>
		Each probe has a corresponding <em class="firstterm">statement block</em>. This statement block is enclosed in braces (<code class="command">{ }</code>) and contains the statements to be executed per event. SystemTap executes these statements in sequence; special separators or terminators are generally not necessary between multiple statements.
	</p><div class="note"><h2>Note</h2><p>
			Statement blocks in SystemTap scripts follow the same syntax and semantics as the C programming language. A statement block can be nested within another statement block.
		</p></div><a id="d0e1930" class="indexterm"/><a id="d0e1937" class="indexterm"/><a id="d0e1944" class="indexterm"/><p>
		Systemtap allows you to write functions to factor out code to be used by a number of probes. Thus, rather than repeatedly writing the same series of statements in multiple probes, you can just place the instructions in a <em class="firstterm">function</em>, as in:
	</p><pre class="screen">function <em class="replaceable"><code>function_name</code></em>(<em class="replaceable"><code>arguments</code></em>) {<em class="replaceable"><code>statements</code></em>}
probe <em class="replaceable"><code>event</code></em> {<em class="replaceable"><code>function_name</code></em>(<em class="replaceable"><code>arguments</code></em>)}</pre><p>
		The <code class="command"><em class="replaceable"><code>statements</code></em></code> in <em class="replaceable"><code>function_name</code></em> are executed when the probe for <em class="replaceable"><code>event</code></em> executes. The <em class="replaceable"><code>arguments</code></em> are optional values passed into the function.
	</p><div class="important"><h2>Important</h2><p>
			<a href="scripts.html" title="3.2. SystemTap Scripts">Section 3.2, “SystemTap Scripts”</a> is designed to introduce readers to the basics of SystemTap scripts. To understand SystemTap scripts better, it is advisable that you refer to <a href="useful-systemtap-scripts.html" title="Chapter 4. Useful SystemTap Scripts">Chapter 4, <i xmlns:xlink="http://www.w3.org/1999/xlink">Useful SystemTap Scripts</i></a>; each section therein provides a detailed explanation of the script, its events, handlers, and expected output.
		</p></div><div class="section" lang="en-US"><div class="titlepage"><div><div><h3 class="title" id="systemtapscript-events">3.2.1. Event</h3></div></div></div><a id="d0e2003" class="indexterm"/><p>
			SystemTap events can be broadly classified into two types: <em class="firstterm">synchronous</em> and <em class="firstterm">asynchronous</em>.
		</p><h5 class="formalpara" id="d0e2016">Synchronous Events</h5><a id="d0e2019" class="indexterm"/><a id="d0e2024" class="indexterm"/>
				A <em class="firstterm">synchronous</em> event occurs when any process executes an instruction at a particular location in kernel code. This gives other events a reference point from which more contextual data may be available.
			<a id="d0e2034" class="indexterm"/><a id="d0e2039" class="indexterm"/><p>
			Examples of synchronous events include:
		</p><div class="variablelist"><dl><dt><span class="term">syscall.<em class="replaceable"><code>system_call</code></em></span></dt><dd><a id="d0e2053" class="indexterm"/><a id="d0e2061" class="indexterm"/><p>
						The entry to the system call <em class="replaceable"><code>system_call</code></em>. If the exit from a syscall is desired, appending a <code class="command">.return</code> to the event monitor the exit of the system call instead. For example, to specify the entry and exit of the system call <code class="command">close</code>, use <code class="command">syscall.close</code> and <code class="command">syscall.close.return</code> respectively.
					</p></dd><dt><span class="term">vfs.<em class="replaceable"><code>file_operation</code></em></span></dt><dd><a id="d0e2092" class="indexterm"/><a id="d0e2100" class="indexterm"/><p>
						The entry to the <em class="replaceable"><code>file_operation</code></em> event for Virtual File System (VFS). Similar to <code class="command">syscall</code> event, appending a <code class="command">.return</code> to the event monitors the exit of the <em class="replaceable"><code>file_operation</code></em> operation.
					</p></dd><dt><span class="term">kernel.function("<em class="replaceable"><code>function</code></em>")</span></dt><dd><a id="d0e2129" class="indexterm"/><a id="d0e2138" class="indexterm"/><p>
						The entry to the kernel function <em class="replaceable"><code>function</code></em>. For example, <code class="command">kernel.function("sys_open")</code> refers to the "event" that occurs when the kernel function <code class="command">sys_open</code> is called by any thread in the system. To specify the <span class="emphasis"><em>return</em></span> of the kernel function <code class="command">sys_open</code>, append the <code class="command">return</code> string to the event statement; i.e. <code class="command">kernel.function("sys_open").return</code>.
					</p><a id="d0e2170" class="indexterm"/><a id="d0e2175" class="indexterm"/><a id="d0e2178" class="indexterm"/><p>
						When defining probe events, you can use asterisk (<code class="literal">*</code>) for wildcards. You can also trace the entry or exit of a function in a kernel source file. Consider the following example:
					</p><div class="example" id="wildcards"><div class="example-contents"><pre class="programlisting">probe kernel.function("*@net/socket.c") { }
probe kernel.function("*@net/socket.c").return { }</pre></div><h6>Example 3.1. wildcards.stp</h6></div><br class="example-break"/><p>
						In the previous example, the first probe's event specifies the entry of ALL functions in the kernel source file <code class="filename">net/socket.c</code>. The second probe specifies the exit of all those functions. Note that in this example, there are no statements in the handler; as such, no information will be collected or displayed.
					</p></dd><dt><span class="term">module("<em class="replaceable"><code>module</code></em>").function("<em class="replaceable"><code>function</code></em>")</span></dt><dd><a id="d0e2208" class="indexterm"/><a id="d0e2217" class="indexterm"/><p>
						Allows you to probe functions within modules. For example:
					</p><div class="example" id="eventsmodules"><div class="example-contents"><pre class="programlisting">probe module("ext3").function("*") { }
probe module("ext3").function("*").return { }</pre></div><h6>Example 3.2. moduleprobe.stp</h6></div><br class="example-break"/><p>
						The first probe in <a href="scripts.html#eventsmodules" title="Example 3.2. moduleprobe.stp">Example 3.2, “moduleprobe.stp”</a> points to the entry of <span class="emphasis"><em>all</em></span> functions for the <code class="filename">ext3</code> module. The second probe points to the exits of all functions for that same module; the use of the <code class="command">.return</code> suffix is similar to <code class="command">kernel.function()</code>. Note that the probes in <a href="scripts.html#eventsmodules" title="Example 3.2. moduleprobe.stp">Example 3.2, “moduleprobe.stp”</a> do not contain statements in the probe handlers, and as such will not print any useful data (as in <a href="scripts.html#wildcards" title="Example 3.1. wildcards.stp">Example 3.1, “wildcards.stp”</a>).
					</p><p>
						A system's kernel modules are typically located in <code class="filename">/lib/modules/<em class="replaceable"><code>kernel_version</code></em></code>, where <em class="replaceable"><code>kernel_version</code></em> refers to the currently loaded kernel version. Modules use the filename extension <code class="filename">.ko</code>.
					</p></dd></dl></div><h5 class="formalpara" id="d0e2266">Asynchronous Events</h5><a id="d0e2269" class="indexterm"/><a id="d0e2274" class="indexterm"/>
				<em class="firstterm">Asynchronous</em> events are not tied to a particular instruction or location in code. This family of probe points consists mainly of counters, timers, and similar constructs.
			<p>
			Examples of asynchronous events include:
		</p><div class="variablelist"><dl><dt><span class="term">begin</span></dt><dd><a id="d0e2291" class="indexterm"/><a id="d0e2297" class="indexterm"/><p>
						The startup of a SystemTap session; i.e. as soon as the SystemTap script is run.
					</p></dd><dt><span class="term">end</span></dt><dd><a id="d0e2309" class="indexterm"/><a id="d0e2315" class="indexterm"/><p>
						The end of a SystemTap session.
					</p></dd><dt><span class="term">timer events</span></dt><dd><a id="d0e2327" class="indexterm"/><a id="d0e2332" class="indexterm"/><p>
						An event that specifies a handler to be executed periodically. For example:
					</p><div class="example" id="timer"><div class="example-contents"><pre class="programlisting">probe timer.s(4)
{
  printf("hello world\n")
}</pre></div><h6>Example 3.3. timer-s.stp</h6></div><br class="example-break"/><p>
						<a href="scripts.html#timer" title="Example 3.3. timer-s.stp">Example 3.3, “timer-s.stp”</a> is an example of a probe that prints <code class="command">hello world</code> every 4 seconds. Note that you can also use the following timer events:
					</p><div class="itemizedlist"><ul><li><p>
								<code class="command">timer.ms(<em class="replaceable"><code>milliseconds</code></em>)</code>
							</p></li><li><p>
								<code class="command">timer.us(<em class="replaceable"><code>microseconds</code></em>)</code>
							</p></li><li><p>
								<code class="command">timer.ns(<em class="replaceable"><code>nanoseconds</code></em>)</code>
							</p></li><li><p>
								<code class="command">timer.hz(<em class="replaceable"><code>hertz</code></em>)</code>
							</p></li><li><p>
								<code class="command">timer.jiffies(<em class="replaceable"><code>jiffies</code></em>)</code>
							</p></li></ul></div><p>
						When used in conjunction with other probes that collect information, timer events allows you to print out get periodic updates and see how that information changes over time.
					</p></dd></dl></div><div class="important"><h2>Important</h2><p>
				SystemTap supports the use of a large collection of probe events. For more information about supported events, refer to <code class="command">man stapprobes</code>. The <em class="citetitle">SEE ALSO</em> section of <code class="command">man stapprobes</code> also contains links to other <code class="command">man</code> pages that discuss supported events for specific subsystems and components.
			</p></div></div></div><ul class="docnav"><li class="previous"><a accesskey="p" href="understanding-how-systemtap-works.html"><strong>Prev</strong>Chapter 3. Understanding How SystemTap Works</a></li><li class="up"><a accesskey="u" href="#"><strong>Up</strong></a></li><li class="home"><a accesskey="h" href="index.html"><strong>Home</strong></a></li><li class="next"><a accesskey="n" href="systemtapscript-handler.html"><strong>Next</strong>3.2.2. Systemtap Handler/Body</a></li></ul></body></html>