<!--#include virtual="/linux/layout/header.shtml" -->
<script>setTitle('SLC6: Scientific Linux CERN 6');</script>

<h1>What is Scientific Linux CERN 6 (SLC6) ?</h1>
<p>Scientific Linux CERN 6 is a Linux distribution build within the
framework of <a href="http://www.scientificlinux.org">Scientific Linux</a>
which in turn is rebuilt from the freely
available <a href="https://www.redhat.com/software/rhel/">Red Hat
Enterprise Linux 6 (Server)</a>
product sources under terms and conditions of the Red Hat EULA.
Scientific Linux CERN is built to integrate into the CERN computing
environment but it is not a site-specific product:
all CERN site customizations are optional and
can be deactivated for external users. </p>

<!--
<h1>When will Scientific Linux CERN 6 be available ?</h1>
Red Hat released Red Hat Enterprise Linux 6.0 on 11th of November 2010, then version 6.1 on 20th of May 2011 and
version 6.2 on 6th of December 2011. 
<strike>We - the CERN linux team - intend to prepare a first test version of SLC6 in 
coming weeks.</strike>

<strike>A <a href="test1.shtml"><em>TEST1</em> SLC 6.0 preview version is available</a> as of 29.11.2010.</strike>
<strike>A <a href="beta.shtml"><em>BETA</em>SLC 6.0 version is available</a> as of 14.02.2011.</strike>
<strike>A <a href="beta.shtml"><em>BETA</em>SLC 6.1 version is available</a> as of 09.06.2011.</strike>
<p>SLC 6.2 TEST (Beta) is available as of 13.12.2011. 
<p>Production release - SLC 6.2 is foreseen
<b>End of January 2012</b>
-->

<h1>Documentation</h1>
  <ul>
    <li><a href="docs/">User documentation pages</a>
    <li><a href="docs/install.shtml">Installation instructions</a> 
    <ul><li><a href="https://twiki.cern.ch/twiki/bin/view/LinuxSupport/Aims2">AIMS2 (kickstart based) installations</a>
    <!--        <li><a href="docs/kickstart.shtml">Example kickstart file</a> -->
        </ul>	
    <li><a href="hardware">Supported hardware</a>
    <li><a href="../updates/updates-slc6.shtml">Software updates (security and bugfix)</a> 
    <li><a href="../updates/slc6.shtml">Available software (repositories)</a>
  </ul>


<h1>Scientific Linux CERN 6 Support Lifetime</h1>
Since SLC/SL are based on sources of Red Hat Enterprise Linux 6 (RHEL6) sources
the support lifetime cannot be longer than the one proposed by Red Hat.
Due to CERN environment constraints (available manpower, hardware purchases
procedures .. etc.) Scientific Linux CERN 6 Lifetime may be actually shorter
than RHEL6 one, subject to Linux Certification Committee decision.
<br>
At present following End of Support dates are foreseen:
<ul>
<li><b>SLC6  / i386  - End of Support: 30th November 2020</b>
<li><b>SLC6  / x86_64 - End of Support: 30th November 2020</b>
</ul>
In addition to the above dates please note that from 
Q2 2016 we will not accept any request for new features.
From Q2 2017 only security updates will be provided (except for CERN-specific
software included).
(please see: <a href="http://www.redhat.com/security/updates/errata/">RHEL6 product support life cycle</a>)



<h1>Certification status</h1>
Formal certification has ended and Scientific Linux 6 CERN (SLC6) has
been declared as certified and supported Linux version
for production use at CERN as of 1st of February 2012.
The <a href="cert/">certification status page</a> contains details about
certification procedure and its status.


<a name="currver"></a><h1>Current SLC6 release</h1>

Current release is Scientific Linux CERN 6.9 released on 10th of April 2017. It merges all updates
released since previous releases in the installation tree.


<!--#include virtual="/linux/layout/footer.shtml" -->
