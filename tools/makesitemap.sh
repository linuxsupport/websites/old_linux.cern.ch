#!/bin/bash

SITEMAP="sitemap_full.xml"
SITEMAPOD="sitemap_ondemand.xml"
ODURLS="
	index.shtml
        news.shtml
	updates/index.shtml
        updates/updates-slc6.shtml
	updates/updates-slc5.shtml
	updates/updates-slc4.shtml
        scientific6/index.shtml
	scientific5/index.shtml
	scientific4/index.shtml
        scientific6/docs/index.shtml
	scientific5/docs/index.shtml
	scientific4/docs/index.shtml
	install/index.shtml
	"

TODAY=`/bin/date +%Y-%m-%d`
PREFIX="/afs/cern.ch/project/linux/wwwtest/"
WWWPREFIX="http://linux.web.cern.ch/linux/"

OUTF=$PREFIX$SITEMAP

XMLHEAD="
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">
"

XMLFOOT="
</urlset>
"

NOLOOK="/layout/|/internal/|/dualboot/|/cgi-bin/|/clug/|/attic/|/section/|/scientific3/|/updates/slc.*|/scientific4/cert/sub/|/scientific5/cert/sub/|/scientific6/cert/sub/|header.*\.shtml|footer.*\.shtml|/docs/LXCERT/"


echo $XMLHEAD > $OUTF

for PAGE in `find $PREFIX -type f -name "*shtml" | grep -vP $NOLOOK`; do


MODDATE=`/usr/bin/stat $PAGE | /bin/sed -nr 's/^Modify: ([0-9-]+) .*/\1/p'`
echo "<url>" >> $OUTF
echo "  <loc>$WWWPREFIX${PAGE##$PREFIX}</loc>" >> $OUTF
echo "looking: $WWWPREFIX${PAGE##$PREFIX}"
echo "  <lastmod>$MODDATE</lastmod>" >> $OUTF
echo "  <changefreq>weekly</changefreq>" >> $OUTF
echo "  <priority>0.5</priority>" >> $OUTF
echo "</url>" >> $OUTF

done
echo $XMLFOOT >> $OUTF

OUTF=$PREFIX$SITEMAPOD

echo $XMLHEAD > $OUTF

for PAGE in ${ODURLS}; do
MODDATE=`/usr/bin/stat $PREFIX/$PAGE | /bin/sed -nr 's/^Modify: ([0-9-]+) .*/\1/p'`
echo "<url>" >> $OUTF
echo "  <loc>$WWWPREFIX${PAGE##$PREFIX}</loc>" >> $OUTF
echo "looking: $WWWPREFIX${PAGE##$PREFIX}"
echo "  <lastmod>$MODDATE</lastmod>" >> $OUTF
echo "  <changefreq>daily</changefreq>" >> $OUTF
echo "  <priority>0.9</priority>" >> $OUTF
echo "</url>" >> $OUTF

done 

echo $XMLFOOT >> $OUTF

