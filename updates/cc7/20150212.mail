Dear Linux users.

Tonight CERN CentOS Linux 7 (CC 7X) system update
will contain following packages:


CENTOSPLUS repository:
    kernel-plus-3.10.0-123.20.1.el7.centos.plus
    kernel-plus-abi-whitelists-3.10.0-123.20.1.el7.centos.plus
    kernel-plus-devel-3.10.0-123.20.1.el7.centos.plus
    kernel-plus-doc-3.10.0-123.20.1.el7.centos.plus
    kernel-plus-headers-3.10.0-123.20.1.el7.centos.plus
    kernel-plus-tools-3.10.0-123.20.1.el7.centos.plus
    kernel-plus-tools-libs-3.10.0-123.20.1.el7.centos.plus
    kernel-plus-tools-libs-devel-3.10.0-123.20.1.el7.centos.plus
    kmod-openafs-1.6.9-2.3.10.0_123.20.1.el7.centos.plus

CERN repository:
    cern-get-certificate-0.5-1.el7.cern
    cern-get-keytab-0.9.6-1.el7.cern
    cern-get-keytab-0.9.7-1.el7.cern
    java-1.8.0-oracle-1.8.0.31-1.0.el7.cern
    java-1.8.0-oracle-devel-1.8.0.31-1.0.el7.cern
    java-1.8.0-oracle-headless-1.8.0.31-1.0.el7.cern
    java-1.8.0-oracle-javafx-1.8.0.31-1.0.el7.cern
    java-1.8.0-oracle-plugin-1.8.0.31-1.0.el7.cern
    java-1.8.0-oracle-src-1.8.0.31-1.0.el7.cern
    msktutil-0.5.1.1-0.el7.cern

UPDATES repository:
    libvirt-1.1.1-29.el7_0.7 [B]
    libvirt-client-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-config-network-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-config-nwfilter-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-driver-interface-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-driver-lxc-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-driver-network-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-driver-nodedev-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-driver-nwfilter-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-driver-qemu-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-driver-secret-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-driver-storage-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-kvm-1.1.1-29.el7_0.7 [B]
    libvirt-daemon-lxc-1.1.1-29.el7_0.7 [B]
    libvirt-devel-1.1.1-29.el7_0.7 [B]
    libvirt-docs-1.1.1-29.el7_0.7 [B]
    libvirt-lock-sanlock-1.1.1-29.el7_0.7 [B]
    libvirt-login-shell-1.1.1-29.el7_0.7 [B]
    libvirt-python-1.1.1-29.el7_0.7 [B]
    mariadb-5.5.41-2.el7_0 [S]
    mariadb-bench-5.5.41-2.el7_0 [S]
    mariadb-devel-5.5.41-2.el7_0 [S]
    mariadb-embedded-5.5.41-2.el7_0 [S]
    mariadb-embedded-devel-5.5.41-2.el7_0 [S]
    mariadb-libs-5.5.41-2.el7_0 [S]
    mariadb-server-5.5.41-2.el7_0 [S]
    mariadb-test-5.5.41-2.el7_0 [S]
    mod_dav_svn-1.7.14-7.el7_0 [S]
    resource-agents-3.9.5-26.el7_0.7
    subversion-1.7.14-7.el7_0 [S]
    subversion-devel-1.7.14-7.el7_0 [S]
    subversion-gnome-1.7.14-7.el7_0 [S]
    subversion-javahl-1.7.14-7.el7_0 [S]
    subversion-kde-1.7.14-7.el7_0 [S]
    subversion-libs-1.7.14-7.el7_0 [S]
    subversion-perl-1.7.14-7.el7_0 [S]
    subversion-python-1.7.14-7.el7_0 [S]
    subversion-ruby-1.7.14-7.el7_0 [S]
    subversion-tools-1.7.14-7.el7_0 [S]
    tzdata-2015a-1.el7_0
    tzdata-java-2015a-1.el7_0


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

This update can also be applied before nightly automated
 update run, by running as root on your machine:

# /usr/bin/yum -y update

Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Jarek Polok for Linux.Support@cern.ch
 