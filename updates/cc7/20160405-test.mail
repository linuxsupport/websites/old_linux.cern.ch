Dear Linux users.

Tonight CERN CentOS Linux 7 (CC 7X) TEST system update
will contain following packages:

 *******************************************************
 *** NOTE:                                           ***
 *** Packages listed below are provided for TESTS    ***
 *** ONLY, DO NOT USE THESE ON PRODUCTION SYSTEMS    ***
 *** these may NEVER be released to regular updates  ***
 *** and may be broken, requiring manual fixes on    ***
 *** on your system.                                 ***
 *******************************************************

CENTOSPLUS repository:
    kernel-plus-3.10.0-327.13.1.el7.centos.plus
    kernel-plus-abi-whitelists-3.10.0-327.13.1.el7.centos.plus
    kernel-plus-devel-3.10.0-327.13.1.el7.centos.plus
    kernel-plus-doc-3.10.0-327.13.1.el7.centos.plus
    kernel-plus-headers-3.10.0-327.13.1.el7.centos.plus
    kernel-plus-tools-3.10.0-327.13.1.el7.centos.plus
    kernel-plus-tools-libs-3.10.0-327.13.1.el7.centos.plus
    kernel-plus-tools-libs-devel-3.10.0-327.13.1.el7.centos.plus
    perf-3.10.0-327.13.1.el7.centos.plus
    python-perf-3.10.0-327.13.1.el7.centos.plus

CERN repository:
    aims2-client-2.13.3-1.el7.cern
    aims2-client-2.13.4-1.el7.cern
    aims2-server-2.13.3-1.el7.cern
    aims2-server-2.13.4-1.el7.cern

EXTRAS repository:
    atomic-1.9-4.gitff44c6a.el7 [B]
    centos-release-xen-46-8-1.el7
    centos-release-xen-8-1.el7
    centos-release-xen-common-8-1.el7
    cockpit-0.96-2.el7.centos
    cockpit-bridge-0.96-2.el7.centos
    cockpit-doc-0.96-2.el7.centos
    cockpit-docker-0.96-2.el7.centos
    cockpit-kubernetes-0.96-2.el7.centos
    cockpit-networkmanager-0.96-2.el7.centos
    cockpit-pcp-0.96-2.el7.centos
    cockpit-shell-0.96-2.el7.centos
    cockpit-sosreport-0.96-2.el7.centos
    cockpit-storaged-0.96-2.el7.centos
    cockpit-subscriptions-0.96-2.el7.centos
    cockpit-ws-0.96-2.el7.centos
    docker-1.9.1-25.el7.centos
    docker-distribution-2.3.1-1.el7 [B]
    docker-forward-journald-1.9.1-25.el7.centos
    docker-logrotate-1.9.1-25.el7.centos
    docker-selinux-1.9.1-25.el7.centos
    docker-unit-test-1.9.1-25.el7.centos
    dpdk-2.2.0-2.el7 [E]
    dpdk-devel-2.2.0-2.el7 [E]
    dpdk-doc-2.2.0-2.el7 [E]
    dpdk-tools-2.2.0-2.el7 [E]
    driverctl-0.59-2.el7 [E]
    etcd-2.2.5-1.el7 [B]
    kubernetes-1.2.0-0.9.alpha1.gitb57e8bd.el7 [B]
    kubernetes-client-1.2.0-0.9.alpha1.gitb57e8bd.el7 [B]
    kubernetes-master-1.2.0-0.9.alpha1.gitb57e8bd.el7 [B]
    kubernetes-node-1.2.0-0.9.alpha1.gitb57e8bd.el7 [B]
    kubernetes-unit-test-1.2.0-0.9.alpha1.gitb57e8bd.el7
    libssh-0.7.1-2.el7 [S]
    libssh-devel-0.7.1-2.el7 [S]
    python-docker-py-1.7.2-1.el7 [B]
    runc-0.0.8-1.git4155b68.el7 [B]
    skopeo-1.9-4.gitff44c6a.el7 [B]


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

 This TEST update can be applied on your system
 (providing it is set up to receive test updates
 according to: http://cern.ch/linux/updates/cc7.shtml),
 by running as root on your machine:

 # /usr/bin/yum -y update

 Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
  for Linux.Support@cern.ch
