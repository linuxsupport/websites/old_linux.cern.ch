Dear Linux users.

Tonight CERN CentOS Linux 7 (CC 7X) TEST system update
will contain following packages:

 *******************************************************
 *** NOTE:                                           ***
 *** Packages listed below are provided for TESTS    ***
 *** ONLY, DO NOT USE THESE ON PRODUCTION SYSTEMS    ***
 *** these may NEVER be released to regular updates  ***
 *** and may be broken, requiring manual fixes on    ***
 *** on your system.                                 ***
 *******************************************************

CENTOSPLUS repository:
    kernel-plus-3.10.0-693.11.1.el7.centos.plus
    kernel-plus-abi-whitelists-3.10.0-693.11.1.el7.centos.plus
    kernel-plus-devel-3.10.0-693.11.1.el7.centos.plus
    kernel-plus-doc-3.10.0-693.11.1.el7.centos.plus
    kernel-plus-headers-3.10.0-693.11.1.el7.centos.plus
    kernel-plus-tools-3.10.0-693.11.1.el7.centos.plus
    kernel-plus-tools-libs-3.10.0-693.11.1.el7.centos.plus
    kernel-plus-tools-libs-devel-3.10.0-693.11.1.el7.centos.plus
    kmod-openafs-1.6.9-2.3.10.0_693.11.1.el7.centos.plus
    perf-3.10.0-693.11.1.el7.centos.plus
    python-perf-3.10.0-693.11.1.el7.centos.plus

CERN repository:
    kmod-openafs-1.6.9-2.3.10.0_693.11.1.el7

CLOUD repository:
    openstack-ceilometer-api-9.0.3-1.el7
    openstack-ceilometer-central-9.0.3-1.el7
    openstack-ceilometer-collector-9.0.3-1.el7
    openstack-ceilometer-common-9.0.3-1.el7
    openstack-ceilometer-compute-9.0.3-1.el7
    openstack-ceilometer-ipmi-9.0.3-1.el7
    openstack-ceilometer-notification-9.0.3-1.el7
    openstack-ceilometer-polling-9.0.3-1.el7
    openstack-selinux-0.8.11-1.el7
    openstack-selinux-devel-0.8.11-1.el7
    openstack-selinux-test-0.8.11-1.el7
    openstack-tempest-17.2.0-1.el7
    openstack-tempest-all-17.2.0-1.el7
    openstack-tempest-doc-17.2.0-1.el7
    puppet-aodh-11.4.0-1.el7
    puppet-barbican-11.3.1-1.el7
    puppet-ceilometer-11.4.0-1.el7
    puppet-cinder-11.4.0-1.el7
    puppet-congress-11.3.1-1.el7
    puppet-designate-11.3.1-1.el7
    puppet-ec2api-11.3.1-1.el7
    puppet-glance-11.4.0-1.el7
    puppet-gnocchi-11.4.0-1.el7
    puppet-heat-11.4.0-1.el7
    puppet-horizon-11.4.1-1.el7
    puppet-ironic-11.4.0-1.el7
    puppet-keystone-11.3.1-1.el7
    puppet-magnum-11.3.2-1.el7
    puppet-manila-11.3.1-1.el7
    puppet-mistral-11.3.1-1.el7
    puppet-murano-11.3.2-1.el7
    puppet-neutron-11.4.0-1.el7
    puppet-nova-11.5.1-1.el7
    puppet-octavia-11.4.0-1.el7
    puppet-openstack_extras-11.4.0-1.el7
    puppet-openstacklib-11.4.0-1.el7
    puppet-oslo-11.3.1-1.el7
    puppet-ovn-11.3.1-1.el7
    puppet-panko-11.4.0-1.el7
    puppet-sahara-11.3.1-1.el7
    puppet-swift-11.3.1-1.el7
    puppet-tacker-11.3.1-1.el7
    puppet-tempest-11.4.0-1.el7
    puppet-trove-11.3.1-1.el7
    puppet-vswitch-7.3.1-1.el7
    puppet-zaqar-11.3.1-1.el7
    python2-networking-fujitsu-5.0.1-1.el7
    python2-ovsdbapp-0.4.1-1.el7
    python2-ovsdbapp-tests-0.4.1-1.el7
    python-ceilometer-9.0.3-1.el7
    python-ceilometer-tests-9.0.3-1.el7
    python-paunch-1.5.2-1.el7
    python-paunch-doc-1.5.2-1.el7
    python-paunch-tests-1.5.2-1.el7
    python-tempest-17.2.0-1.el7
    python-tempest-tests-17.2.0-1.el7

EXTRAS repository:
    centos-release-ovirt42-1.0-1.el7.centos

RT repository:
    kernel-rt-3.10.0-693.11.1.rt56.632.el7
    kernel-rt-debug-3.10.0-693.11.1.rt56.632.el7
    kernel-rt-debug-devel-3.10.0-693.11.1.rt56.632.el7
    kernel-rt-debug-kvm-3.10.0-693.11.1.rt56.632.el7
    kernel-rt-devel-3.10.0-693.11.1.rt56.632.el7
    kernel-rt-doc-3.10.0-693.11.1.rt56.632.el7
    kernel-rt-kvm-3.10.0-693.11.1.rt56.632.el7
    kernel-rt-trace-3.10.0-693.11.1.rt56.632.el7
    kernel-rt-trace-devel-3.10.0-693.11.1.rt56.632.el7
    kernel-rt-trace-kvm-3.10.0-693.11.1.rt56.632.el7

UPDATES repository:
    389-ds-base-1.3.6.1-24.el7_4 [B]
    389-ds-base-devel-1.3.6.1-24.el7_4 [B]
    389-ds-base-libs-1.3.6.1-24.el7_4 [B]
    389-ds-base-snmp-1.3.6.1-24.el7_4 [B]
    accountsservice-0.6.45-3.el7_4.1 [B]
    accountsservice-devel-0.6.45-3.el7_4.1 [B]
    accountsservice-libs-0.6.45-3.el7_4.1 [B]
    autofs-5.0.7-70.el7_4.1 [B]
    bind-9.9.4-51.el7_4.1 [B]
    bind-chroot-9.9.4-51.el7_4.1 [B]
    bind-devel-9.9.4-51.el7_4.1 [B]
    bind-libs-9.9.4-51.el7_4.1 [B]
    bind-libs-lite-9.9.4-51.el7_4.1 [B]
    bind-license-9.9.4-51.el7_4.1 [B]
    bind-lite-devel-9.9.4-51.el7_4.1 [B]
    bind-pkcs11-9.9.4-51.el7_4.1 [B]
    bind-pkcs11-devel-9.9.4-51.el7_4.1 [B]
    bind-pkcs11-libs-9.9.4-51.el7_4.1 [B]
    bind-pkcs11-utils-9.9.4-51.el7_4.1 [B]
    bind-sdb-9.9.4-51.el7_4.1 [B]
    bind-sdb-chroot-9.9.4-51.el7_4.1 [B]
    bind-utils-9.9.4-51.el7_4.1 [B]
    copy-jdk-configs-2.2-5.el7_4 [E]
    cpp-4.8.5-16.el7_4.1 [B]
    cryptsetup-1.7.4-3.el7_4.1 [B]
    cryptsetup-devel-1.7.4-3.el7_4.1 [B]
    cryptsetup-libs-1.7.4-3.el7_4.1 [B]
    cryptsetup-python-1.7.4-3.el7_4.1 [B]
    cryptsetup-reencrypt-1.7.4-3.el7_4.1 [B]
    dleyna-server-0.5.0-2.el7_4 [B]
    firefox-52.5.1-1.el7.centos
    flatpak-0.8.7-3.el7_4 [B]
    flatpak-builder-0.8.7-3.el7_4 [B]
    flatpak-devel-0.8.7-3.el7_4 [B]
    flatpak-libs-0.8.7-3.el7_4 [B]
    gcc-4.8.5-16.el7_4.1 [B]
    gcc-c++-4.8.5-16.el7_4.1 [B]
    gcc-gfortran-4.8.5-16.el7_4.1 [B]
    gcc-gnat-4.8.5-16.el7_4.1 [B]
    gcc-go-4.8.5-16.el7_4.1 [B]
    gcc-objc-4.8.5-16.el7_4.1 [B]
    gcc-objc++-4.8.5-16.el7_4.1 [B]
    gcc-plugin-devel-4.8.5-16.el7_4.1 [B]
    glibc-2.17-196.el7_4.2 [B]
    glibc-common-2.17-196.el7_4.2 [B]
    glibc-devel-2.17-196.el7_4.2 [B]
    glibc-headers-2.17-196.el7_4.2 [B]
    glibc-static-2.17-196.el7_4.2 [B]
    glibc-utils-2.17-196.el7_4.2 [B]
    ipa-client-4.5.0-22.el7.centos
    ipa-client-common-4.5.0-22.el7.centos
    ipa-common-4.5.0-22.el7.centos
    ipa-python-compat-4.5.0-22.el7.centos
    ipa-server-4.5.0-22.el7.centos
    ipa-server-common-4.5.0-22.el7.centos
    ipa-server-dns-4.5.0-22.el7.centos
    ipa-server-trust-ad-4.5.0-22.el7.centos
    java-1.7.0-openjdk-1.7.0.161-2.6.12.0.el7_4 [S]
    java-1.7.0-openjdk-accessibility-1.7.0.161-2.6.12.0.el7_4 [S]
    java-1.7.0-openjdk-demo-1.7.0.161-2.6.12.0.el7_4 [S]
    java-1.7.0-openjdk-devel-1.7.0.161-2.6.12.0.el7_4 [S]
    java-1.7.0-openjdk-headless-1.7.0.161-2.6.12.0.el7_4 [S]
    java-1.7.0-openjdk-javadoc-1.7.0.161-2.6.12.0.el7_4 [S]
    java-1.7.0-openjdk-src-1.7.0.161-2.6.12.0.el7_4 [S]
    jss-4.4.0-9.el7_4 [B]
    jss-javadoc-4.4.0-9.el7_4 [B]
    kernel-3.10.0-693.11.1.el7 [S]
    kernel-abi-whitelists-3.10.0-693.11.1.el7 [S]
    kernel-debug-3.10.0-693.11.1.el7 [S]
    kernel-debug-devel-3.10.0-693.11.1.el7 [S]
    kernel-devel-3.10.0-693.11.1.el7 [S]
    kernel-doc-3.10.0-693.11.1.el7 [S]
    kernel-headers-3.10.0-693.11.1.el7 [S]
    kernel-tools-3.10.0-693.11.1.el7 [S]
    kernel-tools-libs-3.10.0-693.11.1.el7 [S]
    kernel-tools-libs-devel-3.10.0-693.11.1.el7 [S]
    kmod-20-15.el7_4.6 [B]
    kmod-devel-20-15.el7_4.6 [B]
    kmod-libs-20-15.el7_4.6 [B]
    kpatch-0.4.0-2.el7_4 [B]
    libasan-4.8.5-16.el7_4.1 [B]
    libasan-static-4.8.5-16.el7_4.1 [B]
    libatomic-4.8.5-16.el7_4.1 [B]
    libatomic-static-4.8.5-16.el7_4.1 [B]
    libblkid-2.23.2-43.el7_4.2 [B]
    libblkid-devel-2.23.2-43.el7_4.2 [B]
    libgcc-4.8.5-16.el7_4.1 [B]
    libgfortran-4.8.5-16.el7_4.1 [B]
    libgfortran-static-4.8.5-16.el7_4.1 [B]
    libgnat-4.8.5-16.el7_4.1 [B]
    libgnat-devel-4.8.5-16.el7_4.1 [B]
    libgnat-static-4.8.5-16.el7_4.1 [B]
    libgo-4.8.5-16.el7_4.1 [B]
    libgo-devel-4.8.5-16.el7_4.1 [B]
    libgomp-4.8.5-16.el7_4.1 [B]
    libgo-static-4.8.5-16.el7_4.1 [B]
    libipa_hbac-1.15.2-50.el7_4.8 [S]
    libipa_hbac-devel-1.15.2-50.el7_4.8 [S]
    libitm-4.8.5-16.el7_4.1 [B]
    libitm-devel-4.8.5-16.el7_4.1 [B]
    libitm-static-4.8.5-16.el7_4.1 [B]
    liblouis-2.5.2-12.el7_4 [S]
    liblouis-devel-2.5.2-12.el7_4 [S]
    liblouis-doc-2.5.2-12.el7_4 [S]
    liblouis-python-2.5.2-12.el7_4 [S]
    liblouis-utils-2.5.2-12.el7_4 [S]
    libmount-2.23.2-43.el7_4.2 [B]
    libmount-devel-2.23.2-43.el7_4.2 [B]
    libmudflap-4.8.5-16.el7_4.1 [B]
    libmudflap-devel-4.8.5-16.el7_4.1 [B]
    libmudflap-static-4.8.5-16.el7_4.1 [B]
    libobjc-4.8.5-16.el7_4.1 [B]
    libpciaccess-0.13.4-3.1.el7_4 [B]
    libpciaccess-devel-0.13.4-3.1.el7_4 [B]
    libquadmath-4.8.5-16.el7_4.1 [B]
    libquadmath-devel-4.8.5-16.el7_4.1 [B]
    libquadmath-static-4.8.5-16.el7_4.1 [B]
    libreswan-3.20-5.el7_4 [B]
    libsss_autofs-1.15.2-50.el7_4.8 [S]
    libsss_certmap-1.15.2-50.el7_4.8 [S]
    libsss_certmap-devel-1.15.2-50.el7_4.8 [S]
    libsss_idmap-1.15.2-50.el7_4.8 [S]
    libsss_idmap-devel-1.15.2-50.el7_4.8 [S]
    libsss_nss_idmap-1.15.2-50.el7_4.8 [S]
    libsss_nss_idmap-devel-1.15.2-50.el7_4.8 [S]
    libsss_simpleifp-1.15.2-50.el7_4.8 [S]
    libsss_simpleifp-devel-1.15.2-50.el7_4.8 [S]
    libsss_sudo-1.15.2-50.el7_4.8 [S]
    libstdc++-4.8.5-16.el7_4.1 [B]
    libstdc++-devel-4.8.5-16.el7_4.1 [B]
    libstdc++-docs-4.8.5-16.el7_4.1 [B]
    libstdc++-static-4.8.5-16.el7_4.1 [B]
    libstoragemgmt-1.4.0-5.el7_4 [B]
    libstoragemgmt-devel-1.4.0-5.el7_4 [B]
    libstoragemgmt-hpsa-plugin-1.4.0-5.el7_4 [B]
    libstoragemgmt-megaraid-plugin-1.4.0-5.el7_4 [B]
    libstoragemgmt-netapp-plugin-1.4.0-5.el7_4 [B]
    libstoragemgmt-nstor-plugin-1.4.0-5.el7_4 [B]
    libstoragemgmt-python-1.4.0-5.el7_4 [B]
    libstoragemgmt-python-clibs-1.4.0-5.el7_4 [B]
    libstoragemgmt-smis-plugin-1.4.0-5.el7_4 [B]
    libstoragemgmt-targetd-plugin-1.4.0-5.el7_4 [B]
    libstoragemgmt-udev-1.4.0-5.el7_4 [B]
    libtsan-4.8.5-16.el7_4.1 [B]
    libtsan-static-4.8.5-16.el7_4.1 [B]
    libuuid-2.23.2-43.el7_4.2 [B]
    libuuid-devel-2.23.2-43.el7_4.2 [B]
    libvirt-3.2.0-14.el7_4.4 [B]
    libvirt-admin-3.2.0-14.el7_4.4 [B]
    libvirt-client-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-config-network-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-config-nwfilter-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-interface-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-lxc-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-network-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-nodedev-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-nwfilter-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-qemu-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-secret-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-storage-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-storage-core-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-storage-disk-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-storage-gluster-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-storage-iscsi-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-storage-logical-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-storage-mpath-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-storage-rbd-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-driver-storage-scsi-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-kvm-3.2.0-14.el7_4.4 [B]
    libvirt-daemon-lxc-3.2.0-14.el7_4.4 [B]
    libvirt-devel-3.2.0-14.el7_4.4 [B]
    libvirt-docs-3.2.0-14.el7_4.4 [B]
    libvirt-libs-3.2.0-14.el7_4.4 [B]
    libvirt-lock-sanlock-3.2.0-14.el7_4.4 [B]
    libvirt-login-shell-3.2.0-14.el7_4.4 [B]
    libvirt-nss-3.2.0-14.el7_4.4 [B]
    mod_fcgid-2.3.9-4.el7_4.1 [B]
    mutter-3.22.3-12.el7_4 [B]
    mutter-devel-3.22.3-12.el7_4 [B]
    nscd-2.17-196.el7_4.2 [B]
    pacemaker-1.1.16-12.el7_4.5
    pacemaker-cli-1.1.16-12.el7_4.5
    pacemaker-cluster-libs-1.1.16-12.el7_4.5
    pacemaker-cts-1.1.16-12.el7_4.5
    pacemaker-doc-1.1.16-12.el7_4.5
    pacemaker-libs-1.1.16-12.el7_4.5
    pacemaker-libs-devel-1.1.16-12.el7_4.5
    pacemaker-nagios-plugins-metadata-1.1.16-12.el7_4.5
    pacemaker-remote-1.1.16-12.el7_4.5
    pcs-0.9.158-6.el7.centos.1
    perf-3.10.0-693.11.1.el7 [S]
    pki-base-10.4.1-17.el7_4 [B]
    pki-base-java-10.4.1-17.el7_4 [B]
    pki-ca-10.4.1-17.el7_4 [B]
    pki-javadoc-10.4.1-17.el7_4 [B]
    pki-kra-10.4.1-17.el7_4 [B]
    pki-server-10.4.1-17.el7_4 [B]
    pki-symkey-10.4.1-17.el7_4 [B]
    pki-tools-10.4.1-17.el7_4 [B]
    python2-ipaclient-4.5.0-22.el7.centos
    python2-ipalib-4.5.0-22.el7.centos
    python2-ipaserver-4.5.0-22.el7.centos
    python-libipa_hbac-1.15.2-50.el7_4.8 [S]
    python-libsss_nss_idmap-1.15.2-50.el7_4.8 [S]
    python-perf-3.10.0-693.11.1.el7 [S]
    python-sss-1.15.2-50.el7_4.8 [S]
    python-sssdconfig-1.15.2-50.el7_4.8 [S]
    python-sss-murmur-1.15.2-50.el7_4.8 [S]
    qemu-img-1.5.3-141.el7_4.4 [S]
    qemu-kvm-1.5.3-141.el7_4.4 [S]
    qemu-kvm-common-1.5.3-141.el7_4.4 [S]
    qemu-kvm-tools-1.5.3-141.el7_4.4 [S]
    resource-agents-3.9.5-105.el7_4.3
    rhnsd-5.0.13-7.3.el7_4 [B]
    selinux-policy-3.13.1-166.el7_4.7 [B]
    selinux-policy-devel-3.13.1-166.el7_4.7 [B]
    selinux-policy-doc-3.13.1-166.el7_4.7 [B]
    selinux-policy-minimum-3.13.1-166.el7_4.7 [B]
    selinux-policy-mls-3.13.1-166.el7_4.7 [B]
    selinux-policy-sandbox-3.13.1-166.el7_4.7 [B]
    selinux-policy-targeted-3.13.1-166.el7_4.7 [B]
    sssd-1.15.2-50.el7_4.8 [S]
    sssd-ad-1.15.2-50.el7_4.8 [S]
    sssd-client-1.15.2-50.el7_4.8 [S]
    sssd-common-1.15.2-50.el7_4.8 [S]
    sssd-common-pac-1.15.2-50.el7_4.8 [S]
    sssd-dbus-1.15.2-50.el7_4.8 [S]
    sssd-ipa-1.15.2-50.el7_4.8 [S]
    sssd-kcm-1.15.2-50.el7_4.8 [S]
    sssd-krb5-1.15.2-50.el7_4.8 [S]
    sssd-krb5-common-1.15.2-50.el7_4.8 [S]
    sssd-ldap-1.15.2-50.el7_4.8 [S]
    sssd-libwbclient-1.15.2-50.el7_4.8 [S]
    sssd-libwbclient-devel-1.15.2-50.el7_4.8 [S]
    sssd-polkit-rules-1.15.2-50.el7_4.8 [S]
    sssd-proxy-1.15.2-50.el7_4.8 [S]
    sssd-tools-1.15.2-50.el7_4.8 [S]
    sssd-winbind-idmap-1.15.2-50.el7_4.8 [S]
    systemtap-3.1-4.el7_4 [B]
    systemtap-client-3.1-4.el7_4 [B]
    systemtap-devel-3.1-4.el7_4 [B]
    systemtap-initscript-3.1-4.el7_4 [B]
    systemtap-runtime-3.1-4.el7_4 [B]
    systemtap-runtime-java-3.1-4.el7_4 [B]
    systemtap-runtime-python2-3.1-4.el7_4 [B]
    systemtap-runtime-virtguest-3.1-4.el7_4 [B]
    systemtap-runtime-virthost-3.1-4.el7_4 [B]
    systemtap-sdt-devel-3.1-4.el7_4 [B]
    systemtap-server-3.1-4.el7_4 [B]
    systemtap-testsuite-3.1-4.el7_4 [B]
    thunderbird-52.5.0-1.el7.centos
    tigervnc-1.8.0-2.el7_4 [B]
    tigervnc-icons-1.8.0-2.el7_4 [B]
    tigervnc-license-1.8.0-2.el7_4 [B]
    tigervnc-server-1.8.0-2.el7_4 [B]
    tigervnc-server-applet-1.8.0-2.el7_4 [B]
    tigervnc-server-minimal-1.8.0-2.el7_4 [B]
    tigervnc-server-module-1.8.0-2.el7_4 [B]
    util-linux-2.23.2-43.el7_4.2 [B]
    uuidd-2.23.2-43.el7_4.2 [B]
    veritysetup-1.7.4-3.el7_4.1 [B]
    virt-who-0.19-7.el7_4 [B]
    webkitgtk4-2.14.7-3.el7 [B]
    webkitgtk4-devel-2.14.7-3.el7 [B]
    webkitgtk4-doc-2.14.7-3.el7 [B]
    webkitgtk4-jsc-2.14.7-3.el7 [B]
    webkitgtk4-jsc-devel-2.14.7-3.el7 [B]
    webkitgtk4-plugin-process-gtk2-2.14.7-3.el7 [B]

VIRT repository:
    hystrix-core-1.4.21-6.el7
    hystrix-metrics-event-stream-1.4.21-6.el7
    jackson-core-2.6.3-1.el7
    jackson-core-javadoc-2.6.3-1.el7
    libvirt-3.10.0-1.el7
    libvirt-3.9.0-1.el7
    libvirt-admin-3.10.0-1.el7
    libvirt-admin-3.9.0-1.el7
    libvirt-client-3.10.0-1.el7
    libvirt-client-3.9.0-1.el7
    libvirt-daemon-3.10.0-1.el7
    libvirt-daemon-3.9.0-1.el7
    libvirt-daemon-config-network-3.10.0-1.el7
    libvirt-daemon-config-network-3.9.0-1.el7
    libvirt-daemon-config-nwfilter-3.10.0-1.el7
    libvirt-daemon-config-nwfilter-3.9.0-1.el7
    libvirt-daemon-driver-interface-3.10.0-1.el7
    libvirt-daemon-driver-interface-3.9.0-1.el7
    libvirt-daemon-driver-lxc-3.10.0-1.el7
    libvirt-daemon-driver-lxc-3.9.0-1.el7
    libvirt-daemon-driver-network-3.10.0-1.el7
    libvirt-daemon-driver-network-3.9.0-1.el7
    libvirt-daemon-driver-nodedev-3.10.0-1.el7
    libvirt-daemon-driver-nodedev-3.9.0-1.el7
    libvirt-daemon-driver-nwfilter-3.10.0-1.el7
    libvirt-daemon-driver-nwfilter-3.9.0-1.el7
    libvirt-daemon-driver-qemu-3.10.0-1.el7
    libvirt-daemon-driver-qemu-3.9.0-1.el7
    libvirt-daemon-driver-secret-3.10.0-1.el7
    libvirt-daemon-driver-secret-3.9.0-1.el7
    libvirt-daemon-driver-storage-3.10.0-1.el7
    libvirt-daemon-driver-storage-3.9.0-1.el7
    libvirt-daemon-driver-storage-core-3.10.0-1.el7
    libvirt-daemon-driver-storage-core-3.9.0-1.el7
    libvirt-daemon-driver-storage-disk-3.10.0-1.el7
    libvirt-daemon-driver-storage-disk-3.9.0-1.el7
    libvirt-daemon-driver-storage-gluster-3.10.0-1.el7
    libvirt-daemon-driver-storage-gluster-3.9.0-1.el7
    libvirt-daemon-driver-storage-iscsi-3.10.0-1.el7
    libvirt-daemon-driver-storage-iscsi-3.9.0-1.el7
    libvirt-daemon-driver-storage-logical-3.10.0-1.el7
    libvirt-daemon-driver-storage-logical-3.9.0-1.el7
    libvirt-daemon-driver-storage-mpath-3.10.0-1.el7
    libvirt-daemon-driver-storage-mpath-3.9.0-1.el7
    libvirt-daemon-driver-storage-rbd-3.10.0-1.el7
    libvirt-daemon-driver-storage-rbd-3.9.0-1.el7
    libvirt-daemon-driver-storage-scsi-3.10.0-1.el7
    libvirt-daemon-driver-storage-scsi-3.9.0-1.el7
    libvirt-daemon-kvm-3.10.0-1.el7
    libvirt-daemon-kvm-3.9.0-1.el7
    libvirt-daemon-lxc-3.10.0-1.el7
    libvirt-daemon-lxc-3.9.0-1.el7
    libvirt-devel-3.10.0-1.el7
    libvirt-devel-3.9.0-1.el7
    libvirt-docs-3.10.0-1.el7
    libvirt-docs-3.9.0-1.el7
    libvirt-libs-3.10.0-1.el7
    libvirt-libs-3.9.0-1.el7
    libvirt-lock-sanlock-3.10.0-1.el7
    libvirt-lock-sanlock-3.9.0-1.el7
    libvirt-login-shell-3.10.0-1.el7
    libvirt-login-shell-3.9.0-1.el7
    libvirt-nss-3.10.0-1.el7
    libvirt-nss-3.9.0-1.el7
    libvirt-python-3.10.0-1.el7
    libvirt-python-3.9.0-1.el7
    makeself-2.2.0-3.el7
    openstack-java-ceilometer-client-3.1.2-2.el7
    openstack-java-ceilometer-model-3.1.2-2.el7
    openstack-java-cinder-client-3.1.2-2.el7
    openstack-java-cinder-model-3.1.2-2.el7
    openstack-java-client-3.1.2-2.el7
    openstack-java-glance-client-3.1.2-2.el7
    openstack-java-glance-model-3.1.2-2.el7
    openstack-java-javadoc-3.1.2-2.el7
    openstack-java-keystone-client-3.1.2-2.el7
    openstack-java-keystone-model-3.1.2-2.el7
    openstack-java-nova-client-3.1.2-2.el7
    openstack-java-nova-model-3.1.2-2.el7
    openstack-java-quantum-client-3.1.2-2.el7
    openstack-java-quantum-model-3.1.2-2.el7
    openstack-java-resteasy-connector-3.1.2-2.el7
    openstack-java-swift-client-3.1.2-2.el7
    openstack-java-swift-model-3.1.2-2.el7
    ovirt-cockpit-sso-0.0.4-1.el7
    ovirt-guest-agent-common-1.0.14-1.el7
    ovirt-guest-agent-pam-module-1.0.14-1.el7
    pyOpenSSL-doc-16.2.0-3.el7
    python2-debtcollector-1.17.0-1.el7
    python2-iso8601-0.1.11-1.el7
    python2-mox3-0.23.0-1.el7
    python2-oslotest-2.17.0-1.el7
    python2-ovsdbapp-0.6.0-1.el7
    python2-ovsdbapp-tests-0.6.0-1.el7
    python2-pbr-3.1.1-1.el7
    python2-pyOpenSSL-16.2.0-3.el7
    python2-subunit-1.2.0-14.el7
    python-debtcollector-doc-1.17.0-1.el7
    python-fixtures-3.0.0-2.el7
    python-junitxml-0.7-1.el7
    python-passlib-1.6.5-2.el7 [E]
    python-testrepository-0.0.18-2.el7
    python-testscenarios-0.5.0-5.el7
    python-wrapt-1.10.8-2.el7
    sshpass-1.06-2.el7 [E]
    subunit-1.2.0-14.el7
    subunit-cppunit-1.2.0-14.el7
    subunit-cppunit-devel-1.2.0-14.el7
    subunit-devel-1.2.0-14.el7
    subunit-filters-1.2.0-14.el7
    subunit-perl-1.2.0-14.el7
    subunit-shell-1.2.0-14.el7
    unboundid-ldapsdk-3.2.1-1.el7
    unboundid-ldapsdk-javadoc-3.2.1-1.el7


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

 This TEST update can be applied on your system
 (providing it is set up to receive test updates
 according to: http://cern.ch/linux/updates/cc7.shtml),
 by running as root on your machine:

 # /usr/bin/yum -y update

 Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Jarek Polok for Linux.Support@cern.ch
