Dear Linux users.

Tonight CERN CentOS Linux 7 (CC 7X) TEST system update
will contain following packages:

 *******************************************************
 *** NOTE:                                           ***
 *** Packages listed below are provided for TESTS    ***
 *** ONLY, DO NOT USE THESE ON PRODUCTION SYSTEMS    ***
 *** these may NEVER be released to regular updates  ***
 *** and may be broken, requiring manual fixes on    ***
 *** on your system.                                 ***
 *******************************************************

CERN repository:
    aims2-client-2.27-2.el7.cern
    aims2-server-2.27-2.el7.cern

CLOUD repository:
    ansible-role-openstack-ml2-0.2.0-1.el7
    puppet-rabbitmq-9.0.1-0.2.7613f08git.el7
    python2-designateclient-2.7.1-1.el7
    python2-designateclient-tests-2.7.1-1.el7
    python2-ironicclient-1.17.2-1.el7
    python2-keystoneauth1-3.1.1-1.el7
    python2-keystonemiddleware-4.17.1-1.el7
    python2-networking-ansible-0.2.0-1.el7
    python2-networking-ansible-doc-0.2.0-1.el7
    python2-networking-ansible-tests-0.2.0-1.el7
    python2-networking-arista-2018.2.2-1.el7
    python2-novaclient-9.1.3-1.el7
    python2-os-client-config-1.28.1-1.el7
    python2-os-client-config-doc-1.28.1-1.el7
    python2-oslo-policy-1.25.4-1.el7
    python2-os-traits-0.3.4-1.el7
    python2-os-traits-tests-0.3.4-1.el7
    python2-os-vif-1.7.2-1.el7
    python2-os-vif-tests-1.7.2-1.el7
    python2-os-win-2.2.1-1.el7
    python2-shade-1.22.3-1.el7
    python2-vmware-nsx-tests-tempest-2.0.0.0-1.el7
    python2-zaqarclient-1.7.1-1.el7
    python-designateclient-doc-2.7.1-1.el7
    python-ironic-inspector-client-2.1.1-1.el7
    python-ironic-lib-2.10.2-1.el7
    python-keystoneauth1-doc-3.1.1-1.el7
    python-keystonemiddleware-doc-4.17.1-1.el7
    python-novaclient-doc-9.1.3-1.el7
    python-oslo-policy-doc-1.25.4-1.el7
    python-oslo-policy-lang-1.25.4-1.el7
    python-oslo-policy-tests-1.25.4-1.el7
    python-os-traits-doc-0.3.4-1.el7
    python-os-vif-doc-1.7.2-1.el7
    python-os-win-doc-2.2.1-1.el7
    python-vmware-nsx-tests-tempest-doc-2.0.0.0-1.el7

STORAGE repository:
    ceph-ansible-4.0.0-0.rc5.1.el7

VIRT repository:
    katello-agent-3.3.5-5.1.el7
    katello-host-tools-3.3.5-5.1.el7
    katello-host-tools-fact-plugin-3.3.5-5.1.el7
    katello-host-tools-tracer-3.3.5-5.1.el7
    ovirt-guest-agent-common-1.0.16-1.el7
    ovirt-guest-agent-pam-module-1.0.16-1.el7
    pulp-admin-client-2.16.4.2-1.1.el7
    pulp-agent-2.16.4.2-1.1.el7
    pulp-consumer-client-2.16.4.2-1.1.el7
    pulp-rpm-admin-extensions-2.16.4.2-1.1.el7
    pulp-rpm-consumer-extensions-2.16.4.2-1.1.el7
    pulp-rpm-handlers-2.16.4.2-1.1.el7
    pulp-rpm-yumplugins-2.16.4.2-1.1.el7
    python-pulp-agent-lib-2.16.4.2-1.1.el7
    python-pulp-bindings-2.16.4.2-1.1.el7
    python-pulp-client-lib-2.16.4.2-1.1.el7
    python-pulp-common-2.16.4.2-1.1.el7
    python-pulp-devel-2.16.4.2-1.1.el7
    python-pulp-rpm-common-2.16.4.2-1.1.el7


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

 This TEST update can be applied on your system
 (providing it is set up to receive test updates
 according to: http://cern.ch/linux/updates/cc7.shtml),
 by running as root on your machine:

 # /usr/bin/yum -y update

 Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Thomas Oulevey for Linux.Support@cern.ch
