Dear Linux users.

Tonight CERN CentOS Linux 7 (CC 7X) TEST system update
will contain following packages:

 *******************************************************
 *** NOTE:                                           ***
 *** Packages listed below are provided for TESTS    ***
 *** ONLY, DO NOT USE THESE ON PRODUCTION SYSTEMS    ***
 *** these may NEVER be released to regular updates  ***
 *** and may be broken, requiring manual fixes on    ***
 *** on your system.                                 ***
 *******************************************************

CERN repository:
    kopano-deskapp-2.3.11-2038.1

CLOUD repository:
    ansible-2.8.0-1.el7.ans
    instack-undercloud-8.4.8-1.el7
    openstack-tripleo-common-8.7.0-1.el7
    openstack-tripleo-common-9.6.0-1.el7
    openstack-tripleo-common-container-base-8.7.0-1.el7
    openstack-tripleo-common-container-base-9.6.0-1.el7
    openstack-tripleo-common-containers-8.7.0-1.el7
    openstack-tripleo-common-containers-9.6.0-1.el7
    openstack-tripleo-common-devtools-8.7.0-1.el7
    openstack-tripleo-common-devtools-9.6.0-1.el7
    openstack-tripleo-heat-templates-8.4.0-1.el7
    openstack-tripleo-heat-templates-9.4.0-1.el7
    openstack-tripleo-puppet-elements-8.1.0-1.el7
    openstack-tripleo-puppet-elements-9.1.0-1.el7
    openstack-tripleo-validations-8.5.0-1.el7
    openstack-tripleo-validations-9.4.0-1.el7
    openstack-tripleo-validations-doc-8.5.0-1.el7
    openstack-tripleo-validations-doc-9.4.0-1.el7
    openstack-tripleo-validations-tests-8.5.0-1.el7
    openstack-tripleo-validations-tests-9.4.0-1.el7
    os-apply-config-8.3.2-1.el7
    os-apply-config-9.1.2-1.el7
    os-net-config-8.5.0-1.el7
    os-net-config-9.4.0-1.el7
    puppet-manila-14.4.1-1.el7
    puppet-tripleo-8.5.0-1.el7
    puppet-tripleo-9.5.0-1.el7
    python2-novaclient-13.0.1-1.el7
    python2-octaviaclient-1.8.1-1.el7
    python2-octaviaclient-tests-1.8.1-1.el7
    python2-tripleo-common-9.6.0-1.el7
    python-novaclient-doc-13.0.1-1.el7
    python-octaviaclient-doc-1.8.1-1.el7
    python-tripleoclient-10.7.0-1.el7
    python-tripleoclient-9.3.0-1.el7
    python-tripleoclient-heat-installer-10.7.0-1.el7
    python-tripleoclient-heat-installer-9.3.0-1.el7

STORAGE repository:
    glusterfs-4.1.9-1.el7
    glusterfs-6.3-1.el7
    glusterfs-api-4.1.9-1.el7
    glusterfs-api-6.3-1.el7
    glusterfs-api-devel-4.1.9-1.el7
    glusterfs-api-devel-6.3-1.el7
    glusterfs-cli-4.1.9-1.el7
    glusterfs-cli-6.3-1.el7
    glusterfs-client-xlators-4.1.9-1.el7
    glusterfs-client-xlators-6.3-1.el7
    glusterfs-cloudsync-plugins-6.3-1.el7
    glusterfs-devel-4.1.9-1.el7
    glusterfs-devel-6.3-1.el7
    glusterfs-events-4.1.9-1.el7
    glusterfs-events-6.3-1.el7
    glusterfs-extra-xlators-4.1.9-1.el7
    glusterfs-extra-xlators-6.3-1.el7
    glusterfs-fuse-4.1.9-1.el7
    glusterfs-fuse-6.3-1.el7
    glusterfs-geo-replication-4.1.9-1.el7
    glusterfs-geo-replication-6.3-1.el7
    glusterfs-libs-4.1.9-1.el7
    glusterfs-libs-6.3-1.el7
    glusterfs-rdma-4.1.9-1.el7
    glusterfs-rdma-6.3-1.el7
    glusterfs-resource-agents-4.1.9-1.el7
    glusterfs-resource-agents-6.3-1.el7
    glusterfs-server-4.1.9-1.el7
    glusterfs-server-6.3-1.el7
    glusterfs-thin-arbiter-6.3-1.el7
    python2-gluster-4.1.9-1.el7
    python2-gluster-6.3-1.el7

VIRT repository:
    openstack-java-ceilometer-client-3.2.7-1.el7
    openstack-java-ceilometer-model-3.2.7-1.el7
    openstack-java-cinder-client-3.2.7-1.el7
    openstack-java-cinder-model-3.2.7-1.el7
    openstack-java-client-3.2.7-1.el7
    openstack-java-glance-client-3.2.7-1.el7
    openstack-java-glance-model-3.2.7-1.el7
    openstack-java-heat-client-3.2.7-1.el7
    openstack-java-heat-model-3.2.7-1.el7
    openstack-java-javadoc-3.2.7-1.el7
    openstack-java-keystone-client-3.2.7-1.el7
    openstack-java-keystone-model-3.2.7-1.el7
    openstack-java-nova-client-3.2.7-1.el7
    openstack-java-nova-model-3.2.7-1.el7
    openstack-java-quantum-client-3.2.7-1.el7
    openstack-java-quantum-model-3.2.7-1.el7
    openstack-java-resteasy-connector-3.2.7-1.el7
    openstack-java-swift-client-3.2.7-1.el7
    openstack-java-swift-model-3.2.7-1.el7


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

 This TEST update can be applied on your system
 (providing it is set up to receive test updates
 according to: http://cern.ch/linux/updates/cc7.shtml),
 by running as root on your machine:

 # /usr/bin/yum -y update

 Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Thomas Oulevey for Linux.Support@cern.ch
