Dear Linux users.

Tonight CERN CentOS Linux 7 (CC 7X) system update
will contain following packages:


CENTOSPLUS repository:
    bpftool-3.10.0-957.27.2.el7.centos.plus.bug16242
    kernel-plus-3.10.0-957.27.2.el7.centos.plus.bug16242
    kernel-plus-abi-whitelists-3.10.0-957.27.2.el7.centos.plus.bug16242
    kernel-plus-devel-3.10.0-957.27.2.el7.centos.plus.bug16242
    kernel-plus-doc-3.10.0-957.27.2.el7.centos.plus.bug16242
    kernel-plus-headers-3.10.0-957.27.2.el7.centos.plus.bug16242
    kernel-plus-tools-3.10.0-957.27.2.el7.centos.plus.bug16242
    kernel-plus-tools-libs-3.10.0-957.27.2.el7.centos.plus.bug16242
    kernel-plus-tools-libs-devel-3.10.0-957.27.2.el7.centos.plus.bug16242
    perf-3.10.0-957.27.2.el7.centos.plus.bug16242
    python-perf-3.10.0-957.27.2.el7.centos.plus.bug16242

CERN repository:
    kmod-openafs-1.6.22.3-1.3.10.0_957.27.2.el7

CLOUD repository:
    openstack-cinder-12.0.8-1.el7
    openstack-cinder-doc-12.0.8-1.el7
    python2-ldap-3.1.0-1.el7
    python2-pyasn1-0.3.7-6.el7
    python2-pyasn1-modules-0.3.7-6.el7
    python2-pysnmp-4.4.9-2.el7
    python-cinder-12.0.8-1.el7
    python-cinder-tests-12.0.8-1.el7
    python-pyasn1-doc-0.3.7-6.el7

EXTRAS repository:
    buildah-1.9.0-1.el7.centos
    containernetworking-plugins-0.8.1-1.el7.centos
    containers-common-0.1.37-1.el7.centos
    container-selinux-2.107-1.el7_6
    docker-1.13.1-102.git7f2769b.el7.centos
    docker-client-1.13.1-102.git7f2769b.el7.centos
    docker-common-1.13.1-102.git7f2769b.el7.centos
    docker-logrotate-1.13.1-102.git7f2769b.el7.centos
    docker-lvm-plugin-1.13.1-102.git7f2769b.el7.centos
    docker-novolume-plugin-1.13.1-102.git7f2769b.el7.centos
    docker-v1.10-migrator-1.13.1-102.git7f2769b.el7.centos
    oci-umount-2.5-1.el7_6
    podman-1.4.4-2.el7.centos
    podman-docker-1.4.4-2.el7.centos
    runc-1.0.0-64.rc8.el7.centos
    skopeo-0.1.37-1.el7.centos

STORAGE repository:
    ceph-ansible-4.0.0-0.rc13.1.el7
    glusterfs-5.8-1.el7
    glusterfs-api-5.8-1.el7
    glusterfs-api-devel-5.8-1.el7
    glusterfs-cli-5.8-1.el7
    glusterfs-client-xlators-5.8-1.el7
    glusterfs-cloudsync-plugins-5.8-1.el7
    glusterfs-devel-5.8-1.el7
    glusterfs-events-5.8-1.el7
    glusterfs-extra-xlators-5.8-1.el7
    glusterfs-fuse-5.8-1.el7
    glusterfs-geo-replication-5.8-1.el7
    glusterfs-libs-5.8-1.el7
    glusterfs-rdma-5.8-1.el7
    glusterfs-resource-agents-5.8-1.el7
    glusterfs-server-5.8-1.el7
    python2-gluster-5.8-1.el7

UPDATES repository:
    389-ds-base-1.3.8.4-25.1.el7_6 [S]
    389-ds-base-devel-1.3.8.4-25.1.el7_6 [S]
    389-ds-base-libs-1.3.8.4-25.1.el7_6 [S]
    389-ds-base-snmp-1.3.8.4-25.1.el7_6 [S]
    bind-9.9.4-74.el7_6.2 [B]
    bind-chroot-9.9.4-74.el7_6.2 [B]
    bind-devel-9.9.4-74.el7_6.2 [B]
    bind-libs-9.9.4-74.el7_6.2 [B]
    bind-libs-lite-9.9.4-74.el7_6.2 [B]
    bind-license-9.9.4-74.el7_6.2 [B]
    bind-lite-devel-9.9.4-74.el7_6.2 [B]
    bind-pkcs11-9.9.4-74.el7_6.2 [B]
    bind-pkcs11-devel-9.9.4-74.el7_6.2 [B]
    bind-pkcs11-libs-9.9.4-74.el7_6.2 [B]
    bind-pkcs11-utils-9.9.4-74.el7_6.2 [B]
    bind-sdb-9.9.4-74.el7_6.2 [B]
    bind-sdb-chroot-9.9.4-74.el7_6.2 [B]
    bind-utils-9.9.4-74.el7_6.2 [B]
    bpftool-3.10.0-957.27.2.el7 [S]
    cloud-utils-growpart-0.29-2.el7_6.2 [B]
    ctdb-4.8.3-6.el7_6
    ctdb-tests-4.8.3-6.el7_6
    curl-7.29.0-51.el7_6.3 [S]
    foomatic-4.0.9-8.el7_6.1 [B]
    foomatic-filters-4.0.9-8.el7_6.1 [B]
    gdm-3.28.2-12.el7_6.2 [B]
    gdm-devel-3.28.2-12.el7_6.2 [B]
    gdm-pam-extensions-devel-3.28.2-12.el7_6.2 [B]
    httpd-2.4.6-89.el7.centos.1
    httpd-devel-2.4.6-89.el7.centos.1
    httpd-manual-2.4.6-89.el7.centos.1
    httpd-tools-2.4.6-89.el7.centos.1
    ipa-client-4.6.4-10.el7.centos.6
    ipa-client-common-4.6.4-10.el7.centos.6
    ipa-common-4.6.4-10.el7.centos.6
    ipa-python-compat-4.6.4-10.el7.centos.6
    ipa-server-4.6.4-10.el7.centos.6
    ipa-server-common-4.6.4-10.el7.centos.6
    ipa-server-dns-4.6.4-10.el7.centos.6
    ipa-server-trust-ad-4.6.4-10.el7.centos.6
    java-11-openjdk-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-debug-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-demo-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-demo-debug-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-devel-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-devel-debug-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-headless-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-headless-debug-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-javadoc-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-javadoc-debug-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-javadoc-zip-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-javadoc-zip-debug-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-jmods-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-jmods-debug-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-src-11.0.4.11-0.el7_6 [S]
    java-11-openjdk-src-debug-11.0.4.11-0.el7_6 [S]
    java-1.7.0-openjdk-1.7.0.231-2.6.19.1.el7_6 [S]
    java-1.7.0-openjdk-accessibility-1.7.0.231-2.6.19.1.el7_6 [S]
    java-1.7.0-openjdk-demo-1.7.0.231-2.6.19.1.el7_6 [S]
    java-1.7.0-openjdk-devel-1.7.0.231-2.6.19.1.el7_6 [S]
    java-1.7.0-openjdk-headless-1.7.0.231-2.6.19.1.el7_6 [S]
    java-1.7.0-openjdk-javadoc-1.7.0.231-2.6.19.1.el7_6 [S]
    java-1.7.0-openjdk-src-1.7.0.231-2.6.19.1.el7_6 [S]
    java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-accessibility-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-accessibility-debug-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-debug-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-demo-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-demo-debug-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-devel-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-devel-debug-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-headless-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-headless-debug-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-javadoc-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-javadoc-debug-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-javadoc-zip-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-javadoc-zip-debug-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-src-1.8.0.222.b10-0.el7_6 [S]
    java-1.8.0-openjdk-src-debug-1.8.0.222.b10-0.el7_6 [S]
    keepalived-1.3.5-8.el7_6.5 [B]
    kernel-3.10.0-957.27.2.el7 [S]
    kernel-abi-whitelists-3.10.0-957.27.2.el7 [S]
    kernel-debug-3.10.0-957.27.2.el7 [S]
    kernel-debug-devel-3.10.0-957.27.2.el7 [S]
    kernel-devel-3.10.0-957.27.2.el7 [S]
    kernel-doc-3.10.0-957.27.2.el7 [S]
    kernel-headers-3.10.0-957.27.2.el7 [S]
    kernel-tools-3.10.0-957.27.2.el7 [S]
    kernel-tools-libs-3.10.0-957.27.2.el7 [S]
    kernel-tools-libs-devel-3.10.0-957.27.2.el7 [S]
    kexec-tools-2.0.15-21.el7_6.4 [B]
    kexec-tools-anaconda-addon-2.0.15-21.el7_6.4 [B]
    kexec-tools-eppic-2.0.15-21.el7_6.4 [B]
    libcurl-7.29.0-51.el7_6.3 [S]
    libcurl-devel-7.29.0-51.el7_6.3 [S]
    libgudev1-219-62.el7_6.9 [B]
    libgudev1-devel-219-62.el7_6.9 [B]
    libsmbclient-4.8.3-6.el7_6 [B]
    libsmbclient-devel-4.8.3-6.el7_6 [B]
    libssh2-1.4.3-12.el7_6.3 [S]
    libssh2-devel-1.4.3-12.el7_6.3 [S]
    libssh2-docs-1.4.3-12.el7_6.3 [S]
    libwbclient-4.8.3-6.el7_6 [B]
    libwbclient-devel-4.8.3-6.el7_6 [B]
    mesa-libGLw-8.0.0-4.1.el7_6 [B]
    mesa-libGLw-devel-8.0.0-4.1.el7_6 [B]
    ModemManager-1.6.10-3.el7_6 [B]
    ModemManager-devel-1.6.10-3.el7_6 [B]
    ModemManager-glib-1.6.10-3.el7_6 [B]
    ModemManager-glib-devel-1.6.10-3.el7_6 [B]
    ModemManager-vala-1.6.10-3.el7_6 [B]
    mod_ldap-2.4.6-89.el7.centos.1
    mod_proxy_html-2.4.6-89.el7.centos.1
    mod_session-2.4.6-89.el7.centos.1
    mod_ssl-2.4.6-89.el7.centos.1
    mutter-3.28.3-8.el7_6 [B]
    mutter-devel-3.28.3-8.el7_6 [B]
    net-snmp-5.7.2-38.el7_6.2 [B]
    net-snmp-agent-libs-5.7.2-38.el7_6.2 [B]
    net-snmp-devel-5.7.2-38.el7_6.2 [B]
    net-snmp-gui-5.7.2-38.el7_6.2 [B]
    net-snmp-libs-5.7.2-38.el7_6.2 [B]
    net-snmp-perl-5.7.2-38.el7_6.2 [B]
    net-snmp-python-5.7.2-38.el7_6.2 [B]
    net-snmp-sysvinit-5.7.2-38.el7_6.2 [B]
    net-snmp-utils-5.7.2-38.el7_6.2 [B]
    pcs-0.9.165-6.el7.centos.2
    pcs-snmp-0.9.165-6.el7.centos.2
    perf-3.10.0-957.27.2.el7 [S]
    python2-ipaclient-4.6.4-10.el7.centos.6
    python2-ipalib-4.6.4-10.el7.centos.6
    python2-ipaserver-4.6.4-10.el7.centos.6
    python-perf-3.10.0-957.27.2.el7 [S]
    qemu-img-1.5.3-160.el7_6.3 [S]
    qemu-kvm-1.5.3-160.el7_6.3 [S]
    qemu-kvm-common-1.5.3-160.el7_6.3 [S]
    qemu-kvm-tools-1.5.3-160.el7_6.3 [S]
    rear-2.4-5.el7_6 [B]
    resource-agents-4.1.1-12.el7_6.19
    resource-agents-aliyun-4.1.1-12.el7_6.19
    resource-agents-gcp-4.1.1-12.el7_6.19
    samba-4.8.3-6.el7_6 [B]
    samba-client-4.8.3-6.el7_6 [B]
    samba-client-libs-4.8.3-6.el7_6 [B]
    samba-common-4.8.3-6.el7_6 [B]
    samba-common-libs-4.8.3-6.el7_6 [B]
    samba-common-tools-4.8.3-6.el7_6 [B]
    samba-dc-4.8.3-6.el7_6 [B]
    samba-dc-libs-4.8.3-6.el7_6 [B]
    samba-devel-4.8.3-6.el7_6 [B]
    samba-krb5-printing-4.8.3-6.el7_6 [B]
    samba-libs-4.8.3-6.el7_6 [B]
    samba-pidl-4.8.3-6.el7_6 [B]
    samba-python-4.8.3-6.el7_6 [B]
    samba-python-test-4.8.3-6.el7_6 [B]
    samba-test-4.8.3-6.el7_6 [B]
    samba-test-libs-4.8.3-6.el7_6 [B]
    samba-vfs-glusterfs-4.8.3-6.el7_6 [B]
    samba-winbind-4.8.3-6.el7_6 [B]
    samba-winbind-clients-4.8.3-6.el7_6 [B]
    samba-winbind-krb5-locator-4.8.3-6.el7_6 [B]
    samba-winbind-modules-4.8.3-6.el7_6 [B]
    selinux-policy-3.13.1-229.el7_6.15 [B]
    selinux-policy-devel-3.13.1-229.el7_6.15 [B]
    selinux-policy-doc-3.13.1-229.el7_6.15 [B]
    selinux-policy-minimum-3.13.1-229.el7_6.15 [B]
    selinux-policy-mls-3.13.1-229.el7_6.15 [B]
    selinux-policy-sandbox-3.13.1-229.el7_6.15 [B]
    selinux-policy-targeted-3.13.1-229.el7_6.15 [B]
    sos-3.6-19.el7.centos
    sysstat-10.1.5-17.el7_6.1 [B]
    systemd-219-62.el7_6.9 [B]
    systemd-devel-219-62.el7_6.9 [B]
    systemd-journal-gateway-219-62.el7_6.9 [B]
    systemd-libs-219-62.el7_6.9 [B]
    systemd-networkd-219-62.el7_6.9 [B]
    systemd-python-219-62.el7_6.9 [B]
    systemd-resolved-219-62.el7_6.9 [B]
    systemd-sysv-219-62.el7_6.9 [B]
    tuned-2.10.0-6.el7_6.4 [B]
    tuned-gtk-2.10.0-6.el7_6.4 [B]
    tuned-profiles-atomic-2.10.0-6.el7_6.4 [B]
    tuned-profiles-compat-2.10.0-6.el7_6.4 [B]
    tuned-profiles-cpu-partitioning-2.10.0-6.el7_6.4 [B]
    tuned-profiles-mssql-2.10.0-6.el7_6.4 [B]
    tuned-profiles-oracle-2.10.0-6.el7_6.4 [B]
    tuned-utils-2.10.0-6.el7_6.4 [B]
    tuned-utils-systemtap-2.10.0-6.el7_6.4 [B]
    xorg-x11-server-common-1.20.1-5.6.el7_6 [B]
    xorg-x11-server-devel-1.20.1-5.6.el7_6 [B]
    xorg-x11-server-source-1.20.1-5.6.el7_6 [B]
    xorg-x11-server-Xdmx-1.20.1-5.6.el7_6 [B]
    xorg-x11-server-Xephyr-1.20.1-5.6.el7_6 [B]
    xorg-x11-server-Xnest-1.20.1-5.6.el7_6 [B]
    xorg-x11-server-Xorg-1.20.1-5.6.el7_6 [B]
    xorg-x11-server-Xvfb-1.20.1-5.6.el7_6 [B]
    xorg-x11-server-Xwayland-1.20.1-5.6.el7_6 [B]

VIRT repository:
    kernel-azure-3.10.0-957.27.2.el7.azure
    kernel-azure-debug-3.10.0-957.27.2.el7.azure
    kernel-azure-debug-devel-3.10.0-957.27.2.el7.azure
    kernel-azure-devel-3.10.0-957.27.2.el7.azure
    kernel-azure-headers-3.10.0-957.27.2.el7.azure
    qemu-img-ev-2.12.0-18.el7_6.7.1
    qemu-kvm-common-ev-2.12.0-18.el7_6.7.1
    qemu-kvm-ev-2.12.0-18.el7_6.7.1
    qemu-kvm-tools-ev-2.12.0-18.el7_6.7.1


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

This update can also be applied before nightly automated
 update run, by running as root on your machine:

# /usr/bin/yum -y update

Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Daniel Abad for Linux.Support@cern.ch
 