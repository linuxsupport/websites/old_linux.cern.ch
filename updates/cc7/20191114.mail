Dear Linux users,

Tonight's CERN CentOS Linux 7 (CC 7X) system update
will contain following packages:


CERN repository:
    cern-linuxsupport-access-1.0-2.el7.cern

CLOUD repository:
    ansible-2.8.6-1.el7
    ndisc6-1.0.3-9.el7
    openstack-heat-api-11.0.2-1.el7
    openstack-heat-api-cfn-11.0.2-1.el7
    openstack-heat-common-11.0.2-1.el7
    openstack-heat-engine-11.0.2-1.el7
    openstack-heat-monolith-11.0.2-1.el7
    openstack-keystone-13.0.4-1.el7
    openstack-keystone-doc-13.0.4-1.el7
    openstack-kolla-9.0.0-0.1.0rc1.el7
    openstack-magnum-api-9.1.0-1.el7
    openstack-magnum-common-9.1.0-1.el7
    openstack-magnum-conductor-9.1.0-1.el7
    openstack-magnum-doc-9.1.0-1.el7
    python2-keystoneauth1-3.4.1-1.el7
    python2-keystoneclient-3.15.1-1.el7
    python2-keystoneclient-tests-3.15.1-1.el7
    python2-keystonemiddleware-4.22.0-1.el7
    python2-magnum-9.1.0-1.el7
    python2-magnum-tests-9.1.0-1.el7
    python2-novaclient-10.1.1-1.el7
    python2-openstackclient-3.16.3-1.el7
    python2-oslo-utils-3.41.2-1.el7
    python2-oslo-utils-tests-3.41.2-1.el7
    python2-os-vif-1.9.2-1.el7
    python2-os-vif-tests-1.9.2-1.el7
    python-heat-tests-11.0.2-1.el7
    python-keystone-13.0.4-1.el7
    python-keystoneauth1-doc-3.4.1-1.el7
    python-keystoneclient-doc-3.15.1-1.el7
    python-keystonemiddleware-doc-4.22.0-1.el7
    python-keystone-tests-13.0.4-1.el7
    python-novaclient-doc-10.1.1-1.el7
    python-openstackclient-doc-3.16.3-1.el7
    python-openstackclient-lang-3.16.3-1.el7
    python-oslo-utils-doc-3.41.2-1.el7
    python-oslo-utils-lang-3.41.2-1.el7
    python-os-vif-doc-1.9.2-1.el7

RT repository:
    kernel-rt-3.10.0-1062.4.2.rt56.1028.el7
    kernel-rt-debug-3.10.0-1062.4.2.rt56.1028.el7
    kernel-rt-debug-devel-3.10.0-1062.4.2.rt56.1028.el7
    kernel-rt-debug-kvm-3.10.0-1062.4.2.rt56.1028.el7
    kernel-rt-devel-3.10.0-1062.4.2.rt56.1028.el7
    kernel-rt-doc-3.10.0-1062.4.2.rt56.1028.el7
    kernel-rt-kvm-3.10.0-1062.4.2.rt56.1028.el7
    kernel-rt-trace-3.10.0-1062.4.2.rt56.1028.el7
    kernel-rt-trace-devel-3.10.0-1062.4.2.rt56.1028.el7
    kernel-rt-trace-kvm-3.10.0-1062.4.2.rt56.1028.el7

STORAGE repository:
    ceph-ansible-3.2.30.1-1.el7

VIRT repository:
    ansible-2.9.0-2.el7
    ansible-doc-2.9.0-2.el7
    xen-4.10.4-2.el7
    xen-4.12.1.65.g278e46ae8f-1.el7
    xen-4.8.5.48.gc67210f60d-1.el7
    xen-devel-4.10.4-2.el7
    xen-devel-4.12.1.65.g278e46ae8f-1.el7
    xen-devel-4.8.5.48.gc67210f60d-1.el7
    xen-doc-4.10.4-2.el7
    xen-doc-4.12.1.65.g278e46ae8f-1.el7
    xen-doc-4.8.5.48.gc67210f60d-1.el7
    xen-hypervisor-4.10.4-2.el7
    xen-hypervisor-4.12.1.65.g278e46ae8f-1.el7
    xen-hypervisor-4.8.5.48.gc67210f60d-1.el7
    xen-libs-4.10.4-2.el7
    xen-libs-4.12.1.65.g278e46ae8f-1.el7
    xen-libs-4.8.5.48.gc67210f60d-1.el7
    xen-licenses-4.10.4-2.el7
    xen-licenses-4.12.1.65.g278e46ae8f-1.el7
    xen-licenses-4.8.5.48.gc67210f60d-1.el7
    xen-livepatch-build-tools-4.10.4-2.el7
    xen-livepatch-build-tools-4.12.1.65.g278e46ae8f-1.el7
    xen-livepatch-build-tools-4.8.5.48.gc67210f60d-1.el7
    xen-ocaml-4.10.4-2.el7
    xen-ocaml-4.12.1.65.g278e46ae8f-1.el7
    xen-ocaml-4.8.5.48.gc67210f60d-1.el7
    xen-ocaml-devel-4.10.4-2.el7
    xen-ocaml-devel-4.12.1.65.g278e46ae8f-1.el7
    xen-ocaml-devel-4.8.5.48.gc67210f60d-1.el7
    xen-runtime-4.10.4-2.el7
    xen-runtime-4.12.1.65.g278e46ae8f-1.el7
    xen-runtime-4.8.5.48.gc67210f60d-1.el7


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

This update can also be applied before nightly automated
 update run, by running as root on your machine:

# /usr/bin/yum -y update

Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Daniel Juarez for Linux.Support@cern.ch
 