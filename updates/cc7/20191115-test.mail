Dear Linux users,

Tonight's CERN CentOS Linux 7 (CC 7X) TEST system update
will contain following packages:

 *******************************************************
 *** NOTE:                                           ***
 *** Packages listed below are provided for TESTS    ***
 *** ONLY, DO NOT USE THESE ON PRODUCTION SYSTEMS    ***
 *** these may NEVER be released to regular updates  ***
 *** and may be broken, requiring manual fixes on    ***
 *** on your system.                                 ***
 *******************************************************

CENTOSPLUS repository:
    bpftool-3.10.0-1062.4.3.el7.centos.plus
    kernel-plus-3.10.0-1062.4.3.el7.centos.plus
    kernel-plus-abi-whitelists-3.10.0-1062.4.3.el7.centos.plus
    kernel-plus-devel-3.10.0-1062.4.3.el7.centos.plus
    kernel-plus-doc-3.10.0-1062.4.3.el7.centos.plus
    kernel-plus-headers-3.10.0-1062.4.3.el7.centos.plus
    kernel-plus-tools-3.10.0-1062.4.3.el7.centos.plus
    kernel-plus-tools-libs-3.10.0-1062.4.3.el7.centos.plus
    kernel-plus-tools-libs-devel-3.10.0-1062.4.3.el7.centos.plus
    kmod-openafs-1.6.22.3-1.3.10.0_1062.4.3.el7.centos.plus
    perf-3.10.0-1062.4.3.el7.centos.plus
    python-perf-3.10.0-1062.4.3.el7.centos.plus

CERN repository:
    kmod-openafs-1.6.22.3-1.3.10.0_1062.4.2.el7
    kmod-openafs-1.6.22.3-1.3.10.0_1062.4.3.el7

CLOUD repository:
    openstack-manila-7.4.0-1.el7
    openstack-manila-8.1.0-1.el7
    openstack-manila-9.1.0-1.el7
    openstack-manila-doc-7.4.0-1.el7
    openstack-manila-doc-8.1.0-1.el7
    openstack-manila-doc-9.1.0-1.el7
    openstack-manila-share-7.4.0-1.el7
    openstack-manila-share-8.1.0-1.el7
    openstack-manila-share-9.1.0-1.el7
    openstack-nova-20.0.1-1.el7
    openstack-nova-api-20.0.1-1.el7
    openstack-nova-common-20.0.1-1.el7
    openstack-nova-compute-20.0.1-1.el7
    openstack-nova-conductor-20.0.1-1.el7
    openstack-nova-console-20.0.1-1.el7
    openstack-nova-migration-20.0.1-1.el7
    openstack-nova-network-20.0.1-1.el7
    openstack-nova-novncproxy-20.0.1-1.el7
    openstack-nova-scheduler-20.0.1-1.el7
    openstack-nova-serialproxy-20.0.1-1.el7
    openstack-nova-spicehtml5proxy-20.0.1-1.el7
    python2-manila-8.1.0-1.el7
    python2-manila-9.1.0-1.el7
    python2-manila-tests-8.1.0-1.el7
    python2-manila-tests-9.1.0-1.el7
    python2-nova-20.0.1-1.el7
    python2-nova-tests-20.0.1-1.el7
    python-manila-7.4.0-1.el7
    python-manila-tests-7.4.0-1.el7

RT repository:
    kernel-rt-3.10.0-1062.4.3.rt56.1029.el7
    kernel-rt-debug-3.10.0-1062.4.3.rt56.1029.el7
    kernel-rt-debug-devel-3.10.0-1062.4.3.rt56.1029.el7
    kernel-rt-debug-kvm-3.10.0-1062.4.3.rt56.1029.el7
    kernel-rt-devel-3.10.0-1062.4.3.rt56.1029.el7
    kernel-rt-doc-3.10.0-1062.4.3.rt56.1029.el7
    kernel-rt-kvm-3.10.0-1062.4.3.rt56.1029.el7
    kernel-rt-trace-3.10.0-1062.4.3.rt56.1029.el7
    kernel-rt-trace-devel-3.10.0-1062.4.3.rt56.1029.el7
    kernel-rt-trace-kvm-3.10.0-1062.4.3.rt56.1029.el7

UPDATES repository:
    bpftool-3.10.0-1062.4.2.el7
    bpftool-3.10.0-1062.4.3.el7
    kernel-3.10.0-1062.4.2.el7
    kernel-3.10.0-1062.4.3.el7
    kernel-abi-whitelists-3.10.0-1062.4.2.el7
    kernel-abi-whitelists-3.10.0-1062.4.3.el7
    kernel-debug-3.10.0-1062.4.2.el7
    kernel-debug-3.10.0-1062.4.3.el7
    kernel-debug-devel-3.10.0-1062.4.2.el7
    kernel-debug-devel-3.10.0-1062.4.3.el7
    kernel-devel-3.10.0-1062.4.2.el7
    kernel-devel-3.10.0-1062.4.3.el7
    kernel-doc-3.10.0-1062.4.2.el7
    kernel-doc-3.10.0-1062.4.3.el7
    kernel-headers-3.10.0-1062.4.2.el7
    kernel-headers-3.10.0-1062.4.3.el7
    kernel-tools-3.10.0-1062.4.2.el7
    kernel-tools-3.10.0-1062.4.3.el7
    kernel-tools-libs-3.10.0-1062.4.2.el7
    kernel-tools-libs-3.10.0-1062.4.3.el7
    kernel-tools-libs-devel-3.10.0-1062.4.2.el7
    kernel-tools-libs-devel-3.10.0-1062.4.3.el7
    microcode_ctl-2.1-53.3.el7_7
    perf-3.10.0-1062.4.2.el7
    perf-3.10.0-1062.4.3.el7
    python-perf-3.10.0-1062.4.2.el7
    python-perf-3.10.0-1062.4.3.el7

VIRT repository:
    qemu-img-ev-2.12.0-33.1.el7_7.4
    qemu-kvm-common-ev-2.12.0-33.1.el7_7.4
    qemu-kvm-ev-2.12.0-33.1.el7_7.4
    qemu-kvm-tools-ev-2.12.0-33.1.el7_7.4


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

 This TEST update can be applied on your system
 (providing it is set up to receive test updates
 according to: http://cern.ch/linux/updates/cc7.shtml),
 by running as root on your machine:

 # /usr/bin/yum -y update

 Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Daniel Juarez for Linux.Support@cern.ch
