Dear Linux users,

Tonight's CERN CentOS Linux 7 (CC 7X) TEST system update
will contain following packages:

 *******************************************************
 *** NOTE:                                           ***
 *** Packages listed below are provided for TESTS    ***
 *** ONLY, DO NOT USE THESE ON PRODUCTION SYSTEMS    ***
 *** these may NEVER be released to regular updates  ***
 *** and may be broken, requiring manual fixes on    ***
 *** on your system.                                 ***
 *******************************************************

EXTRAS repository:
    containernetworking-plugins-0.8.1-4.el7.centos
    docker-1.13.1-109.gitcccb291.el7.centos
    docker-client-1.13.1-109.gitcccb291.el7.centos
    docker-common-1.13.1-109.gitcccb291.el7.centos
    docker-logrotate-1.13.1-109.gitcccb291.el7.centos
    docker-lvm-plugin-1.13.1-109.gitcccb291.el7.centos
    docker-novolume-plugin-1.13.1-109.gitcccb291.el7.centos
    docker-v1.10-migrator-1.13.1-109.gitcccb291.el7.centos
    python-docker-py-1.10.6-11.el7
    python-docker-pycreds-0.3.0-11.el7
    rhel-system-roles-1.0-10.el7_7
    WALinuxAgent-2.2.38-2.el7_7

STORAGE repository:
    ceph-14.2.7-0.el7
    ceph-base-14.2.7-0.el7
    ceph-common-14.2.7-0.el7
    cephfs-java-14.2.7-0.el7
    ceph-fuse-14.2.7-0.el7
    ceph-grafana-dashboards-14.2.7-0.el7
    ceph-mds-14.2.7-0.el7
    ceph-mgr-14.2.7-0.el7
    ceph-mgr-dashboard-14.2.7-0.el7
    ceph-mgr-diskprediction-cloud-14.2.7-0.el7
    ceph-mgr-diskprediction-local-14.2.7-0.el7
    ceph-mgr-k8sevents-14.2.7-0.el7
    ceph-mgr-rook-14.2.7-0.el7
    ceph-mgr-ssh-14.2.7-0.el7
    ceph-mon-14.2.7-0.el7
    ceph-osd-14.2.7-0.el7
    ceph-radosgw-14.2.7-0.el7
    ceph-resource-agents-14.2.7-0.el7
    ceph-selinux-14.2.7-0.el7
    ceph-test-14.2.7-0.el7
    libcephfs2-14.2.7-0.el7
    libcephfs-devel-14.2.7-0.el7
    libcephfs_jni1-14.2.7-0.el7
    libcephfs_jni-devel-14.2.7-0.el7
    librados2-14.2.7-0.el7
    librados-devel-14.2.7-0.el7
    libradospp-devel-14.2.7-0.el7
    libradosstriper1-14.2.7-0.el7
    libradosstriper-devel-14.2.7-0.el7
    librbd1-14.2.7-0.el7
    librbd-devel-14.2.7-0.el7
    librgw2-14.2.7-0.el7
    librgw-devel-14.2.7-0.el7
    python-ceph-argparse-14.2.7-0.el7
    python-ceph-compat-14.2.7-0.el7
    python-cephfs-14.2.7-0.el7
    python-rados-14.2.7-0.el7
    python-rbd-14.2.7-0.el7
    python-rgw-14.2.7-0.el7
    rados-objclass-devel-14.2.7-0.el7
    rbd-fuse-14.2.7-0.el7
    rbd-mirror-14.2.7-0.el7
    rbd-nbd-14.2.7-0.el7

UPDATES repository:
    http-parser-2.7.1-8.el7_7.2
    http-parser-devel-2.7.1-8.el7_7.2
    xerces-c-3.1.1-10.el7_7
    xerces-c-devel-3.1.1-10.el7_7
    xerces-c-doc-3.1.1-10.el7_7


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

 This TEST update can be applied on your system
 (providing it is set up to receive test updates
 according to: http://cern.ch/linux/updates/cc7.shtml),
 by running as root on your machine:

 # /usr/bin/yum -y update

 Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Julien Rische for Linux.Support@cern.ch
