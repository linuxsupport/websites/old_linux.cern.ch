Dear Linux users.

Tonight Scientific Linux CERN 5 (SLC 5X) TEST system update
will contain following packages:

 *******************************************************
 *** NOTE:                                           ***
 *** Packages listed below are provided for TESTS    ***
 *** ONLY, DO NOT USE THESE ON PRODUCTION SYSTEMS    ***
 *** these may NEVER be released to regular updates  ***
 *** and may be broken, requiring manual fixes on    ***
 *** on your system.                                 ***
 *******************************************************

DEVTOOLSET repository:
    devtoolset-2-2.1-3.el5
    devtoolset-2-binutils-2.23.52.0.1-10.el5
    devtoolset-2-binutils-devel-2.23.52.0.1-10.el5
    devtoolset-2-build-2.1-3.el5
    devtoolset-2-dyninst-8.0-6dw.el5
    devtoolset-2-dyninst-devel-8.0-6dw.el5
    devtoolset-2-dyninst-doc-8.0-6dw.el5
    devtoolset-2-dyninst-static-8.0-6dw.el5
    devtoolset-2-dyninst-testsuite-8.0-6dw.el5
    devtoolset-2-elfutils-0.157-2.el5
    devtoolset-2-elfutils-devel-0.157-2.el5
    devtoolset-2-elfutils-libelf-0.157-2.el5
    devtoolset-2-elfutils-libelf-devel-0.157-2.el5
    devtoolset-2-elfutils-libs-0.157-2.el5
    devtoolset-2-gcc-4.8.2-15.el5
    devtoolset-2-gcc-c++-4.8.2-15.el5
    devtoolset-2-gcc-gfortran-4.8.2-15.el5
    devtoolset-2-gcc-plugin-devel-4.8.2-15.el5
    devtoolset-2-gdb-7.6.1-47.el5
    devtoolset-2-gdb-doc-7.6.1-47.el5
    devtoolset-2-gdb-gdbserver-7.6.1-47.el5
    devtoolset-2-libasan-devel-4.8.2-15.el5
    devtoolset-2-libatomic-devel-4.8.2-15.el5
    devtoolset-2-libitm-devel-4.8.2-15.el5
    devtoolset-2-libquadmath-devel-4.8.2-15.el5
    devtoolset-2-libstdc++-devel-4.8.2-15.el5
    devtoolset-2-libstdc++-docs-4.8.2-15.el5
    devtoolset-2-libtsan-devel-4.8.2-15.el5
    devtoolset-2-oprofile-0.9.8-6.el5
    devtoolset-2-oprofile-devel-0.9.8-6.el5
    devtoolset-2-oprofile-gui-0.9.8-6.el5
    devtoolset-2-oprofile-jit-0.9.8-6.el5
    devtoolset-2-perftools-2.1-3.el5
    devtoolset-2-runtime-2.1-3.el5
    devtoolset-2-strace-4.7-13.el5
    devtoolset-2-toolchain-2.1-3.el5
    devtoolset-2-valgrind-3.8.1-30.8.el5
    devtoolset-2-valgrind-devel-3.8.1-30.8.el5
    devtoolset-2-valgrind-openmpi-3.8.1-30.8.el5
    libasan-4.8.2-15.el5
    libatomic-4.8.2-15.el5
    libitm-4.8.2-15.el5
    libtsan-4.8.2-15.el5
    scl-utils-20120927-9.el5
    scl-utils-build-20120927-9.el5

EXTRAS repository:
    flash-plugin-11.2.202.346-1.slc5
    kernel-module-fglrx-2.6.18-371.6.1.el5-10.2-2.cern
    kernel-module-fglrx-2.6.18-371.6.1.el5PAE-10.2-2.cern
    kernel-module-ixgbe-2.6.18-371.6.1.el5-2.0.44-silicom.2.slc5
    kernel-module-nvidia-173xx-2.6.18-371.6.1.el5-173.14.31-1.cern
    kernel-module-nvidia-173xx-2.6.18-371.6.1.el5PAE-173.14.31-1.cern
    kernel-module-nvidia-2.6.18-371.6.1.el5-195.36.15-1.cern
    kernel-module-nvidia-2.6.18-371.6.1.el5PAE-195.36.15-1.cern
    kernel-module-nvidia-96xx-2.6.18-371.6.1.el5-96.43.20-1.cern
    kernel-module-nvidia-96xx-2.6.18-371.6.1.el5PAE-96.43.20-1.cern
    kernel-module-scst-2.6.18-371.6.1.el5-1.0.1.1-3.cern

UPDATES repository:
    kernel-2.6.18-371.6.1.el5 [S]
    kernel-debug-2.6.18-371.6.1.el5 [S]
    kernel-debug-devel-2.6.18-371.6.1.el5 [S]
    kernel-devel-2.6.18-371.6.1.el5 [S]
    kernel-doc-2.6.18-371.6.1.el5 [S]
    kernel-headers-2.6.18-371.6.1.el5 [S]
    kernel-module-ndiswrapper-2.6.18-371.6.1.el5-1.53-1.SL
    kernel-module-openafs-2.6.18-371.6.1.el5-1.4.15-0.cern
    kernel-module-openafs-2.6.18-371.6.1.el5PAE-1.4.15-0.cern
    kernel-module-openafs-2.6.18-371.6.1.el5xen-1.4.15-0.cern
    kernel-module-xfs-2.6.18-371.6.1.el5-1-2.slc5
    kernel-PAE-2.6.18-371.6.1.el5 [S]
    kernel-PAE-devel-2.6.18-371.6.1.el5 [S]
    kernel-xen-2.6.18-371.6.1.el5 [S]
    kernel-xen-devel-2.6.18-371.6.1.el5 [S]


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

 This TEST update can be applied on your system
 (providing it is set up to receive test updates
 according to: http://cern.ch/linux/updates/slc5.shtml),
 by running as root on your machine:

 # /usr/bin/yum -y update

 Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Thomas Oulevey for Linux.Support@cern.ch
