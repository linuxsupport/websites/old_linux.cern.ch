<!--#include virtual="/linux/layout/header.shtml" -->
<script>setTitle('SLC6 software repositories');</script>

<h2>Scientific Linux CERN 6 software repositories</h2>


<hr>
<h3>System software repository</h3>
Access: Internal CERN and External Network.<br>
Support: <em>SUPPORTED</em> by <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a><br>
Location:<br>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/">http://linuxsoft.cern.ch/cern/slc6X/</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/i386/yum/os/repoview/">[i386]</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/os/repoview/">[x86_64]</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/updates/">http://linuxsoft.cern.ch/cern/slc6X/updates/</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/i386/yum/updates/repoview/">[i386]</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/updates/repoview/">[x86_64]</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/extras/">http://linuxsoft.cern.ch/cern/slc6X/extras/</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/i386/yum/extras/repoview/">[i386]</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/extras/repoview/">[x86_64]</a>
Configuration:
<pre>
/etc/yum.repos.d/slc6-os.repo
/etc/yum.repos.d/slc6-updates.repo
/etc/yum.repos.d/slc6-extras.repo
</pre>
<em>DO NOT DISABLE</em> in your configuration. (system updates are coming from this repository).
Configured in /etc/yum.repos.d/slc6*.repo to take precedence over additional
repositories listed below.


<hr>
<h3>Non-freely distributable software repository</h3>
Access: Internal CERN Network.<br>
Support: <em>SUPPORTED</em> by <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a><br>
Location:<br>
<a href="http://linuxsoft.cern.ch/onlycern/slc6X/">http://linuxsoft.cern.ch/onlcern/slc6X/</a>
<a href="http://linuxsoft.cern.ch/onlycern/slc6X/i386/yum/cernonly/repoview/">[i386]</a>
<a href="http://linuxsoft.cern.ch/onlycern/slc6X/x86_64/yum/cernonly/repoview/">[x86_64]</a>
Configuration:
<pre>
/etc/yum.repos.d/slc6-cernonly.repo
</pre>
Not enabled in default configuration. Contains ~ 20 packages.
This repository contains various packages that cannot be freely distributed 
outside of CERN due to license constraints (as Oracle Instant Client or Sun Java) and also 
some packages which depend on them (as php-oci8).

<hr>
<h3>Testing repository</h3>
Access: Internal CERN and External Network.<br>
Support: <em>SUPPORTED</em> by <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a><br>
Location:<br>
<a href="http://linuxsoft.cern.ch/cern/slc6X/updates/testing/">http://linuxsoft.cern.ch/cern/slc6X/updates/testing/</a>
<a href="http://linuxsoft.cern.ch/cern/slc6X/i386/yum/testing/repoview/">[i386]</a>
<a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/testing/repoview/">[x86_64]</a>
Configuration:
<pre>
/etc/yum.repos.d/slc6-testing.repo
</pre>
Not enabled in default configuration.<em>NOT RECOMMENDED</em> for production systems.This repository contains
packages that are expected to be released as updates in the next few days. Your help in
validating that these function properly and do not break your environment is appreciated.Please subscribe to
<a href="https://mmm.cern.ch/public/archive-list/l/linux-announce-test/">linux-announce-test@cern.ch</a>
if you use this repository.</br>
<b>Warning:</b> Packages from this repository may occasionally be broken and may
require manual intervention.


<hr>
<h3>Additional software repository: EPEL</h3>
Access: Internal CERN and External Network.<br>
Support: <em>SUPPORTED</em> by <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a><br>
Location:<br>
<a href="http://linuxsoft.cern.ch/epel/">http://linuxsoft.cern.ch/epel/</a>
<a href="http://linuxsoft.cern.ch/epel/beta/6/i386/repoview/">[i386]</a>
<a href="http://linuxsoft.cern.ch/epel/beta/6/x86_64/repoview/">[x86_64]</a><br>
Configuration:
<pre>
/etc/yum.repos.d/epel.repo
</pre>
Enabled in default configuration. This is a mirror of Extra Packages for Enterprise Linux (EPEL) repository
(see: <a href="http://fedoraproject.org/wiki/EPEL">EPEL</a>)


<hr>
<h3>Additional software repository: RPM Forge</h3>
Access: Internal CERN and External Network.<br>
Support: <em>NOT SUPPORTED</em> provided <em>AS-IS</em>.<br>
Location:<br>
<a href="http://linuxsoft.cern.ch/rpmforge/redhat/el6/">http://linuxsoft.cern.ch/rpmforge/redhat/el6/</a>
<a href="http://linuxsoft.cern.ch/rpmforge/redhat/el6/en/i386/rpmforge/">[i386]</a>
<a href="http://linuxsoft.cern.ch/rpmforge/redhat/el6/en/x86_64/rpmforge/">[x86_64]</a><br>
Configuration:
<pre>
/etc/yum.repos.d/rpmforge.repo
</pre>
This is a nightly mirror of <a href="http://rpmforge.net">http://rpmforge.net</a>
repository of additional software compatible with Red Hat Enterprise Linux 6 <br>
(therefore also with SLC6).Contains ~ 1400 packages.


<hr>
<h3>Additional software repository: EL Repo</h3>
Access: Internal CERN and External Network.<br>
Support: <em>NOT SUPPORTED</em> provided <em>AS-IS</em>.<br>
Location:<br>
<a href="http://linuxsoft.cern.ch/elrepo/elrepo/el6/">http://linuxsoft.cern.ch/elrepo/elrepo/el6/</a><br>
<a href="http://linuxsoft.cern.ch/elrepo/elrepo/el6/i386/">[i386]</a>
<a href="http://linuxsoft.cern.ch/elrepo/elrepo/el6/x86_64/">[x86_64]</a>
<br>
Configuration:
<pre>
/etc/yum.repos.d/elrepo.repo
</pre>
This is a nightly mirror of <a href="http://elrepo.org">http://elrepo.org</a>
repository of additional software compatible with Red Hat Enterprise Linux 6 <br>
(therefore also with SLC6).
<hr>

<h3>Additional software repository: Linuxtech</h3>
Access: Internal CERN and External Network.<br>
Support: <em>NOT SUPPORTED</em> provided <em>AS-IS</em>.<br>
Location:<br>
<a href="http://linuxsoft.cern.ch/linuxtech/el6/">http://linuxsoft.cern.ch/linuxtech/el6/</a><br>
<br>
Configuration:
<br>
Download <a href="linuxtech.repo">linuxtech.repo</a> and save it as <pre>/etc/yum.repos.d/linuxtech.repo</pre>

This is a nightly mirror of <a href="http://pkgrepo.linuxtech.net/el6">http://pkgrepo.linuxtech.net/el6</a>
repository of additional software compatible with Red Hat Enterprise Linux 6 <br>
(therefore also with SLC6).<br>

<em>Note</em>: This repository is disabled by default since it contains few packages not compatible
with other (including system) repositories. In order to install <b>VLC</b> from this repository, please execute:
<pre>
# yum --enablerepo=linuxtech-release --disableplugin=protectbase --disableplugin=priorities install vlc
</pre>
<hr>


<!--#include virtual="/linux/layout/footer.shtml" -->
</body>
</html>
