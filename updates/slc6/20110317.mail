Dear Linux users.

Tonight Scientific Linux CERN 6 (SLC 6X) system update
will contain following packages:

compat-dapl-1.2.15-2.1.el6_0.1
compat-dapl-devel-1.2.15-2.1.el6_0.1
compat-dapl-static-1.2.15-2.1.el6_0.1
compat-dapl-utils-1.2.15-2.1.el6_0.1
compat-openldap-2.4.19_2.3.43-15.el6_0.2 [S]
dapl-2.0.25-5.el6_0.2
dapl-devel-2.0.25-5.el6_0.2
dapl-static-2.0.25-5.el6_0.2
dapl-utils-2.0.25-5.el6_0.2
glibc-2.12-1.7.el6_0.4 [B]
glibc-common-2.12-1.7.el6_0.4 [B]
glibc-devel-2.12-1.7.el6_0.4 [B]
glibc-headers-2.12-1.7.el6_0.4 [B]
glibc-static-2.12-1.7.el6_0.4 [B]
glibc-utils-2.12-1.7.el6_0.4 [B]
kernel-2.6.32-71.18.2.el6 [S]
kernel-debug-2.6.32-71.18.2.el6 [S]
kernel-debug-devel-2.6.32-71.18.2.el6 [S]
kernel-devel-2.6.32-71.18.2.el6 [S]
kernel-doc-2.6.32-71.18.2.el6 [S]
kernel-firmware-2.6.32-71.18.2.el6 [S]
kernel-headers-2.6.32-71.18.2.el6 [S]
kernel-module-openafs-2.6.32-71.18.2.el6-1.4.14-0.cern.slc6
libnl-1.1-12.el6_0.1 [B]
libnl-devel-1.1-12.el6_0.1 [B]
logwatch-7.3.6-49.el6 [S]
mod_dav_svn-1.6.11-2.el6_0.3 [S]
nscd-2.12-1.7.el6_0.4 [B]
ocsinventory-agent-1.1.2.1-6.slc6
oddjob-0.30-5.el6 [B]
oddjob-mkhomedir-0.30-5.el6 [B]
openldap-2.4.19-15.el6_0.2 [S]
openldap-clients-2.4.19-15.el6_0.2 [S]
openldap-devel-2.4.19-15.el6_0.2 [S]
openldap-servers-2.4.19-15.el6_0.2 [S]
openldap-servers-sql-2.4.19-15.el6_0.2 [S]
perf-2.6.32-71.18.2.el6 [S]
qemu-img-0.12.1.2-2.113.el6_0.8
qemu-kvm-0.12.1.2-2.113.el6_0.8
qemu-kvm-tools-0.12.1.2-2.113.el6_0.8
scsi-target-utils-1.0.4-3.el6_0.1 [S]
strace-4.5.19-1.10.el6 [B]
subversion-1.6.11-2.el6_0.3 [S]
subversion-devel-1.6.11-2.el6_0.3 [S]
subversion-gnome-1.6.11-2.el6_0.3 [S]
subversion-javahl-1.6.11-2.el6_0.3 [S]
subversion-kde-1.6.11-2.el6_0.3 [S]
subversion-perl-1.6.11-2.el6_0.3 [S]
subversion-ruby-1.6.11-2.el6_0.3 [S]
subversion-svn2cl-1.6.11-2.el6_0.3 [S]
thunderbird-lightning-exchange-provider-0.12-1.slc6
tomcat6-6.0.24-24.el6_0 [S]
tomcat6-admin-webapps-6.0.24-24.el6_0 [S]
tomcat6-docs-webapp-6.0.24-24.el6_0 [S]
tomcat6-el-2.1-api-6.0.24-24.el6_0 [S]
tomcat6-javadoc-6.0.24-24.el6_0 [S]
tomcat6-jsp-2.1-api-6.0.24-24.el6_0 [S]
tomcat6-lib-6.0.24-24.el6_0 [S]
tomcat6-log4j-6.0.24-24.el6_0 [S]
tomcat6-servlet-2.5-api-6.0.24-24.el6_0 [S]
tomcat6-webapps-6.0.24-24.el6_0 [S]
tzdata-2011b-1.el6 [E]
tzdata-java-2011b-1.el6 [E]
vsftpd-2.2.2-6.el6_0.1 [S]
wacomcpl-0.9.0-1.el6_0.2 [B]
xorg-x11-drv-wacom-0.10.5-6.el6_0.2 [B]
xorg-x11-drv-wacom-devel-0.10.5-6.el6_0.2 [B]
xorg-x11-server-common-1.7.7-26.el6_0.3 [B]
xorg-x11-server-devel-1.7.7-26.el6_0.3 [B]
xorg-x11-server-source-1.7.7-26.el6_0.3 [B]
xorg-x11-server-Xdmx-1.7.7-26.el6_0.3 [B]
xorg-x11-server-Xephyr-1.7.7-26.el6_0.3 [B]
xorg-x11-server-Xnest-1.7.7-26.el6_0.3 [B]
xorg-x11-server-Xorg-1.7.7-26.el6_0.3 [B]
xorg-x11-server-Xvfb-1.7.7-26.el6_0.3 [B]

fixing multiple security vulnerabilities and/or
providing bugfixes and enhancements.

For more information about vulnerabilities
fixed please check:

http://cern.ch/linux/updates/

This update can also be applied before nightly automated
update run, by running as root on your machine:

# /usr/bin/yum -y update

Note: updates - [S] - security, [B] - bug fix, [E] - enhancement 
--
Jarek Polok for Linux.Support@cern.ch
