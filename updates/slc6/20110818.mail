Dear Linux users.

Tonight Scientific Linux CERN 6 (SLC 6X) system update
will contain following packages:

boost-1.41.0-11.el6_1.2 [B]
boost-date-time-1.41.0-11.el6_1.2 [B]
boost-devel-1.41.0-11.el6_1.2 [B]
boost-doc-1.41.0-11.el6_1.2 [B]
boost-filesystem-1.41.0-11.el6_1.2 [B]
boost-graph-1.41.0-11.el6_1.2 [B]
boost-graph-mpich2-1.41.0-11.el6_1.2 [B]
boost-graph-openmpi-1.41.0-11.el6_1.2 [B]
boost-iostreams-1.41.0-11.el6_1.2 [B]
boost-math-1.41.0-11.el6_1.2 [B]
boost-mpich2-1.41.0-11.el6_1.2 [B]
boost-mpich2-devel-1.41.0-11.el6_1.2 [B]
boost-mpich2-python-1.41.0-11.el6_1.2 [B]
boost-openmpi-1.41.0-11.el6_1.2 [B]
boost-openmpi-devel-1.41.0-11.el6_1.2 [B]
boost-openmpi-python-1.41.0-11.el6_1.2 [B]
boost-program-options-1.41.0-11.el6_1.2 [B]
boost-python-1.41.0-11.el6_1.2 [B]
boost-regex-1.41.0-11.el6_1.2 [B]
boost-serialization-1.41.0-11.el6_1.2 [B]
boost-signals-1.41.0-11.el6_1.2 [B]
boost-static-1.41.0-11.el6_1.2 [B]
boost-system-1.41.0-11.el6_1.2 [B]
boost-test-1.41.0-11.el6_1.2 [B]
boost-thread-1.41.0-11.el6_1.2 [B]
boost-wave-1.41.0-11.el6_1.2 [B]
cern-linuxsupport-access-0.3-1.slc6
curl-openssl-7.21.7-72.2.slc6
dbus-1.2.24-5.el6_1 [S]
dbus-devel-1.2.24-5.el6_1 [S]
dbus-doc-1.2.24-5.el6_1 [S]
dbus-libs-1.2.24-5.el6_1 [S]
dbus-x11-1.2.24-5.el6_1 [S]
dhclient-4.1.1-19.P1.el6_1.1 [S]
dhcp-4.1.1-19.P1.el6_1.1 [S]
dhcp-devel-4.1.1-19.P1.el6_1.1 [S]
firefox-3.6.20-2.el6_1 [S]
flash-plugin-11.0.1.98-1.slc6
libcurl-openssl-7.21.7-72.2.slc6
libcurl-openssl-devel-7.21.7-72.2.slc6
libsaml7-2.4.3-3.1.slc6
libsaml-devel-2.4.3-3.1.slc6
libXfont-1.4.1-2.el6_1 [S]
libXfont-devel-1.4.1-2.el6_1 [S]
libxml-security-c16-1.6.1-3.1.slc6
libxml-security-c-devel-1.6.1-3.1.slc6
libxmltooling5-1.4.2-2.1.slc6
libxmltooling-devel-1.4.2-2.1.slc6
lohit-assamese-fonts-2.4.3-5.el6 [E]
lohit-bengali-fonts-2.4.3-6.el6 [E]
lohit-gujarati-fonts-2.4.4-4.el6 [E]
lohit-kannada-fonts-2.4.5-5.el6 [E]
lohit-malayalam-fonts-2.4.4-5.el6 [E]
lohit-oriya-fonts-2.4.3-6.el6 [E]
lohit-punjabi-fonts-2.4.4-2.el6 [E]
lohit-tamil-fonts-2.4.5-5.el6 [E]
lohit-telugu-fonts-2.4.5-5.el6 [E]
opensaml-bin-2.4.3-3.1.slc6
opensaml-schemas-2.4.3-3.1.slc6
python-dmidecode-3.10.12-1.el6_1.1 [B]
shibboleth-2.4.3-2.1.slc6
shibboleth-devel-2.4.3-2.1.slc6
sssd-1.5.1-34.el6_1.3 [B]
sssd-client-1.5.1-34.el6_1.3 [B]
sssd-tools-1.5.1-34.el6_1.3 [B]
systemtap-1.4-6.el6_1.3 [B]
systemtap-client-1.4-6.el6_1.3 [B]
systemtap-grapher-1.4-6.el6_1.3 [B]
systemtap-initscript-1.4-6.el6_1.3 [B]
systemtap-runtime-1.4-6.el6_1.3 [B]
systemtap-sdt-devel-1.4-6.el6_1.3 [B]
systemtap-server-1.4-6.el6_1.3 [B]
systemtap-testsuite-1.4-6.el6_1.3 [B]
tftp-0.49-7.el6 [B]
tftp-server-0.49-7.el6 [B]
thunderbird-3.1.12-1.el6_1 [S]
xml-security-c-bin-1.6.1-3.1.slc6
xmltooling-schemas-1.4.2-2.1.slc6
xulrunner-1.9.2.20-2.el6_1 [S]
xulrunner-devel-1.9.2.20-2.el6_1 [S]

fixing multiple security vulnerabilities and/or
providing bugfixes and enhancements.

For more information about vulnerabilities
fixed please check:

http://cern.ch/linux/updates/

This update can also be applied before nightly automated
update run, by running as root on your machine:

# /usr/bin/yum -y update

Note: updates - [S] - security, [B] - bug fix, [E] - enhancement 
--
Jarek Polok for Linux.Support@cern.ch
