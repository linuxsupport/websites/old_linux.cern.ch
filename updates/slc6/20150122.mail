Dear Linux users.

Tonight Scientific Linux CERN 6 (SLC 6X) system update
will contain following packages:


CERNONLY repository:
    splunk-6.2.1-245427
    splunkforwarder-6.2.1-245427

EV repository:
    qemu-img-rhev-0.12.1.2-2.446.el6
    qemu-kvm-rhev-0.12.1.2-2.446.el6
    qemu-kvm-rhev-tools-0.12.1.2-2.446.el6

EXTRAS repository:
    aims2-client-2.9.18-2.slc6
    aims2-server-2.9.18-2.slc6
    cern-get-certificate-0.4-1.slc6

MRG repository:
    condor-7.8.10-0.2.el6
    condor-aviary-7.8.10-0.2.el6
    condor-classads-7.8.10-0.2.el6
    condor-classads-devel-7.8.10-0.2.el6
    condor-cluster-resource-agent-7.8.10-0.2.el6
    condor-deltacloud-gahp-7.8.10-0.2.el6
    condor-kbdd-7.8.10-0.2.el6
    condor-plumage-7.8.10-0.2.el6
    condor-qmf-7.8.10-0.2.el6
    condor-vm-gahp-7.8.10-0.2.el6

RHCOMMON repository:
    cloud-init-0.7.5-1.el6
    python-backports-1.0-3.el6
    python-backports-ssl_match_hostname-3.4.0.2-1.el6
    python-boto-2.25.0-2.el6
    python-jsonpatch-1.2-2.el6
    python-jsonpointer-1.0-2.el6
    python-six-1.6.1-1.el6
    python-urllib3-1.5-5.1.2.el6

SCL repository:
    thermostat1-thermostat-1.0.4-60.6.el6
    thermostat1-thermostat-javadoc-1.0.4-60.6.el6
    thermostat1-thermostat-webapp-1.0.4-60.6.el6

UPDATES repository:
    chromium-browser-39.0.2171.99-1.el6_6 [B]
    device-mapper-multipath-0.4.9-80.el6_6.2 [B]
    device-mapper-multipath-libs-0.4.9-80.el6_6.2 [B]
    java-1.7.0-openjdk-1.7.0.75-2.5.4.0.el6_6 [S]
    java-1.7.0-openjdk-demo-1.7.0.75-2.5.4.0.el6_6 [S]
    java-1.7.0-openjdk-devel-1.7.0.75-2.5.4.0.el6_6 [S]
    java-1.7.0-openjdk-javadoc-1.7.0.75-2.5.4.0.el6_6 [S]
    java-1.7.0-openjdk-src-1.7.0.75-2.5.4.0.el6_6 [S]
    java-1.8.0-openjdk-1.8.0.31-1.b13.el6_6 [S]
    java-1.8.0-openjdk-demo-1.8.0.31-1.b13.el6_6 [S]
    java-1.8.0-openjdk-devel-1.8.0.31-1.b13.el6_6 [S]
    java-1.8.0-openjdk-headless-1.8.0.31-1.b13.el6_6 [S]
    java-1.8.0-openjdk-javadoc-1.8.0.31-1.b13.el6_6 [S]
    java-1.8.0-openjdk-src-1.8.0.31-1.b13.el6_6 [S]
    kpartx-0.4.9-80.el6_6.2 [B]
    openssl-1.0.1e-30.el6_6.5 [S]
    openssl-devel-1.0.1e-30.el6_6.5 [S]
    openssl-perl-1.0.1e-30.el6_6.5 [S]
    openssl-static-1.0.1e-30.el6_6.5 [S]
    selinux-policy-3.7.19-260.el6_6.2 [B]
    selinux-policy-doc-3.7.19-260.el6_6.2 [B]
    selinux-policy-minimum-3.7.19-260.el6_6.2 [B]
    selinux-policy-mls-3.7.19-260.el6_6.2 [B]
    selinux-policy-targeted-3.7.19-260.el6_6.2 [B]
    tree-1.5.3-3.el6 [B]


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

This update can also be applied before nightly automated
 update run, by running as root on your machine:

# /usr/bin/yum -y update

Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Jarek Polok for Linux.Support@cern.ch
 