Dear Linux users.

Tonight Scientific Linux CERN 6 (SLC 6X) TEST system update
will contain following packages:

 *******************************************************
 *** NOTE:                                           ***
 *** Packages listed below are provided for TESTS    ***
 *** ONLY, DO NOT USE THESE ON PRODUCTION SYSTEMS    ***
 *** these may NEVER be released to regular updates  ***
 *** and may be broken, requiring manual fixes on    ***
 *** on your system.                                 ***
 *******************************************************

UPDATES repository:
    389-ds-base-1.2.11.15-84.el6_8
    389-ds-base-devel-1.2.11.15-84.el6_8 [S]
    389-ds-base-libs-1.2.11.15-84.el6_8
    cern-linuxsupport-access-0.7-1.slc6
    firefox-45.5.0-1.el6_8
    flash-plugin-beta-24.0.0.145-0.beta.slc6
    httpd-2.2.15-54.el6_8.1
    httpd-2.2.15-55.el6_8.2
    httpd-devel-2.2.15-54.el6_8.1
    httpd-devel-2.2.15-55.el6_8.2
    httpd-manual-2.2.15-54.el6_8.1
    httpd-manual-2.2.15-55.el6_8.2
    httpd-tools-2.2.15-54.el6_8.1
    httpd-tools-2.2.15-55.el6_8.2
    kernel-2.6.32-642.11.1.el6
    kernel-abi-whitelists-2.6.32-642.11.1.el6
    kernel-debug-2.6.32-642.11.1.el6
    kernel-debug-devel-2.6.32-642.11.1.el6
    kernel-devel-2.6.32-642.11.1.el6
    kernel-doc-2.6.32-642.11.1.el6
    kernel-firmware-2.6.32-642.11.1.el6
    kernel-headers-2.6.32-642.11.1.el6
    kernel-module-openafs-2.6.32-642.11.1.el6-1.6.6-cern4.1.slc6
    kexec-tools-2.0.0-300.el6_8.1
    kexec-tools-eppic-2.0.0-300.el6_8.1 [B]
    libblkid-2.17.2-12.24.el6_8.1
    libblkid-devel-2.17.2-12.24.el6_8.1
    libuuid-2.17.2-12.24.el6_8.1
    libuuid-devel-2.17.2-12.24.el6_8.1
    mod_ssl-2.2.15-54.el6_8.1
    mod_ssl-2.2.15-55.el6_8.2
    nss-3.21.3-2.el6_8
    nss-3.21.3-2.el6_8.cern
    nss-devel-3.21.3-2.el6_8
    nss-devel-3.21.3-2.el6_8.cern
    nss-pkcs11-devel-3.21.3-2.el6_8
    nss-pkcs11-devel-3.21.3-2.el6_8.cern
    nss-sysinit-3.21.3-2.el6_8
    nss-sysinit-3.21.3-2.el6_8.cern
    nss-tools-3.21.3-2.el6_8
    nss-tools-3.21.3-2.el6_8.cern
    nss-util-3.21.3-1.el6_8
    nss-util-devel-3.21.3-1.el6_8
    perf-2.6.32-642.11.1.el6
    perl-IO-Socket-SSL-1.31-3.el6_8.2
    policycoreutils-2.0.83-30.1.el6_8 [S]
    policycoreutils-gui-2.0.83-30.1.el6_8 [S]
    policycoreutils-newrole-2.0.83-30.1.el6_8 [S]
    policycoreutils-python-2.0.83-30.1.el6_8 [S]
    policycoreutils-sandbox-2.0.83-30.1.el6_8 [S]
    python-perf-2.6.32-642.11.1.el6 [S]
    selinux-policy-3.7.19-292.el6_8.2
    selinux-policy-doc-3.7.19-292.el6_8.2
    selinux-policy-minimum-3.7.19-292.el6_8.2
    selinux-policy-mls-3.7.19-292.el6_8.2
    selinux-policy-targeted-3.7.19-292.el6_8.2
    util-linux-ng-2.17.2-12.24.el6_8.1
    uuidd-2.17.2-12.24.el6_8.1
    wpa_supplicant-0.7.3-8.el6_8.1


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

 This TEST update can be applied on your system
 (providing it is set up to receive test updates
 according to: http://cern.ch/linux/updates/slc6.shtml),
 by running as root on your machine:

 # /usr/bin/yum -y update

 Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Thomas Oulevey for Linux.Support@cern.ch
