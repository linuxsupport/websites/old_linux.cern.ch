Dear Linux users.

Tonight Scientific Linux CERN 6 (SLC 6X) system update
will contain following packages:


SCLO repository:
    httpd24-httpd-2.4.34-7.el6.1 [S]
    httpd24-httpd-devel-2.4.34-7.el6.1 [S]
    httpd24-httpd-manual-2.4.34-7.el6.1 [S]
    httpd24-httpd-tools-2.4.34-7.el6.1 [S]
    httpd24-mod_ldap-2.4.34-7.el6.1 [S]
    httpd24-mod_proxy_html-2.4.34-7.el6.1 [S]
    httpd24-mod_session-2.4.34-7.el6.1 [S]
    httpd24-mod_ssl-2.4.34-7.el6.1 [S]
    sclo-php70-php-pecl-xdebug-2.7.1-1.el6
    sclo-php70-unit-php-1.8.0-1.el6

UPDATES repository:
    autofs-5.0.5-140.el6_10.1 [B]
    cups-1.4.2-81.el6_10 [B]
    cups-devel-1.4.2-81.el6_10 [B]
    cups-libs-1.4.2-81.el6_10 [B]
    cups-lpd-1.4.2-81.el6_10 [B]
    cups-php-1.4.2-81.el6_10 [B]
    flash-plugin-32.0.0.171-1.slc6
    glibc-2.12-1.212.el6_10.3 [B]
    glibc-common-2.12-1.212.el6_10.3 [B]
    glibc-devel-2.12-1.212.el6_10.3 [B]
    glibc-headers-2.12-1.212.el6_10.3 [B]
    glibc-static-2.12-1.212.el6_10.3 [B]
    glibc-utils-2.12-1.212.el6_10.3 [B]
    java-1.7.0-openjdk-1.7.0.221-2.6.18.0.el6_10 [S]
    java-1.7.0-openjdk-demo-1.7.0.221-2.6.18.0.el6_10 [S]
    java-1.7.0-openjdk-devel-1.7.0.221-2.6.18.0.el6_10 [S]
    java-1.7.0-openjdk-javadoc-1.7.0.221-2.6.18.0.el6_10 [S]
    java-1.7.0-openjdk-src-1.7.0.221-2.6.18.0.el6_10 [S]
    java-1.8.0-openjdk-1.8.0.212.b04-0.el6_10 [S]
    java-1.8.0-openjdk-debug-1.8.0.212.b04-0.el6_10 [S]
    java-1.8.0-openjdk-demo-1.8.0.212.b04-0.el6_10 [S]
    java-1.8.0-openjdk-demo-debug-1.8.0.212.b04-0.el6_10 [S]
    java-1.8.0-openjdk-devel-1.8.0.212.b04-0.el6_10 [S]
    java-1.8.0-openjdk-devel-debug-1.8.0.212.b04-0.el6_10 [S]
    java-1.8.0-openjdk-headless-1.8.0.212.b04-0.el6_10 [S]
    java-1.8.0-openjdk-headless-debug-1.8.0.212.b04-0.el6_10 [S]
    java-1.8.0-openjdk-javadoc-1.8.0.212.b04-0.el6_10 [S]
    java-1.8.0-openjdk-javadoc-debug-1.8.0.212.b04-0.el6_10 [S]
    java-1.8.0-openjdk-src-1.8.0.212.b04-0.el6_10 [S]
    java-1.8.0-openjdk-src-debug-1.8.0.212.b04-0.el6_10 [S]
    kernel-2.6.32-754.12.1.el6 [S]
    kernel-abi-whitelists-2.6.32-754.12.1.el6 [S]
    kernel-debug-2.6.32-754.12.1.el6 [S]
    kernel-debug-devel-2.6.32-754.12.1.el6 [S]
    kernel-devel-2.6.32-754.12.1.el6 [S]
    kernel-doc-2.6.32-754.12.1.el6 [S]
    kernel-firmware-2.6.32-754.12.1.el6 [S]
    kernel-headers-2.6.32-754.12.1.el6 [S]
    kernel-module-openafs-2.6.32-754.12.1.el6-1.6.6-cern5.1.slc6
    nscd-2.12-1.212.el6_10.3 [B]
    nss-pam-ldapd-0.7.5-32.el6_10.1 [B]
    openssh-5.3p1-124.el6_10 [S]
    openssh-askpass-5.3p1-124.el6_10 [S]
    openssh-clients-5.3p1-124.el6_10 [S]
    openssh-ldap-5.3p1-124.el6_10 [S]
    openssh-server-5.3p1-124.el6_10 [S]
    pam_ssh_agent_auth-0.9.3-124.el6_10 [S]
    perf-2.6.32-754.12.1.el6 [S]
    python-perf-2.6.32-754.12.1.el6 [S]


 fixing multiple security vulnerabilities and/or
 providing bugfixes and enhancements.

 For more information about vulnerabilities
 fixed please check:

 http://cern.ch/linux/updates/

This update can also be applied before nightly automated
 update run, by running as root on your machine:

# /usr/bin/yum -y update

Note: updates - [S] - security, [B] - bug fix, [E] - enhancement
 -- 
 Thomas Oulevey for Linux.Support@cern.ch
 